# LC-HSVI-nips18

POMDP and rho-POMDP solver exploiting the Lipschitz-continuity of finite-horizon value functions
(source code for NIPS 2018)

# ====================================

- To compile the source code:
ant compile

- To run it and get help:
ant run -Dargs='-h'
(note: Here, using ant, arguments are typically passed through -Dargs='...')

- To run experiments:
./XP-g5k.sh
(read inside file if you can and want to use a Grid5000 cluster)

- To generate tables or graphs from generated log files:
./genTableAlgo.sh
./genTableIntCrit.sh
./genGraphsAlgo.sh
