#! /bin/bash

export LC_NUMERIC=C # avoid pbs with floats (commas vs points)

# =============== BEGIN genTableLarge =================
# INPUT:
# * SOLVERS : list of solvers
# * PBS : list of problems
#
# * PBDIR="./resources/pomdp/"
# * OUTDIR="./XP-dir/"
# 
# * TIMEOUT=600
# * EPSILON=0.1
# * PRINTWIDTH=YES
#
function genTable
{
    echo -n "\\begin{tabular}{ c"
    VS=" " # vertical separator (" ", "|", "@{\quad}", ... ?) -> NIPS recommends white spaces
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	echo -n "${VS}"
	if [ "$PRINTWIDTH" == "YES" ] ; then 
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		echo -n "R@{}R@{}r"
	    else
		echo -n "R@{}R@{}R@{}r"
	    fi
	else
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		echo -n "R@{}R@{}R@{}r"
	    else
		echo -n "R@{}R@{}R@{}R@{}r"
	    fi
	fi
    done
    echo -n " "
    echo "}"

    echo "\\toprule"
    VS=" " # vertical separator (" ", "|", "@{\quad}", ... ?) -> NIPS recommends white spaces
    echo -n "\multicolumn{1}{c}{\$x\$-HSVI}"
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	if [ "$PRINTWIDTH" == "YES" ] ; then 
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		echo -n "& \multicolumn{3}{${VS}c}{${SOLVERarr[1]}} "
	    else
		echo -n "& \multicolumn{4}{${VS}c}{${SOLVERarr[1]}} "
	    fi
	else
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		echo -n "& \multicolumn{4}{${VS}c}{${SOLVERarr[1]}} "
	    else
		echo -n "& \multicolumn{5}{${VS}c}{${SOLVERarr[1]}} "
	    fi
	fi
    done
    echo "\\\\"
    
    #echo "\\midrule"
    #echo -n "\\cmidrule(lr){1-1}"
    END=1
    for SOLVER in $SOLVERS ; do
	BEG=$((END+1))
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	if [ "$PRINTWIDTH" == "YES" ] ; then 
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		END=$((BEG + 2))
	    else
		END=$((BEG + 3))
	    fi
	else
	    if [ "${SOLVERarr[0]}" == "point" ] ; then
		END=$((BEG + 3))
	    else
		END=$((BEG + 4))
	    fi
	fi
	echo -n "\\cmidrule(lr){${BEG}-${END}} "
    done
    echo ""

    
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
		if [ "$PRINTWIDTH" == "YES" ] ; then 
		    TMP_STR1="  \$gap(b_0)\$ "
		else
		    TMP_STR1="  \$L(b_0)\$ & \$U(b_0)\$ "
		fi
	if [ "${SOLVERarr[0]}" == "point" ] ; then
	    echo -n "& \$t\$ (s) & (\$\#it\$) & ${TMP_STR1} "
	else
	    echo -n "& \$t\$ (s) & (\$\#it\$) & ${TMP_STR1} & \$\\lambda\$ "
	fi
    done
    echo "\\\\"
    echo "\\midrule"
    
    GREY=0
    for PB in $PBS ; do
	IFS='#' read -r -a PBarr <<< $PB
	if [ -z "${PBarr[2]}" ]; then
	    PBarr[2]=${PBarr[0]}
	fi
	
	# LOGFILE=${OUTDIR}/pwlc-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}.log
	# ISTIMEOUT=`grep -c Timeout ${LOGFILE}`
	# if [ "${ISTIMEOUT}" -eq "1" ] ; then
	#     continue
	# fi

	if [ "${GREY}" -eq "0" ] ; then
	    #echo "\rowcolor{lightgray}"
	    echo "\rowcolor[gray]{.95}[.5\tabcolsep] "
	    GREY=1
	else
	    GREY=0
	fi
	
	echo -n ${PBarr[2]}" "
	for SOLVER in $SOLVERS ; do
	    IFS='#' read -r -a SOLVERarr <<< $SOLVER
	    LOGFILE=${OUTDIR}/${SOLVERarr[0]}-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}${SOLVERarr[2]}.log.gz
	    read -r -a OUTPUT <<< `zgrep \# ${LOGFILE} | grep -v "#-" | tail -n 2 | head -n 1 | cut -d " " -f 2,3,4,5,6,7,8`
	    COMPLETE=`zgrep -c "THE END" ${LOGFILE}`
	    if [ "${COMPLETE}" -ne "1" ] ; then	
		if [ "$PRINTWIDTH" == "YES" ] ; then 
		    TMP_STR1=" -- "
		else
		    TMP_STR1=" -- & -- "
		fi
		if [ "${SOLVERarr[0]}" == "point" ] ; then
		    printf " & -- & (--) & ${TMP_STR1} "
		else
		    printf " & -- & (--) & ${TMP_STR1} & --"
		fi
	    else
		ISTIMEOUT=`zgrep -c Timeout ${LOGFILE}`
		if [ "${ISTIMEOUT}" -eq "0" ] ; then
		    TO="\\\\textbf"
		    if [ "${SOLVERarr[2]}" == "--exUR" ] ; then
			read -r -a OUTPUT <<< `zgrep \# ${LOGFILE} | grep -v "#-" | tail -n 1 | head -n 1 | cut -d " " -f 2,3,4,5,6,7,8`
		    fi
		else
		    TO=""
		    if [ "${SOLVERarr[0]}" == "itconic" ] ; then
			read -r -a OUTPUT <<< `zgrep \# ${LOGFILE} | grep -v "#-" | tail -n 1 | head -n 1 | cut -d " " -f 2,3,4,5,6,7,8`
		    fi
		fi
		if [ "$PRINTWIDTH" == "YES" ] ; then 
		    TMP_STR1=" \$%.2f\$ "
		    TMP_STR2=`echo "${OUTPUT[3]} - ${OUTPUT[2]}" | bc`
		else
		    TMP_STR1=" \$%.2f\$ & \$%.2f\$ "
		    TMP_STR2="${OUTPUT[2]} ${OUTPUT[3]} "
		fi
		if [ "${SOLVERarr[0]}" == "point" ] ; then
		    printf " & ${TO}{%.0f} & (%d) & ${TMP_STR1} " ${OUTPUT[1]} ${OUTPUT[0]} ${TMP_STR2}
		else
		    if [ "${SOLVERarr[0]}" == "itconic" ] ; then
			printf " & ${TO}{%.0f} & (%d) & ${TMP_STR1} & %d " ${OUTPUT[1]} ${OUTPUT[0]} ${TMP_STR2}  ${OUTPUT[6]/.0*/}
		    else
			if [ "${SOLVERarr[0]}" == "pwlc" ] ; then
			    printf " & ${TO}{%.0f} & (%d) &${TMP_STR1} & %.2f " ${OUTPUT[1]} ${OUTPUT[0]} ${TMP_STR2}  ${OUTPUT[6]/.0*/}
			    #printf " & ${TO}{%.0f} & (%d) & \$%.2f\$ & \$%.2f\$ & %.2f " ${OUTPUT[1]} ${OUTPUT[0]} ${OUTPUT[2]} ${OUTPUT[3]}  ${OUTPUT[6]/.0*/}
			else
			    printf " & ${TO}{%.0f} & (%d) & ${TMP_STR1} & %.1e " ${OUTPUT[1]} ${OUTPUT[0]} ${TMP_STR2}  ${OUTPUT[6]/.0*/}
			fi
		    fi
		fi
	    fi
	done
	echo "\\\\"
    done
    echo "\\bottomrule"
    echo "\\end{tabular}"
}
# =============== END genTableLarge =================


# =============== BEGIN genTableLarge =================
# INPUT:
# * SOLVERS : list of solvers
# * PBS : list of problems
#
# * PBDIR="./resources/pomdp/"
# * OUTDIR="./XP-dir/"
# 
# * TIMEOUT=600
# * EPSILON=0.1
# * PRINTWIDTH=YES
#
function genTableLarge {
    echo -n "\\begin{tabular}{ c"
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	if [ "${SOLVERarr[0]}" == "point" ] ; then
	    echo -n "r@{ }rr@{ }rr@{ }r|"
	else
	    echo -n "r@{ }rr@{ }rr@{ }rr|"
	fi	    
    done
    echo "}"
    echo "\\hline"
    echo -n "*-HSVI "
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	if [ "${SOLVERarr[0]}" == "point" ] ; then
	    echo -n "& \multicolumn{6}{c|}{${SOLVERarr[1]}} "
	else
	    echo -n "& \multicolumn{7}{c|}{${SOLVERarr[1]}} "
	fi
    done
    echo "\\\\"
    echo "\\hline"
    
    for SOLVER in $SOLVERS ; do
	    IFS='#' read -r -a SOLVERarr <<< $SOLVER
	if [ "${SOLVERarr[0]}" == "point" ] ; then
	    echo -n "& \$t\$ (s) & (\$\#it\$) & \$L(b_0)\$ & \$U(b_0)\$ & \$\\abs{L}\$ & \$\\abs{U}\$ "
	else
	    echo -n "& \$t\$ (s) & (\$\#it\$) & \$L(b_0)\$ & \$U(b_0)\$ & \$\\abs{L}\$ & \$\\abs{U}\$ & \$\\lambda_{max}\$ "
	fi
    done
    echo "\\\\"
    echo "\\hline"
    
    for PB in $PBS ; do
	IFS='#' read -r -a PBarr <<< $PB
	if [ -z "${PBarr[2]}" ]; then
	    PBarr[2]=${PBarr[0]}
	fi
	echo -n ${PBarr[2]}" "
	for SOLVER in $SOLVERS ; do
	    IFS='#' read -r -a SOLVERarr <<< $SOLVER
	    LOGFILE=${OUTDIR}/${SOLVERarr[0]}-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}${SOLVERarr[2]}.log.gz
	    COMPLETE=`zgrep -c "THE END" ${LOGFILE}`
	    if [ "${COMPLETE}" -ne "1" ] ; then
		if [ "${SOLVERarr[0]}" == "point" ] ; then
		    printf " & -- & (--) & -- & -- & -- & -- "
		else
		    printf " & -- & (--) & -- & -- & -- & -- & --"
		fi
	    else
		read -r -a OUTPUT <<< `zgrep \# ${LOGFILE} | grep -v "#-" | tail -n 2 | head -n 1 | cut -d " " -f 2,3,4,5,6,7,8`
		ISTIMEOUT=`zgrep -c Timeout ${LOGFILE}`
		if [ "${ISTIMEOUT}" -ne "1" ] ; then
		    TO="\\\\textbf"
		else
		    TO=""
		fi
		if [ "${SOLVERarr[0]}" == "point" ] ; then
		    printf " & ${TO}{%.0f} & (%d) & \$%.2f\$ & \$%.2f\$ & %d & %d " ${OUTPUT[1]} ${OUTPUT[0]} ${OUTPUT[2]} ${OUTPUT[3]} ${OUTPUT[4]} ${OUTPUT[5]}
		else
		    printf " & ${TO}{%.0f} & (%d) & \$%.2f\$ & \$%.2f\$ & %d & %d & %.1e" ${OUTPUT[1]} ${OUTPUT[0]} ${OUTPUT[2]} ${OUTPUT[3]} ${OUTPUT[4]} ${OUTPUT[5]} ${OUTPUT[6]/.0*/}
		fi
	    fi
	done
	echo "\\\\"
    done
    echo "\\hline"
    echo "\\end{tabular}"
}
# =============== END genTableLarge =================

cat<<EOF
==============
Initialization
==============
EOF

PBDIR="./resources/pomdp/"
OUTDIR="./XP-dir/"

TIMEOUT=600
EPSILON=0.1
PRINTWIDTH=YES

SOLVERS="pwlc#pwlc \
point#pw#--exNUI \
xconic#lc#--exNUI \
itconic#inc-lc(no\\nui)#--exNUI"
NB_SOLVERS=`echo ${SOLVERS} | wc -w`
echo [solvers] ${NB_SOLVERS} - ${SOLVERS}

#PBS=`ls ${PBDIR}*.pomdp`1d
#======ALL=====
# PBS="4x3.95 \
# 4x4.95 \
# cheese.95 \
# cit \
# fourth \
# hallway \
# hallway2 \
# milos-aaai97 \
# mini-hall2 \
# mit \
# network \
# paint.95 \
# parr95.95 \
# pentagon \
# shuttle.95 \
# sunysb \
# tiger85 \
# tiger-grid \
# grid-info#GridInfoKX#grid-info~k\$x\$ \
# grid-info#GridInfoKY#grid-info~k\$y\$ \
# grid-info#GridInfoUX#grid-info~\$\neg\$k\$x\$ \
# grid-info#GridInfoUY#grid-info~\$\neg\$k\$y\$"
#========SELECTED=========
PBS="4x3.95 \
4x4.95 \
cheese.95 \
cit \
hallway \
hallway2 \
milos-aaai97 \
mit \
network \
paint.95 \
pentagon \
shuttle.95 \
tiger85 \
tiger-grid \
grid-info#GridInfoKX#grid-info~k\$x\$ \
grid-info#GridInfoKY#grid-info~k\$y\$ \
grid-info#GridInfoUX#grid-info~\$\\neg\$k\$x\$ \
grid-info#GridInfoUY#grid-info~\$\\neg\$k\$y\$"
#======================
# PBS="4x3.95 \
# cheese.95 \
# tiger85 \
# grid-info#GridInfoUX#grid-info~\$\\neg\$k\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
NB_PBS=`echo ${PBS} | wc -w`
echo [PBs] ${NB_PBS} - ${PBS//${PBDIR}/} | sed -e 's/.pomdp//g'

# RUNTIME=$(expr $(expr 2 + $TIMEOUT) \* $NB_SOLVERS \* $NB_PBS)
# HOURS=$(expr $RUNTIME / 3600)
# MINUTES=$(expr $(expr $RUNTIME % 3600) / 60)
# SECONDS=$(expr $RUNTIME % 60)
# echo "RER: (Roughly Estimated Runtime)"
# echo " (2+TIMEOUT)*NB_SOLVERS*NB_PBS = (2+$TIMEOUT)*$NB_SOLVERS*$NB_PBS"
# echo " = $RUNTIME\" = ${HOURS}h$MINUTES'$SECONDS\""
# echo ""

# mkdir $OUTDIR

if [ ! -d ${OUTDIR} ] ; then
    echo "No directory ${OUTDIR}"
    exit 1
fi

cat <<EOF
=======================
Generating LaTeX tables
=======================
EOF

genTable > ${OUTDIR}/tableAlgo.tex
# genTableLarge > ${OUTDIR}/tableAlgo++.tex


PBS="4x3.95 \
4x4.95 \
hallway \
hallway2 \
milos-aaai97 \
network \
paint.95 \
pentagon \
shuttle.95 \
tiger85 \
grid-info#GridInfoKX#grid-info~k\$x\$ \
grid-info#GridInfoKY#grid-info~k\$y\$ \
grid-info#GridInfoUX#grid-info~\$\\neg\$k\$x\$ \
grid-info#GridInfoUY#grid-info~\$\\neg\$k\$y\$"

genTable > ${OUTDIR}/tableAlgoPoster.tex

cat<<EOF
==========
Job's done
==========
EOF
