package util.container;

import org.junit.Before;
import org.junit.Test;
import util.grid.Direction;

import static org.junit.Assert.*;

public class Position2DTest {

    private Position2D position1;
    private Position2D position1copy;
    private Position2D position1bis;
    private Position2D position2;
    private Position2D center;


    @Before
    public void setUp() throws Exception {
        position1 = new Position2D(4,2);
        position1copy = new Position2D(position1);
        position1bis = new Position2D(4,2);
        position2 = new Position2D(3,3);
        center = new Position2D(0,0);
    }

    @Test
    public void getNeighbour() throws Exception {
        Position2D none = center.getAdjacentPosition(Direction.none);
        Position2D north = center.getAdjacentPosition(Direction.north);
        Position2D south = center.getAdjacentPosition(Direction.south);
        Position2D east = center.getAdjacentPosition(Direction.east);
        Position2D west = center.getAdjacentPosition(Direction.west);
        assertEquals("The two position should be equal",none, new Position2D(0,0));
        assertEquals("The two position should be equal",north, new Position2D(0,-1));
        assertEquals("The two position should be equal",south, new Position2D(0,1));
        assertEquals("The two position should be equal",east, new Position2D(1,0));
        assertEquals("The two position should be equal",west, new Position2D(-1,0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getNeighbourFail() throws Exception {
        center.getAdjacentPosition(null);
    }


    @Test
    public void equals() throws Exception {
        assertEquals("The two positions should be equal",position1,position1copy);
        assertEquals("The two positions should be equal",position1,position1bis);
        assertNotEquals("The two positions should not be equal",position1,position2);
        assertNotEquals("The two positions should not be equal",position1copy,position2);
    }

    @Test
    public void hashCodeTest() throws Exception {
        assertEquals("The two hashcode should be the equal",position1.hashCode(),position1copy.hashCode());
        assertEquals("The two hashcode should be the equal",position1.hashCode(),position1bis.hashCode());
    }

}