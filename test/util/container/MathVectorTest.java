package util.container;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.junit.Assert.*;


public class MathVectorTest {

    MathVector vector1;
    MathVector vector2;
    MathVector vector2bis;
    MathVector vector2copy;
    MathVector vector3;
    MathVector vectorUniform;
    MathVector vectorEmpty;

    @Before
    public void setUp() throws Exception {
        vector1 = new MathVector(5);
        for(int i = 0; i<5; i++) {
            vector1.set(i,i);
        }

        vector2 = new MathVector(5);
        for(int i = 0; i<5; i++) {
            vector2.set(i,i+1);
        }

        vector2bis = new MathVector(5);
        for(int i = 0; i<5; i++) {
            vector2bis.set(i,i+1);
        }

        vector2copy = new MathVector(vector2);

        vector3 = new MathVector(10);
        for(int i = 0; i<10; i++) {
            vector3.set(i,i);
        }


        vectorUniform = new MathVector(5,2);
        vectorEmpty = new MathVector(0);
    }

    //Test the copy constructor
    @Test
    public void copy() throws Exception {
        assertEquals("The copied vector should be the same than the original one",vector2,vector2copy);
    }


    @Test
    public void get() throws Exception {
        for(int i = 0; i<5; i++) {
            assertEquals("The get function should return the correct value",i,vector1.get(i),0.0001);
        }
        for(int i = 0; i<5; i++) {
            assertEquals("The get function should return the correct value",i+1,vector2.get(i),0.0001);
        }
        for(int i = 0; i<10; i++) {
            assertEquals("The get function should return the correct value",i,vector3.get(i),0.0001);
        }
        for(int i = 0; i<5; i++) {
            assertEquals("The get function should return the correct value",2,vectorUniform.get(i),0.0001);
        }
    }


    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void getFail() throws Exception {
        vector1.get(5);
    }


    @Test
    public void scalar() throws Exception {
        assertEquals("The scalar function should be returning the scalar product of two elements",
                40, MathVector.scalar(vector1,vector2),0.00001);
    }


    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void scalarFail1() throws Exception {
        MathVector.scalar(vector1,vector3);
    }


    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void scalarFail2() throws Exception {
        MathVector.scalar(vector3,vector1);
    }


    @Test
    public void l1Distance() throws Exception {
        assertEquals("The function should return the l1 distance", 5, MathVector.getL1Distance(vector1,vector2),0.001);
        assertEquals("The function should return the l1 distance",0, MathVector.getL1Distance(vector2,vector2bis),0.001);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void l1DistanceFail() throws Exception {
        MathVector.getL1Distance(vector3,vector1);
        assertTrue("The function should throw an exception when the dimensions don't match",true);
    }

    @Test
    public void getDimension() throws Exception {
        assertEquals("The getDimension function should return the dimension of the vector",5,vector1.getDimension());
        assertEquals("The getDimension function should return the dimension of the vector",5,vector2.getDimension());
        assertEquals("The getDimension function should return the dimension of the vector",10,vector3.getDimension());
    }

    @Test
    public void equals() throws Exception {
        assertNotEquals("The two vectors should be different",vector1,vector2);
        assertNotEquals("The two vectors should be different",vector1,vector3);
        assertNotEquals("The two vectors should be different",vector2,vector3);
        assertEquals("The two vectors should be equals",vector2,vector2bis);
    }

    @Test
    public void toStringTest() throws Exception {
        assertNotEquals("The toString function should not return an empty string","",vectorEmpty.toString());
    }

    @Test
    public void hashCodeTest() throws Exception {
        assertEquals("The two hashcode should be equal",vector2.hashCode(),vector2bis.hashCode());
    }

    @Test
    public void serializationTest() {
        MathVector mv = new MathVector(3);
        MathVector mv2 = null;
        mv.set(0,8);
        mv.set(1,3);
        mv.set(2,100);
        try {
            ByteArrayOutputStream buffOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buffOut);
            out.writeObject(mv);
            out.close();
            ByteArrayInputStream buffIn = new ByteArrayInputStream(buffOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(buffIn);
            mv2 = (MathVector) in.readObject();
            in.close();
            buffIn.close();
            buffOut.close();
        } catch(Exception ignored) {
        }
        assertEquals("The serialization/deserialization process " +
                     "should return an object equal to the starting one",mv,mv2);
    }
}