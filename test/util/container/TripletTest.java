package util.container;

import org.junit.Before;
import org.junit.Test;
import util.container.Triplet;

import static org.junit.Assert.*;

public class TripletTest {
    Triplet<Integer,Integer,Integer> triplet1;
    Triplet<Integer,Integer,Integer> triplet1bis;
    Triplet<Integer,Integer,Integer> triplet2;

    @Before
    public void setUp() throws Exception {
        triplet1 = new Triplet<>(4,0,4);
        triplet1bis = new Triplet<>(4,0,4);
        triplet2 = new Triplet<>(null,0,4);
    }

    @Test
    public void equals() throws Exception {
        assertEquals("The pairs should be equal",triplet1,triplet1bis);
        assertNotEquals("The pairs should not be equal",triplet1,triplet2);
        assertNotEquals("The pairs should not be equal",triplet1bis,triplet2);
    }

    @Test
    public void hashCodeTest() throws Exception {
        assertEquals("The two hashcode should be equal", triplet1.hashCode(), triplet1bis.hashCode());
    }

}