package util.container;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class ObjectIntegerConverterTest {

    ObjectIntegerConverter<Integer> converter;
    ObjectIntegerConverter<Integer> converterList;
    List<Integer> list;

    @Before
    public void setUp() throws Exception {
        converter = new ObjectIntegerConverter<>();
        for(int i = 99; i>=0; i--) {
            converter.add(i);
        }
        list = new ArrayList<>();
        for(int i = 99; i>=0; i--) {
            list.add(i);
        }
        converterList = new ObjectIntegerConverter<>(list);
    }

    @Test
    public void bijectiveTest() throws Exception {
        for(int i = 0; i<100; i++) {
            assertEquals("The converter should be a bijection",(long)i,(long)converter.toA(converter.toInteger(i)));
        }
        for(int i = 0; i<100; i++) {
            assertEquals("The converter should be a bijection",(long)i,(long)converterList.toA(converterList.toInteger(i)));
        }
    }

    @Test
    public void rangeTest() throws Exception {
        for(int i = 0; i<100; i++) {
            assertTrue("The converter should convert to integers of the right range",
                    converter.toInteger(i)>=0 && converter.toInteger(i)<100);
        }
        for(int i = 0; i<100; i++) {
            assertTrue("The converter should convert to integers of the right range",
                    converterList.toInteger(i)>=0 && converterList.toInteger(i)<100);
        }
    }

    @Test
    public void listTest() throws Exception {
        for(int i = 0; i<100; i++) {
            assertTrue("The list constructor should keep the indices of elements",list.get(i).equals(converterList.toA(i)));
        }
    }


    @Test
    public void getSize() throws Exception {
        assertEquals("The function should return the number of elements added",100,converter.getSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void toIntegerFail() throws Exception {
        converter.toInteger(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toAFail() throws Exception {
        converter.toA(1000000);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toAFailNegative() throws Exception {
        converter.toInteger(-1);
    }

}