package util.container;

import org.junit.Before;
import org.junit.Test;
import util.container.Pair;

import static org.junit.Assert.*;


public class PairTest {

    Pair<Integer,Integer> pair1;
    Pair<Integer,Integer> pair1bis;
    Pair<Integer,Integer> pair2;

    @Before
    public void setUp() throws Exception {
        pair1 = new Pair<>(4,2);
        pair1bis = new Pair<>(4,2);
        pair2 = new Pair<>(4,1);
    }

    @Test
    public void equals() throws Exception {
        assertEquals("The two pairs should be equal",pair1,pair1bis);
        assertNotEquals("The two pairs should not be equal",pair1,pair2);
        assertNotEquals("The two pairs should not be equal",pair1bis,pair2);
    }

    @Test
    public void hashCodeTest() throws Exception {
        assertEquals("The two hashcode should be equals",pair1.hashCode(),pair1bis.hashCode());
    }
}