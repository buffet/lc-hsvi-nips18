package util.signal;

import org.junit.Before;
import org.junit.Test;
import sun.misc.Signal;

import static org.junit.Assert.*;

public class SingleSignalHandlerTest {

    SingleSignalHandler handler;

    @Before
    public void setUp() throws Exception {
        handler = new SingleSignalHandler();
        Signal.handle(new Signal("INT"),handler);
    }

    @Test
    public void test() throws Exception {
        assertFalse("The handler hasn't handle the signal yet",handler.hasHandleSignal());
        handler.handle(new Signal("INT"));
        assertTrue("The handler should have handle the signal",handler.hasHandleSignal());
    }
}