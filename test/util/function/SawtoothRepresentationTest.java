package util.function;

import util.function.SawtoothRepresentation.Tooth;
import org.junit.Before;
import org.junit.Test;
import util.container.MathVector;
import util.container.Pair;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;

import static org.junit.Assert.*;

public class SawtoothRepresentationTest {

    int dimension = 2;
    MathVector cornerValues;
    MathVector point;
    SawtoothRepresentation functionMax;
    SawtoothRepresentation functionMaxCopy;
    SawtoothRepresentation functionMin;
    SawtoothRepresentation functionMinCopy;

    @Before
    public void setUp() throws Exception {
        cornerValues = new MathVector(2,0);
        functionMax = new SawtoothRepresentation(true,cornerValues);
        functionMaxCopy = new SawtoothRepresentation(true,cornerValues);
        functionMin = new SawtoothRepresentation(false,cornerValues);
        functionMinCopy = new SawtoothRepresentation(false,cornerValues);
        point = new MathVector(2,0.5);
    }

    @Test
    public void addCornerValues() throws Exception {
        MathVector newCorner = new MathVector(2);
        newCorner.set(0,1);
        newCorner.set(1,-1);
        functionMin.addCornerValues(newCorner);
        functionMax.addCornerValues(newCorner);
        MathVector corner0 = new MathVector(2,0);
        corner0.set(0,1);
        MathVector corner1 = new MathVector(2,0);
        corner1.set(1,1);
        double valueMin0 = functionMin.getValue(corner0);
        double valueMin1 = functionMin.getValue(corner1);
        double valueMax0 = functionMax.getValue(corner0);
        double valueMax1 = functionMax.getValue(corner1);
        assertEquals("The corner must be added only if non dominated",0.0,valueMin0,0.001);
        assertEquals("The corner must be added only if non dominated",0.0,valueMax1,0.001);
        assertEquals("The corner must be added if non dominated", -1, valueMin1,0.001);
        assertEquals("The corner must be added if non dominated", 1, valueMax0,0.001);
    }

    @Test
    public void addImprovingPoint() throws Exception {
        Random random = new Random(42);
        for(int i = 0; i<100; i++) {
            double mv0 = random.nextDouble();
            double mv1 = random.nextDouble();
            MathVector mv = new MathVector(2);
            mv.set(0,mv0);
            mv.set(0,mv1);
            double v = random.nextDouble();

            Tooth tooth = new Tooth(mv,v);
            functionMax.addTooth(tooth);
            functionMaxCopy.addImprovingTooth(tooth);
            functionMin.addTooth(tooth);
            functionMinCopy.addImprovingTooth(tooth);
        }
        for(int i = 0; i<100; i++) {
            MathVector vector = new MathVector(2);
            vector.set(0,((double)i)/100.0);
            vector.set(1,((double)(100-i))/100.0);
            double valueMax1 = functionMax.getValue(vector);
            double valueMax2 = functionMaxCopy.getValue(vector);
            double valueMin1 = functionMin.getValue(vector);
            double valueMin2 = functionMinCopy.getValue(vector);
            assertEquals("Using addTooth or addImprovingTooth should not change the result",valueMin1,valueMin2,0.001);
            assertEquals("Using addTooth or addImprovingTooth should not change the result",valueMax1,valueMax2,0.001);
        }
    }

    @Test
    public void addDominatedPoint() throws Exception {
        Tooth toothMax = new Tooth(point,-1);
        functionMax.addTooth(toothMax);
        double valueMax = functionMax.getValue(point);
        Tooth toothMin = new Tooth(point,1);
        functionMin.addTooth(toothMin);
        double valueMin = functionMin.getValue(point);
        assertEquals("The value shouldn't be changed if the point added is dominated",0.0,valueMax,0.001);
        assertEquals("The value shouldn't be changed if the point added is dominated",0.0,valueMin,0.001);
    }


    @Test
    public void getApproximateValue() throws Exception {
        double valueMax = functionMax.getValue(point);
        double valueMin = functionMin.getValue(point);
        assertEquals("The value should be equal to 0, since no points were added",0,valueMin,0.001);
        assertEquals("The value should be equal to 0, since no points were added",0,valueMax,0.001);

        Tooth toothMax = new Tooth(point,0.3);
        functionMax.addTooth(toothMax);
        valueMax = functionMax.getValue(point);
        Tooth toothMin = new Tooth(point,-0.3);
        functionMin.addTooth(toothMin);
        valueMin = functionMin.getValue(point);
        assertEquals("The value on a point should be equal to the point's value",0.3,valueMax,0.001);
        assertEquals("The value on a point should be equal to the point's value",-0.3,valueMin,0.001);
    }

    @Test
    public void isPointDominated() throws Exception {
        boolean isDominatedMin = functionMin.isPointDominated(new Tooth(point,1.0));
        boolean isDominatedMax = functionMax.isPointDominated(new Tooth(point,-1.0));
        assertTrue("This point should be dominated",isDominatedMin);
        assertTrue("This point should be dominated",isDominatedMax);

        functionMax.addTooth(new Tooth(point,0.5));
        functionMin.addTooth(new Tooth(point,-0.5));
        isDominatedMin = functionMin.isPointDominated(new Tooth(point,-0.49));
        isDominatedMax = functionMax.isPointDominated(new Tooth(point,0.49));
        assertTrue("This point should be dominated",isDominatedMin);
        assertTrue("This point should be dominated",isDominatedMax);
    }

    @Test(expected = NullPointerException.class)
    public void addPointFail() {
        functionMax.addTooth(null);
    }


    @Test(expected = NullPointerException.class)
    public void addImprovingPointFail() {
        functionMax.addImprovingTooth(null);
    }

    @Test(expected = NullPointerException.class)
    public void createToothFail() {
        Tooth tooth = new Tooth(null,0);
    }

    @Test
    public void serializationTest() {
        SawtoothRepresentation functionTemp = null;
        try {
            ByteArrayOutputStream buffOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buffOut);
            out.writeObject(functionMax);
            out.close();
            ByteArrayInputStream buffIn = new ByteArrayInputStream(buffOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(buffIn);
            functionTemp = (SawtoothRepresentation) in.readObject();
            in.close();
            buffIn.close();
            buffOut.close();
        } catch(Exception ignored) {
        }
        assertEquals("The serialization/deserialization process " +
                "should return an object equal to the starting one",functionMax,functionTemp);
    }

}