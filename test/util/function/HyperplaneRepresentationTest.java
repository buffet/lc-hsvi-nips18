package util.function;

import org.junit.Before;
import org.junit.Test;
import util.container.MathVector;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.junit.Assert.*;

public class HyperplaneRepresentationTest {

    int dimension = 2;
    MathVector cornerValues;
    HyperplaneRepresentation functionMax;
    HyperplaneRepresentation functionMin;
    MathVector hyperplane1;
    MathVector hyperplane2;
    MathVector point;


    @Before
    public void setUp() throws Exception {
        functionMax = new HyperplaneRepresentation(true,2);
        functionMin = new HyperplaneRepresentation(false,2);

        hyperplane1 = new MathVector(2);
        hyperplane1.set(0,1.0);
        hyperplane1.set(1,2.0);

        hyperplane2 = new MathVector(2);
        hyperplane2.set(0,2.0);
        hyperplane2.set(1,1.0);

        functionMax.addHyperplane(hyperplane1);
        functionMax.addHyperplane(hyperplane2);
        functionMin.addHyperplane(hyperplane1);
        functionMin.addHyperplane(hyperplane2);

        point = new MathVector(2);
        point.set(0,0.2);
        point.set(1,0.4);
    }


    @Test
    public void getValue() throws Exception {
        double valueMax = functionMax.getValue(point);
        double valueMin = functionMin.getValue(point);
        assertEquals("The value on a point should be equal to the point's value",1.0,valueMax,0.001);
        assertEquals("The value on a point should be equal to the point's value",0.8,valueMin,0.001);
    }


    @Test
    public void getRepresentative() throws Exception {
        MathVector tempMin = functionMin.getRepresentative(point);
        assertEquals("The representative must be the function minimizing a min-plane representation",tempMin,hyperplane2);
        MathVector tempMax = functionMax.getRepresentative(point);
        assertEquals("The representative must be the hyperplane maximizing a max-plane representation",tempMax,hyperplane1);
    }


    @Test
    public void isHyperplaneDominated() throws Exception {
        MathVector hyperplane = new MathVector(2);
        hyperplane.set(0,1.0);
        hyperplane.set(1,1.999);
        boolean isDominated = functionMax.isHyperplaneDominated(hyperplane);
        assertTrue("This hyperplane should be dominated",isDominated);
    }


    @Test(expected = NullPointerException.class)
    public void addNullHyperplane() throws Exception {
        functionMin.addHyperplane(null);
    }

    @Test
    public void test() {
        HyperplaneRepresentation function = new HyperplaneRepresentation(true,2);
    }

    @Test
    public void serializationTest() {
        HyperplaneRepresentation functionTemp = null;
        try {
            ByteArrayOutputStream buffOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buffOut);
            out.writeObject(functionMax);
            out.close();
            ByteArrayInputStream buffIn = new ByteArrayInputStream(buffOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(buffIn);
            functionTemp = (HyperplaneRepresentation) in.readObject();
            in.close();
            buffIn.close();
            buffOut.close();
        } catch(Exception ignored) {
        }
        assertEquals("The serialization/deserialization process " +
                "should return an object equal to the starting one",functionMax,functionTemp);
    }
}