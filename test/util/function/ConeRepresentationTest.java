package util.function;

import org.junit.Before;
import org.junit.Test;
import util.container.MathVector;
import util.random.RandomGenerator;
import util.function.ConeRepresentation.Cone;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import static org.junit.Assert.*;


public class ConeRepresentationTest {

    int dimension = 2;
    MathVector point;
    Cone cone1;
    Cone cone2;
    ConeRepresentation functionMax;
    ConeRepresentation functionMaxCopy;
    ConeRepresentation functionMin;
    ConeRepresentation functionMinCopy;


    @Before
    public void setUp() throws Exception {
        functionMax = new ConeRepresentation(true, 2);
        functionMaxCopy = new ConeRepresentation(true, 2);
        functionMin = new ConeRepresentation(false, 2);
        functionMinCopy = new ConeRepresentation(false, 2);
        point = new MathVector(2, 0.5);
        cone1 = new Cone(new MathVector(2, 1.0), 0.75, 1.0);
        cone2 = new Cone(new MathVector(2, 0), 0.5, 3.0);
        functionMax.addCone(cone1);
        functionMaxCopy.addImprovingCone(cone1);
        functionMin.addCone(cone1);
        functionMinCopy.addImprovingCone(cone1);
        functionMax.addCone(cone2);
        functionMaxCopy.addImprovingCone(cone2);
        functionMin.addCone(cone2);
        functionMinCopy.addImprovingCone(cone2);
    }


    @Test
    public void addImprovingCone() throws Exception {
        for (int i = 0; i < 100; i++) {
            MathVector point = new MathVector(2, 0);
            point.set(0, RandomGenerator.getDouble());
            point.set(1, RandomGenerator.getDouble());
            MathVector vertex = new MathVector(2, 0);
            vertex.set(0, RandomGenerator.getDouble());
            vertex.set(1, RandomGenerator.getDouble());
            double value = RandomGenerator.getDouble();
            double slope = RandomGenerator.getDouble();
            Cone cone = new Cone(vertex, value, slope);
            functionMax.addCone(cone);
            functionMaxCopy.addImprovingCone(cone);
            functionMin.addCone(cone);
            functionMinCopy.addImprovingCone(cone);
            double valueMax = functionMax.getValue(point);
            double valueMaxCopy = functionMaxCopy.getValue(point);
            double valueMin = functionMin.getValue(point);
            double valueMinCopy = functionMinCopy.getValue(point);
            assertEquals("Using addCone or addImprovingCone should not change the result", valueMax, valueMaxCopy, 0.001);
            assertEquals("Using addCone or addImprovingCone should not change the result", valueMin, valueMinCopy, 0.001);
        }
    }


    @Test
    public void getValue() throws Exception {
        double valueMax = functionMax.getValue(point);
        double valueMin = functionMin.getValue(point);
        assertEquals("The getValue function should return the correct result", -0.25,valueMax,0.001);
        assertEquals("The getValue function should return the correct result", 1.75,valueMin,0.001);
    }


    @Test
    public void isConeDominated() throws Exception {
        Cone cone3 = new Cone(point,-0.26,1.001);
        Cone cone4 = new Cone(point,-0.26,1.0);
        Cone cone5 = new Cone(point, -0.26, 0.99);
        Cone cone6 = new Cone(point, 1.76, 1.001);
        Cone cone7 = new Cone(point, 1.76, 1.0);
        Cone cone8 = new Cone(point, 1.76, 0.99);
        assertTrue("The cone should be dominated",functionMax.isConeDominated(cone3));
        assertTrue("The cone should be dominated",functionMax.isConeDominated(cone4));
        assertFalse("The cone should not be dominated because its slope is lower",functionMax.isConeDominated(cone5));
        assertTrue("The cone should be dominated",functionMin.isConeDominated(cone6));
        assertTrue("The cone should be dominated",functionMin.isConeDominated(cone7));
        assertFalse("The cone should not be dominated because its slope is lower",functionMin.isConeDominated(cone8));
    }

    @Test(expected = NullPointerException.class)
    public void addConeFail() throws Exception {
        functionMax.addCone(null);
    }

    @Test(expected = NullPointerException.class)
    public void addImprovingConeFail() throws Exception {
        functionMax.addImprovingCone(null);
    }

    @Test(expected = NullPointerException.class)
    public void createConeFail() throws Exception {
        Cone cone = new Cone(null,1,1);
    }

    @Test
    public void serializationTest() {
        ConeRepresentation functionTemp = null;
        try {
            ByteArrayOutputStream buffOut = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buffOut);
            out.writeObject(functionMax);
            out.close();
            ByteArrayInputStream buffIn = new ByteArrayInputStream(buffOut.toByteArray());
            ObjectInputStream in = new ObjectInputStream(buffIn);
            functionTemp = (ConeRepresentation) in.readObject();
            in.close();
            buffIn.close();
            buffOut.close();
        } catch(Exception ignored) {
        }
        assertEquals("The serialization/deserialization process " +
                "should return an object equal to the starting one",functionMax,functionTemp);
    }
}