package util.random;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.abs;
import static org.junit.Assert.*;

public class DistributionTest {

    Distribution<Integer> distribution;
    Distribution<Integer> distributionEmpty;
    Distribution<Integer> distributionUnique;

    @Before
    public void setUp() throws Exception {
        distribution = new Distribution<>();
        distribution.setWeight(0, 0.0);
        distribution.setWeight(1, 0.25);
        distribution.setWeight(2, 0.25);
        distribution.setWeight(3, 0.5);
        distributionUnique = new Distribution<>(1);
        distributionEmpty = new Distribution<>();
    }

    @Test
    public void add() throws Exception {
        distribution.setWeight(7, 1.0);
        double weight = distribution.getWeight(7);
        assertEquals("The element should have been added with a given weight", 1.0, weight, 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addFail() throws Exception {
        distribution.setWeight(3, -1);
    }

    @Test
    public void remove() throws Exception {
        distribution.remove(1);
        double probability = distribution.getProbability(1);
        assertEquals("The element should have a probability of 0.0 since it has been removed", 0.0, probability, 0.0001);
    }

    @Test
    public void normalize() throws Exception {
        distribution.normalize();
        distribution.setWeight(7, 1.0);
        double value = distribution.getProbability(7);
        assertEquals("After a normalization the sum of weights should equal 1.0 internally", 0.5, value, 0.001);
    }

    @Test
    public void normalizeEmpty() throws Exception {
        distributionEmpty.normalize();
        distributionEmpty.normalize(2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void normalizeNegativeFail() throws Exception {
        distribution.normalize(-2);
    }

    @Test
    public void normalize1() throws Exception {
        distribution.normalize(0.8);
        distribution.setWeight(7,0.8);
        double value = distribution.getProbability(7);
        assertEquals("After a normalization the sum of all weights should equal the new value",0.5,value,0.001);
    }

    @Test
    public void generate() throws Exception {
        Distribution<Boolean> uniformDistribution = new Distribution<>();
        uniformDistribution.setWeight(true,0.5);
        uniformDistribution.setWeight(false,0.5);
        int trueCount = 0;
        int falseCount = 0;
        for(int i = 0; i<1000; i++) {
            if (uniformDistribution.generate()) {
                trueCount++;
            } else {
                falseCount++;
            }
        }
        assertTrue("The generation should follows the distribution", trueCount < 700 && trueCount > 300);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void generateEmpty() throws Exception {
        distributionEmpty.generate();
    }

    @Test
    public void getWeight() throws Exception {
        distribution.setWeight(10,3);
        double weight = distribution.getWeight(10);
        distribution.getProbability(3);
        assertEquals("Distribution should keep the weights of elements unless normalize is called",3,weight,0.0001);
        weight = distributionUnique.getProbability(1);
        assertEquals("Distribution should keep the weights of elements unless normalize is called",weight,1,0.0001);
    }

    @Test
    public void getWeightOnUnknown() throws Exception {
        double weight = distribution.getWeight(123);
        assertEquals("The weight of an unknown element should be 0.0",0.0,weight,0.0001);
    }

    @Test
    public void getProbability() throws Exception {
        Set<Integer> elements = distribution.getNonZeroElements();
        double sum = 0.0;
        for (int element: elements) {
            sum += distribution.getProbability(element);
        }
        //This test normalize and getProbability, since normalize should be called
        //in getProbability
        assertTrue("The sum of all probabilities should equals 1.0",abs(sum - 1.0) < 0.00001);
    }

    @Test
    public void getNonZeroElements() throws Exception {
        distribution.setWeight(-1,0);
        Set<Integer> nonZeroElements = distribution.getNonZeroElements();
        Set<Integer> expected = new HashSet<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        assertEquals("getNonZeroElements should return only the elements with weights positive",expected,nonZeroElements);
    }

}