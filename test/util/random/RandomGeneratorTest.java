package util.random;

import org.junit.Test;
import util.random.RandomGenerator;

import static org.junit.Assert.*;


public class RandomGeneratorTest {

    @Test
    public void regenerateRandomGenerator() throws Exception {
        RandomGenerator.regenerateRandomGenerator();
        int int1 = RandomGenerator.getInt();
        double double1 = RandomGenerator.getDouble();
        RandomGenerator.regenerateRandomGenerator();
        int int2 = RandomGenerator.getInt();
        double double2 = RandomGenerator.getDouble();
        assertTrue("Redoing the same action after a regeneration should do the same thing.",
                int1 == int2 && double1 == double2);
    }

    @Test
    public void regenerateRandomGenerator1() throws Exception {
        RandomGenerator.regenerateRandomGenerator(7);
        int int1 = RandomGenerator.getInt();
        double double1 = RandomGenerator.getDouble();
        RandomGenerator.regenerateRandomGenerator(7);
        int int2 = RandomGenerator.getInt();
        double double2 = RandomGenerator.getDouble();
        assertTrue("Redoing the same action after a regeneration should do the same thing.",
                int1 == int2 && double1 == double2);
    }

    @Test
    public void getDouble() throws Exception {
        RandomGenerator.regenerateRandomGenerator();
        double double1 = RandomGenerator.getDouble();
        double double2 = RandomGenerator.getDouble();
        assertTrue("There is a high probability the randomGenerator generate always the same value",
                double1 != double2);
    }

    @Test
    public void getInt() throws Exception {
        RandomGenerator.regenerateRandomGenerator();
        double int1 = RandomGenerator.getDouble();
        double int2 = RandomGenerator.getDouble();
        assertTrue("There is a high probability the randomGenerator generate always the same value",
                int1 != int2);
    }

    @Test
    public void getInt1() throws Exception {
        RandomGenerator.regenerateRandomGenerator();
        int mini = 3;
        int maxi = 10;
        for(int i = 0; i<100; i++) {
            int value = RandomGenerator.getInt(mini,maxi);
            assertTrue("The getInt(mini,maxi) function should return a value in the interval [mini,maxi[",
                    mini <= value && maxi > value);
        }
    }

}