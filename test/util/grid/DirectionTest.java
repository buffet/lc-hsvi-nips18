package util.grid;

import org.junit.Test;

import static org.junit.Assert.*;

public class DirectionTest {

    @Test
    public void oneLetterToString() throws Exception {
        for(Direction direction : Direction.values()) {
            assertTrue("The function should return a String containing a unique character", direction.oneLetterToString().length() == 1);
        }
    }

}