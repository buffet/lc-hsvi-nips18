package util.grid;

import org.junit.Before;
import org.junit.Test;
import util.container.Position2D;

import static org.junit.Assert.*;


public class TorGridTest {
    Position2D pos1;
    Position2D pos2;
    Position2D pos3;
    Position2D pos4;
    Position2D pos5;
    TorGrid grid;


    @Before
    public void setUp() throws Exception {
        grid = new TorGrid(3,8);
        pos1 = new Position2D(1,0);
        pos2 = new Position2D(1,7);
        pos3 = new Position2D(1,3);
        pos4 = new Position2D(0,3);
        pos5 = new Position2D(2,3);

    }

    @Test
    public void getCaseFromDirection() throws Exception {
        assertEquals("The case returned should be correct",
                new Position2D(1,7),grid.getAdjacentPosition(pos1, Direction.north));
        assertEquals("The case returned should be correct",
                new Position2D(1,0),grid.getAdjacentPosition(pos2, Direction.south));
        assertEquals("The case returned should be correct",
                new Position2D(2,3),grid.getAdjacentPosition(pos4, Direction.west));
        assertEquals("The case returned should be correct",
                new Position2D(0,3),grid.getAdjacentPosition(pos5, Direction.east));
        assertEquals("The case in the none direction should be the current case",
                pos1,grid.getAdjacentPosition(pos1,Direction.none));
        assertEquals("The case in the none direction should be the current case",
                pos2,grid.getAdjacentPosition(pos2,Direction.none));
        assertEquals("The case in the none direction should be the current case",
                pos3,grid.getAdjacentPosition(pos3,Direction.none));
        assertEquals("The case returned should be correct",
                new Position2D(1,2),grid.getAdjacentPosition(pos3,Direction.north));
        assertEquals("The case returned should be correct",
                new Position2D(1,4),grid.getAdjacentPosition(pos3,Direction.south));
        assertEquals("The case returned should be correct",
                new Position2D(2,3),grid.getAdjacentPosition(pos3,Direction.east));
        assertEquals("The case returned should be correct",
                new Position2D(0,3),grid.getAdjacentPosition(pos3,Direction.west));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCaseFromDirectionFail() throws Exception {
        grid.getAdjacentPosition(new Position2D(2,9),Direction.none);
    }

}