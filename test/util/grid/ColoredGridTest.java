package util.grid;

import org.junit.Before;
import org.junit.Test;
import util.container.Position2D;

import static org.junit.Assert.*;


public class ColoredGridTest {
    Position2D pos1;
    Position2D pos2;
    Grid grid;
    TorGrid torGrid;
    ColoredGrid coloredGrid;
    ColoredGrid coloredTorGrid;


    @Before
    public void setUp() throws Exception {
        grid = new Grid(3,8);
        torGrid = new TorGrid(3,8);
        coloredGrid = new ColoredGrid(grid,10);
        coloredTorGrid = new ColoredGrid(torGrid,10);
        pos1 = new Position2D(1,0);
        pos2 = new Position2D(1,4);
    }

    @Test
    public void getAdjacentPosition() throws Exception {
        assertEquals("The coloredGrid should use the underlying grid",
                null,coloredGrid.getAdjacentPosition(pos1, Direction.north));
        assertEquals("The coloredGrid should use the underlying grid",
                new Position2D(1,7),coloredTorGrid.getAdjacentPosition(pos1, Direction.north));
    }

    @Test
    public void getSetColors() throws Exception {
        coloredGrid.setColor(pos2,3);
        assertEquals("The color should be the one set", 3,(long)coloredGrid.getColor(pos2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getColorFail() throws Exception {
        Position2D outside = new Position2D(-1,0);
        coloredGrid.getColor(outside);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setColorFail() throws Exception {
        Position2D outside = new Position2D(1,42);
        coloredGrid.setColor(outside,3);
    }
}