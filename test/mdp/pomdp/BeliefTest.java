package mdp.pomdp;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;


public class BeliefTest {

    Belief<Integer> belief;

    @Before
    public void setUp() throws Exception {
        belief = new Belief<>();
        belief.setWeight(0,0.5);
        belief.setWeight(1,0.25);
        belief.setWeight(2,0.0);
        belief.setWeight(3,0.25);
    }

    @Test
    public void getProbability() throws Exception {
        double prob0 = belief.getProbability(0);
        double prob1 = belief.getProbability(1);
        double prob2 = belief.getProbability(2);
        double prob3 = belief.getProbability(3);
        assertEquals("The getter should return the correct value",0.5,prob0,0.00001);
        assertEquals("The getter should return the correct value",0.25,prob1,0.00001);
        assertEquals("The getter should return the correct value",0.0,prob2,0.00001);
        assertEquals("The getter should return the correct value",0.25,prob3,0.00001);
    }

    @Test
    public void getNonZero() throws Exception {
        Set<Integer> set = new HashSet<>();
        set.add(0);
        set.add(1);
        set.add(3);
        assertEquals("The getNonZero function should return only the states with non zero probability",set,belief.getNonZero());
    }

    @Test
    public void generate() throws Exception {
        for(int i = 0; i<1000; i++) {
            int a = belief.generate();
            assertTrue("The value generated must have a probability greater than 0",a==0 || a==1 || a==3);
        }
    }
}