package mdp.pomdp;

import org.junit.Before;
import org.junit.Test;
import util.random.Distribution;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ObservationSetGenericTest {
    ObservationSetGeneric<Integer,Integer,Integer> os;
    Distribution<Integer> distribution;
    Distribution<Integer> distributionU;

    @Before
    public void setUp() throws Exception {
        Set<Integer> states = new HashSet<>();
        states.add(0);
        states.add(1);
        states.add(2);
        states.add(3);

        Set<Integer> actions = new HashSet<>();
        actions.add(0);
        actions.add(1);

        Set<Integer> observations = new HashSet<>();
        observations.add(0);
        observations.add(1);
        observations.add(2);

        os = new ObservationSetGeneric<>(states,actions,observations);
        distribution = new Distribution<>();
        distribution.setWeight(0,1);
        distribution.setWeight(1,2);
        distribution.setWeight(2,1);

        distributionU = new Distribution<>();
        distributionU.setWeight(0,1);
        distributionU.setWeight(1,1);
        distributionU.setWeight(2,1);
    }


    @Test
    public void withSetDistribution() throws Exception {
        os.setDistribution(null,null,distributionU);
        os.setDistribution(1,2,distribution);
        os.initGetters();
        for(int a = 0; a < 2; a++) {
            for(int es = 0; es<4; es++) {
                for(int o = 0; o<3; o++) {
                    double expected;
                    if( a == 1 && es == 2) {
                        expected = distribution.getProbability(o);
                    } else {
                        expected = distributionU.getProbability(o);
                    }
                    assertEquals("The probability should have been correctly set",
                            expected,os.getProbability(a,es,o),0.001);
                }
            }
        }
    }


    @Test
    public void withSetProbability() throws Exception {
        os.setProbability(null,null,null,0.333333);
        os.setProbability(1,2,0,distribution.getProbability(0));
        os.setProbability(1,2,1,distribution.getProbability(1));
        os.setProbability(1,2,2,distribution.getProbability(2));
        os.initGetters();
        for(int a = 0; a < 2; a++) {
            for(int es = 0; es<4; es++) {
                for(int o = 0; o<3; o++) {
                    double expected;
                    if( a == 1 && es == 2) {
                        expected = distribution.getProbability(o);
                    } else {
                        expected = distributionU.getProbability(o);
                    }
                    assertEquals("The probability should have been correctly set",
                            expected,os.getProbability(a,es,o),0.001);
                }
            }
        }
    }


    @Test
    public void getPossibleNextObservationsWithDistribution() throws Exception {
        os.setDistribution(null,null,distributionU);
        os.setDistribution(1,1,distribution);
        os.initGetters();
        assertEquals("The possible next states should be the ones with non null probability",
                distributionU.getNonZeroElements(),os.getPossibleObservations(0,0));
        assertEquals("The possible next states should be the ones with non null probability",
                distribution.getNonZeroElements(),os.getPossibleObservations(1,1));
    }


    @Test
    public void getPossibleNextObservationsWithProbabilities() throws Exception {
        os.setProbability(null,null,null,0.333333);
        os.setProbability(1,1,0,distribution.getProbability(0));
        os.setProbability(1,1,1,distribution.getProbability(1));
        os.setProbability(1,1,2,distribution.getProbability(2));
        os.initGetters();
        assertEquals("The possible next states should be the ones with non null probability",
                distributionU.getNonZeroElements(),os.getPossibleObservations(0,0));
        assertEquals("The possible next states should be the ones with non null probability",
                distribution.getNonZeroElements(),os.getPossibleObservations(1,1));
    }


    @Test(expected = IllegalStateException.class)
    public void initGettersFail() {
        os.setProbability(0,null,0,1.0);
        os.setProbability(1,0,1,1.0);
        os.setProbability(1,1,1,1.0);
        os.initGetters();
    }

    @Test
    public void generateObservation() {
        os.setDistribution(null,null,distributionU);
        os.setProbability(1,1,0,0);
        os.setProbability(1,1,1,0.5);
        os.setProbability(1,1,2,0.5);
        os.initGetters();

        int nb1 = 0;
        int nb2 = 0;
        for(int i = 0; i<1000; i++) {
            int temp = os.generate(1,1);
            if(temp == 1) {
                nb1++;
            } else if(temp == 2) {
                nb2++;
            } else {
                assert true : "The generate function should only generate possible next observations";
            }
        }

        assertNotEquals("There is a high probability that the first observation is never generated, but it should",
                nb1,0,0.001);
        assertNotEquals("There is a high probability that the second observation is never generated, but it should",
                nb2,0,0.001);
    }
}