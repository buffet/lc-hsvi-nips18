package mdp.solver;

import mdp.example.MDPFactory;
import mdp.mdp.MDP;
import mdp.policy.FiniteHorizonDeterministicPolicy;
import org.junit.Test;
import util.container.Position2D;
import util.grid.Direction;

import static org.junit.Assert.*;


public class FiniteHorizonMDPSolverTest {

    MDP<Position2D,Direction> mdp;
    FiniteHorizonMDPSolver<Position2D,Direction> solver;

    @Test
    public void findOptimalStrategy() throws Exception {
        mdp = MDPFactory.generate3x3GridMDP(-10000, -100, 1000, -1);
        solver = new FiniteHorizonMDPSolver<>();
        FiniteHorizonDeterministicPolicy<Position2D,Direction> policy = solver.findOptimalStrategy(mdp,5);
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(0,0),4));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,0),4));
        assertEquals("The policy is not the optimal one",Direction.none,policy.getAction(new Position2D(2,0),4));
        assertEquals("The policy is not the optimal one",Direction.north,policy.getAction(new Position2D(0,1),4));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,1),4));
        assertEquals("The policy is not the optimal one",Direction.north,policy.getAction(new Position2D(2,1),4));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(0,2),4));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,2),4));
        assertEquals("The policy is not the optimal one",Direction.north,policy.getAction(new Position2D(2,2),4));
        assertEquals("The policy is not the optimal one",Direction.none,policy.getAction(new Position2D(-1,-1),4));
        assertEquals("The value function is not the optimal one",898,policy.getValue(new Position2D(0,0),4),1);
        assertEquals("The value function is not the optimal one",999,policy.getValue(new Position2D(1,0),4),1);
        assertEquals("The value function is not the optimal one",1000,policy.getValue(new Position2D(2,0),4),1);
        assertEquals("The value function is not the optimal one",897,policy.getValue(new Position2D(0,1),4),1);
        assertEquals("The value function is not the optimal one",998,policy.getValue(new Position2D(1,1),4),1);
        assertEquals("The value function is not the optimal one",999,policy.getValue(new Position2D(2,1),4),1);
        assertEquals("The value function is not the optimal one",996,policy.getValue(new Position2D(0,2),4),1);
        assertEquals("The value function is not the optimal one",997,policy.getValue(new Position2D(1,2),4),1);
        assertEquals("The value function is not the optimal one",998,policy.getValue(new Position2D(2,2),4),1);
        assertEquals("The value function is not the optimal one",0,policy.getValue(new Position2D(-1,-1),4),1);
    }

}