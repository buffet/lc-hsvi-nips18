package mdp.solver;

import mdp.example.MDPFactory;
import mdp.mdp.MDP;
import mdp.policy.InfiniteHorizonDeterministicPolicy;
import org.junit.Test;
import util.container.Position2D;
import util.grid.Direction;

import static org.junit.Assert.*;


public class InfiniteHorizonMDPSolverTest {

    MDP<Position2D,Direction> mdp;
    InfiniteHorizonMDPSolver<Position2D,Direction> solver;

    @Test
    public void findOptimalPolicy() throws Exception {
        mdp = MDPFactory.generate3x3GridMDP(-10000, -100, 1000, -1);
        solver = new InfiniteHorizonMDPSolver<>();
        InfiniteHorizonDeterministicPolicy<Position2D, Direction> policy = solver.findOptimalPolicy(mdp, 0.99, 0.001);
        assertEquals("The policy is not the optimal one",Direction.south,policy.getAction(new Position2D(0,0)));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,0)));
        assertEquals("The policy is not the optimal one",Direction.none,policy.getAction(new Position2D(2,0)));
        assertEquals("The policy is not the optimal one",Direction.south,policy.getAction(new Position2D(0,1)));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,1)));
        assertEquals("The policy is not the optimal one",Direction.north,policy.getAction(new Position2D(2,1)));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(0,2)));
        assertEquals("The policy is not the optimal one",Direction.east,policy.getAction(new Position2D(1,2)));
        assertEquals("The policy is not the optimal one",Direction.north,policy.getAction(new Position2D(2,2)));
        assertEquals("The policy is not the optimal one",Direction.none,policy.getAction(new Position2D(-1,-1)));
        assertEquals("The value function is not the optimal one",935,policy.getValue(new Position2D(0,0)),1);
        assertEquals("The value function is not the optimal one",989,policy.getValue(new Position2D(1,0)),1);
        assertEquals("The value function is not the optimal one",1000,policy.getValue(new Position2D(2,0)),1);
        assertEquals("The value function is not the optimal one",946,policy.getValue(new Position2D(0,1)),1);
        assertEquals("The value function is not the optimal one",978,policy.getValue(new Position2D(1,1)),1);
        assertEquals("The value function is not the optimal one",989,policy.getValue(new Position2D(2,1)),1);
        assertEquals("The value function is not the optimal one",956,policy.getValue(new Position2D(0,2)),1);
        assertEquals("The value function is not the optimal one",967,policy.getValue(new Position2D(1,2)),1);
        assertEquals("The value function is not the optimal one",978,policy.getValue(new Position2D(2,2)),1);
        assertEquals("The value function is not the optimal one",0,policy.getValue(new Position2D(-1,-1)),1);
    }

}