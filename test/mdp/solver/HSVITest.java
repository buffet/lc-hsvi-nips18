package mdp.solver;

import mdp.example.tiger.TigerAction;
import mdp.example.tiger.TigerObservation;
import mdp.example.tiger.TigerState;
import mdp.pomdp.POMDP;
import mdp.pomdp.Belief;
import mdp.example.POMDPFactory;
import mdp.rhopomdp.RhoPOMDP;
import mdp.example.RhoPOMDPFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class HSVITest {

    POMDP<TigerState,TigerAction,TigerObservation> pomdp;
    RhoPOMDP<TigerState, TigerAction, TigerObservation> rhoPOMDP;
    HSVI<TigerState,TigerAction,TigerObservation> hsvi1;
    HSVI<TigerState,TigerAction,TigerObservation> hsvi2;
    HSVI<TigerState,TigerAction,TigerObservation> hsvi3;
    HSVI<TigerState,TigerAction,TigerObservation> hsvi4;
    HSVI<TigerState,TigerAction,TigerObservation> hsvi5;

    @Before
    public void setUp() throws Exception {
        pomdp = POMDPFactory.generateTigerPOMDP(0.85,-1.0,-100.0,10.0);
        rhoPOMDP = RhoPOMDPFactory.generateTigerRhoPOMDP(0.85,-1.0,-100.0,10.0);
        hsvi1 =  HSVIFactory.buildClassicHSVI(pomdp,0.75,0.001);
        hsvi2 =  HSVIFactory.buildConicHSVI(pomdp,0.75,0.001);
        hsvi3 =  HSVIFactory.buildConicHSVIWithSlope(40,pomdp,0.75,0.001);
        hsvi4 = HSVIFactory.buildConicHSVI(rhoPOMDP,0.75,0.001);

    }

    @Test
    public void getOptimalValue() throws Exception {
        Belief<TigerState> belief = new Belief<>();
        belief.setWeight(TigerState.left,0.5);
        belief.setWeight(TigerState.right,0.5);
        assertEquals("HSVI should return a correct approximation",hsvi1.getOptimalValue(belief),1.933,0.001);
        assertEquals("HSVI should return a correct approximation",hsvi2.getOptimalValue(belief),1.933,0.001);
        assertEquals("HSVI should return a correct approximation",hsvi3.getOptimalValue(belief),1.933,0.001);
        assertEquals("HSVI should return a correct approximation",hsvi4.getOptimalValue(belief),1.933,0.001);
    }

}