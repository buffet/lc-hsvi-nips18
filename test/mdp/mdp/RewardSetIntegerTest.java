package mdp.mdp;

import org.junit.Before;
import org.junit.Test;
import util.random.Distribution;

import static org.junit.Assert.*;

public class RewardSetIntegerTest {

    TransitionSetInteger ts;
    RewardSetInteger rs;
    RewardSetInteger rsEmpty;
    Distribution<Integer> distribution;
    Distribution<Integer> distributionU;


    @Before
    public void setUp() throws Exception {
        ts = new TransitionSetInteger(3,2);
        distribution = new Distribution<>();
        distribution.setWeight(0,1);
        distribution.setWeight(1,2);
        distribution.setWeight(2,1);
        distributionU = new Distribution<>();
        distributionU.setWeight(0,1);
        distributionU.setWeight(1,1);
        distributionU.setWeight(2,1);
        ts.setDistribution(null,null,distributionU);
        ts.setDistribution(0,0,distribution);
        ts.initGetters();

        rs = new RewardSetInteger(3,2);
        rs.setReward(null,null,null,1);
        rs.setReward(0,0,0,2);
        rs.setReward(0,0,1,3);
        rs.setReward(0,0,2,4);
        rs.initGetters(ts);

        rsEmpty = new RewardSetInteger(3,2);
        rsEmpty.initGetters(ts);
    }


    @Test
    public void getReward() throws Exception {
        for(int ss = 0; ss < 3; ss++) {
            for (int a = 0; a < 2; a++) {
                for (int es = 0; es < 3; es++) {
                    double expected;
                    if(ss == 0 && a == 0) {
                        expected = es+2;
                    } else {
                        expected = 1;
                    }
                    assertEquals("The reward should be the one added",expected,rs.getReward(ss,a,es),0.001);
                }
            }
        }
    }

    @Test
    public void getExpectedReward() throws Exception {
        for(int ss = 0; ss < 3; ss++) {
            for (int a = 0; a < 2; a++) {
                double expected;
                if(ss == 0 && a == 0) {
                    expected = 3;
                } else {
                    expected = 1;
                }
                assertEquals("The reward should be the one added",expected,rs.getExpectedReward(ss,a),0.001);
            }
        }
    }

    @Test
    public void getRewardUndefinedTest() throws Exception {
        double reward = rsEmpty.getReward(1,1,2);
        assertEquals("The reward should be equal to 0, since it wasn't defined",0,reward,0);
    }
}