package mdp.mdp;

import org.junit.Before;
import org.junit.Test;
import util.random.Distribution;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class TransitionSetGenericTest {

    TransitionSetGeneric<Integer,Integer> ts;
    Distribution<Integer> distribution;
    Distribution<Integer> distributionU;

    @Before
    public void setUp() throws Exception {
        Set<Integer> states = new HashSet<>();
        states.add(0);
        states.add(1);
        states.add(2);

        Set<Integer> actions = new HashSet<>();
        actions.add(0);
        actions.add(1);

        ts = new TransitionSetGeneric<>(states,actions);

        distribution = new Distribution<>();
        distribution.setWeight(0,1);
        distribution.setWeight(1,2);
        distribution.setWeight(2,1);

        distributionU = new Distribution<>();
        distributionU.setWeight(0,1);
        distributionU.setWeight(1,1);
        distributionU.setWeight(2,1);
    }

    @Test
    public void withSetDistribution() throws Exception {
        ts.setDistribution(null,null,distributionU);
        ts.setDistribution(2,1,distribution);
        ts.initGetters();
        for(int ss = 0; ss < 3; ss++) {
            for(int a = 0; a<2; a++) {
                for(int es = 0; es<3; es++) {
                    double expected;
                    if( ss == 2 && a == 1) {
                        expected = distribution.getProbability(es);
                    } else {
                        expected = distributionU.getProbability(es);
                    }
                    assertEquals("The probability should have been correctly set",
                            expected,ts.getProbability(ss,a,es),0.001);
                }
            }
        }
    }


    @Test
    public void withSetProbability() throws Exception {
        ts.setProbability(null,null,null,0.333333);
        ts.setProbability(2,1,0,distribution.getProbability(0));
        ts.setProbability(2,1,1,distribution.getProbability(1));
        ts.setProbability(2,1,2,distribution.getProbability(2));
        ts.initGetters();
        for(int ss = 0; ss < 3; ss++) {
            for(int a = 0; a<2; a++) {
                for(int es = 0; es<3; es++) {
                    double expected;
                    if( ss == 2 && a == 1) {
                        expected = distribution.getProbability(es);
                    } else {
                        expected = distributionU.getProbability(es);
                    }
                    assertEquals("The probability should have been correctly set",
                            expected,ts.getProbability(ss,a,es),0.001);
                }
            }
        }
    }

    @Test
    public void getPossibleNextStatesWithDistribution() throws Exception {
        ts.setProbability(null,null,null,0.333333);
        ts.setDistribution(0,0,distributionU);
        ts.setDistribution(1,1,distribution);
        ts.initGetters();
        assertEquals("The possible next states should be the ones with non null probability",
                distributionU.getNonZeroElements(),ts.getPossibleNextStates(0,0));
        assertEquals("The possible next states should be the ones with non null probability",
                distribution.getNonZeroElements(),ts.getPossibleNextStates(1,1));
    }


    @Test
    public void getPossibleNextStatesWithProbabilities() throws Exception {
        ts.setProbability(null,null,null,0.333333);
        ts.setProbability(1,1,0,distribution.getProbability(0));
        ts.setProbability(1,1,1,distribution.getProbability(1));
        ts.setProbability(1,1,2,distribution.getProbability(2));
        ts.initGetters();
        assertEquals("The possible next states should be the ones with non null probability",
                distributionU.getNonZeroElements(),ts.getPossibleNextStates(0,0));
        assertEquals("The possible next states should be the ones with non null probability",
                distribution.getNonZeroElements(),ts.getPossibleNextStates(1,1));
    }

    @Test
    public void generateTransition() throws Exception {
        Distribution<Integer> distributionTemp = new Distribution<>();
        distributionTemp.setWeight(1,2);
        distributionTemp.setWeight(2,3);
        ts.setDistribution(null,null,distributionTemp);
        ts.initGetters();
        int nb1 = 0;
        int nb2 = 0;
        for(int i = 0; i<1000; i++) {
            int temp = ts.generateTransition(0,0);
            if(temp == 1) {
                nb1++;
            } else if(temp == 2) {
                nb2++;
            } else {
                assert true : "The generate function should only generate possible next states";
            }
        }

        assertNotEquals("There is a high probability that the first element is never generated, but it should",
                nb1,0,0.001);
        assertNotEquals("There is a high probability that the second element is never generated, but it should",
                nb2,0,0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setDistributionTest() {
        ts.setDistribution(0,0,null);
    }
}