package mdp.mdp;

import org.junit.Before;
import org.junit.Test;
import util.random.Distribution;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class RewardSetGenericTest {

    TransitionSetGeneric<Integer,Integer> ts;
    RewardSetGeneric<Integer,Integer> rs;
    RewardSetGeneric<Integer,Integer> rsEmpty;
    Distribution<Integer> distribution;
    Distribution<Integer> distributionU;


    @Before
    public void setUp() throws Exception {
        Set<Integer> states = new HashSet<>();
        states.add(0);
        states.add(1);
        states.add(2);

        Set<Integer> actions = new HashSet<>();
        actions.add(0);
        actions.add(1);

        ts = new TransitionSetGeneric<>(states,actions);

        distribution = new Distribution<>();
        distribution.setWeight(0,1);
        distribution.setWeight(1,2);
        distribution.setWeight(2,1);

        distributionU = new Distribution<>();
        distributionU.setWeight(0,1);
        distributionU.setWeight(1,1);
        distributionU.setWeight(2,1);

        ts.setDistribution(null,null,distributionU);
        ts.setDistribution(0,0,distribution);
        ts.initGetters();

        rs = new RewardSetGeneric<>(states,actions);
        rs.setReward(null,null,null,0);
        rs.setReward(0,0,0,2);
        rs.setReward(0,0,1,3);
        rs.setReward(0,0,2,4);
        rs.initGetters(ts);

        rsEmpty = new RewardSetGeneric<>(states,actions);
        rsEmpty.initGetters(ts);
    }


    @Test
    public void getReward() throws Exception {
        for(int ss = 0; ss < 3; ss++) {
            for (int a = 0; a < 2; a++) {
                for (int es = 0; es < 3; es++) {
                    double expected;
                    if(ss == 0 && a == 0) {
                        expected = es+2;
                    } else {
                        expected = 0;
                    }
                    assertEquals("The reward should be the one added",expected,rs.getReward(ss,a,es),0.001);
                }
            }
        }
    }

    @Test
    public void getExpectedReward() throws Exception {
        for(int ss = 0; ss < 3; ss++) {
            for (int a = 0; a < 2; a++) {
                double expected;
                if(ss == 0 && a == 0) {
                    expected = 3;
                } else {
                    expected = 0;
                }
                assertEquals("The reward should be the one added",expected,rs.getExpectedReward(ss,a),0.001);
            }
        }
    }

    @Test
    public void getRewardUndefinedTest() throws Exception {
        double reward = rsEmpty.getReward(1,1,2);
        assertEquals("The reward should be equal to 0, since it wasn't defined",0,reward,0);
    }
}