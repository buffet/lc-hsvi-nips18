package mdp.interpreter;

import mdp.solver.HSVIFactory;
import mdp.solver.HSVI;
import org.junit.Test;

import static org.junit.Assert.*;


public class POMDPReaderTest {

    @Test
    public void getPOMDPFromFile() throws Exception {
        POMDPFromFile pomdp =  POMDPReader.getPOMDPFromFile("test-resources/tiger85.pomdp");
        HSVI<Integer,Integer,Integer> hsvi = HSVIFactory.buildClassicHSVI(pomdp.pomdp,pomdp.discount,0.001);
        assertEquals("HSVI should return a correct approximation",hsvi.getOptimalValue(pomdp.initialBelief),1.933,0.001);
    }

}