#! /bin/bash

export LC_NUMERIC=C # avoid pbs with floats (commas vs points)

cat<<EOF
==============
Initialization
==============
EOF

PBDIR="./resources/pomdp/"
OUTDIR="./XP-dir/"

TIMEOUT=600
EPSILON=0.1

SOLVERS="pwlc#pwlc \
point#pw#--exNUI \
xconic#lc#--exNUI \
itconic#inc-lc#--exNUI"
NB_SOLVERS=`echo ${SOLVERS} | wc -w`
echo [solvers] ${NB_SOLVERS} - ${SOLVERS}

#PBS=`ls ${PBDIR}*.pomdp`1d
PBS="4x3.95 \
4x4.95 \
cheese.95 \
cit \
fourth \
hallway \
hallway2 \
milos-aaai97 \
mini-hall2 \
mit \
network \
paint.95 \
parr95.95 \
pentagon \
shuttle.95 \
sunysb \
tiger85 \
tiger-grid \
grid-info#GridInfoKX#grid-info~k\$x\$ \
grid-info#GridInfoKY#grid-info~k\$y\$ \
grid-info#GridInfoUX#grid-info~u\$x\$ \
grid-info#GridInfoUY#grid-info~u\$y\$"
#======================
# PBS="4x3.95 \
# 4x4.95 \
# cheese.95 \
# cit \
# hallway2 \
# hallway \
# milos-aaai97 \
# mini-hall2 \
# mit \
# network \
# paint.95 \
# pentagon \
# shuttle.95 \
# sunysb \
# tiger85 \
# tiger-grid \
# grid-info#GridInfoUX#grid-info~u\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
#======================
# PBS="4x3.95 \
# cheese.95 \
# tiger85 \
# grid-info#GridInfoUX#grid-info~u\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
NB_PBS=`echo ${PBS} | wc -w`
echo [PBs] ${NB_PBS} - ${PBS//${PBDIR}/} | sed -e 's/.pomdp//g'

# RUNTIME=$(expr $(expr 2 + $TIMEOUT) \* $NB_SOLVERS \* $NB_PBS)
# HOURS=$(expr $RUNTIME / 3600)
# MINUTES=$(expr $(expr $RUNTIME % 3600) / 60)
# SECONDS=$(expr $RUNTIME % 60)
# echo "RER: (Roughly Estimated Runtime)"
# echo " (2+TIMEOUT)*NB_SOLVERS*NB_PBS = (2+$TIMEOUT)*$NB_SOLVERS*$NB_PBS"
# echo " = $RUNTIME\" = ${HOURS}h$MINUTES'$SECONDS\""
# echo ""

# mkdir $OUTDIR


if [ ! -d ${OUTDIR} ] ; then
    echo "No directory ${OUTDIR}"
    exit 1
fi

cat <<EOF
=========================
Generating gnuplot graphs
=========================
EOF

{
    for PB in $PBS ; do
	IFS='#' read -r -a PBarr <<< $PB
	if [ -z "${PBarr[2]}" ]; then
	    PBarr[2]=${PBarr[0]}
	fi

	FILE=${OUTDIR}/${PBarr[0]/\./_}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON/\./_}.pdf
	#FILE=${OUTDIR}/${PBarr[0]/\./_}-t${TIMEOUT}-e${EPSILON/\./_}.pdf
	echo "\begin{figure}"
	echo "\includegraphics[width=\linewidth]{../../../${FILE}}"
	echo "\caption{${PBarr[2]}}"
	echo "\end{figure}"
	
	cat > tmp.gp <<EOF
set term pdf
set output "${FILE}"
set xlabel "time (s)"
set logscale x
set key right bottom

EOF

	FIRST=true
	COUNTER=1
	for SOLVER in $SOLVERS ; do
	    IFS='#' read -r -a SOLVERarr <<< $SOLVER
	    #echo "Solver: ${SOLVERarr[0]}"
	    LOGFILE=${OUTDIR}/${SOLVERarr[0]}-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}${SOLVERarr[2]}.log.gz
	    TMPFILE=${SOLVERarr[0]}.tmp
	    
	    zgrep "#" ${LOGFILE} | sed -e 's/#.//g' > ${TMPFILE}

	    if [ ${FIRST} == "true" ] ; then
		echo "plot \\" >> tmp.gp
		FIRST="false"
	    else
		echo ", \\" >> tmp.gp
	    fi
	    echo " \"${TMPFILE}\" using 2:3 linecolor ${COUNTER} title '${SOLVERarr[1]}' with lines, \\" >> tmp.gp
	    echo -n " \"${TMPFILE}\" using 2:4 linecolor ${COUNTER} notitle with lines" >> tmp.gp

	    COUNTER=$(expr ${COUNTER} + 1)
	    
	done
	echo " " >> tmp.gp

	gnuplot tmp.gp
	
    done
    
    for SOLVER in $SOLVERS ; do
    	IFS='#' read -r -a SOLVERarr <<< $SOLVER
    	TMPFILE=${SOLVERarr[0]}.tmp
	
    	rm -f ${TMPFILE}
    done

    rm -f tmp.gp
}

cat<<EOF
==========
Job's done
==========
EOF
