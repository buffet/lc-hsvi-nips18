#! /bin/bash

export LC_NUMERIC=C # avoid pbs with floats (commas vs points)

cat<<EOF
==============
Initialization
==============
EOF

PBDIR="./resources/pomdp/"
OUTDIR="./XP-dir/"

TIMEOUT=600
EPSILON=0.1

# SOLVERS="pwlc#pwlc \
# point#pw#--exNUI \
# xconic#lc#--exNUI \
# itconic#inc-lc \
# itconic#noLXU#--exLXU \
# itconic#noNUI#--exNUI \
# itconic#noUR#--exUR"
SOLVERS="pwlc#pwlc \
point#pw#--exNUI \
xconic#lc#--exNUI \
itconic#inc-lc \
itconic#noLXU#--exLXU \
itconic#noNUI#--exNUI \
itconic#noUR#--exUR"
#SOLVERS="xconic#lc#--exNUI"
#SOLVERS="pwlc#pwlc"
NB_SOLVERS=`echo ${SOLVERS} | wc -w`
echo [solvers] ${NB_SOLVERS} - ${SOLVERS}

#PBS=`ls ${PBDIR}*.pomdp`1d
PBS="4x3.95 \
4x4.95 \
cheese.95 \
cit \
fourth \
hallway \
hallway2 \
milos-aaai97 \
mini-hall2 \
mit \
network \
paint.95 \
parr95.95 \
pentagon \
shuttle.95 \
sunysb \
tiger85 \
tiger-grid \
grid-info#GridInfoKX#grid-info~k\$x\$ \
grid-info#GridInfoKY#grid-info~k\$y\$ \
grid-info#GridInfoUX#grid-info~u\$x\$ \
grid-info#GridInfoUY#grid-info~u\$y\$"
#======================
# PBS="4x3.95 \
# 4x4.95 \
# cheese.95 \
# cit \
# hallway2 \
# hallway \
# milos-aaai97 \
# mini-hall2 \
# mit \
# network \
# paint.95 \
# pentagon \
# shuttle.95 \
# sunysb \
# tiger70 \
# tiger-grid \
# grid-info#GridInfoUX#grid-info~u\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
#======== PETITS PBS pour tester==============
PBS="4x3.95 \
cheese.95 \
tiger85 \
grid-info#GridInfoUX#grid-info~u\$x\$ \
grid-info#GridInfoKX#grid-info~k\$x\$"
NB_PBS=`echo ${PBS} | wc -w`
echo [PBs] ${NB_PBS} - ${PBS//${PBDIR}/} | sed -e 's/.pomdp//g'

RUNTIME=$(expr $(expr 2 + $TIMEOUT) \* $NB_SOLVERS \* $NB_PBS)
HOURS=$(expr $RUNTIME / 3600)
MINUTES=$(expr $(expr $RUNTIME % 3600) / 60)
SECONDS=$(expr $RUNTIME % 60)
echo "RER: (Roughly Estimated Runtime)"
echo " (2+TIMEOUT)*NB_SOLVERS*NB_PBS = (2+$TIMEOUT)*$NB_SOLVERS*$NB_PBS"
echo " = $RUNTIME\" = ${HOURS}h$MINUTES'$SECONDS\""
echo ""

mkdir $OUTDIR

ant compile
if [ $? -ne 0 ] ; then
    echo "=================="
    echo "COMPILATION FAILED"
    echo "=================="
    exit 0
fi

cat <<EOF
===================
Running experiments
===================
EOF

for PB in $PBS ; do
    IFS='#' read -r -a PBarr <<< $PB
    echo ${PBarr[0]} ${PBarr[1]} ${PBarr[2]}
    if [ -z "${PBarr[2]}" ]; then
	PBarr[2]=${PBarr[0]}
    fi
    FILE=${PBDIR}/${PBarr[0]}.pomdp
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	echo $SOLVER $PB
	LOGFILE=${OUTDIR}/${SOLVERarr[0]}-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}${SOLVERarr[2]}.log
	rm -f output.log # SAFETY OPERATION
	
	if [ ! -f ${LOGFILE}.gz ] || [ `zgrep -c "THE END" ${LOGFILE}.gz` -eq "0" ] ; then
	    ant justrun -Dargs="solve ${SOLVERarr[0]} ${FILE} --rhoClass=${PBarr[1]} --timeout=${TIMEOUT} --epsilon=${EPSILON} ${SOLVERarr[2]}"
	    cp output.log ${LOGFILE}
	    gzip -f ${LOGFILE}
	else
	    echo "*** ALREADY DONE ***"
	fi
    done
done

cat<<EOF
==========
Job's done
==========
EOF
