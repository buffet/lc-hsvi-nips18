package util.function;

import util.container.MatrixX;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * Represent a function in the simplex R^n.
 * Each point of the function is defined as the maximum/minimum value of a set of cones.
 * If the function represent the maximum values of these cones, the cone points towards the positive values.
 * If the function represent the minimum values of these cones, the cone points towards the negative values.
 * A value of a cone is defined as c(x') = v + d(x,x')*s,
 * with x the position of the vertex of the cone, s the slope and v the value of the cone in its vertex, and d the L1 distance.
 *
 * This example show how to create a basic function, setWeight cones, and get the values.
 *
 * <code> {@code
 * // The function represent the maximum of the cones
 * boolean representMax = true;
 * // The dimension of the input size of the function
 * int dimension = 5;
 * // Create the function
 * ConeRepresentation function = new ConeRepresentation(representMax,dimension,slope);
 *
 * // Add two cones
 * function.addCone(cone1);
 * function.addCone(cone2);
 * ...
 *
 * // Now, the function is represented by the maximum of a set of cones,
 *
 * // Get the value of the function
 * double value = function.getValue(point);
 * }</code>
 *
 * </pre>
 */
public class ConeRepresentation implements Serializable {

    /**
     * A structure representing a cone
     */
    public static class Cone implements Serializable {
        private static final long serialVersionUID = 1L;
        public final MatrixX vertex;
        public final double value;
        public final double slope;
        public Cone(MatrixX vertex, double value, double slope) {
            if(vertex == null) {
                throw new NullPointerException("The vertex of a cone cannot be null");
            }
            this.vertex = vertex;
            this.value = value;
            this.slope = slope;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Cone)) return false;
            Cone cone = (Cone) o;
            if (Double.compare(cone.value, value) != 0) return false;
            if (Double.compare(cone.slope, slope) != 0) return false;
            return vertex.equals(cone.vertex);
        }

        @Override
        public int hashCode() {
            int result = vertex.hashCode();
            long temp = Double.doubleToLongBits(value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * Is true if the boolean represent the maximum value of the cones on a point
     */
    private boolean representMax;

    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The dimension of the space
     */
    private int dimension;

    /**
     * The set of cones
     */
    private HashSet<Cone> cones;

    /**
     * The set of cones added after the last pruning
     */
    private HashSet<Cone> newCones;

    /**
     * The number of stored cones after the last pruning
     */
    private int nbOfConesInLastPruning;



    /**
     * Creates the set
     *
     * @param representMax   True if we take the maximum values of the cones
     * @param dimension      The dimension of the space
     * @param pruning        Whether pruning is turned on/off
     */
    public ConeRepresentation(boolean representMax, int dimension, boolean pruning) {
        this.representMax = representMax;
        this.dimension = dimension;
	this.pruning = pruning;
        cones = new HashSet<>();
        newCones = new HashSet<>();
        nbOfConesInLastPruning = 10;
    }


    /**
     * Add a cone to the representation, knowing it is not dominated
     *
     * @param cone The cone to setWeight
     */
    public void addImprovingCone(Cone cone) {
        if(cone == null) {
            throw new NullPointerException("The added cone cannot be null");
        }
        cones.add(cone);
	if (pruning) {
	    newCones.add(cone);
	    if (cones.size() > 1.1*nbOfConesInLastPruning) {
		prune();
	    }
        }
    }

    /**
     * Add a cone to the representation
     *
     * @param cone The cone
     */
    public void addCone(Cone cone) {
        if(!pruning || !isConeDominated(cone)) {
            addImprovingCone(cone);
        }
    }


    /**
     * Get the value of the represented function
     *
     * @param point The point where to get the value
     * @return The value on that point
     */
    public double getValue(MatrixX point) {
        double value;
        if(representMax) {
            value = Double.NEGATIVE_INFINITY;
        } else {
            value = Double.POSITIVE_INFINITY;
        }

        for(Cone cone : cones) {
            if(representMax) {
                value = max(value,getValueForCone(point,cone));
            } else {
                value = min(value,getValueForCone(point,cone));
            }
        }

        return value;
    }



    /**
     * Get the value of a cone on a point
     *
     * @param point The point where to get the value
     * @param cone  The cone used to get the value
     * @return The value of the cone on that point
     */
    private double getValueForCone(MatrixX point, Cone cone) {
        if(representMax) {
            return cone.value - cone.slope* point.norm1(cone.vertex);
        } else {
            return cone.value + cone.slope* point.norm1(cone.vertex);
        }
    }


    /**
     * Remove the useless cones from the representation
     */
    private void prune() {
        if(newCones.size() == 0) {
            return;
        }
        HashSet<Cone> newConeSet = new HashSet<>();

        for(Cone cone : cones) {
            if(!isConeDominatedByNewCones(cone)) {
                newConeSet.add(cone);
            }
        }

        newCones.clear();
        cones = newConeSet;
        nbOfConesInLastPruning = cones.size();
    }

    /**
     * Get the maximum slope
     */
    public double getMaxSlope() {
	double maxSlope = Double.NEGATIVE_INFINITY;
	
	prune();

        for(Cone cone : cones) {
	    if (cone.slope > maxSlope) {
		maxSlope = cone.slope;
	    }
        }

	return maxSlope;
    }

    /**
     * Check if a cone is dominated by another cone of the representation
     *
     * @param  cone The cone to check
     * @return true if the cone is dominated by another cone
     */
    public boolean isConeDominated(Cone cone) {
        return isConeDominatedByConeCollection(cone,cones);
    }


    /**
     * Check if a cone is dominated by a new cone
     *
     * @param  cone The cone to check
     * @return true if the cone is dominated by another cone
     */
    private boolean isConeDominatedByNewCones(Cone cone) {
        return isConeDominatedByConeCollection(cone,newCones);
    }


    /**
     * Check if a cone is dominated by another cone in one collection
     *
     * @param cone              The cone to check
     * @param coneCollection    The collection of cone
     * @return true if the cone is dominated by a distinct cone in the cone collection
     */
    private boolean isConeDominatedByConeCollection(Cone cone, Collection<Cone> coneCollection) {
        for(Cone otherCone : coneCollection) {
            if(otherCone.equals(cone)) {
                continue;
            }
            if(isConeDominatedByCone(cone,otherCone)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if a cone is dominated by another cone
     *
     * @param cone       The cone we want to check
     * @param otherCone  The cone for which we want to know if it dominates the other
     * @return true if otherCone dominates cone
     */
    private boolean isConeDominatedByCone(Cone cone, Cone otherCone) {
        double value = getValueForCone(cone.vertex,otherCone);
        if(representMax && value >= cone.value && cone.slope >= otherCone.slope) {
            return true;
        }
        if(!representMax && value <= cone.value && cone.slope >= otherCone.slope) {
            return true;
        }
        return false;
    }



    /**
     * Method used to serialize object
     *
     * @param stream    The stream where to serialize object
     */
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);
        stream.writeObject(dimension);
        stream.writeObject(cones);
    }


    /**
     * Method used to deserialize object
     *
     * @param stream    The stream where to serialize object
     * @throws java.io.IOException [blabla]
     */
    @SuppressWarnings("unchecked")
    private void readObject(final ObjectInputStream stream) throws IOException,ClassNotFoundException {
        representMax = (boolean) stream.readObject();
        dimension = (int) stream.readObject();
        cones = (HashSet<Cone>) stream.readObject();
        newCones = new HashSet<>();
        nbOfConesInLastPruning = cones.size();
    }


    /**
     * Test the equality of two representations
     * The equality is defined by the value on every point
     * @param o The object to compare to
     *
     * @return  True if the object have the same value on every point
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConeRepresentation)) return false;

        ConeRepresentation that = (ConeRepresentation) o;

        if (representMax != that.representMax) return false;
        if (dimension != that.dimension) return false;
        this.prune();
        that.prune();
        return cones.equals(that.cones);
    }

    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return cones.size();
    }
    
}
