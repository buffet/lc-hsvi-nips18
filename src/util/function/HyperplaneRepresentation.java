package util.function;

import util.container.MatrixX;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * Represent a convex or concave function in the simplex R^n.
 * Each point of the function is defined as the maximum/minimum value of a set of hyperplanes.
 *
 * This example show how to create a basic function, setWeight hyperplanes, and get the values.
 *
 * <code> {@code
 * //The function represent the maximum of the hyperplanes, and thus is convex
 * boolean representMax = true;
 * //The dimension of the input size of the function
 * int dimension = 3;
 * //Create the function
 * HyperplaneRepresentation function = new HyperplaneRepresentation(representMax,dimension);
 *
 * //Add hyperplanes
 * function.addHyperplane(point1);
 * function.addHyperplane(point2);
 * ...
 *
 * //Now, the function is represented by the maximum of a set of hyperplanes,
 *
 * //Get the value of the function
 * double value = function.getValue(point);
 * }</code>
 *
 * </pre>
 */
public class HyperplaneRepresentation implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * If representMax is true, we store the information of the upper bound of the set
     */
    private boolean representMax;

    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The dimension of the space
     */
    private int dimension;

    /**
     * The hyperplanes that are stored
     */
    private Set<MatrixX> hyperplanes;

    /**
     * The hyperplanes added after the last pruning
     */
    private Set<MatrixX> newHyperplanes;

    /**
     * The number of hyperplanes in the set when the last pruning was made
     */
    private int nbOfHyperplanesInLastPruning;


    /**
     * Initialize the hyperplane set representation
     *
     * @param representMax Is true if we consider the upper bound of the set
     * @param dimension The dimension of the set
     * @param pruning        Whether pruning is turned on/off
     */
    public HyperplaneRepresentation(boolean representMax, int dimension, boolean pruning) {
        this.representMax = representMax;
        this.dimension = dimension;
	this.pruning = pruning;
        hyperplanes = new HashSet<>();
        newHyperplanes = new HashSet<>();
        nbOfHyperplanesInLastPruning = 0;
    }


    /**
     * Add a hyperplane to the representation
     *
     * @param hyperplane The hyperplane to setWeight
     */
    public void addHyperplane(MatrixX hyperplane) {
        if(hyperplane == null) {
            throw new NullPointerException("The hyperplane given can't be null");
        }
        hyperplanes.add(hyperplane);
	if (pruning) {
	    newHyperplanes.add(hyperplane);
	    if (hyperplanes.size() > 1.1*nbOfHyperplanesInLastPruning) {
		prune();
	    }
	}
    }


    /**
     * Get the value in a particular point
     *
     * @param point The point where we want the value
     * @return The value on that point
     */
    public double getValue(MatrixX point) {
        double value;

        if(representMax) {
            value = Double.NEGATIVE_INFINITY;
        } else {
            value = Double.POSITIVE_INFINITY;
        }

        for(MatrixX hyperplane : hyperplanes) {
            if(representMax) {
                value = max(value, hyperplane.times(point).get(0,0));
            } else {
                value = min(value, hyperplane.times(point).get(0,0));
            }
        }

        return value;
    }


    /**
     * Remove the useless hyperplanes from the representation
     */
    private void prune() {
        if(newHyperplanes.size() == 0) {
            return;
        }
        Set<MatrixX> newHyperplaneSet = new HashSet<>();

        for(MatrixX hyperplane : hyperplanes) {
            if (!isHyperplaneDominatedByNewHyperplanes(hyperplane)) {
                newHyperplaneSet.add(hyperplane);
            }
        }

        newHyperplanes.clear();
        hyperplanes = newHyperplaneSet;
        nbOfHyperplanesInLastPruning = hyperplanes.size();
    }

    /**
     * Get the maximum slope
     */
    public double getMaxSlope() {
	prune();
	
	double minV = Double.POSITIVE_INFINITY;
	double maxV = Double.NEGATIVE_INFINITY;
        for(MatrixX hyperplane : hyperplanes) {
	    for(int i=0; i<hyperplane.getColumnDimension(); i++) {
		double V = hyperplane.get(0,i);
		if (V > maxV) maxV = V;
		if (V < minV) minV = V;
	    }
	}
	
	double medV = (minV+maxV)/2.;
	double maxSlope = Double.NEGATIVE_INFINITY;
        for(MatrixX hyperplane : hyperplanes) {
	    for(int i=0; i<hyperplane.getColumnDimension(); i++) {
		double V = Math.abs(hyperplane.get(0,i)-medV);
		if (V > maxSlope) maxSlope = V;
	    }
	}

	return maxSlope;
    }
    

    /**
     * Return the hyperplane which in the representative of the function on a particular point
     *
     * @param point  The point where we want the representative
     * @return The representative on that point
     */
    public MatrixX getRepresentative(MatrixX point) {
        double value;
        MatrixX representative = null;
        if(representMax) {
            value = Double.NEGATIVE_INFINITY;
        } else {
            value = Double.POSITIVE_INFINITY;
        }
        for(MatrixX hyperplane : hyperplanes) {
            double temp = hyperplane.times(point).get(0,0);
            if(representMax && value < temp) {
                value = temp;
                representative = hyperplane;
            } else if(!representMax && value > temp){
                value = temp;
                representative = hyperplane;
            }
        }
        return representative;
    }


    /**
     * Check if a hyperplane is dominated by another one in the current representation
     *
     * @param hyperplane The hyperplane to check
     * @return true if the hyperplane is dominated by another one
     */
    public boolean isHyperplaneDominated(MatrixX hyperplane) {
        return isHyperplaneDominatedByHyperplaneCollection(hyperplane,hyperplanes);
    }


    /**
     * Check if a hyperplane is dominated by a new hyperplane
     *
     * @param hyperplane The hyperplane to check
     * @return true if the hyperplane is dominated by another one
     */
    private boolean isHyperplaneDominatedByNewHyperplanes(MatrixX hyperplane) {
        return isHyperplaneDominatedByHyperplaneCollection(hyperplane,newHyperplanes);
    }


    /**
     * Check if a hyperplane si dominated by a hyperplane in a certain collection
     *
     * @param hyperplane                The hyperplane to check
     * @param hyperplaneCollection      The set of hyperplane to compare
     * @return true if one hyperplane dominates the given hyperplane
     */
    private boolean isHyperplaneDominatedByHyperplaneCollection(MatrixX hyperplane, Collection<MatrixX> hyperplaneCollection) {
        for(MatrixX otherHyperplane : hyperplaneCollection) {
            if(hyperplane.equals(otherHyperplane)) {
                continue;
            }
            if(isHyperplaneDominatedByHyperplane(hyperplane,otherHyperplane)) {
                return true;
            }
        }

        return false;
    }



    /**
     * Check of a hyperplane is dominated by another one
     *
     * @param hyperplane       The first hyperplane
     * @param otherHyperplane  The second hyperplane
     * @return true if the first hyperplane dominate the second one
     */
    private boolean isHyperplaneDominatedByHyperplane(MatrixX hyperplane, MatrixX otherHyperplane) {
        for(int d = 0; d<dimension; d++) {
            if(hyperplane.get(0,d) > otherHyperplane.get(0,d) && representMax) {
                return false;
            }
            if(hyperplane.get(0,d) < otherHyperplane.get(0,d) && !representMax) {
                return false;
            }
        }

        return true;
    }


    /**
     * Method used to serialize object
     *
     * @param stream    The stream where to serialize object
     */
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);
        stream.writeObject(dimension);
        stream.writeObject(hyperplanes);
    }


    /**
     * Method used to deserialize object
     *
     * @param stream    The stream where to serialize object
     */
    @SuppressWarnings("unchecked")
    private void readObject(final ObjectInputStream stream) throws IOException,ClassNotFoundException {
        representMax = (boolean) stream.readObject();
        dimension = (int) stream.readObject();
        hyperplanes = (HashSet<MatrixX>) stream.readObject();
        newHyperplanes = new HashSet<>();
        nbOfHyperplanesInLastPruning = hyperplanes.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HyperplaneRepresentation)) return false;
        HyperplaneRepresentation that = (HyperplaneRepresentation) o;
        if (representMax != that.representMax) return false;
        if (dimension != that.dimension) return false;
        this.prune();
        that.prune();
        return hyperplanes.equals(that.hyperplanes);
    }


    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return hyperplanes.size();
    }

}
