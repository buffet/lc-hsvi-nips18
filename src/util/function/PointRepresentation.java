package util.function;


import util.container.MatrixX;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * Represent a convex or concave function in the simplex [0;1]^n.
 * The function is defined as the maximum/minimum value of each point, for each point
 *
 * This example show how to create a basic function, setWeight points, and get the values.
 *
 * <code> {@code
 * //Create the corner values [0;0];
 * MatrixX cornerValues = new MatrixX(2,3);
 * //The function represent the minimum part of the hull, and thus is convex
 * boolean representMax = false;
 * //Create the function
 * PointRepresentation function = new PointRepresentation(representMax,cornerValues);
 *
 * //Add teeth, and give their values
 * function.addPoint(point1);
 * function.addPoint(point2);
 * ...
 *
 * //Get the approximate value of the function
 * double value = function.getValue(point);
 * }</code>
 *
 * </pre>
 */
public class PointRepresentation implements Serializable {


    /**
     * A struct representing a point
     */
    public static class Point implements Serializable {
        private static final long serialVersionUID = 1L;
        public final MatrixX vertex;
        public final double value;
        public Point(MatrixX vertex, double value) {
            if(vertex == null) {
                throw new NullPointerException("The vertex of a point cannot be null");
            }
            this.vertex = vertex;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Point)) return false;
            Point point = (Point) o;
            if (Double.compare(point.value, value) != 0) return false;
            return vertex.equals(point.vertex);
        }

        @Override
        public int hashCode() {
            int result = vertex.hashCode();
            long temp = Double.doubleToLongBits(value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * Is true if the boolean represent the maximum of the "convex hull" defined by the points
     */
    private boolean representMax;

    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The dimension of the space
     */
    private int dimension;

    /**
     * Values in the corner of the represented function
     */
    private MatrixX cornerValues;

    /**
     * The list of points used in the representation
     */
    private Set<Point> points;

    /**
     * The list of new teeth (relative to the last pruning)
     */
    private Set<Point> newPoints;

    /**
     * The number of teeth in the set when the last pruning was made
     */
    private int nbOfPointsInLastPruning;


    /**
     * Creates a new sawpoint representation with corner values
     *
     * @param representMax   True if we take the maximum values of the cones
     * @param cornerValues   Values in the corners of the simplex
     * @param pruning        Whether pruning is turned on/off
     */
    public PointRepresentation(boolean representMax, MatrixX cornerValues, boolean pruning) {
        this.representMax = representMax;
        this.dimension = cornerValues.getColumnDimension();
	this.pruning = pruning;
        points = new HashSet<>();
        newPoints = new HashSet<>();
        nbOfPointsInLastPruning = 0;
        this.cornerValues = new MatrixX(cornerValues);
    }


    /**
     * Add corner values if non dominated
     *
     * @param newCornerValues The values to setWeight
     */
    public void addCornerValues(MatrixX newCornerValues) {
        for(int i = 0; i<dimension; i++) {
            if(representMax && cornerValues.get(0,i) < newCornerValues.get(0,i)) {
                cornerValues.set(0,i,newCornerValues.get(0,i));
            } else if(!representMax && cornerValues.get(0,i) > newCornerValues.get(0,i)) {
                cornerValues.set(0,i,newCornerValues.get(0,i));
            }
        }
    }


    /**
     * Add a point to the representation, knowing it will not be dominated
     *
     * @param point The point to setWeight
     */
    public void addImprovingPoint(Point point) {
        if(point == null) {
            throw new NullPointerException("The added point can't be null");
        }
        points.add(point);
	if (pruning) {
	    newPoints.add(point);
	    if (points.size() >= nbOfPointsInLastPruning*1.1) {
		prune();
	    }
	}
    }


    /**
     * Add a point to the representation if necessary
     *
     * @param point The point to setWeight
     */
    public void addPoint(Point point) {
        if(point == null) {
            throw new NullPointerException("The added point can't be null");
        }
        if(!isPointDominated(point)) {
            addImprovingPoint(point);
        }
    }


    /**
     * Get the value of the represented function in a point
     *
     * @param point The point where we want the value
     * @return The value at that point
     */
    public double getValue(MatrixX belief) {
        double x0 = cornerValues.times(belief).get(0,0);
        double approximationValue = x0;
        for(Point point : points) {
            double temp = getValueForPointKnowingX0(belief,point,x0);

            if(representMax) {
                approximationValue = max(approximationValue,temp);
            } else {
                approximationValue = min(approximationValue,temp);
            }
        }
        return approximationValue;
    }


    /**
     * Get value of a point on a convex hull formed by a known point (point) and the corner points
     *
     * @param belief     The point where we want to calculate the value
     * @param point     The point
     * @return The value of the representation on the point
     */
    private double getValueForPoint(MatrixX belief, Point point) {
        double x0 = cornerValues.times(belief).get(0,0);
        return getValueForPointKnowingX0(belief,point,x0);
    }


    /**
     * Get the value of a point on a convex hull formed by a known point (point) and the corner points
     * knowing the scalar product between the point and the corner values
     *
     * @param point       The point where we want to calculate the value
     * @param point       A known point that is not in a corner
     * @param x0          The scalar product between the point and the corner values
     * @return The value of the representation on the point
     */
    private double getValueForPointKnowingX0(MatrixX belief, Point point, double x0) {

	if ( (point.vertex.norm1(belief)) < 0.0000001 ) {
	    return point.value;
	} else {
	    if(representMax) {
		return Double.NEGATIVE_INFINITY;
	    } else {
		return Double.POSITIVE_INFINITY;
	    }	    
	}
    }


    /**
     * Remove the useless points from the representation
     */
    private void prune() {
        if(newPoints.size() == 0) {
            return;
        }
        Set<Point> newPointSet = new HashSet<>();

        for(Point point : points) {
            if(!isPointDominatedByNewPoints(point)) {
                newPointSet.add(point);
            } else {
		//System.out.println("Point dominated.");
	    }
        }

        newPoints.clear();
        points = newPointSet;
        nbOfPointsInLastPruning = points.size();
    }

    /**
     * Get the maximum slope
     */
    public double getMaxSlope() {
	return 0.0;
    }

    /**
     * Check if a point is dominated by another point of the representation.
     * If the point is equal to an existing point in the representation, it will not be considered
     * as dominated if it is not dominated by another point.
     *
     * @param  point The point to check
     * @return true if the point is dominated by another point
     */
    public boolean isPointDominated(Point point) {
        return isPointDominatedByPointCollection(point, points);
    }



    /**
     * Check if a point is dominated by one of the new points
     *
     * @param  point The point to check
     * @return true if the point is dominated by another point
     */
    private boolean isPointDominatedByNewPoints(Point point) {
        return isPointDominatedByPointCollection(point, newPoints);
    }

    /**
     * Check if a point is dominated another point in a collection
     *
     * @param point The point to check
     * @return true if the point is dominated by another point
     */
    private boolean isPointDominatedByPointCollection(Point point, Collection<Point> pointCollection) {
        for(Point knownPoint : pointCollection) {
            if(knownPoint.equals(point)) {
                continue;
            }
            if(isPointDominatedByPoint(point,knownPoint)) {
                return true;
            }
        }
        if(representMax && (cornerValues.times(point.vertex).get(0,0) > point.value)) {
            return true;
        } else if(!representMax && (cornerValues.times(point.vertex).get(0,0) < point.value)) {
            return true;
        }
        return false;
    }


    /**
     * Check if a point is dominated by another point
     *
     * @param point1    The first point
     * @param point2    The second point
     * @return true if the first point is dominated by the second
     */
    private boolean isPointDominatedByPoint(Point point1, Point point2) {
	if ( (point1.vertex.norm1(point2.vertex)) > 0.0000001 ) {
	    return false;
	} else {
	    if(representMax) {
		return (point2.value >= point1.value);
	    } else {
		return (point2.value <= point1.value);
	    }
	}
    }


    /**
     * Method used to serialize object
     *
     * @param stream    The stream where to serialize object
     */
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);
        stream.writeObject(dimension);
        stream.writeObject(cornerValues);
        stream.writeObject(points);
    }


    /**
     * Method used to deserialize object
     *
     * @param stream    The stream where to serialize object
     */
    @SuppressWarnings("unchecked")
    private void readObject(final ObjectInputStream stream) throws IOException,ClassNotFoundException {
        representMax = (boolean) stream.readObject();
        dimension = (int) stream.readObject();
        cornerValues = (MatrixX) stream.readObject();
        points = (HashSet<Point>) stream.readObject();
        newPoints = new HashSet<>();
        nbOfPointsInLastPruning = points.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PointRepresentation)) return false;
        PointRepresentation that = (PointRepresentation) o;
        if (representMax != that.representMax) return false;
        if (dimension != that.dimension) return false;
        if (cornerValues != null ? !cornerValues.equals(that.cornerValues) : that.cornerValues != null) return false;
        prune();
        that.prune();
        return points.equals(that.points);
    }
    
    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return points.size();
    }
    
}
