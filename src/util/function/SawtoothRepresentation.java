package util.function;


import util.container.MatrixX;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * Represent a convex or concave function in the simplex [0;1]^n.
 * The function is defined as the maximum/minimum value of the convex hull of the corner teeth and each point, for each point
 *
 * This example show how to create a basic function, setWeight teeth, and get the values.
 *
 * <code> {@code
 * //Create the corner values [0;0];
 * MatrixX cornerValues = new MatrixX(2,3);
 * //The function represent the minimum part of the hull, and thus is convex
 * boolean representMax = false;
 * //Create the function
 * SawtoothRepresentation function = new SawtoothRepresentation(representMax,cornerValues);
 *
 * //Add teeth, and give their values
 * function.addPoint(tooth1);
 * function.addPoint(tooth2);
 * ...
 *
 * //Get the approximate value of the function
 * double value = function.getValue(point);
 * }</code>
 *
 * </pre>
 */
public class SawtoothRepresentation implements Serializable {


    /**
     * A struct representing a tooth
     */
    public static class Tooth implements Serializable {
        private static final long serialVersionUID = 1L;
        public final MatrixX vertex;
        public final double value;
        public Tooth(MatrixX vertex, double value) {
            if(vertex == null) {
                throw new NullPointerException("The vertex of a tooth cannot be null");
            }
            this.vertex = vertex;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Tooth)) return false;
            Tooth tooth = (Tooth) o;
            if (Double.compare(tooth.value, value) != 0) return false;
            return vertex.equals(tooth.vertex);
        }

        @Override
        public int hashCode() {
            int result = vertex.hashCode();
            long temp = Double.doubleToLongBits(value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }
    }

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * Is true if the boolean represent the maximum of the convex hull defined by the teeth
     */
    private boolean representMax;

    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The dimension of the space
     */
    private int dimension;

    /**
     * Values in the corner of the represented function
     */
    private MatrixX cornerValues;

    /**
     * The list of teeth used in the representation
     */
    private Set<Tooth> teeth;

    /**
     * The list of new teeth (relative to the last pruning)
     */
    private Set<Tooth> newTeeth;

    /**
     * The number of teeth in the set when the last pruning was made
     */
    private int nbOfTeethInLastPruning;


    /**
     * Creates a new sawtooth representation with corner values
     *
     * @param representMax   True if we take the maximum values of the cones
     * @param cornerValues   Values in the corners of the simplex
     * @param pruning        Whether pruning is turned on/off
     */
    public SawtoothRepresentation(boolean representMax, MatrixX cornerValues, boolean pruning) {
        this.representMax = representMax;
        this.dimension = cornerValues.getColumnDimension();
	this.pruning = pruning;
        teeth = new HashSet<>();
        newTeeth = new HashSet<>();
        nbOfTeethInLastPruning = 0;
        this.cornerValues = new MatrixX(cornerValues);
    }


    /**
     * Add corner values if non dominated
     *
     * @param newCornerValues The values to setWeight
     */
    public void addCornerValues(MatrixX newCornerValues) {
        for(int i = 0; i<dimension; i++) {
            if(representMax && cornerValues.get(0,i) < newCornerValues.get(0,i)) {
                cornerValues.set(0,i, newCornerValues.get(0,i));
            } else if(!representMax && cornerValues.get(0,i) > newCornerValues.get(0,i)) {
                cornerValues.set(0,i, newCornerValues.get(0,i));
            }
        }
    }


    /**
     * Add a tooth to the representation, knowing it will not be dominated
     *
     * @param tooth The tooth to setWeight
     */
    public void addImprovingTooth(Tooth tooth) {
        if(tooth == null) {
            throw new NullPointerException("The added point can't be null");
        }
        teeth.add(tooth);
	if (pruning) {
	    newTeeth.add(tooth);
	    if (teeth.size() >= nbOfTeethInLastPruning*1.1) {
		prune();
	    }
	}
    }


    /**
     * Add a tooth to the representation if necessary
     *
     * @param tooth The point to setWeight
     */
    public void addTooth(Tooth tooth) {
        if(tooth == null) {
            throw new NullPointerException("The added point can't be null");
        }
        if(!isPointDominated(tooth)) {
            addImprovingTooth(tooth);
        }
    }


    /**
     * Get the value of the represented function in a point
     *
     * @param point The point where we want the value
     * @return The value at that point
     */
    public double getValue(MatrixX point) {
        double x0 = cornerValues.times(point).get(0,0);
        double approximationValue = x0;
        for(Tooth tooth : teeth) {
            double temp = getValueForPointKnowingX0(point,tooth,x0);

            if(representMax) {
                approximationValue = max(approximationValue,temp);
            } else {
                approximationValue = min(approximationValue,temp);
            }
        }
        return approximationValue;
    }


    /**
     * Get value of a point on a convex hull formed by a known point (tooth) and the corner points
     *
     * @param point     The point where we want to calculate the value
     * @param tooth     The tooth
     * @return The value of the representation on the point
     */
    private double getValueForPoint(MatrixX point, Tooth tooth) {
        double x0 = cornerValues.times(point).get(0,0);
        return getValueForPointKnowingX0(point, tooth, x0);
    }


    /**
     * Get the value of a point on a convex hull formed by a known point (tooth) and the corner points
     * knowing the scalar product between the tooth and the corner values
     *
     * @param point       The point where we want to calculate the value
     * @param tooth       A known point that is not in a corner
     * @param x0          The scalar product between the point and the corner values
     * @return The value of the representation on the point
     */
    private double getValueForPointKnowingX0(MatrixX point, Tooth tooth, double x0) {
        double miniPhi = Double.POSITIVE_INFINITY;
        for(int d = 0; d<dimension; d++) {
            if(tooth.vertex.get(d,0) > 0) {
                miniPhi = min(miniPhi, (point.get(d,0) / tooth.vertex.get(d,0)));
            }
        }

        return x0 + miniPhi*(tooth.value - cornerValues.times(tooth.vertex).get(0,0));
    }


    /**
     * Remove the useless teeth from the representation
     */
    private void prune() {
        if(newTeeth.size() == 0) {
            return;
        }
        Set<Tooth> newPointSet = new HashSet<>();

        for(Tooth tooth : teeth) {
            if(!isPointDominatedByNewPoints(tooth)) {
                newPointSet.add(tooth);
            } else {
		//System.out.println("Tooth dominated.");
	    }
        }

        newTeeth.clear();
        teeth = newPointSet;
        nbOfTeethInLastPruning = teeth.size();
    }

    /**
     * Get the maximum slope
     */
    public double getMaxSlope() {
	prune();

	double maxSlope = Double.NEGATIVE_INFINITY;
	
	return maxSlope;
    }

    /**
     * Check if a tooth is dominated by another tooth of the representation.
     * If the tooth is equal to an existing tooth in the representation, it will not be considered
     * as dominated if it is not dominated by another tooth.
     *
     * @param  tooth The tooth to check
     * @return true if the point is dominated by another tooth
     */
    public boolean isPointDominated(Tooth tooth) {
        return isPointDominatedByPointCollection(tooth, teeth);
    }



    /**
     * Check if a tooth is dominated by one of the new teeth
     *
     * @param  tooth The tooth to check
     * @return true if the tooth is dominated by another point
     */
    private boolean isPointDominatedByNewPoints(Tooth tooth) {
        return isPointDominatedByPointCollection(tooth, newTeeth);
    }

    /**
     * Check if a tooth is dominated another tooth in a collection
     *
     * @param tooth The tooth to check
     * @return true if the point is dominated by another tooth
     */
    private boolean isPointDominatedByPointCollection(Tooth tooth, Collection<Tooth> toothCollection) {
        for(Tooth knownTooth : toothCollection) {
            if(knownTooth.equals(tooth)) {
                continue;
            }
            if(isPointDominatedByPoint(tooth,knownTooth)) {
                return true;
            }
        }
        if(representMax && (cornerValues.times(tooth.vertex).get(0,0) > tooth.value)) {
            return true;
        } else if(!representMax && (cornerValues.times(tooth.vertex).get(0,0) < tooth.value)) {
            return true;
        }
        return false;
    }


    /**
     * Check if a tooth is dominated by another tooth
     *
     * @param tooth1    The first tooth
     * @param tooth2    The second tooth
     * @return true if the first tooth is dominated by the second
     */
    private boolean isPointDominatedByPoint(Tooth tooth1, Tooth tooth2) {
        double value = getValueForPoint(tooth1.vertex,tooth2);
        if(representMax) {
	    return (value >= tooth1.value);
        } else {
	    return (value <= tooth1.value);
        }
    }


    /**
     * Method used to serialize object
     *
     * @param stream    The stream where to serialize object
     */
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);
        stream.writeObject(dimension);
        stream.writeObject(cornerValues);
        stream.writeObject(teeth);
    }


    /**
     * Method used to deserialize object
     *
     * @param stream    The stream where to serialize object
     */
    @SuppressWarnings("unchecked")
    private void readObject(final ObjectInputStream stream) throws IOException,ClassNotFoundException {
        representMax = (boolean) stream.readObject();
        dimension = (int) stream.readObject();
        cornerValues = (MatrixX) stream.readObject();
        teeth = (HashSet<Tooth>) stream.readObject();
        newTeeth = new HashSet<>();
        nbOfTeethInLastPruning = teeth.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SawtoothRepresentation)) return false;
        SawtoothRepresentation that = (SawtoothRepresentation) o;
        if (representMax != that.representMax) return false;
        if (dimension != that.dimension) return false;
        if (cornerValues != null ? !cornerValues.equals(that.cornerValues) : that.cornerValues != null) return false;
        prune();
        that.prune();
        return teeth.equals(that.teeth);
    }
    
    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return teeth.size();
    }
    
}
