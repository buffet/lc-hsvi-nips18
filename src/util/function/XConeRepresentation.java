package util.function;

import util.container.MatrixX;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * Represent a function in the simplex R^n.
 * Each point of the function is defined as the maximum/minimum value of a set of xcones.
 * If the function represent the maximum values of these xcones, the xcone points towards the positive values.
 * If the function represent the minimum values of these xcones, the xcone points towards the negative values.
 * A value of a xcone is defined as c(x') = v + s.|x-x'|,
 * with x the position of the vertex of the xcone, s the local (vector) slope and v the value of the xcone in its vertex.
 *
 * This example show how to create a basic function, setWeight xcones, and get the values.
 *
 * <code> {@code
 * // The function represent the maximum of xcones pointing upwards (e.g., a lower bounding function)
 * boolean representMax = true;
 * // The dimension of the input size of the function
 * int dimension = 5;
 * // Create the function
 * XConeRepresentation function = new XConeRepresentation(representMax,dimension,slope);
 *
 * // Add two xcones
 * function.addXCone(xcone1);
 * function.addXCone(xcone2);
 * ...
 *
 * // Now, the function is represented by the maximum of a set of xcones,
 *
 * // Get the value of the function
 * double value = function.getValue(point);
 * }</code>
 *
 * </pre>
 */
public class XConeRepresentation implements Serializable {

    /**
     * A structure representing a xcone
     */
    public static class XCone implements Serializable {
        private static final long serialVersionUID = 1L;
        public final MatrixX vertex;
        public final double value;
        public final MatrixX slope;
        public final MatrixX vCorners;
        public XCone(MatrixX vertex, double value, MatrixX slope, boolean representMax) {
            if(vertex == null) {
                throw new NullPointerException("The vertex of an xcone cannot be null");
            }
            if(slope == null) {
                throw new NullPointerException("The slope of an xcone cannot be null");
            }
            this.vertex = vertex;
            this.value = value;
            this.slope = slope;

	    /**
	     * Compute value in each corner of the simplex
	     */
	    MatrixX tmpM = new MatrixX(slope.getColumnDimension(),1,1);
	    tmpM.minus(vertex);
	    this.vCorners = new MatrixX( ( tmpM.transpose() ).arrayTimesEquals(slope) );
	    this.vCorners.arrayAbsEquals();
	    if (representMax)
		this.vCorners.timesEquals(-1);
	    this.vCorners.plusEquals( new MatrixX(1, slope.getColumnDimension(), value) );
	    
        }

        @Override
        public boolean equals(Object o) {
	    /*System.out.println("XCone.equals()");
	    System.out.format("%s =? %s%n",
			      System.identityHashCode(this),
			      System.identityHashCode(o));*/
	    if (this == o) return true;
            if (!(o instanceof XCone)) return false;
            XCone that = (XCone) o;
            if (Double.compare(that.value, value) != 0) return false;
            if (! slope.equals(that.slope)) return false;
            if (! vCorners.equals(that.vCorners)) return false;
            if (! vertex.equals(that.vertex)) return false;
	    // System.out.println("equal xcones");
	    // System.out.println(this);
	    // System.out.println(that);
	    return true;
        }

        @Override
        public int hashCode() {
            int result = vertex.hashCode();
            long temp = Double.doubleToLongBits(value);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

	public String toString() {
	    return String.format("%s %.3f %s %s", vertex, value, slope, vCorners);
	}
    }

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * Is true if the structure represent the maximum value of the xcones on a point
     */
    private boolean representMax;

    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The dimension of the space
     */
    private int dimension;

    /**
     * The set of xcones
     */
    private HashSet<XCone> xcones;

    /**
     * The set of xcones added after the last pruning
     */
    private HashSet<XCone> newXCones;

    /**
     * The set of xcones added before the last pruning
     */
    private HashSet<XCone> oldXCones;

    /**
     * The number of stored xcones after the last pruning
     */
    private int nbOfXConesInLastPruning;



    /**
     * Creates the set
     *
     * @param representMax   True if we take the maximum values of the xcones
     * @param dimension      The dimension of the space
     * @param pruning        Whether pruning is turned on/off
     */
    public XConeRepresentation(boolean representMax, int dimension, boolean pruning) {
        this.representMax = representMax;
        this.dimension = dimension;
	this.pruning = pruning;
        xcones = new HashSet<>();
        newXCones = new HashSet<>();
        oldXCones = new HashSet<>();
        nbOfXConesInLastPruning = 10;
    }


    /**
     * Add a xcone to the representation, knowing it is not dominated
     *
     * @param xcone The xcone to setWeight
     */
    public void addImprovingXCone(XCone xcone) {
        if(xcone == null) {
            throw new NullPointerException("The added xcone cannot be null");
        }
        xcones.add(xcone);
	if (pruning) {
	    newXCones.add(xcone);
	    if(xcones.size() > 1.1*nbOfXConesInLastPruning) {
		prune();
	    }
	}
    }

    /**
     * Add a xcone to the representation
     *
     * @param xcone The xcone
     */
    public void addXCone(XCone xcone) {
	//System.out.println("<addXCone>");
        if(!pruning || !isXConeDominated(xcone)) {
	    //System.out.println("*");
            addImprovingXCone(xcone);
        } else {
	    //System.out.println(xcone);
	    //System.out.println("<not>");
	}
    }


    /**
     * Get the value of the represented function
     *
     * @param point The point where to get the value
     * @return The value on that point
     */
    public double getValue(MatrixX point) {
        double value;
        if(representMax) {
            value = Double.NEGATIVE_INFINITY;
        } else {
            value = Double.POSITIVE_INFINITY;
        }

        for(XCone xcone : xcones) {
            if(representMax) {
                value = max(value,getValueForXCone(point,xcone));
            } else {
                value = min(value,getValueForXCone(point,xcone));
            }
        }

        return value;
    }

    /**
     * Get the point Beta where the best approximation is obtained.
     * (if better than init "value")
     *
     * @param point : The point where to get the value
     * @param value : reference value that must be improved upon
     * @return The value on that point
     */
    public XCone getBeta(MatrixX point, double value) {
	/** 
	double value;
        if(representMax) {
            value = Double.NEGATIVE_INFINITY;
        } else {
            value = Double.POSITIVE_INFINITY;
        }
	*/

	XCone beta = null;
        for(XCone xcone : xcones) {
	    double v = getValueForXCone(point,xcone);
            if(representMax) {
		if (v>value) {
		    value = v;
		    beta = xcone;
		}
            } else {
		if (v<value) {
		    value = v;
		    beta = xcone;
		}
            }
        }

        return beta;
    }



    /**
     * Get the value of a xcone on a point
     *
     * @param point The point where to get the value
     * @param xcone  The xcone used to get the value
     * @return The value of the xcone on that point
     */
    private double getValueForXCone(MatrixX point, XCone xcone) {
	////if (Math.abs(xcone.slope.normInf())<1000000000) {
	
	// double delta = xcone.slope.times( new MatrixX( point.minus(xcone.vertex) ).arrayAbsEquals() ).getArray()[0][0];
	
	// if(representMax) {
	//     return xcone.value - delta;
	// } else {
	//     return xcone.value + delta;
	// }

	//// } else
	
	// if ( (point.minus(xcone.vertex)).normInf() < 0.0000001 ) { // Way too slow!
	if ( point.normInf(xcone.vertex) < 0.0000001 ) {
	    return xcone.value;
	} else {
	    if(representMax) {
		return Double.NEGATIVE_INFINITY;
	    } else {
		return Double.POSITIVE_INFINITY;
	    }
	}
    }


    /**
     * Remove the useless xcones from the representation
     */
    private void prune() {
	//System.out.println("<prune>");
        if(newXCones.size() == 0) {
            return;
        }
        HashSet<XCone> newXConeSet = new HashSet<>();

	double maxSlope = Double.NEGATIVE_INFINITY;
	
	//System.out.println("<oldXCs>");
        for(XCone xcone : oldXCones) {
            if(!isXConeDominatedByNewXCones(xcone)) {
		//System.out.println("+");
                newXConeSet.add(xcone);
		MatrixX M = xcone.slope;
		double[][] A = M.getArray();
		for(int j=0; j<M.getRowDimension(); j++) {
		    if (A[0][j] > maxSlope)
			maxSlope = A[0][j];
		}
            } else {
		//System.out.println("-");
	    }
        }
	
	//System.out.println("<newXCs>");
        //for(XCone xcone : newXCones) {
	for (Iterator<XCone> it = newXCones.iterator(); it.hasNext();) {
	    XCone xcone = it.next();

            if(!isXConeDominatedByNewXCones(xcone)) {
		//System.out.println("+");
                newXConeSet.add(xcone);
		MatrixX M = xcone.slope;
		double[][] A = M.getArray();
		for(int j=0; j<M.getRowDimension(); j++) {
		    if (A[0][j] > maxSlope)
			maxSlope = A[0][j];
		}
            } else {
		//System.out.println("--");
		it.remove();
	    }
        }

	System.out.format("MaxSlope: %s%n",String.format("%6.3e",maxSlope));
	
	// if (newXConeSet.size() != xcones.size())
	//     System.out.format("xcones(%s): %d -> %d%n", representMax, xcones.size(), newXConeSet.size());
	// System.out.println("");

	oldXCones = xcones;
        xcones = newXConeSet;
        newXCones.clear();
        nbOfXConesInLastPruning = xcones.size();
	//System.out.println("</prune>");
    }

    /**
     * Get the maximum slope
     */
    public double getMaxSlope() {
	prune();
	
	double maxSlope = Double.NEGATIVE_INFINITY;
	
	for(XCone xcone : oldXCones) {
	    MatrixX M = xcone.slope;
	    double[][] A = M.getArray();
	    for(int j=0; j<M.getRowDimension(); j++) {
		if (A[0][j] > maxSlope)
		    maxSlope = A[0][j];
	    }
        }
	
	return maxSlope;
    }

    /**
     * Check if a xcone is dominated by another xcone of the representation
     *
     * @param  xcone The xcone to check
     * @return true if the xcone is dominated by another xcone
     */
    public boolean isXConeDominated(XCone xcone) {
        return isXConeDominatedByXConeCollection(xcone,xcones);
    }


    /**
     * Check if a xcone is dominated by a new xcone
     *
     * @param  xcone The xcone to check
     * @return true if the xcone is dominated by another xcone
     */
    private boolean isXConeDominatedByNewXCones(XCone xcone) {
        return isXConeDominatedByXConeCollection(xcone,newXCones);
    }


    /**
     * Check if a xcone is dominated by another xcone in one collection
     *
     * @param xcone              The xcone to check
     * @param xconeCollection    The collection of xcone
     * @return true if the xcone is dominated by a distinct xcone in the xcone collection
     */
    private boolean isXConeDominatedByXConeCollection(XCone xcone, Collection<XCone> xconeCollection) {
        for(XCone otherXCone : xconeCollection) {
	    if(otherXCone==xcone) {
		continue;
	    }
            if(otherXCone.equals(xcone)) {
		return true; //continue;
            }
            if(isXConeDominatedByXCone(xcone,otherXCone)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if an xcone is dominated by another xcone
     *
     * @param xcone       The xcone we want to check
     * @param otherXCone  The xcone for which we want to know if it dominates the other
     * @return true if otherXCone dominates xcone
     */
    private boolean isXConeDominatedByXConeOld(XCone xcone, XCone otherXCone) {
        double value = getValueForXCone(xcone.vertex,otherXCone);
	/*
	if (representMax)
	    System.out.println("[lowerBound]");
	else
	    System.out.println("[upperBound]");
	System.out.format("xcone %.3f %s%n", 0.0, xcone.toString());
	System.out.format("other %.3f %s%n", value, otherXCone.toString());
	*/
        if(representMax) {
	    boolean answer = (xcone.value <= value && (xcone.slope).isGE(otherXCone.slope) );
	    /*if (answer) {
		System.out.println("[lowerBound]");
		System.out.format("xcone %.3f %s%n", 0.0, xcone);
		System.out.format("other %.3f %s%n", value, otherXCone);
		}*/
	    return answer;
        } else {
	    boolean answer = (xcone.value >= value && (xcone.slope).isGE(otherXCone.slope) );
	    /*if (answer) {
		System.out.println("[upperBound]");
		System.out.format("xcone %.3f %s%n", 0.0, xcone);
		System.out.format("other %.3f %s%n", value, otherXCone);
		}*/
	    return answer;
        }
    }
    
    private boolean isXConeDominatedByXCone(XCone xcone, XCone otherXCone) {
        double value = getValueForXCone(xcone.vertex,otherXCone);
	/*
	if (representMax)
	    System.out.println("[lowerBound]");
	else
	    System.out.println("[upperBound]");
	System.out.format("xcone %.3f %s%n", 0.0, xcone.toString());
	System.out.format("other %.3f %s%n", value, otherXCone.toString());
	*/
        if(representMax) {
	    /* lower bound */
	    boolean answer = (xcone.value <= value && (xcone.vCorners).isLE(otherXCone.vCorners) );
	    /*if (answer) {
		System.out.println("[lowerBound]");
		System.out.format("xcone %.3f %s%n", 0.0, xcone);
		System.out.format("other %.3f %s%n", value, otherXCone);
		}*/
	    return answer;
        } else {
	    /* upper bound */
	    boolean answer = (xcone.value >= value && (xcone.vCorners).isGE(otherXCone.vCorners) );
	    /*if (answer) {
		System.out.println("[upperBound]");
		System.out.format("xcone %.3f %s%n", 0.0, xcone);
		System.out.format("other %.3f %s%n", value, otherXCone);
		}*/
	    return answer;
        }
    }



    /**
     * Method used to serialize object
     *
     * @param stream    The stream where to serialize object
     */
    private void writeObject(final ObjectOutputStream stream) throws IOException {
        prune();
        stream.writeObject(representMax);
        stream.writeObject(dimension);
        stream.writeObject(xcones);
    }


    /**
     * Method used to deserialize object
     *
     * @param stream    The stream where to serialize object
     */
    @SuppressWarnings("unchecked")
    private void readObject(final ObjectInputStream stream) throws IOException,ClassNotFoundException {
        representMax = (boolean) stream.readObject();
        dimension = (int) stream.readObject();
        xcones = (HashSet<XCone>) stream.readObject();
        newXCones = new HashSet<>();
        nbOfXConesInLastPruning = xcones.size();
    }


    /**
     * Test the equality of two representations
     * The equality is defined by the value on every point
     * @param o The object to compare to
     *
     * @return  True if the object have the same value on every point
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof XConeRepresentation)) return false;

        XConeRepresentation that = (XConeRepresentation) o;

        if (representMax != that.representMax) return false;
        if (dimension != that.dimension) return false;
        this.prune();
        that.prune();
        return xcones.equals(that.xcones);
    }
    
    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return xcones.size();
    }

}
