package util.container;

import util.grid.Direction;

/**
 * <pre>
 * Represent a position in a 2D space with integers.
 * The north is the negative values of the y axis.
 * The west is the negative values of the x axis.
 *
 * This example show how to get the adjacent position of a given position
 *
 * <code> {@code
 * Position2D position = new Position2D(0,0);
 * Position2D positionNorth = position.getAdjacentPosition(Direction.north);
 * // Now positionNorth is equal to (-1,0);
 * }</code></pre>
 */
public class Position2D {

    /**
     * The position on the x axis
     */
    public final int x;


    /**
     * The position on the y axis
     */
    public final int y;


    /**
     * Creates a new position. The two axis cannot be modified
     *
     * @param x The position on the x axis
     * @param y The position on the y axis
     */
    public Position2D(int x,int y) {
        this.x = x;
        this.y = y;
    }


    /**
     * Copy a position. The two axis cannot be modified
     *
     * @param position The position to copy
     */
    public Position2D(Position2D position) {
        this.x = position.x;
        this.y = position.y;
    }


    /**
     * Get the nearest position in a direction
     *
     * @param direction The direction
     * @return The nearest position in that direction
     */
    public Position2D getAdjacentPosition(Direction direction) {
        if(direction == null) {
            throw new IllegalArgumentException("The direction shouldn't be null");
        }
        switch(direction) {
            case north :
                return new Position2D(x,y-1);
            case south :
                return new Position2D(x, y+1);
            case east :
                return new Position2D(x+1, y);
            case west :
                return new Position2D(x-1, y);
            default :
                return this;
        }
    }

    @Override
    public String toString() {
        return "("+x+","+y+")";
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Position2D)) return false;

        Position2D that = (Position2D) object;

        if (x != that.x) return false;
        if (y != that.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
