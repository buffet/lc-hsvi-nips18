package util.container;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * Convert object of a given class to an integer.
 * The conversion is bijective, and the integers are in the range [0,n-1] if n objects are added.
 *
 * This example show how to create the object, and use it.
 *
 * <code> {@code
 * ObjectIntegerConverter<Character> converter = new ObjectIntegerConverter<Character>();
 * converter.setWeight('a');
 * converter.setWeight('b');
 * ...
 *
 * // Return the index associated with 'a' (which here is 0 if 'a' was the first element added)
 * int index = converter.toInteger('a');
 *
 * // Return the object associated with index ('a')
 * char value = converter.toA(index);
 * }</code></pre>
 *
 * @param <A> The class of the objects converted
 */
public class ObjectIntegerConverter<A> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * Map associating A objects to integers
     */
    private Map<A,Integer> toIntegers;

    /**
     * Map associating integers to A objects
     */
    private List<A> toA;

    /**
     * Create a basic converter, nothing is stored yet
     */
    public ObjectIntegerConverter() {
        toIntegers = new HashMap<>();
        toA = new ArrayList<>();
    }


    /**
     * Create a converter, given the bijection in a List
     *
     * @param elemList The bijection
     */
    public ObjectIntegerConverter(List<A> elemList) {
        toIntegers = new HashMap<>();
        toA = new ArrayList<>(elemList);
        for(int i = 0; i<elemList.size(); i++) {
            toIntegers.put(elemList.get(i),i);
        }
    }

    /**
     * Get object corresponding to an integer.
     *
     * @param i The integer
     * @return The object associated
     */
    public A toA(int i) throws IllegalArgumentException {
        if(i < toA.size() && i >= 0) {
            return toA.get(i);
        }
        throw new IllegalArgumentException("No element has index " + i + " in the converter.");
    }

    /**
     * Get integer corresponding to an object
     *
     * @param a The object
     * @return The integer
     */
    public int toInteger(A a) throws IllegalArgumentException{
        // if(toIntegers.containsKey(a)) { // Useless test! (AFAICT)
        //     return toIntegers.get(a);
        // }
	Integer i = toIntegers.get(a);
        if( i != null ) {
            return i;
        }
        throw new IllegalArgumentException("The element " + a + " was not added in the converter.");
    }


    /**
     * Add an object to the converter
     *
     * @param a The element to setWeight
     */
    public void add(A a) {
        toIntegers.put(a,toA.size());
        toA.add(a);
    }


    /**
     * Get the number of elements added to the converter
     *
     * @return The number of elements stored in the converter
     */
    public int getSize() {
        return toA.size();
    }
}
