package util.container;

import java.io.Serializable;

/**
 * <pre>
 * A simple struct containing two elements.
 * </pre>
 * @param <A>  The class of the first element
 * @param <B>  The class of the second element
 */
public class Pair<A,B> implements Serializable {
    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The first element
     */
    public final A a;

    /**
     * The second element
     */
    public final B b;

    /**
     * Creates a new pair. The two objects cannot be changed
     *
     * @param a The first element
     * @param b The second element
     */
    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "("+a+","+b+")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair<?, ?> pair = (Pair<?, ?>) o;

        if (a != null ? !a.equals(pair.a) : pair.a != null) return false;
        return b != null ? b.equals(pair.b) : pair.b == null;
    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        return result;
    }
}
