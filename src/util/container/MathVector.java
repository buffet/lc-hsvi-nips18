package util.container;

import java.io.Serializable;
import java.util.Arrays;

import static java.lang.Math.abs;

/**
 * <pre>
 * Represent a mathematical vector of R^n.
 *
 * The following example show how to use it.
 *
 * <code>
 * // Creates a vector in dimension 4, which has a value of 2 in every dimension
 * MathVector vector = new MathVector(4,2);
 *
 * // Change some of its dimensions
 * vector.set(3,2.3);
 * vector.set(0,1.2);
 *
 *
 * // Get the value of the 2-th dimension:
 * double value = vector.get(2);
 *
 * // Compute the scalar product of two vectors
 * double scalarProduct = MathVector.scalar(vector1,vector2);
 *
 * // Compute the l1 distance between two vectors
 * double distance = MathVector.getL1Distance(vector1,vector2);
 * </code></pre>
 */
public class MathVector implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The array containing the values of the vector
     */
    private double[] data;

    /**
     * Create a vector of a given dimension
     *
     * @param dimension The dimension of the vector
     */
    public MathVector(int dimension) {
        data = new double[dimension];
    }


    /**
     * Create a vector which have the same value in every dimension
     *
     * @param dimension The dimension of the vector
     * @param value     The value of every dimension
     */
    public MathVector(int dimension, double value) {
        data = new double[dimension];
        for(int i = 0; i<dimension; i++) {
            data[i] = value;
        }
    }

    /**
     * Copy a vector
     *
     * @param vector The vector to copy
     */
    public MathVector(MathVector vector) {
        data = new double[vector.getDimension()];
        System.arraycopy(vector.data,0,data,0,data.length);
    }


    /**
     * Return the i-th dimension value of the vector
     *
     * @param i The index of the dimension
     * @return The value of the i-th dimension
     */
    public double get(int i) throws IndexOutOfBoundsException {
        return data[i];
    }


    /**
     * Set the i-th dimension value of the vector
     *
     * @param i     The index of the dimension
     * @param value The new value of the i-th dimension
     */
    public void set(int i, double value) {
        data[i] = value;
    }


    /**
     * Return the scalar product between two vectors
     *
     * @param vector1  The first vector
     * @param vector2  The second vector
     * @return The scalar product
     */
    public static double scalar(MathVector vector1, MathVector vector2) throws ArrayIndexOutOfBoundsException {
        if(vector2.getDimension() != vector1.getDimension()) {
            String errorMessage = "The two MathVector should have the same size in order to compute their scalar product." +
                    "The sizes : " + vector1.getDimension() +" and " + vector2.getDimension() + " were given";
            throw new ArrayIndexOutOfBoundsException(errorMessage);
        }
        double value = 0.0;
        for(int d = 0; d<vector1.getDimension(); d++) {
            if(vector1.get(d) != 0.0 && vector2.get(d) != 0.0) {
                value += vector1.get(d) * vector2.get(d);
            }
        }
        return value;
    }


    /**
     * Return the L1 distance between two vectors
     *
     * @param vector1 The first vector
     * @param vector2 The second vector
     * @return The L1 distance
     */
    public static double getL1Distance(MathVector vector1, MathVector vector2) throws ArrayIndexOutOfBoundsException {
        double value = 0;
        if(vector1.getDimension() != vector2.getDimension()) {
            String errorMessage = "The two MathVector should have the same size in order to compute their L1 distance. " +
                    "The sizes : " + vector1.getDimension() +" and " + vector2.getDimension() + " were given";
            throw new ArrayIndexOutOfBoundsException(errorMessage);
        }

        for(int i = 0; i<vector1.getDimension(); i++) {
            value += abs(vector1.get(i)-vector2.get(i));
        }
        return value;
    }


    /**
     * Return the dimension of the vector
     *
     * @return The dimension
     */
    public int getDimension() {
        return data.length;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MathVector)) return false;

        MathVector that = (MathVector) o;

        if (!Arrays.equals(data, that.data)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }


    /**
     * Display the vector like an array
     * @return The result of the toString function applied to the internal array
     */
    @Override
    public String toString() {
        return Arrays.toString(data);
    }

}
