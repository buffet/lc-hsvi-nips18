package util.container;

/**
 * <pre>
 * A simple struct containing three elements.
 * </pre>
 *
 * @param <A> The class of the first element
 * @param <B> The class of the second element
 * @param <C> The class of the third element
 */
public class Triplet<A,B,C> {

    /**
     * The first element
     */
    public final A a;

    /**
     * The second element
     */
    public final B b;

    /**
     * The third element
     */
    public final C c;

    /**
     * Create a new triplet, the elements cannot be changed (but can be modified)
     *
     * @param a The first element
     * @param b The second element
     * @param c The third element
     */
    public Triplet(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triplet)) return false;

        Triplet<?, ?, ?> triplet = (Triplet<?, ?, ?>) o;

        if (a != null ? !a.equals(triplet.a) : triplet.a != null) return false;
        if (b != null ? !b.equals(triplet.b) : triplet.b != null) return false;
        return c != null ? c.equals(triplet.c) : triplet.c == null;
    }


    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result = 31 * result + (b != null ? b.hashCode() : 0);
        result = 31 * result + (c != null ? c.hashCode() : 0);
        return result;
    }
}
