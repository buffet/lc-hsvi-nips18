package util.random;

import java.util.Random;

/**
 * Generates doubles or integers randomly
 * The seed can be changed and the generator regenerated to allow replication.
 *
 * This example show how to generate values:
 *
 * <pre><code> {@code
 * // Regenerate a generator. This is optional since when the program start,
 * // a generator is automatically generated with seed 42
 * long seed = 42;
 * RandomGenerator.regenerateRandomGenerator(seed);
 *
 * // Generate uniformly an int and a double
 * int i = RandomGenerator.getInt();
 * double d = RandomGenerator.getDouble();
 * }</code></pre>
 */
public class RandomGenerator {

    /**
     * The random generator used
     */
    private static Random random = new Random(42);


    /**
     * Regenerate the random generator
     */
    public static void regenerateRandomGenerator() {
        random = new Random(42);
	random.nextInt(); // Skip first value (would not change much if similar seeds are used)
    }

    /**
     * Regenerate the random generator with a seed
     *
     * @param seed : A seed to initialize the PRNG
     */
    public static void regenerateRandomGenerator(long seed) {
        random = new Random(seed);
	random.nextInt(); // Skip first value (would not change much if similar seeds are used)
    }

    /**
     * Return a random double uniformly between 0 and 1
     *
     * @return The random value
     */
    public static double getDouble() {
        return random.nextDouble();
    }


    /**
     * Return a random integer uniformly
     *
     * @return The random value
     */
    public static int getInt() {
        return random.nextInt();
    }


    /**
     * Return a random integer uniformly in the range [mini,maxi[
     *
     * @param mini The minimum value the integer can have
     * @param maxi The maximum value the integer can have, plus one
     * @return The random value
     */
    public static int getInt(int mini, int maxi) {
        return (random.nextInt(Integer.MAX_VALUE) % (maxi-mini)) + mini;
    }
}
