package util.grid;


import util.container.Position2D;


/**
 * <pre>
 * Represent a 2D rectangle tor
 * The north is the negative values of the y axis.
 * The west is the negative values of the x axis.
 *
 * The positions in the tor are always positives, and start with 0.
 *
 * This example show how to get the position of the next element in the north direction
 * of a position. The class check if the position is in the tor, and return the position
 * in that direction (which will be in the tor)
 *
 *
 * <code> {@code
 * TorGrid grid = new grid(4,5);  // width and height
 * Position2D position = new position(2,0);
 * Position2D positionNorth = grid.getAdjacentPosition(position,Direction.north);
 * // PositionNorth is equals to (2,4)
 * } </code></pre>
 */
public class TorGrid extends Grid {

    /**
     * Create a new tor grid with given dimensions
     *
     * @param height The height of the tor grid
     * @param width The width of the tor grid
     */
    public TorGrid(int width, int height) {
        super(width,height);
    }


    /**
     * Get the next case in the direction
     *
     * @param pos The current position
     * @param dir The direction
     * @return The next case in that direction
     */
    @Override
    public Position2D getAdjacentPosition(Position2D pos, Direction dir) {
        if(pos.x < 0 || pos.x>= width || pos.y < 0 || pos.y >= height) {
            String message = "The position should be on the tor.\n" +
                    "("+pos.x+","+pos.y+") given, the size of the tor is ("+width+","+height+").";
            throw new IllegalArgumentException(message);
        }
        Position2D result = pos.getAdjacentPosition(dir);
        return new Position2D((width + result.x) % width, (height + result.y) % height);
    }
}
