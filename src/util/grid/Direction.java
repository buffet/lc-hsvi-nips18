package util.grid;

/**
 * Represent one of the quadrant direction (or no direction)
 */
public enum Direction{
    north, south, west, east, none;

    /**
     * Return a letter representing the direction
     *
     * @return The short description of the direction
     */
    public String oneLetterToString() {
        switch(this) {
            case north:
                return "N";
            case south:
                return "S";
            case west:
                return "W";
            case east:
                return "E";
            default:
                return "n";
        }
    }
}
