package util.grid;


import util.container.Position2D;
import java.util.ArrayList;
import java.util.List;


/**
 * <pre>
 * Represent a 2D rectangle grid where cases are colored
 * The north is the negative values of the y axis.
 * The west is the negative values of the x axis.
 *
 * The positions in the tor are always positives, and start with 0.
 *
 * This example show how to get the position of the next element in the north direction
 * of a position, and to color it.
 *
 * <code> {@code
 * TorGrid grid = new grid(4,5); // width and height of the underlying grid
 * ColoredGrid coloredGrid = new ColoredGrid<>(grid,10); //We use 10 colors
 * Position2D position = new position(2,0);
 * Position2D positionNorth = grid.getAdjacentPosition(position,Direction.north);
 * coloredGrid.setColor(positionNorth,3);
 * assert(coloredGrid.getColor(positionNorth) == 3);
 * } </code></pre>
 */
public class ColoredGrid {

    /**
     * The grid used to find adjacent positions
     */
    public final Grid grid;

    /**
     * The coloration of the grid
     */
    private int[][] colors;

    /**
     * The number of colors used
     */
    public final int nbColors;


    /**
     * Create a colored grid
     *
     * @param grid      The grid used to find adjacent positions
     * @param nbColors  The number of colors used (The colors are between 0 and nbColors)
     */
    public ColoredGrid(Grid grid, int nbColors) {
        this.grid = grid;
        this.nbColors = nbColors;
        colors = new int[grid.width][grid.height];
    }


    /**
     * Set the color of a case in the grid
     *
     * @param pos   The position of the case in the grid
     * @param color The new color of that position
     */
    public void setColor(Position2D pos, int color) {
        colors[pos.x][pos.y] = color;
    }


    /**
     * Get the color of a case in the grid
     *
     * @param pos   The position of the case in the grid
     * @return      The color of this case
     */
    public int getColor(Position2D pos) {
        return colors[pos.x][pos.y];
    }


    /**
     * Get the next case in the direction
     *
     * @param pos The current position
     * @param dir The direction
     * @return The next case in the direction if it exists, null otherwise
     */
    public Position2D getAdjacentPosition(Position2D pos, Direction dir) {
        return grid.getAdjacentPosition(pos, dir);
    }
}
