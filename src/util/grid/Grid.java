package util.grid;

import util.container.Position2D;


/**
 * <pre>
 * Represent a 2D rectangle grid.
 * The north is the negative values of the y axis.
 * The west is the negative values of the x axis.
 *
 * This example show how to get the position of the next element in the north direction
 * of a position. This is better than using the getAdjacentPosition function from the Position2D
 * class, since this function check if the given position is in the grid, and if
 * the next element is in the grid
 *
 * <code> {@code
 * Grid grid = new grid(width,height);
 * Position2D position = new position(2,3);
 * Position2D positionNorth = grid.getAdjacentPosition(position,Direction.north);
 * } </code></pre>
 */
public class Grid {

    /**
     * The height of the grid
     */
    public final int height;

    /**
     * The width of the grid
     */
    public final int width;


    /**
     * Create a new grid with given dimensions
     *
     * @param height The height of the grid
     * @param width The width of the grid
     */
    public Grid(int width, int height) {
        this.height = height;
        this.width = width;
    }


    /**
     * Get the next case in the direction
     *
     * @param dir The direction
     * @param pos The current position
     * @return The next case in the direction if it exists, null otherwise
     */
    public Position2D getAdjacentPosition(Position2D pos, Direction dir) {
        if(pos.x < 0 || pos.x>= width || pos.y < 0 || pos.y >= height) {
            String message = "The position should be on the grid.\n" +
                    "("+pos.x+","+pos.y+") given, the size of the grid is ("+width+","+height+").";
            throw new IllegalArgumentException(message);
        }
        Position2D result = pos.getAdjacentPosition(dir);
        if(result.x < 0 || result.x>= width || result.y < 0 || result.y >= height) {
            return null;
        }
        return result;
    }
}
