package util.signal;
import sun.misc.Signal;
import sun.misc.SignalHandler;


/**
 * <pre>
 * This class is used when you want to handle a single signal.
 * No action are made when the signal is handled.
 * If the signal is to handle a second time, it will act like ExitSignalHandler.
 *
 * The following example show how to handle the SIGINT (Ctrl+C).
 *
 * <code> {@code
 * SingleSignalHandler handler = new SingleSignalHandler();
 * Signal.handle(new Signal("INT"),handler);
 * // Now, every Ctrl-C will result in an exit of the system
 * while(condition && !signal.hasHandleSignal()) {
 *     // Do stuff here
 *     // If Ctrl-C is used once, we will get out of the while loop
 *     // If it is used once more, the ExitSignalHandler will be used
 * }
 * // When we do not need the SingleSignalHandler anymore,
 * // we can switch back to the usual one
 * Signal.handle(new Signal("INT"),new ExitSignalHandler());
 * }</code></pre>
 */
public class SingleSignalHandler implements SignalHandler {

    /**
     * The boolean keeping track
     */
    private boolean handled;

    /**
     * Create a new single signal handler
     */
    public SingleSignalHandler() {
        handled = false;
    }

    /**
     * Check if a signal was handled
     *
     * @return  True if a signal was handled
     */
    public boolean hasHandleSignal() {
        return handled;
    }


    /**
     * The action to do when handling a signal
     *
     * @param signal    The signal to handle
     */
    @Override
    public void handle(Signal signal) {
        handled = true;
        System.out.println("Signal sent");
        System.out.println("Now, emitting this signal will result in an exit of the program");
        Signal.handle(signal,new ExitSignalHandler());
    }
}
