package util.signal;

import sun.misc.Signal;
import sun.misc.SignalHandler;

/**
 * <pre>
 * This class is used when you want that a certain signal will trigger an exit of the program.
 * The exit will have the code ExitCode + 128  (UNIX standard).
 *
 * The following example show how to handle the SIGINT (Ctrl+C).
 *
 * <code> {@code
 * SignalHandler handler = new ExitSignalHandler();
 * Signal.handle(new Signal("INT"),handler);
 * // Now, every Ctrl-C will result in an exit of the system
 * }</code></pre>
 */
public class ExitSignalHandler implements SignalHandler {

    /**
     * The action to do when handling a signal
     *
     * @param signal    The signal to handle
     */
    @Override
    public void handle(Signal signal) {
        System.exit(signal.getNumber() + 128);
    }
}
