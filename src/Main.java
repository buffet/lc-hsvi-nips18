import mdp.example.positionsearch.PSAction;
import mdp.example.positionsearch.PSObs;
import mdp.example.positionsearch.PSState;
import mdp.example.RhoPOMDPFactory;
import mdp.example.csurveillance.CSAction;
import mdp.example.csurveillance.CSObs;
import mdp.example.csurveillance.CSState;
import mdp.interpreter.POMDPFromFile;
import mdp.interpreter.POMDPReader;
import mdp.pomdp.Belief;
import mdp.pomdp.POMDP;
import mdp.rhopomdp.KLReward;
import mdp.rhopomdp.RhoPOMDP;
import mdp.rhopomdp.RhoPOMDP2;
import mdp.rhopomdp.RewardFunction;
import mdp.rhopomdp.RhoGridInfoKX;
import mdp.rhopomdp.RhoGridInfoUX;
import mdp.rhopomdp.RhoGridInfoKY;
import mdp.rhopomdp.RhoGridInfoUY;
import mdp.simulation.POMDPSimulation;
import mdp.solver.HSVIFactory;
import mdp.solver.HSVI;
import mdp.solver.ILHSVI;
import mdp.solver.HSVIPythonPrinter;
import mdp.solver.HSVIPythonPrinterAnimated;
import mdp.solver.HSVIPythonPrinterFixed;
import util.container.Pair;
import util.container.Position2D;
import util.grid.ColoredGrid;
import util.grid.Grid;
import util.grid.TorGrid;

import util.random.RandomGenerator;

import java.io.*;
import java.util.*;
import java.util.List;

import java.lang.management.*;


public class Main {


    /** Get CPU time in nanoseconds. */
    public static long getCpuTime( ) {
	ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	return bean.isCurrentThreadCpuTimeSupported( ) ?
	    bean.getCurrentThreadCpuTime( ) : 0L;
    }
    
    /** Get user time in nanoseconds. */
    public static long getUserTime( ) {
	ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	return bean.isCurrentThreadCpuTimeSupported( ) ?
	    bean.getCurrentThreadUserTime( ) : 0L;
    }
    
    /** Get system time in nanoseconds. */
    public static long getSystemTime( ) {
	ThreadMXBean bean = ManagementFactory.getThreadMXBean( );
	return bean.isCurrentThreadCpuTimeSupported( ) ?
	    (bean.getCurrentThreadCpuTime( ) - bean.getCurrentThreadUserTime( )) : 0L;
    }


    
    /*
    public static void testCS() {
        Pair<RhoPOMDP<CSState,CSAction,CSObs>,Belief<CSState>> rhoPOMDP;
        rhoPOMDP = RhoPOMDPFactory.generateChildrenSurveillance(4,2,new KLReward<>(64));
        HSVI<CSState,CSAction,CSObs> hsvi = HSVIFactory.buildRhoHSVI(rhoPOMDP.a,0.95,0.1,true,true,true);
	long time = System.currentTimeMillis();
	System.out.println(hsvi.getOptimalValue(rhoPOMDP.b, time, 60*1000, -1));

        POMDPSimulation<CSState,CSAction,CSObs> simulation = new POMDPSimulation<>(rhoPOMDP.a,hsvi.lowerBound,rhoPOMDP.b,0.95);
        simulation.simulateOnce(0.01,true,false);
        System.out.println(simulation.simulate(0.001,1000));
        System.exit(0);
    }

    public static void testPS() {
        Grid grid = new TorGrid(3,3);
        ColoredGrid coloredGrid = new ColoredGrid(grid,2);
        coloredGrid.setColor(new Position2D(1,1),1);
        Pair<RhoPOMDP<PSState,PSAction,PSObs>,Belief<PSState>> rhoPOMDP;
        rhoPOMDP = RhoPOMDPFactory.generatePositionSearch(1.0,1.0,coloredGrid);
        ILHSVI<PSState,PSAction,PSObs> hsvi = new ILHSVI<>(rhoPOMDP.a,0.95,0.01,true,true,true,true);
	long time = System.currentTimeMillis();
        System.out.println(hsvi.getOptimalValue(rhoPOMDP.b, time, 60*1000, -1));

        POMDPSimulation<PSState,PSAction,PSObs> simulation = new POMDPSimulation<>(rhoPOMDP.a,hsvi.getLowerBound(),rhoPOMDP.b,0.95);
        simulation.simulateOnce(0.01,true,false);
        System.out.println(simulation.simulate(0.001,1000));

        System.exit(0);
    }
    */

    private static void help() {
	System.out.println(" #################### ");
	System.out.println(" ## Lipschitz HSVI ## ");
	System.out.println(" #################### ");
	System.out.println("");
	System.out.println("Usage:");
	System.out.println(" java Main [solve|simulate|info] [+args...]");
	System.out.println("or ./run.sh [solve|simulate|info] [+args...]");
	System.out.println("");
	System.out.println("Additional arguments:");
	System.out.println(" -h/--help (help page)");
	System.exit(1);
    }

    public static void main(String[] args) {

	// Start by ensuring System.out.format does not output
	// French-formatted numbers (with commas).
	Locale locale = new Locale("En", "US");
	Locale.setDefault(locale);
	// Locale.setDefault(Locale.Category.DISPLAY, locale);
	// Locale.setDefault(Locale.Category.FORMAT, locale);
		
	//System.out.println("args.length="+args.length);
	System.out.print("args: ");
	for(int i = 0; i<args.length; i++)    System.out.print("["+i+"] "+args[i]+" ");
	System.out.println("");
	
        if(args.length < 1)
	    help();

        if(args[0].equals("--help") || args[0].equals("-h")) {
            help();
        }if(args[0].equals("solve")) {
            doSolve(args);
        } else if(args[0].equals("simulate")) {
            doSimulate(args);
        } else if(args[0].equals("info")) {
            doGetInfo(args);
        } else {
            System.out.println("Unrecognized command: " + args[0]);
            System.out.println("");
	    help();
            //System.exit(1);
        }
	
	System.out.println("<THE END>");
    }

    private static void helpSolve() {
	System.out.println(" #################### ");
	System.out.println(" ## Lipschitz HSVI ## ");
	System.out.println(" #################### ");
	System.out.println("");
	System.out.println("Usage:");
	System.out.println("     java Main solve [Expected arguments] [Additionnal arguments]");
	System.out.println("or   ./run.sh solve [Expected arguments] [Additionnal arguments]");
	System.out.println("");
	System.out.println("* Expected arguments:");
	//System.out.println("  solverType [hsvi|conic|itconic|xconic|point]");
	System.out.println("  solverType [pwlc|conic|xconic|point|itconic]");
	System.out.println("  pomdpFileName");
	System.out.println("");
	System.out.println("* Additionnal arguments:");
	System.out.println(" -h/--help (this help page)");
	System.out.println(" -p/--print no/anim/last");
	System.out.println(" --timeout=INTEGER (seconds)");
	System.out.println(" --iterations=INTEGER");
	System.out.println(" -e/--epsilon=REAL_NUMBER");
	System.out.println(" --noPruning");
	System.out.println(" --slope=REAL_NUMBER");
	System.out.println("   * not specified => automatically computed");
	System.out.println("   * for 'xconic' solver:");
	System.out.println("     * >= 0 => manually chosen value");
	System.out.println("     * < 0  => automatically computed value (possibly infinite)");
	System.out.println("     * not specified => automatic computation for each xcone");
	System.out.println(" --saveFile=FILE_NAME");
	System.out.println(" --exLXU (don't check that L and U bounds cross each other)");
	System.out.println(" --exNUI (don't check that bounds are uniformly improvable)");
	System.out.println(" --exUR (don't check if results are stable (only for itconic))");
	System.out.println(" --rhoClass=ClassName (for $\\rho$-POMDPs, the name of the class to use to complete the POMDP file definition)");
	System.out.println("   I only know about:");
	System.out.println("    * GridInfoKX (try to know $x$),");
	System.out.println("    * GridInfoUX (try not to know $x$),");
	System.out.println("    * GridInfoKY (try to know $y$), and");
	System.out.println("    * GridInfoUY (try not to know $y$).");
	System.exit(1);
    }

    private static void doSolve(String[] args) {
        if(args.length < 3) {
            System.out.println("There are not enough arguments.");
            System.out.println("");
	    helpSolve();
	}

        String solverType = args[1];
        String pomdpFilePath = args[2];

        Map<String,String> options = new HashMap<>();
        options.put("epsilon","0.001");
        options.put("solverType",args[1]);
        options.put("pomdpFilePath",args[2]);
        options.put("print","no");
        options.put("timeout","0");
        options.put("iterations","-1");
        options.put("noPruning","false");
        options.put("slope","computed");
        options.put("exNUI","true");
        options.put("exLXU","true");
        options.put("exUR","true");
	options.put("rhoClass","");

        for(int i = 3; i<args.length; i++) {
            if(args[i].equals("--help") || args[i].equals("-h")) {
                helpSolve();
            } else if(args[i].equals("--noPruning")) {
                options.put("noPruning","true");
            } else if(args[i].equals("--print") || args[i].equals("-p")) {
                options.put("print",args[++i]);
            } else if(args[i].startsWith("--timeout=")) {
                options.put("timeout",args[i].substring(10,args[i].length()));
            } else if(args[i].startsWith("--iterations=")) {
                options.put("iterations",args[i].substring(13,args[i].length()));
            } else if(args[i].startsWith("-e=")) {
                options.put("epsilon",args[i].substring(3,args[i].length()));
            } else if(args[i].startsWith("--epsilon=")) {
                options.put("epsilon",args[i].substring(10,args[i].length()));
            } else if(args[i].startsWith("--slope=")) {
                options.put("slope",args[i].substring(8,args[i].length()));
            } else if(args[i].startsWith("--saveFile=")) {
                options.put("saveFile",args[i].substring(11,args[i].length()));
            } else if(args[i].equals("--exNUI")) {
                options.put("exNUI","false");
            } else if(args[i].equals("--exLXU")) {
                options.put("exLXU","false");
            } else if(args[i].equals("--exUR")) {
                options.put("exUR","false");
            } else if(args[i].startsWith("--rhoClass=")) {
                options.put("rhoClass",args[i].substring(11,args[i].length()));
            } else {
                System.out.println("Unrecognized parameter : " + args[i]);
		System.out.println("");
		helpSolve();
                //System.exit(1);
            }
        }

        POMDPFromFile pomdpff;
        try{
            pomdpff = POMDPReader.getPOMDPFromFile(pomdpFilePath);
        } catch(Exception e) {
            System.out.println("Error while reading the pomdp file: " + e.getLocalizedMessage());
            System.exit(1);
            return; //Written so IDEA understand that this this branch will exit the function
        }

	System.out.format("|S|= %d |A|= %d |O|= %d g= %.3f\n",
			  pomdpff.pomdp.getStates().size(),
			  pomdpff.pomdp.getActions().size(),
			  pomdpff.pomdp.getObservations().size(),
			  pomdpff.discount);
	
        if(options.get("print").equals("true") && pomdpff.pomdp.getStates().size() != 2) {
            System.out.println("Can't print when the number of states is different from 2.");
            System.exit(1);
        }

        System.out.println(pomdpff.pomdp.getValueFunctionLowerBound(pomdpff.discount, 0.0001));

        switch(solverType) {
            case "pwlc":
            case "conic":
            case "xconic":
	    case "point":
                doSolveHSVI(pomdpff,options);
                return;
            case "itconic":
                doSolveItConic(pomdpff,options);
                return;
            default:
                System.out.println("The solver type was not recognized");
                System.out.println("");
		helpSolve();
                //System.exit(1);
        }
    }


    public static void doSolveHSVI(POMDPFromFile pomdpff, Map<String,String> options) {
        HSVI<Integer,Integer,Integer> hsvi;
        double epsilon = Double.parseDouble(options.get("epsilon"));
	Boolean exLXU = Boolean.parseBoolean(options.get("exLXU"));
	Boolean exNUI = Boolean.parseBoolean(options.get("exNUI"));
	Boolean exUR = Boolean.parseBoolean(options.get("exUR"));
	Boolean pruning = !Boolean.parseBoolean(options.get("noPruning"));
	long timelimit = Long.parseLong(options.get("timeout"))*1000;
	int iterations = Integer.parseInt(options.get("iterations"));

	HSVIPythonPrinter printer = null;
    
	if (options.get("print").equals("no")) {
	    printer = null;
	} else if (pomdpff.pomdp.getStates().size() != 2) {
            System.out.println("Can't print when the number of states is different from 2.");
            System.exit(1);
	} else if (options.get("print").equals("anim")) {
	    printer = new HSVIPythonPrinterAnimated("script.py", "visualization_2_.gif", 1000, pomdpff.pomdp);
	} else if (options.get("print").equals("last")) {
	    printer = new HSVIPythonPrinterFixed("script.py", "BURP.txt", 1000, pomdpff.pomdp);
	} else {
	    System.out.println("Unknown value '"+options.get("print")+"' for --print option.");
	    helpSolve();
	}
	
	System.out.println("(rho) reward function/class: "+options.get("rhoClass"));
	RewardFunction reward = null;
	if (options.get("rhoClass").length() == 0) {
	    System.out.println("No specified reward. Using linear definition.");
	    reward = null;
	} else if (options.get("rhoClass").equals("GridInfoKX")) {
	    reward = new RhoGridInfoKX(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoUX")) {
	    reward = new RhoGridInfoUX(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoKY")) {
	    reward = new RhoGridInfoKY(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoUY")) {
	    reward = new RhoGridInfoUY(pomdpff.pomdp.mdp);
	} else {
	    System.out.println("Unknown (rho) reward function/class "+options.get("rhoClass"));
	    helpSolve();
	}

	POMDP pomdp;
	if (reward == null) {
	    pomdp = pomdpff.pomdp;
	} else {
	    pomdp = new RhoPOMDP2(pomdpff.pomdp.getMDP(),
				  pomdpff.pomdp.getObservations(),
				  pomdpff.pomdp.getObservationManager(),
				  reward);
	}
	
        if(options.get("solverType").equals("conic")) {
            if(options.get("slope").equals("computed")) {
                hsvi = HSVIFactory.buildConicHSVI(pomdp, pomdpff.discount, epsilon,
						  exLXU, exNUI, pruning);
            } else {
                double slope = Double.parseDouble(options.get("slope"));
                hsvi = HSVIFactory.buildConicHSVIWithSlope(slope, pomdp, pomdpff.discount, epsilon,
							   exLXU, exNUI, pruning);
            }
        } else if(options.get("solverType").equals("xconic")) {
	    //System.out.println("Currently without pruning.");
	    if(options.get("slope").equals("computed")) {
		hsvi = HSVIFactory.buildXConicHSVI(pomdp, pomdpff.discount, epsilon,
						   exLXU, exNUI, pruning);
	    } else {
		double slope = Double.parseDouble(options.get("slope"));
		hsvi = HSVIFactory.buildXConicHSVIWithSlope(slope, pomdp, pomdpff.discount, epsilon,
							    exLXU, exNUI, pruning);
	    }
	} else if(options.get("solverType").equals("pwlc")) {
	    if (reward != null) {
		System.out.println("Only the default (linear) reward function can be used with the PWLC approximator.");
		System.exit(1);
	    }
	    hsvi = HSVIFactory.buildClassicHSVI(pomdp, pomdpff.discount, epsilon,
						exLXU, exNUI, pruning);
	} else if(options.get("solverType").equals("point")) {
	    hsvi = HSVIFactory.buildPointHSVI(pomdp, pomdpff.discount, epsilon,
					      exLXU, exNUI, pruning);
	} else {
	    System.err.println("I shouldn't be there !!!");
	    return;
	}

	if (printer != null)
	    printer.setBounds(hsvi.lowerBound,hsvi.upperBound);

	long time = System.currentTimeMillis();
	if (timelimit >0)
	    timelimit += time;
	long tCPU = getCpuTime();
	long tUSER = getUserTime();
	long tSystem = getSystemTime();
	
        // if(options.get("print").equals("true")) {
        //     hsvi.solveAndPrint(pomdpff.initialBelief, time, timelimit, iterations, "script.py", "visualization_2_.gif");
        // }
        System.out.println("L(b0)= "+hsvi.getOptimalValue(pomdpff.initialBelief, time, timelimit, iterations, printer));

	long dtCPU = getCpuTime() - tCPU;
	long dtUSER = getUserTime() - tUSER;
	long dtSystem = getSystemTime() - tSystem;
	System.err.format("wallclock\t: %.3fms%n", (double) (System.currentTimeMillis()-time));
	System.err.format("CPU\t\t: %.3fms 100.00%%%nUser\t: %.3fms %.2f%%%nSystem\t: %.3fms %.2f%%%n",
			  dtCPU/1000000.,
			  dtUSER/1000000.,
			  ((double)dtUSER)/dtCPU*100,
			  dtSystem/1000000.,
			  ((double)dtSystem)/dtCPU*100);			  
	
        if(options.containsKey("saveFile")) {
            String saveFileName = options.get("saveFile");
	    System.out.println("Saving policy file "+saveFileName);
            POMDPSimulation<Integer,Integer,Integer> simulation;
            simulation = new POMDPSimulation<>((POMDP) hsvi.getPOMDP(), hsvi.lowerBound, pomdpff.initialBelief, pomdpff.discount);
            try {
                FileOutputStream fileStream = new FileOutputStream(saveFileName);
                ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
                objectStream.writeObject(simulation);
            } catch(FileNotFoundException exception) {
                System.out.println("The file " + saveFileName + " was not found");
                System.exit(1);
            } catch(IOException exception) {
                System.out.println("IOException: " + exception);
                System.exit(1);
            }
        } else {
	    System.out.println("NO policy file to save");
	}

    }


    public static void doSolveItConic(POMDPFromFile pomdpff, Map<String,String> options) {
        double epsilon = Double.parseDouble(options.get("epsilon"));
	Boolean exLXU = Boolean.parseBoolean(options.get("exLXU"));
	Boolean exNUI = Boolean.parseBoolean(options.get("exNUI"));
	Boolean exUR = Boolean.parseBoolean(options.get("exUR"));
	Boolean pruning = !Boolean.parseBoolean(options.get("noPruning"));
	long timelimit = Long.parseLong(options.get("timeout"))*1000;
	if (timelimit >0)
	    timelimit += System.currentTimeMillis();
	int iterations = Integer.parseInt(options.get("iterations"));
	
	HSVIPythonPrinter printer = null;
    
	if (options.get("print").equals("no")) {
	    printer = null;
	} else if (pomdpff.pomdp.getStates().size() != 2) {
            System.out.println("Can't print when the number of states is different from 2.");
            System.exit(1);
	} else if (options.get("print").equals("anim")) {
	    printer = new HSVIPythonPrinterAnimated("script.py", "visualization_2_.gif", 1000, pomdpff.pomdp);
	} else if (options.get("print").equals("last")) {
	    printer = new HSVIPythonPrinterFixed("script.py", "BURP.txt", 1000, pomdpff.pomdp);
	} else {
	    System.out.println("Unknown value '"+options.get("print")+"' for --print option.");
	    helpSolve();
	}

	System.out.println("(rho) reward function/class: "+options.get("rhoClass"));
	RewardFunction reward = null;
	if (options.get("rhoClass").length() == 0) {
	    System.out.println("No specified reward. Using linear definition.");
	    reward = null;
	} else if (options.get("rhoClass").equals("GridInfoKX")) {
	    reward = new RhoGridInfoKX(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoUX")) {
	    reward = new RhoGridInfoUX(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoKY")) {
	    reward = new RhoGridInfoKY(pomdpff.pomdp.mdp);
	} else if (options.get("rhoClass").equals("GridInfoUY")) {
	    reward = new RhoGridInfoUY(pomdpff.pomdp.mdp);
	} else {
	    System.out.println("Unknown (rho) reward function/class "+options.get("rhoClass"));
	    helpSolve();
	}

	POMDP pomdp;
	if (reward == null) {
	    pomdp = pomdpff.pomdp;
	} else {
	    pomdp = new RhoPOMDP2(pomdpff.pomdp.getMDP(),
				  pomdpff.pomdp.getObservations(),
				  pomdpff.pomdp.getObservationManager(),
				  reward);
	}
	
        ILHSVI<Integer,Integer,Integer> ilhsvi = new ILHSVI<>(pomdp, pomdpff.discount, epsilon,
							      exLXU, exNUI, exUR, pruning);
	
	if (printer != null)
	    printer.setBounds(ilhsvi.getHSVI().lowerBound,ilhsvi.getHSVI().upperBound);

	long time = System.currentTimeMillis();
        ilhsvi.getOptimalValue(pomdpff.initialBelief, time, timelimit, iterations, printer);
	
        if(options.containsKey("saveFile")) {
            String saveFileName = options.get("saveFile");
	    System.out.println("Saving policy file "+saveFileName);
            POMDPSimulation<Integer,Integer,Integer> simulation;
            simulation = new POMDPSimulation<>(pomdp, ilhsvi.getHSVI().lowerBound, pomdpff.initialBelief, pomdpff.discount);
            try {
                FileOutputStream fileStream = new FileOutputStream(saveFileName);
                ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
                objectStream.writeObject(simulation);
            } catch(FileNotFoundException exception) {
                System.out.println("The file " + saveFileName + " was not found");
                System.exit(1);
            } catch(IOException exception) {
                System.out.println("IOException: " + exception);
                System.exit(1);
            }
        } else {
	    System.out.println("NO policy file to save");
	}
    }


    private static void helpSimulate() {
	System.out.println(" #################### ");
	System.out.println(" ## Lipschitz HSVI ## ");
	System.out.println(" #################### ");
	System.out.println("");
	System.out.println("* Expected arguments:");
	System.out.println("  policyFileName (saved policy+value file to evaluate)");
	System.out.println("");
	System.out.println("Additionnal arguments:");
	System.out.println(" -h/--help (this help page)");
	System.out.println(" -n/--nbSimulations=INTEGER (defaults to 0)");
	System.out.println(" -e/--epsilon=REAL_NUMBER (defaults to 0.001)");
	System.out.println(" -p/--print (if n=1: verbose mode)");
	System.out.println(" -s/--step  (if n=1: step-by-step mode)");
	System.out.println(" --seed=INTEGER (random seed; defaults to 42)");
	System.exit(1);
    }

    public static void doSimulate(String args[]) {
        String simulationFile = args[1];
        int nbSimulations = 100;
	double epsilon = 0.001;
        boolean doPrint = false;
        boolean doStep = false;
	int seed = 42;
	
        for(int i = 2; i<args.length; i++) {
            if(args[i].equals("--help") || args[i].equals("-h")) {
                helpSimulate();
            } else if(args[i].equals("--print") || args[i].equals("-p")) {
                doPrint = true;
            } else  if(args[i].equals("--step") || args[i].equals("-s")) {
                doStep = true;
            } else if(args[i].startsWith("-n=")) {
                nbSimulations = Integer.parseInt(args[i].substring(3,args[i].length()));
            } else if(args[i].startsWith("--nbSimulations=")) {
                nbSimulations = Integer.parseInt(args[i].substring(16,args[i].length()));
            } else if(args[i].startsWith("-e=")) {
                epsilon = Double.parseDouble(args[i].substring(3,args[i].length()));
            } else if(args[i].startsWith("--epsilon=")) {
                epsilon = Double.parseDouble(args[i].substring(10,args[i].length()));
            } else if(args[i].startsWith("--seed=")) {
                seed = Integer.parseInt(args[i].substring(7,args[i].length()));
            } else {
                System.out.println(args[i] + " parameter was not recognized");
		helpSimulate();
                //System.exit(1);
            }
        }

        POMDPSimulation<?,?,?> simulation = null;
        try {
            FileInputStream fileStream = new FileInputStream(simulationFile);
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            simulation = (POMDPSimulation<?,?,?>) objectStream.readObject();
        } catch(IOException exception) {
            System.out.println("Error while reading file " + simulationFile + ": " + exception);
            System.exit(1);
        } catch(ClassNotFoundException exception) {
            System.out.println("Error while reading the object from the file: " + exception);
            System.exit(1);
        }
	
	RandomGenerator.regenerateRandomGenerator(seed);
        if(nbSimulations == 1) {
            simulation.simulateOnce(epsilon,doPrint,doStep);
        } else {
            simulation.simulate(epsilon, nbSimulations);
        }
    }


    public static void printSlopes() {
        POMDPFromFile pomdpff;
        try{
            pomdpff = POMDPReader.getPOMDPFromFile("resources/pomdp/paint.95.pomdp");
        } catch(Exception e) {
            System.out.println("Error while reading the pomdp file: " + e.getLocalizedMessage());
            System.exit(1);
            return; //Written so IDEA understand that this this branch will exit the function
        }

        List<Double> slopes = new ArrayList<>();
        List<Double> values = new ArrayList<>();

        double i = 0.00001;
        while(i<5) {
            System.out.println(i);
            HSVI<Integer,Integer,Integer> hsvi = HSVIFactory.buildConicHSVIWithSlope(i, pomdpff.pomdp, pomdpff.discount, 0.001,
										     true, true, true);
            try {
		long time = System.currentTimeMillis();
                double value =  hsvi.getOptimalValue(pomdpff.initialBelief, time, 60*1000, -1, null);
                slopes.add(i);
                values.add(value);
            } catch (IllegalStateException ignored) {
            }
            i*=1.01;
        }

        System.out.println();
        System.out.println();
        System.out.println();
        int n = 0;
        for(double s : slopes) {
            n++;
            System.out.format("%f,",s);
            if(n==7) {
                System.out.println();
                n=0;
            }
        }
        n = 0;
        System.out.println();
        System.out.println();
        System.out.println();
        for(double v : values) {
            n++;
            System.out.format("%f,",v);
            if(n==7) {
                System.out.println();
                n=0;
            }
        }
    }


    private static void helpInfo() {
            System.out.println(" -- ilHSVI / info -- ");
	    System.out.println("");
	    System.out.println("No expected arguments.");
	    System.out.println("");
	    System.out.println("Additionnal arguments:");
	    System.out.println(" -h/--help (this help page)");
            System.exit(1);
    }

    public static void doGetInfo(String args[]) {
        if(args.length < 1) {
            System.out.println("There are not enough arguments.");
            System.out.println("");
	    helpSolve();
	}

        String pomdpFilePath = args[1];
	
        for(int i = 2; i<args.length; i++) {
            if(args[i].equals("--help") || args[i].equals("-h")) {
                helpInfo();
            } else {
                System.out.println(args[i] + " parameter was not recognized");
		helpInfo();
                //System.exit(1);
            }
        }

        POMDPFromFile pomdpff;
        try{
            pomdpff = POMDPReader.getPOMDPFromFile(pomdpFilePath);
        } catch(Exception e) {
            System.out.println("Error while reading the pomdp file: " + e.getLocalizedMessage());
            System.exit(1);
            return; //Written so IDEA understand that this this branch will exit the function
        }
	
	System.out.format("|S|= %d |A|= %d |O|= %d g= %.3f\n",
			  pomdpff.pomdp.getStates().size(),
			  pomdpff.pomdp.getActions().size(),
			  pomdpff.pomdp.getObservations().size(),
			  pomdpff.discount);
	
    }

    
}
