package mdp.pomdp;

import util.random.Distribution;
import java.util.Collection;


/**
 * <pre>
 * Abstract class containing the observations.
 * This example show how to get and set probabilities, and how to get the possible observations.
 *
 * <code> {@code
 * ObservationSet<State,Action,Observation> os = ...;
 * //Create a new ObservationSet, with a child class
 *
 * os.setDistribution(action,null,observationDistribution);
 * // Set distributions of probability for observation, for transitions
 * // with action action, for every possible state
 *
 * os.setProbability(action,endState,observation,probability);
 * // Set a probability for an observation
 *
 * ... // We fill the object with probabilities
 *
 * os.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = os.getProbability(action,endState,observation);
 * // Getting the probability of observation, after being in endState because of action.
 *
 * Collection<Observation> possibleObservations = os.getPossibleObservations(action,endStates);
 * // Getting the possible observations, being in endStates because of action
 *
 * Observation newObservation = os.sampleObservation(action,endStates);
 * // Sample an observation following the distribution
 * }</code></pre>
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
public abstract class ObservationSet<State,Action,Observation> {

    /**
     * Set a probability for an observation
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * If observation is null, the probability is set for all observations.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation on the new state
     * @param probability   The new probability of this observation
     */
    public abstract void setProbability(Action action, State endState, Observation observation, double probability);


    /**
     * Set a distribution of probability for observations
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * distribution cannot be null.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param distribution  The distribution of probabilities for observations
     */
    public abstract void setDistribution(Action action, State endState, Distribution<Observation> distribution);


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the sampleNextState functions
     */
    public abstract void initGetters();


    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     *
     * @return  The probability of the observation knowing (action,endState)
     */
    public abstract double getProbability(Action action, State endState, Observation observation);


    /**
     * Get the possible observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     *
     * @return The collection of possible next observations
     */
    public abstract Collection<Observation> getPossibleObservations(Action action, State endState);


    /**
     * Sample an observation
     *
     * @param action    The action of the transition
     * @param endState  The ending state of the transition
     *
     * @return An observation following the observation distribution, knowing (action, endState)
     */
    public abstract Observation sampleObservation(Action action, State endState);
}
