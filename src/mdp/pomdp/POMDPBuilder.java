package mdp.pomdp;


import mdp.mdp.MDPBuilder;
import util.random.Distribution;

/**
 * <pre>
 * Build a POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a POMDP
 *
 * <code> {@code
 * POMDPBuilder<State,Action,Observation> builder = ...;
 * // Instance the object with a child class
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * builder.addReward(startState,action,endState,reward);
 * ...
 * builder.setObservationProbability(action,endState,observation,probability);
 * ...
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions, observations and rewards
 *
 * POMDP<State,Action,Observation> pomdp = builder.build();
 * // Build the final pomdp
 * }</code></pre>
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
abstract public class POMDPBuilder<State,Action,Observation> {

    /**
     * The class managing the build of the underlying mdp
     */
    protected MDPBuilder<State,Action> mdpBuilder;


    /**
     * The class managing the observation distribution function
     */
    protected ObservationSet<State,Action,Observation> observationFunction;


    /**
     * Create a POMDP builder with a builder for the underlying mdp,
     * and a new observation distribution function
     *
     * @param mdpBuilder            The transition managing class
     * @param observationFunction   The reward managing class
     */
    POMDPBuilder(MDPBuilder<State,Action> mdpBuilder, ObservationSet<State,Action,Observation> observationFunction) {
        this.mdpBuilder = mdpBuilder;
        this.observationFunction = observationFunction;
    }


    /**
     * Build a POMDP
     *
     * @return The POMDP which has the parameters entered in the builder
     */
    abstract public POMDP<State,Action,Observation> build();


    /**
     * Setup the transitions and rewards manager
     */
    protected void setupManagers() {
        observationFunction.initGetters();
    }


    /**
     * Set the probability for a transition.
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The ending state of the transition
     * @param probability   Its probability
     */
    public void setTransitionProbability(State startState, Action action, State endState, double probability) {
        mdpBuilder.setTransitionProbability(startState, action, endState, probability);
    }


    /**
     * Set the distribution of the end states
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting states
     * @param action        The action
     * @param endStates     The distribution over the end states
     */
    public void setTransitionDistribution(State startState, Action action, Distribution<State> endStates) {
        mdpBuilder.setTransitionDistribution(startState, action, endStates);
    }


    /**
     * Set a reward for a transition
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endState      The end state
     * @param reward        The reward to add
     */
    public void setReward(State startState, Action action, State endState, double reward) {
        mdpBuilder.setReward(startState, action, endState, reward);
    }


    /**
     * Set a probability for an observation
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * If observation is null, the probability is set for all observations.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation on the new state
     * @param probability   The new probability of this observation
     */
    public void setObservationProbability(Action action, State endState, Observation observation, double probability) {
        observationFunction.setProbability(action, endState, observation, probability);
    }


    /**
     * Set a distribution of probability for observations
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * distribution cannot be null.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param distribution  The distribution of probabilities for observations
     */
    public void setObservationDistribution(Action action, State endState, Distribution<Observation> distribution) {
        observationFunction.setDistribution(action, endState, distribution);
    }
}
