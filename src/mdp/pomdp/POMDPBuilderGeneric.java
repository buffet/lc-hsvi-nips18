package mdp.pomdp;


import mdp.mdp.MDP;
import mdp.mdp.MDPBuilderGeneric;

import java.util.Collection;


/**
 * <pre>
 * Build a POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a POMDP
 *
 * <code> {@code
 * POMDPBuilder<State,Action,Observation> builder = new POMDPBuilderGeneric<>(states,actions,observation);
 * // Instance the object knowing the different states, actions, and observations
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * builder.addReward(startState,action,endState,reward);
 * ...
 * builder.setObservationProbability(action,endState,observation,probability);
 * ...
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions, observations and rewards
 *
 * POMDP<State,Action,Observation> pomdp = builder.build();
 * // Build the final pomdp
 * }</code></pre>
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
public class POMDPBuilderGeneric<State,Action,Observation> extends POMDPBuilder<State,Action,Observation> {

    /**
     * The different observations
     */
    public final Collection<Observation> observations;


    /**
     * Create a POMDP builder with a builder for the underlying mdp,
     * and a new observation distribution function
     *
     * @param states        The different states
     * @param actions       The different actions
     * @param observations  The different observations
     */
    public POMDPBuilderGeneric(Collection<State> states, Collection<Action> actions, Collection<Observation> observations) {
        super(new MDPBuilderGeneric<>(states,actions), new ObservationSetGeneric<>(states,actions,observations));
        this.observations = observations;
    }


    /**
     * Build a MDP
     *
     * @return The MDP which has the parameters entered in the builder
     */
    @Override
    public POMDP<State, Action, Observation> build() {
        setupManagers();
        MDP<State,Action> mdp = mdpBuilder.build();
        return new POMDP<>(mdp, observations, observationFunction);
    }
}
