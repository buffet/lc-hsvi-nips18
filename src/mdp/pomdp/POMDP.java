package mdp.pomdp;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.MDP;
import mdp.simulation.SimulablePOMDP;
import mdp.solver.CornerPOMDPApproximation;
import mdp.solver.SolvableByLHSVI;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;



/**
 * <pre>
 * Contains the POMDP dynamics (transitions, rewards, and observations)
 * To see how to build the POMDP, see POMDPBuilder class.
 *
 * The POMDP can be simulated (see POMDPSimulation), and solved with HSVI and LHSVI (see HSVI)
 *
 * The following example shows the important functions of a POMDP
 *
 * <code> {@code
 * POMDP<State,Action,Observation> pomdp = pomdpBuilder.build();
 * // See POMDPBuilder for more information
 *
 * double reward = pomdp.getReward(startState,action,endState);
 * double expectedReward = pomdp.getReward(startState,action);
 * ...
 * // The POMDP implements the MDP function, so see MDP for more information
 *
 * double probability = pomdp.getObservationProbability(action,endState,observation);
 * Observation observation = pomdp.sampleObservation(action,endState);
 * Collection<Observation> observations = pomdp.getPossibleObservations(action,endState);
 * // Get the probability of an observation, sample an observation, and get the possible next observations
 *
 * Belief<State> newBelief = pomdp.getNewBelief(oldBelief,action,observation);
 * // Get the new belief, knowing the old one, the last action made, and the current observation made
 *
 * }</code>
 * </pre>
 * @param <State>   The class used to represent the states
 * @param <Action>  The class used to represent the actions
 */
public class POMDP<State,Action,Observation> implements SolvableByLHSVI<State,Action,Observation>,
                                                        SimulablePOMDP<State,Action,Observation>,
                                                        Serializable {

    /**
     * The underlying mdp
     */
    public final MDP<State,Action> mdp;

    /**
     * Lipschitz Constant
     */
    private Map<Action,MatrixX> lipschitzConstants;

    /**
     * The collection of observations
     */
    public final Collection<Observation> observations;


    /**
     * The observation distribution function
     */
    private ObservationSet<State,Action,Observation> observationManager;


    /**
     * Initialize the POMDP with an underlying MDP, and an observation manager.
     * Ths observation manager must have been initialize, as well as the mdp
     *
     * @param mdp                   The underlying mdp
     * @param observations          The different observations
     * @param observationManager    The observation distribution function
     */
    public POMDP(MDP<State,Action> mdp, Collection<Observation> observations,
          ObservationSet<State,Action,Observation> observationManager) {
        this.mdp = mdp;
	this.lipschitzConstants = new HashMap<>();
        this.observations = Collections.unmodifiableCollection(observations);
        this.observationManager = observationManager;
    }


    /**
     * Get the MDP
     *
     * @return The MDP
     */
    public MDP<State,Action> getMDP() {
        return mdp;
    }

    /**
     * Get the different states
     *
     * @return The collection of the different states
     */
    @Override
    public Collection<State> getStates() {
	return mdp.states;
    }


    /**
     * Get the different states
     *
     * @return The collection of the different states
     */
    @Override
    public Collection<Action> getActions() {
        return mdp.actions;
    }


    /**
     * Get the different observations
     *
     * @return The collection of the different observations
     */
    @Override
    public Collection<Observation> getObservations() {
        return observations;
    }


    /**
     * Get the observation manager
     *
     * @return The observation manager
     */
    public ObservationSet<State,Action,Observation> getObservationManager() {
        return observationManager;
    }



    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     *
     * @return The probability of the transition
     */
    @Override
    public double getTransitionProbability(State startState, Action action, State endState) {
        return mdp.getTransitionProbability(startState, action, endState);
    }


    /**
     * Get the collection of next possible states, given a couple (startState,action)
     *
     * @param startState    The starting state
     * @param action        The last action made
     *
     * @return The collection of next possible states
     */
    @Override
    public Collection<State> getPossibleNextStates(State startState, Action action) {
        return mdp.getPossibleNextStates(startState, action);
    }


    /**
     * Get the possible next observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     *
     * @return The collection of possible next observations
     */
    @Override
    public Collection<Observation> getPossibleObservations(Action action, State endState) {
        return observationManager.getPossibleObservations(action, endState);
    }


    /**
     * Get the reward given for a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     *
     * @return The reward for this transition
     */
    public double getReward(State startState, Action action, State endState) {
        return mdp.getReward(startState, action, endState);
    }


    /**
     * Get the reward on the last transition.
     *
     * @param startState The starting state of the transition
     * @param action     The last action made
     * @param endState   The end state of the transition (the current state)
     * @param belief     The current belief
     *
     * @return The reward given for that transition
     */
    @Override
    public double getReward(State startState, Action action, State endState, Belief<State> belief) {
	//System.out.println("POMDP.getReward");
        return getReward(startState,action,endState);
    }

    /**
     * Get the expected reward
     *
     * @param startState The starting state
     * @param action     The action made on that state
     */
    public double getExpectedReward(State startState, Action action) {
        return mdp.getExpectedReward(startState, action);
    }

    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    @Override
    public double getExpectedReward(Belief<State> belief, Action action) {
	// System.out.println("===============");
	// System.out.println("POMDP.getExpectedReward(Belief<State> belief, Action action)");
	// System.out.println("");
	// for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
	//     System.out.println(ste);
	// }
	// System.out.println("===============");
	// System.exit(1);

        double value = 0;
        for(State startState : belief.getNonZero()) {
            value += getExpectedReward(startState,action) * belief.getProbability(startState);
        }
        return value;
    }

    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    @Override
    public MatrixX getRewardLipschitzConstant(Belief<State> belief, Action action, ObjectIntegerConverter converter) {
	//System.out.println("POMDP.getRewardLipschitzConstant()");
	//System.exit(1);
	
	MatrixX l = lipschitzConstants.get(action);
	if (l==null) {
	    /* Infinitely many (vector) constants are valid.
	     * Let's pick one minimizing the infinite norm.
	     */
	    l = new MatrixX(1, getStates().size());
	    double[][] A = l.getArray();

	    double rmin= Double.POSITIVE_INFINITY;
	    double rmax= Double.NEGATIVE_INFINITY;
	    for(State state : getStates()) {
		double r = getExpectedReward(state, action);
		rmax = max(r,rmax);
		rmin = min(r,rmin);
	    }
	    double k = (rmax+rmin)/2;
	    System.out.println("Correction for Reward Lipschitz constant of action "+action+" : k="+k);
	    
	    for(State state : getStates()) {
		A[0][converter.toInteger(state)] = Math.abs( getExpectedReward(state, action) - k );
	    }

	    lipschitzConstants.put(action,l);
	}
	
	return l;

	/*	
	MatrixX l = new MatrixX(1, getStates().size());
	double[][] A = l.getArray();

	for(State state : getStates()) {
	    A[0][converter.toInteger(state)] = Math.abs( getExpectedReward(state, action) );
	}

	return l;
	*/
    }


    /**
     * Get the maximum "absolute" reward possible.
     *
     * @return The maximum "absolute" possible reward
     */
    @Override
    public double getMaxAbsReward() {
        double maxi = Double.NEGATIVE_INFINITY;
        for(State startState : getStates()) {
            for(Action action : getActions()) {
                for(State endState : getPossibleNextStates(startState,action)) {
                    maxi = max(maxi,abs(mdp.getReward(startState,action,endState)));
                }
            }
        }
        return maxi;
    }

    /**
     * Get the maximum reward possible.
     *
     * @return The maximum possible reward
     */
    //@Override
    public double getMaxExpectedReward() {
        double maxi = Double.NEGATIVE_INFINITY;
        for(State startState : getStates()) {
            for(Action action : getActions()) {
		double r = mdp.getExpectedReward(startState,action);
		maxi = max(maxi,r);
	    }
        }
        return maxi;
    }

    /**
     * Get the minimum expected reward possible.
     *
     * @return The minimum possible expected reward
     */
    //@Override
    public double getMinExpectedReward() {
        double mini = Double.POSITIVE_INFINITY;
        for(State startState : getStates()) {
            for(Action action : getActions()) {
		double r = mdp.getExpectedReward(startState,action);
		mini = min(mini,r);
	    }
        }
        return mini;
    }

    /**
     * Get the "symetric" reward bound of the problem.
     *
     * @return The "symetric" reward bound of the problem.
     */
    //@Override
    public double getSymExpectedReward() {
        double maxi = Double.NEGATIVE_INFINITY;
        double mini = Double.POSITIVE_INFINITY;
        for(State startState : getStates()) {
            for(Action action : getActions()) {
		double r = mdp.getExpectedReward(startState,action);
		maxi = max(maxi,r);
		mini = min(mini,r);
            }
        }
        return (maxi-mini)/2.;
    }


    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     * @return The probability of the observation knowing (action,endState)
     */
    @Override
    public double getObservationProbability(Action action, State endState, Observation observation) {
        return observationManager.getProbability(action, endState, observation);
    }


    /**
     * Get the conditional probability P(o | b,a) of having an observation, given a belief and an action
     *
     * @param belief       The current belief
     * @param action       The new action
     * @param observation  The observation we want the probability of
     * @return  The probability P(o | b,a)
     */
    @Override
    public double getConditionalProbabilityOfObservation(Belief<State> belief, Action action, Observation observation) {
        double probability = 0.0;
        for(State startState : belief.getNonZero()) {
            if(belief.getProbability(startState) == 0.0) {
                continue;
            }
            double temp = 0.0;
            for(State endState : getPossibleNextStates(startState,action)) {
                temp += getTransitionProbability(startState,action,endState) * getObservationProbability(action,endState,observation);
            }
            probability += temp*belief.getProbability(startState);
        }

        return probability;
    }


    /**
     * Get the new belief, after doing an action and getting an observation
     *
     * @param belief      The old belief
     * @param action      The last action made
     * @param observation The observation resulting of that action
     * @return The new belief
     */
    @Override
    public Belief<State> getNewBelief(Belief<State> belief, Action action, Observation observation) {
        Belief<State> newBelief = new Belief<>();
        for(State endState : getStates()) {
            newBelief.setWeight(endState, getNewBeliefNumerator(belief,action,endState,observation));
        }
        //We do not need to normalize since the belief do it internally
        return newBelief;
    }


    /**
     * Return the new belief on a state before the normalization
     *
     * @param oldBelief     The old belief
     * @param action        The last action made
     * @param endState      The state resulting of the action
     * @param observation   The observation resulting of the last transition
     * @return   The belief on a state before the normalization
     */
    private double getNewBeliefNumerator(Belief<State> oldBelief, Action action, State endState, Observation observation) {
        if(getObservationProbability(action,endState,observation) == 0.0) {
            return 0.0;
        }
        double numerator = 0.0;
        for(State startState : oldBelief.getNonZero()) {
            numerator += getTransitionProbability(startState,action,endState) * oldBelief.getProbability(startState);
        }
        numerator *= getObservationProbability(action,endState,observation);
        return numerator;
    }

    /**
     * Get the slope of the reward function on a belief, on a certain dimension
     *
     * @param belief    The point where to get the tangent
     * @param action    The action associated with the reward (we want the tangent of r(a))
     * @param state     The dimension for which we want the value
     * @return  The tangent
     */
    @Override
    public double getRewardTangent(Belief<State> belief, Action action, State state) {
        //In a POMDP, the reward do not depend on the belief
        return getExpectedReward(state,action);
    }


    /**
     * Get a lower bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionLowerBound(double discount, double errorMargin) {
        //return CornerPOMDPApproximation.getLowerWithWorstRewardBestAction(this,discount,errorMargin);
        return CornerPOMDPApproximation.getLowerWithBestAction(this,discount,errorMargin);
    }

    /**
     * Get a upper bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionUpperBound(double discount, double errorMargin) {
        //return CornerPOMDPApproximation.getUpperWithMDPSolving(this,discount,errorMargin);
        return CornerPOMDPApproximation.getUpperWithFastInformedBound(this,discount,errorMargin);
    }

    /**
     * Sample a next state in a transition
     *
     * @param startState    The current state
     * @param action        The action made
     * @return A new state sampled according to the transition distribution
     */
    @Override
    public State sampleNextState(State startState, Action action) {
        return mdp.sampleNextState(startState, action);
    }


    /**
     * Sample an observation
     *
     * @param action    The action of the transition
     * @param endState  The ending state of the transition
     * @return An observation following the observation distribution, knowing (action, endState)
     */
    @Override
    public Observation sampleObservation(Action action, State endState) {
        return observationManager.sampleObservation(action, endState);
    }


    /**
     * Return the \lambda for 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of the lambda
     */
    // private double getLambda() {
    //     return 1.0;
    // }


    /**
     * Return the \mu{a,o} for the 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @param action      The action
     * @param observation The observation
     * @return The value of the MuAO
     */
    // private double getMuAO(Action action, Observation observation) {
    //     double maxi = Double.NEGATIVE_INFINITY;
    //     for(State beginState : getStates()) {
    //         double value = 0;
    //         for(State endState : getPossibleNextStates(beginState,action)) {
    //             double temp = getTransitionProbability(beginState,action,endState);
    //             temp *= getObservationProbability(action,endState,observation);
    //             value += temp;
    //         }
    //         maxi = max(maxi,value);
    //     }
    //     return maxi;
    // }


    /**
     * Return the \mu for the 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of the mu
     */
    // private double getMu() {
    //     double maxi = Double.NEGATIVE_INFINITY;
    //     for(Action action : getActions()) {
    //         for(Observation observation : getObservations()) {
    //             maxi = max(maxi, getMuAO(action,observation));
    //         }
    //     }
    //     return maxi;
    // }


    /**
     * Return the \rho as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     * (Lipschitz constant of the reward function)
     *
     * @return The value of the rho
     */
    // private double getRho() {
    // 	// [olivier] Cette fonction m'a l'air incorrecte. Il faut tenir compte de R^{max} *et* R^{min}...
    //     double maxi = Double.NEGATIVE_INFINITY;
    //     for(Action action : getActions()) {
    //         for (State state1 : getStates()) {
    //             maxi = max(maxi,abs(getExpectedReward(state1,action)));
    //         }
    //     }
    //     return maxi;
    // }

    /**
     * Return the Lipschitz constant of the reward function
     * (Lipschitz constant of the reward function)
     *
     * @return The value of the Lipschitz constant of the reward function
     */
    private double getLipschitzR() {
        double lmaxi = Double.NEGATIVE_INFINITY;
        for(Action action : getActions()) {
	    double rmini = Double.POSITIVE_INFINITY;
	    double rmaxi = Double.NEGATIVE_INFINITY;
            for (State state : getStates()) {
		double r = mdp.getExpectedReward(state,action);
                rmaxi = max(rmaxi,r);
                rmini = min(rmini,r);
            }
	    lmaxi = max( lmaxi, (rmaxi-rmini)/2. );
        }
        return lmaxi;
    }


    /**
     * Return the value of VLim as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of VLim
     */
    private double getVLim(double discount, double errorMargin) {
        Map<State,Double> lowerCorner = getValueFunctionLowerBound(discount, errorMargin);
        Map<State,Double> upperCorner = getValueFunctionUpperBound(discount, errorMargin);

        double mini = Double.POSITIVE_INFINITY;
        double maxi = Double.NEGATIVE_INFINITY;
        for(Map.Entry<State,Double> entry : lowerCorner.entrySet()) {
            mini = min(mini,entry.getValue());
        }
        for(Map.Entry<State,Double> entry : upperCorner.entrySet()) {
            maxi = max(maxi,entry.getValue());
        }
        return max(abs(maxi),abs(mini)) / (1 - discount);
    }

    /**
     * Return the slope of the cones
     *
     * @return The slope of the cones
     */
    private double getConeSlope(double discount, double errorMargin) {
	if (discount >= 0.5)
	    return Double.POSITIVE_INFINITY;

        double vLim = getVLim(discount, errorMargin);
	
	return (getLipschitzR() + discount * vLim ) / (1 - 2*discount);

	/**
	 * vieille version invalide
	 */
        //double vLim = getVLim(discount, errorMargin);
        //double lambda = getLambda();
        //double mu = getMu();
        //double rho = getRho();

        // if(discount*lambda >= 1.0) {
        //     return Double.POSITIVE_INFINITY;
        // }
        //return (rho + discount * vLim * mu * getObservations().size()) / (1 - discount*lambda);
        //return rho  / (1 - discount);
    }


    /**
     * Get the lipschitz constant of the value function of the decision process
     *
     * @return The lipschitz constant
     */
    @Override
    public double getValueFunctionLipschitzConstant(double discount, double errorMargin) {
	double slope = getConeSlope(discount, errorMargin);
	System.out.println("slope="+slope);
	return slope;
    }
}
