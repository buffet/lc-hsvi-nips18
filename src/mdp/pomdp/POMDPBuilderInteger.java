package mdp.pomdp;


import mdp.mdp.MDP;
import mdp.mdp.MDPBuilderInteger;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * Build a POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a POMDP
 *
 * <code> {@code
 * POMDPBuilder<State,Action,Observation> builder = new POMDPInteger(nbStates, nbActions, nbObservations);
 * // Instance the object knowing the number of states, actions, and observations
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * builder.addReward(startState,action,endState,reward);
 * ...
 * builder.setObservationProbability(action,endState,observation,probability);
 * ...
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions, observations and rewards
 *
 * POMDP<State,Action,Observation> pomdp = builder.build();
 * // Build the final pomdp
 * }</code></pre>
 */
public class POMDPBuilderInteger extends POMDPBuilder<Integer,Integer,Integer> {


    /**
     * The number of observations
     */
    public final int nbObservations;


    /**
     * Create a POMDP builder for integer classes with a given number of states and actions
     *
     * @param nbStates          The number of states
     * @param nbActions         The number of actions
     * @param nbObservations    The number of observations
     */
    public POMDPBuilderInteger(int nbStates, int nbActions, int nbObservations) {
        super(new MDPBuilderInteger(nbStates,nbActions), new ObservationSetInteger(nbStates,nbActions,nbObservations));
        this.nbObservations = nbObservations;
    }


    /**
     * Build a POMDP
     *
     * @return The POMDP which has the parameters entered in the builder
     */
    @Override
    public POMDP<Integer, Integer, Integer> build() {
        setupManagers();
        MDP<Integer,Integer> mdp = mdpBuilder.build();

        List<Integer> observations = new ArrayList<>();
        for(int i = 0; i<nbObservations; i++) {
            observations.add(i);
        }

        return new POMDP<>(mdp, observations,observationFunction);
    }
}
