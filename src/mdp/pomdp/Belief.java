package mdp.pomdp;

import util.random.Distribution;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;
import mdp.solver.SolvableByHSVI;

import java.io.Serializable;
import java.util.Set;
import java.util.Collection;

/**
 * <pre>
 * Represent a pomdp belief
 * The following example show how to use it :
 *
 * <code> {@code
 * Belief<State> belief = new Belief();
 * belief.addWeight(element1,weight1);
 * belief.addWeight(element2,weight2);
 * // Create a belief and fill it with probabilities
 *
 * double prob = belief.getProbability(element);
 * // prob is now equal to the weight of the element, divided by the sum of all weights
 * // If the weights are probabilities, then prob is equal to the weight of the element
 *
 * Set<State> elements = belief.getNonZero();
 * // Contains the elements with non null probabilities
 *
 * State state = belief.sample();
 * // Sample a state following the belief probabilities
 * }</code></pre>
 *
 * @param <State> The states of the belief
 */
public class Belief<State> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The belief is represented internally by a distribution
     */
    private Distribution<State> distribution;

    /**
     * Create an empty belief
     */
    public Belief() {
        distribution = new Distribution<>();
    }

    /**
     * Transform a MatrixX into a Belief
     *
     * @param vector  The MatrixX
     * @return        The Belief associated
     */
    /*
    public Belief(MatrixX vector, ObjectIntegerConverter<State> converter) {
        for(State state : problem.getStates()) {
            this.setWeight(state,vector.get(converter.toInteger(state),0));
        }
    }
    */

    /**
     * Get the probability of a state
     *
     * @param state The state
     * @return The probability of presence of that state
     */
    public double getProbability(State state) {
        return distribution.getProbability(state);
    }

    /**
     * Set the probability of a state
     *
     * @param state        The state
     * @param probability  Its new probability
     */
    public void setWeight(State state, double probability) {
        distribution.setWeight(state,probability);
    }

    /**
     * Get elements with non zero probabilities
     *
     * @return The elements
     */
    public Set<State> getNonZero() {
       return distribution.getNonZeroElements();
    }

    public double getSumOfWeights() {
	return distribution.getSumOfWeights();
    }
    
    /**
     * Sample an element from the belief
     *
     * @return The sampled element
     */
    public State sample() {
        return distribution.sample();
    }

    public String toString() {
	return distribution.toString();
    }


    /**
     * Transform a belief into a MatrixX
     *
     * @param belief  The belief
     * @return        The associated MatrixX (vector)
     */
    public MatrixX toMatrixX(Collection<State> states, ObjectIntegerConverter<State> converter) {
	
        MatrixX vector = new MatrixX(states.size(),1);
	double[][] A = vector.getArray();
        for(State state : states) {
            A[converter.toInteger(state)][0] = this.getProbability(state);
        }
        return vector;
    }
        
}
