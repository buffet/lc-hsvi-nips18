package mdp.pomdp;

import util.container.Pair;
import util.random.Distribution;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * <pre>
 * Abstract class containing the observations.
 * This example show how to get and set probabilities, and how to get the possible observations.
 *
 * <code> {@code
 * ObservationSet<State,Action,Observation> os = new ObservationSetGeneric<>(states,actions,observations);
 * //Create a new ObservationSet, knowing the different states,actions and observations.
 *
 * os.setDistribution(action,null,distributions);
 * // Set distributions of probability for observation, for transitions
 * // with action action, for every possible state
 *
 * os.setProbability(action,endState,observation,probability);
 * // Set a probability for an observation
 *
 * ... // We fill the object with probabilities
 *
 * os.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = os.getProbability(action,endState,observation);
 * // Getting the probability of observation, after being in endState because of action.
 *
 * Collection<Observation> possibleObservations = os.getPossibleObservations(action,endStates);
 * // Getting the possible observations, being in endStates because of action
 *
 * Observation newObservation = os.sampleObservation(action,endStates);
 * // Sample an observation following the distribution
 * }</code></pre>
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
public class ObservationSetGeneric<State,Action,Observation> extends ObservationSet<State,Action,Observation>
                                                             implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The distributions over observations.
     */
    private Map<Pair<Action,State>,Distribution<Observation>> distributions;

    /**
     * The states collection
     */
    private Collection<State> states;

    /**
     * The actions collection
     */
    private Collection<Action> actions;

    /**
     * The observations collection
     */
    private Collection<Observation> observations;


    /**
     * Create a new observation manager for MDP
     *
     * @param states        The different states of the MDP
     * @param actions       The different actions of the MDP
     * @param observations  The different observations of the MDP
     */
    public ObservationSetGeneric(Collection<State> states, Collection<Action> actions, Collection<Observation> observations) {
        this.states = states;
        this.actions = actions;
        this.observations = observations;
        distributions = new HashMap<>();
    }


    /**
     * Set a probability for an observation
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * If observation is null, the probability is set for all observations.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation on the new state
     * @param probability   The new probability of this observation
     */
    @Override
    public void setProbability(Action action, State endState, Observation observation, double probability) {
        if(action == null) {
            for(Action newAction : actions) {
                setProbability(newAction, endState, observation, probability);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setProbability(action, newEndState, observation, probability);
            }
        } else if(observation == null) {
            for(Observation newObservation : observations) {
                setProbability(action, endState, newObservation, probability);
            }
        } else {
            getOrCreateDistribution(action,endState).setWeight(observation,probability);
        }
    }


    /**
     * Set a distribution of probability for observations
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * distribution cannot be null.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param distribution  The distribution of probabilities for observations
     */
    @Override
    public void setDistribution(Action action, State endState, Distribution<Observation> distribution) {
        if(action == null) {
            for(Action newAction : actions) {
                setDistribution(newAction, endState, distribution);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setDistribution(action, newEndState, distribution);
            }
        } else {
            distributions.put(new Pair<>(action,endState), distribution);
        }
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the sampleNextState functions
     */
    @Override
    public void initGetters() {
        //This ensure that there is always a possible observation
        for(Action action : actions) {
            for(State endState : states) {
                Distribution<Observation> distribution = getOrCreateDistribution(action,endState);
                if(distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no observation possible for the couple (" + action + "," + endState + ").");
                }
            }
        }
    }


    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     *
     * @return  The probability of the observation knowing (action,endState)
     */
    @Override
    public double getProbability(Action action, State endState, Observation observation) {
        return distributions.get(new Pair<>(action,endState)).getProbability(observation);
    }


    /**
     * Get the possible observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     *
     * @return The collection of possible next observations
     */
    @Override
    public Collection<Observation> getPossibleObservations(Action action, State endState) {
        return distributions.get(new Pair<>(action,endState)).getNonZeroElements();
    }


    /**
     * Sample an observation
     *
     * @param action    The action of the transition
     * @param endState  The ending state of the transition
     *
     * @return An observation following the observation distribution, knowing (action, endState)
     */
    @Override
    public Observation sampleObservation(Action action, State endState) {
        return distributions.get(new Pair<>(action,endState)).sample();
    }


    /**
     * Get the distribution over the possible observations.
     * If the distribution isn't created yet, create it and setWeight it to the map.
     *
     * @param action   The action of the transition
     * @param endState The end state of the transition
     *
     * @return The distribution over the observations knowing (action,endState)
     */
    private Distribution<Observation> getOrCreateDistribution(Action action, State endState) {
        return distributions.computeIfAbsent(new Pair<>(action, endState), k -> new Distribution<>());
    }
}
