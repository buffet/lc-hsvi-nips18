package mdp.pomdp;


import util.random.Distribution;

import java.io.Serializable;
import java.util.Collection;


/**
 * <pre>
 * Abstract class containing the probabilities.
 * This example show how to get and set probabilities, and how to get the possible probabilities.
 *
 * <code> {@code
 * ObservationSet<Integer,Integer,Integer> os = new ObservationSetInteger(nbStates,nbActions,nbObservations);
 * //Create a new ObservationSet, knowing the different number of states,actions and probabilities.
 *
 * os.setDistribution(action,null,distributions);
 * // Set distributions of probability for observation, for transitions
 * // with action action, for every possible state
 *
 * os.setProbability(action,endState,observation,probability);
 * // Set a probability for an observation
 *
 * ... // We fill the object with probabilities
 *
 * os.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = os.getProbability(action,endState,observation);
 * // Getting the probability of observation, after being in endState because of action.
 *
 * Collection<Integer> possibleObservations = os.getPossibleObservations(action,endStates);
 * // Getting the possible probabilities, being in endStates because of action
 *
 * int newObservation = os.sampleObservation(action,endStates);
 * // Sample an observation following the distribution
 * }</code></pre>
 */
public class ObservationSetInteger extends ObservationSet<Integer,Integer,Integer> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The probabilities of observation
     * probabilities[a][s'][o] gives p(o|a,s')
     */
    private double[][][] probabilities;

    /**
     * The collection of possible distributions for a given couple (action,endState)
     */
    private Distribution<Integer>[][] distributions;

    /**
     * The number of states
     */
    private int nbStates;

    /**
     * The number of actions
     */
    private int nbActions;

    /**
     * the number of probabilities
     */
    private int nbObservations;


    /**
     * Create a basic observation manager
     *
     * @param nbStates          The number of states of the POMDP
     * @param nbActions         The number of actions of the POMDP
     * @param nbObservations    The number of probabilities of the POMDP
     */
    @SuppressWarnings("unchecked")
    public ObservationSetInteger(int nbStates, int nbActions, int nbObservations) {
        this.nbStates = nbStates;
        this.nbActions = nbActions;
        this.nbObservations = nbObservations;
        distributions = new Distribution[nbActions][nbStates];
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the sampleNextState functions
     */
    @Override
    public void initGetters() {
        // Initialize the principal getter
        probabilities = new double[nbActions][nbStates][nbObservations];

        // Ensure the existence of an observation for every transition
        // TODO, use TransitionSet to check if the transition is possible.
        for(int action = 0; action < nbActions; action++) {
            for(int endState = 0; endState < nbStates; endState++) {
                Distribution<Integer> distribution = distributions[action][endState];
                if(distribution == null || distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no observation possible for the couple (" + action + "," + endState + ").");
                }
                for(int observation = 0; observation < nbObservations; observation++) {
                    probabilities[action][endState][observation] = distribution.getProbability(observation);
                }
            }
        }
    }


    /**
     * Set a probability for an observation
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * If observation is null, the probability is set for all observations.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation on the new state
     * @param probability   The new probability of this observation
     */
    @Override
    public void setProbability(Integer action, Integer endState, Integer observation, double probability) {
        if(action == null) {
            for(int newAction = 0; newAction < nbActions; newAction++) {
                setProbability(newAction,endState,observation,probability);
            }
        } else if(endState == null) {
            for(int newEndState = 0; newEndState < nbStates; newEndState++) {
                setProbability(action,newEndState,observation,probability);
            }
        } else if(observation == null) {
            for(int newObservation = 0; newObservation < nbObservations; newObservation++) {
                setProbability(action,endState,newObservation,probability);
            }
        } else {
            if(distributions[action][endState] == null) {
                distributions[action][endState] = new Distribution<>();
            }
            distributions[action][endState].setWeight(observation,probability);
        }
    }


    /**
     * Set a distribution of probability for observations
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * distribution cannot be null.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param distribution  The distribution of probabilities for observations
     */
    @Override
    public void setDistribution(Integer action, Integer endState, Distribution<Integer> distribution) {
        if(action == null) {
            for(int newAction = 0; newAction < nbActions; newAction++) {
                setDistribution(newAction,endState,distribution);
            }
        } else if(endState == null) {
            for(int newEndState = 0; newEndState < nbStates; newEndState++) {
                setDistribution(action,newEndState,distribution);
            }
        } else {
            distributions[action][endState] = distribution;
        }
    }


    /**
     * Get the possible observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     *
     * @return The collection of possible next observations
     */
    @Override
    public Collection<Integer> getPossibleObservations(Integer action, Integer endState) {
        return distributions[action][endState].getNonZeroElements();
    }



    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     *
     * @return  The probability of the observation knowing (action,endState)
     */
    @Override
    public double getProbability(Integer action, Integer endState, Integer observation) {
        return probabilities[action][endState][observation];
    }


    /**
     * Sample an observation
     *
     * @param action    The action of the transition
     * @param endState  The ending state of the transition
     *
     * @return An observation following the observation distribution, knowing (action, endState)
     */
    @Override
    public Integer sampleObservation(Integer action, Integer endState) {
        return distributions[action][endState].sample();
    }
}
