package mdp.mdp;

import util.container.Pair;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * <pre>
 * Abstract class containing the rewards for a MDP
 * This example show how to set rewards for transitions, and how to get them
 *
 * <code> {@code
 * RewardSet<State,Action> rs = new RewardSetGeneric<>(states,actions);
 * //Create a new RewardSet, knowing the different states, and different actions
 *
 * rs.setReward(startState,action,null,reward);
 * // Set a reward for every transitions that that start with startState and have action action
 *
 * ... // Fill the rewardSet with rewards.
 *     // The transitions that are not set have a 0 reward
 *
 * rs.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double reward = rs.getReward(startState,action,endState);
 * // Get the reward for a particular transition
 *
 * double expectedReward = re.getExpectedReward(startState,action);
 * // Get the expected reward for one step, by doing action on startState
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public class RewardSetGeneric<State,Action> extends RewardSet<State,Action> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The reward given for a transition
     */
    private Map<Transition<State,Action>,Double> rewards;

    /**
     * The expected reward given for a couple (initialBelief,action)
     */
    private Map<Pair<State,Action>,Double> expectedReward;

    /**
     * The different states of the MDP
     */
    private Collection<State> states;

    /**
     * The different actions of the MDP
     */
    private Collection<Action> actions;


    /**
     * Create a new reward set
     *
     * @param actions     The different actions
     * @param states      The different states
     */
    public RewardSetGeneric(Collection<State> states, Collection<Action> actions) {
        this.actions = actions;
        this.states = states;
        rewards = new HashMap<>();
        expectedReward = null;
    }


    /**
     * Set the reward for a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param reward        The new reward given for this transition
     */
    @Override
    public void setReward(State startState, Action action, State endState, double reward) {
        //These precomputed values are no longer valid
        expectedReward = null;

        if(startState == null) {
            for(State newStartState : states) {
                setReward(newStartState,action,endState,reward);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setReward(startState,newAction,endState,reward);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setReward(startState,action,newEndState,reward);
            }
        } else {
            rewards.put(new Transition<>(startState,action,endState),reward);
        }
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the generateNextState functions.
     * It uses a TransitionSet object to precompute some data. The transition set should
     * already be initialized.
     *
     * @param transitionSet The probabilities of transition
     */
    @Override
    public void initGetters(TransitionSet<State,Action> transitionSet) {
        expectedReward = new HashMap<>();
        for(State startState : states) {
            for(Action action : actions) {
                expectedReward.put(new Pair<>(startState,action),computeExpectedReward(transitionSet,startState,action));
            }
        }
    }


    /**
     * Get the reward given for a transition
     * If no reward was given for this transition, the reward will be 0.
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The reward for this transition
     */
    @Override
    public double getReward(State startState, Action action, State endState) {
        return rewards.computeIfAbsent(new Transition<>(startState,action,endState),k -> 0.0);
    }


    /**
     * Get the expected reward
     *
     * @param startState The starting state
     * @param action     The action made on that state
     * @return  The expected reward for one step by doing action on startState
     */
    @Override
    public double getExpectedReward(State startState, Action action) {
        return expectedReward.get(new Pair<>(startState,action));
    }
}
