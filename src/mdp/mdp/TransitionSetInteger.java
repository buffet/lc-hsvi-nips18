package mdp.mdp;

import util.random.Distribution;

import java.io.Serializable;
import java.util.*;


/**
 * <pre>
 * Contains the transition dynamics of an MDP
 * This examples show how to get and set probabilities, and how to get the non-zero elements.
 *
 * <code> {@code
 * TransitionSet<Integer,Integer> ts = new TransitionSetInteger<>(nbStates,nbActions);
 * // Create a new transitionSet, knowing the number of states and actions
 *
 * ts.setDistribution(startState,null,endStateDistribution);
 * // Set distributions for possible future endState, for transitions starting with startState, for every action
 *
 * ts.setProbability(startState,action,endState,probability);
 * // Set a probability for a transition
 *
 * ... // We fill the transitions entirely
 *
 * ts.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = ts.getProbability(startState,action,endState);
 * // Getting the probability of being in endState, after performing action in startState
 *
 * Collection<Integer> possibleStates = ts.getPossibleNextStates(startState,action);
 * // Getting the possible end states starting from startState, after performing action
 *
 * int newState = ts.sampleNextState(startState,action);
 * // Sample a new state following the distribution
 * }</code></pre>
 */
public class TransitionSetInteger extends TransitionSet<Integer,Integer> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The transition probabilities.
     * probabilities[initialBelief][action][endState] gives the probability of the transition.
     */
    private double[][][] probabilities;


    /**
     * The probability distributions.
     * distributions[startState][action] gives the probability distribution over end states.
     * It is used to quickly sample a new state.
     */
    private Distribution<Integer>[][] distributions;


    /**
     * The number of different states
     */
    private int nbStates;


    /**
     * The number of different actions
     */
    private int nbActions;


    /**
     * Create a new transition set, given the number of states and actions
     *
     * @param nbStates      The number of states
     * @param nbActions     The number of actions
     */
    @SuppressWarnings("unchecked")
    public TransitionSetInteger(int nbStates, int nbActions) {
        this.nbStates = nbStates;
        this.nbActions = nbActions;
        probabilities = null;
        distributions = new Distribution[nbStates][nbActions];
    }


    /**
     * Set the probability distribution over end states for a given pair (startState,action)
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endStates     The probability distribution over end states
     */
    @Override
    public void setDistribution(Integer startState, Integer action, Distribution<Integer> endStates) {
        if(endStates == null) {
            throw new IllegalArgumentException("The distribution over end states cannot be null");
        }
        if(startState == null) {
            for(int newStartState = 0; newStartState<nbStates; newStartState++) {
                setDistribution(newStartState,action,endStates);
            }
        } else if(action == null) {
            for(int newAction = 0; newAction<nbActions; newAction++) {
                setDistribution(startState,newAction,endStates);
            }
        } else {
            distributions[startState][action] = endStates;
        }
    }


    /**
     * Set the probability for a single transition (the elements are non null)
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions.
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The start state
     * @param action        The action
     * @param endState      The end state
     * @param probability   The probability of this transition
     */
    @Override
    public void setProbability(Integer startState, Integer action, Integer endState, double probability) {
        if(startState == null) {
            for(int newStartState = 0; newStartState<nbStates; newStartState++) {
                setProbability(newStartState,action,endState,probability);
            }
        } else if(action == null) {
            for(int newAction = 0; newAction<nbActions; newAction++) {
                setProbability(startState,newAction,endState,probability);
            }
        } else if(endState == null) {
            for(int newEndState = 0; newEndState<nbStates; newEndState++) {
                setProbability(startState,action,newEndState, probability);
            }
        } else {
            if(distributions[startState][action] == null) {
                distributions[startState][action] = new Distribution<>();
            }
            distributions[startState][action].setWeight(endState,probability);
        }
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters.
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initGetters() {
        probabilities = new double[nbStates][nbActions][nbStates];

        // fill probabilities to be able to use the getters
        for(int startState = 0; startState<nbStates; startState++) {
            for(int action = 0; action<nbActions; action++) {
                Distribution<Integer> distribution = distributions[startState][action];

                // The distributions shouldn't be empty,
                // since there must always be a transition starting with (startState,action)
                if(distribution == null || distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no transitions possible for the couple (" + startState + "," + action +").");
                }

                // Ensure that the probabilities sum to 1.0 by using distribution class
                for(int endState = 0; endState<nbStates; endState++) {
                    double probability = distribution.getProbability(endState);
                    probabilities[startState][action][endState] = probability;
                }
            }
        }
    }


    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The probability of the transition
     */
    @Override
    public double getProbability(Integer startState, Integer action, Integer endState) {
        return probabilities[startState][action][endState];
    }


    /**
     * Get the collection of possible next states, given a pair (startState,action)
     *
     * @param startState The starting state
     * @param action     The last action made
     * @return The collection of possible next states
     */
    @Override
    public Collection<Integer> getPossibleNextStates(Integer startState, Integer action) {
        return distributions[startState][action].getNonZeroElements();
    }


    /**
     * Sample next state in a transition
     *
     * @param startState    The current state
     * @param action        The action made
     * @return A new state sampled according to the transition distribution
     */
    public Integer sampleNextState(Integer startState, Integer action) {
        return distributions[startState][action].sample();
    }



    /**
     * Compute the distribution over end states for a given pair (initialBelief,action)
     *
     * @param startState    The starting state
     * @param action        The action
     * @return  The distribution over end states
     */
    private Distribution<Integer> computeDistribution(int startState, int action) {
        Distribution<Integer> distribution = new Distribution<>();
        for(int endState = 0; endState < nbStates; endState++) {
            distribution.setWeight(endState,getProbability(startState,action,endState));
        }
        return distribution;
    }
}
