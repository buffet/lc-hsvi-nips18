package mdp.mdp;


import util.container.Pair;
import util.random.Distribution;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * <pre>
 * Contains the transition dynamics of a MDP
 * This example shows how to get and set probabilities, and how to get the non-zero elements.
 *
 * <code> {@code
 * TransitionSet<State,Action> ts = new TransitionSetGeneric<>(states,actions);
 * // Create a new transitionSet, knowing the different states and actions
 *
 * ts.setDistribution(startState,null,endStateDistribution);
 * // Set distributions for possible future endState, for transitions starting with startState, for every action
 *
 * ts.setProbability(startState,action,endState,probability);
 * // Set a probability for a transition
 *
 * ... // We fill the transitions entirely
 *
 * ts.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = ts.getProbability(startState,action,endState);
 * // Getting the probability of being in endState, after performing action in startState
 *
 * Collection<State> possibleStates = ts.getPossibleNextStates(startState,action);
 * // Getting the possible end states starting from startState, after performing action
 *
 * State newState = ts.sampleNextState(startState,action);
 * // Sample a new state following the distribution
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public class TransitionSetGeneric<State,Action> extends TransitionSet<State,Action> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The transition probabilities.
     * <code> {@code
     * transitions.get(new Pair<>(startState,action)).getProbability(endState)
     * }</code>
     * gives the probability of the transition.
     */
    private Map<Pair<State,Action>,Distribution<State>> transitions;


    /**
     * The collection of possible states
     */
    private Collection<State> states;


    /**
     * The collection of possible actions
     */
    private Collection<Action> actions;


    /**
     * Create a new transition set, given the collections of states and actions
     *
     * @param states    The collection of possible states
     * @param actions   The collection of possible actions
     */
    public TransitionSetGeneric(Collection<State> states, Collection<Action> actions) {
        this.states = states;
        this.actions = actions;
        transitions = new HashMap<>();
    }


    /**
     * Set the probability distribution over end states for a given (startState,action) pair
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endStates     The probability distribution over the end states
     */
    @Override
    public void setDistribution(State startState, Action action, Distribution<State> endStates) {
        if(endStates == null) {
            throw new IllegalArgumentException("The distribution over end states cannot be null");
        }
        if(startState == null) {
            for(State newStartState : states) {
                setDistribution(newStartState,action,endStates);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setDistribution(startState,newAction,endStates);
            }
        } else {
            transitions.put(new Pair<>(startState, action), endStates);
        }
    }


    /**
     * Set the probability for a single transition (the elements are non-null)
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions.
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The start state
     * @param action        The action
     * @param endState      The end state
     * @param probability   The probability of this transition
     */
    @Override
    public void setProbability(State startState, Action action, State endState, double probability) {
        if(startState == null) {
            for(State newStartState : states) {
                setProbability(newStartState,action,endState,probability);
            }
        } else if(action == null) {
            for(Action newAction : actions) {
                setProbability(startState,newAction,endState,probability);
            }
        } else if(endState == null) {
            for(State newEndState : states) {
                setProbability(startState,action,newEndState,probability);
            }
        } else {
            Distribution<State> distribution = getOrCreateDistribution(startState, action);
            distribution.setWeight(endState, probability);
        }
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters.
     */
    @Override
    public void initGetters() {

        // Ensure that there is always a transition for a given (startState,action) pair
        for(State startState : states) {
            for(Action action : actions) {
                Distribution<State> distribution = getOrCreateDistribution(startState,action);
                if(distribution.getNonZeroElements().size() == 0) {
                    throw new IllegalStateException("There is no transitions possible for the pair (" + startState + "," + action +").");
                }
            }
        }
    }


    /**
     * Return the probability of a transition
     *
     * @param startState The starting state of the transition
     * @param action     The action of the transition
     * @param endState   The end state of the transition
     * @return The probability of the transition
     */
    @Override
    public double getProbability(State startState, Action action, State endState) {
        return transitions.get(new Pair<>(startState,action)).getProbability(endState);
    }


    /**
     * Get the collection of next possible states, given a pair (initialBelief,action)
     *
     * @param startState The starting state
     * @param action     The last action made
     * @return The collection of possible next states
     */
    @Override
    public Collection<State> getPossibleNextStates(State startState, Action action) {
        return transitions.get(new Pair<>(startState,action)).getNonZeroElements();
    }


    /**
     * Sample next state in a transition
     *
     * @param startState The current state
     * @param action     The action made
     * @return A new state sampled according to the transition distribution
     */
    @Override
    public State sampleNextState(State startState, Action action) {
        return transitions.get(new Pair<>(startState,action)).sample();
    }


    /**
     * Return the probability distribution over end states for a pair (startState,action)
     *
     * @param startState The starting state of the transition
     * @param action     The action of the transition
     * @return The distribution of probabilities of end states
     */
    private Distribution<State> getOrCreateDistribution(State startState, Action action) {
        return transitions.computeIfAbsent(new Pair<>(startState, action), k -> new Distribution<>());
    }
}
