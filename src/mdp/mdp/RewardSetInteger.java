package mdp.mdp;

import java.io.Serializable;


/**
 * <pre>
 * Abstract class containing the rewards for a MDP
 * This example show how to set rewards for transitions, and how to get them
 *
 * <code> {@code
 * RewardSet<Integer,Integer> rs = new RewardSetInteger(nbStates, nbActions);
 * //Create a new RewardSet, knowing the number of different states and actions
 *
 * rs.setReward(startState,action,null,reward);
 * // Set a reward for every transitions that that start with startState and have action action
 *
 * ... // Fill the rewardSet with rewards.
 *     // The transitions that are not set have a 0 reward
 *
 * rs.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double reward = rs.getReward(startState,action,endState);
 * // Get the reward for a particular transition
 *
 * double expectedReward = re.getExpectedReward(startState,action);
 * // Get the expected reward for one step, by doing action on startState
 * }</code></pre>
 */
public class RewardSetInteger extends RewardSet<Integer,Integer> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The reward given for a transition
     */
    private double[][][] rewards;


    /**
     * The expected reward given for a couple (initialBelief,action)
     */
    private double[][] expectedReward;


    /**
     * The number of different states
     */
    private int nbStates;


    /**
     * The number of different actions
     */
    private int nbActions;


    /**
     * Create a new reward set
     *
     * @param nbStates      The number of states
     * @param nbActions     The number of actions
     */
    public RewardSetInteger(int nbStates, int nbActions) {
        this.nbStates = nbStates;
        this.nbActions = nbActions;
        rewards = new double[nbStates][nbActions][nbStates];
        expectedReward = null;
    }


    /**
     * Set the reward for a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param reward        The new reward given for this transition
     */
    @Override
    public void setReward(Integer startState, Integer action, Integer endState, double reward) {
        //These precomputed values are no longer valid
        expectedReward = null;

        if(startState == null) {
            for(int newStartState = 0; newStartState<nbStates; newStartState++) {
                setReward(newStartState,action,endState,reward);
            }
        } else if(action == null) {
            for(int newAction = 0; newAction<nbActions; newAction++) {
                setReward(startState,newAction,endState,reward);
            }
        } else if(endState == null) {
            for(int newEndState = 0; newEndState<nbStates; newEndState++) {
                setReward(startState,action,newEndState,reward);
            }
        } else {
            rewards[startState][action][endState] = reward;
        }
    }


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the generateNextState functions.
     * It uses a TransitionSet object to precompute some data. The transition set should
     * already be initialized.
     *
     * @param transitionSet The probabilities of transition
     */
    @Override
    public void initGetters(TransitionSet<Integer,Integer> transitionSet) {
        expectedReward = new double[nbStates][nbActions];
        for(int startState = 0; startState < nbStates; startState++) {
            for(int action = 0; action < nbActions; action++) {
                expectedReward[startState][action] = computeExpectedReward(transitionSet,startState,action);
            }
        }
    }


    /**
     * Get the reward given for a transition.
     * If no reward was given for this transition, the reward will be 0.
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The reward for this transition
     */
    @Override
    public double getReward(Integer startState, Integer action, Integer endState) {
        return rewards[startState][action][endState];
    }


    /**
     * Get the expected reward
     *
     * @param startState The starting state
     * @param action     The action made on that state
     * @return  The expected reward for one step by doing action on startState
     */
    @Override
    public double getExpectedReward(Integer startState, Integer action) {
        return expectedReward[startState][action];
    }
}
