package mdp.mdp;


import java.util.ArrayList;
import java.util.List;


/**
 * <pre>
 * Build a MDP after giving the transitions probabilities and rewards.
 *
 * The following example show how to build a MDP
 *
 * <code> {@code
 * MDPBuilder<State,Action> mdpBuilder = new MDPBuilderInteger(nbStates,nbActions);
 * // Instance the builder, knowing the number of states and actions
 *
 * mdpBuilder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * mdpBuilder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * mdpBuilder.addReward(startState,action,endState,reward);
 * ...
 * // Add transitions and rewards
 *
 * MDP<State,Action> mdp = mdpBuilder.build();
 * // Build the final mdp
 * }</code></pre>
 */
public class MDPBuilderInteger extends MDPBuilder<Integer,Integer> {

    /**
     * The number of states
     */
    public final int nbStates;

    /**
     * The number of actions
     */
    public final int nbActions;

    /**
     * Create a MDP builder for integer classes with a given number of states and actions
     *
     * @param nbStates  The number of states
     * @param nbActions The number of actions
     */
    public MDPBuilderInteger(int nbStates, int nbActions) {
        super(new TransitionSetInteger(nbStates,nbActions),new RewardSetInteger(nbStates,nbActions));
        this.nbStates = nbStates;
        this.nbActions = nbActions;
    }

    /**
     * Build a MDP
     *
     * @return The MDP which has the parameters entered in the builder
     */
    @Override
    public MDP<Integer, Integer> build() {
        setupManagers();
        List<Integer> states = new ArrayList<>(nbStates);
        for(int i = 0; i<nbStates; i++) {
            states.add(i);
        }

        List<Integer> actions = new ArrayList<>(nbActions);
        for(int i = 0; i<nbActions; i++) {
            actions.add(i);
        }

        return new MDP<>(states,actions,transitions,rewards);
    }
}
