package mdp.mdp;


import util.random.Distribution;
import java.util.Collection;


/**
 * <pre>
 * Abstract class containing the transition dynamics of an MDP
 * This example shows how to get and set probabilities, and how to get the non-zero elements.
 *
 * <code> {@code
 * TransitionSet<State,Action> ts = ...;
 * //Create a new TransitionSet, with a child class
 *
 * ts.setDistribution(startState,null,endStateDistribution);
 * // Set distributions for possible future endState, for transitions starting with startState, for every action
 *
 * ts.setProbability(startState,action,endState,probability);
 * // Set a probability for a transition
 *
 * ... // We fill the transitions entirely
 *
 * ts.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double probability = ts.getProbability(startState,action,endState);
 * // Getting the probability of being in endState, after performing action in startState
 *
 * Collection<State> possibleStates = ts.getPossibleNextStates(startState,action);
 * // Getting the possible end states starting from startState, after performing action
 *
 * State newState = ts.sampleNextState(startState,action);
 * // Sample a new state following the distribution
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
abstract public class TransitionSet<State,Action> {


    /**
     * Set the probability distribution over end states for a given (startState,action) pair
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endStates     The probability distribution over the end states
     */
    public abstract void setDistribution(State startState, Action action, Distribution<State> endStates);


    /**
     * Set the probability for a single transition (the elements are non-null)
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions.
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The start state
     * @param action        The action
     * @param endState      The end state
     * @param probability   The probability of this transition
     */
    public abstract void setProbability(State startState, Action action, State endState, double probability);


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the sampleNextState functions
     */
    public abstract void initGetters();


    /**
     * Sample next state in a transition
     *
     * @param startState    The current state
     * @param action        The action made
     * @return A new state sampled according to the transition distribution
     */
    public abstract State sampleNextState(State startState, Action action);


    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The probability of the transition
     */
    public abstract double getProbability(State startState, Action action, State endState);


    /**
     * Get the collection of possible next states, given a pair (initialBelief,action)
     *
     * @param startState    The starting state
     * @param action        The last action made
     * @return  The collection of next possible states
     */
    public abstract Collection<State> getPossibleNextStates(State startState, Action action);
}
