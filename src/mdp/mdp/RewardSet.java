package mdp.mdp;


/**
 * <pre>
 * Abstract class containing the rewards for a MDP
 * This example show how to set rewards for transitions, and how to get them
 *
 * <code> {@code
 * RewardSet<State,Action> rs = ...;
 * //Create a new RewardSet, with a child class
 *
 * rs.setReward(startState,action,null,reward);
 * // Set a reward for every transitions that that start with startState and have action action
 *
 * ... // Fill the rewardSet with rewards.
 *     // The transitions that are not set have a 0 reward
 *
 * rs.initGetters();
 * // Now, it is possible to use the getters, and we shouldn't use the setters anymore
 *
 * double reward = rs.getReward(startState,action,endState);
 * // Get the reward for a particular transition
 *
 * double expectedReward = re.getExpectedReward(startState,action);
 * // Get the expected reward for one step, by doing action on startState
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public abstract class RewardSet<State,Action> {


    /**
     * Set the reward for a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param reward        The new reward given for this transition
     */
    public abstract void setReward(State startState, Action action, State endState, double reward);


    /**
     * Precompute the necessary values to be able to use all getters.
     * This function needs to be called when all the probabilities have been set,
     * and before using the getters or the generateNextState functions.
     * It uses a TransitionSet object to precompute some data. The transition set should
     * already be initialized.
     *
     * @param transitionSet The probabilities of transition
     */
    public abstract void initGetters(TransitionSet<State,Action> transitionSet);


    /**
     * Compute the expected reward for a given starting state, and an action.
     *
     * @param transitionSet The transitions probabilities
     * @param startState    The starting state
     * @param action        The action
     * @return  The expected reward r(s,a) = \sum_{s'} p(s,a,s')*r(s,a,s')
     */
    protected double computeExpectedReward(TransitionSet<State,Action> transitionSet, State startState, Action action) {
        double value = 0;
        for(State endState : transitionSet.getPossibleNextStates(startState,action)) {
            value += transitionSet.getProbability(startState,action,endState) * getReward(startState,action,endState);
        }
        return value;
    }


    /**
     * Get the reward given for a transition.
     * If no reward was given for this transition, the reward will be 0.
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The reward for this transition
     */
    public abstract double getReward(State startState, Action action, State endState);


    /**
     * Get the expected reward
     *
     * @param startState The starting state
     * @param action     The action made on that state
     * @return  The expected reward for one step by doing action on startState
     */
    public abstract double getExpectedReward(State startState, Action action);
}
