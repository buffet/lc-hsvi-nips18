package mdp.mdp;

import java.util.Collection;


/**
 * <pre>
 * Build a MDP after giving the transitions probabilities and rewards.
 *
 * The following example show how to build a MDP
 *
 * <code> {@code
 * MDPBuilder<State,Action> mdpBuilder = new MDPBuilderGeneric<State,Action>(states,actions);
 * // Instance the builder, knowing the different states and actions
 *
 * mdpBuilder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * mdpBuilder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * mdpBuilder.addReward(startState,action,endState,reward);
 * ...
 * // Add transitions and rewards
 *
 * MDP<State,Action> mdp = mdpBuilder.build();
 * // Build the final mdp
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public class MDPBuilderGeneric<State,Action> extends MDPBuilder<State,Action>{

    /**
     * The different states
     */
    public final Collection<State> states;

    /**
     * The different actions
     */
    public final Collection<Action> actions;

    /**
     * Create a MDP builder with a transitionSet and a rewardSet structures
     *
     * @param states    The different states
     * @param actions   The different actions
     */
    public MDPBuilderGeneric(Collection<State> states, Collection<Action> actions) {
        super(new TransitionSetGeneric<>(states, actions), new RewardSetGeneric<>(states,actions));
        this.states = states;
        this.actions = actions;
    }

    /**
     * Build a MDP
     *
     * @return The MDP which has the parameters entered in the builder
     */
    @Override
    public MDP<State, Action> build() {
        setupManagers();
        return new MDP<>(states,actions,transitions,rewards);
    }
}
