package mdp.mdp;


import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;


/**
 * <pre>
 * Contains the MDP dynamics (transitions and rewards)
 * To see how to build the MDP, see MDPBuilder class.
 *
 * The following example show how to use the MDP
 *
 * <code> {@code
 * MDP<State,Action> mdp = mdpBuilder.build();
 * // See MDPBuilder for more information
 *
 * double reward = mpd.getReward(startState,action,endState);
 * // Get the reward for a transition
 *
 * double expectedReward = mdp.getReward(startState,action);
 * // Get the expected reward for doing action in startState
 *
 * State nextState = mdp.sampleNextState(currentState, action);
 * // Sample a next state
 *
 * double probability = mdp.getTransitionProbability(startState,action,endState);
 * // Get the probability of a transition
 *
 * Collection<State> possibleNextStates = mdp.getPossibleNextStates(startState,action);
 * // Get the possible next states (those with a non null transition probability)
 *
 * }</code>
 * </pre>
 * @param <State>   The class used to represent the states
 * @param <Action>  The class used to represent the actions
 */
public class MDP<State,Action> implements Serializable {

    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The transition manager class
     */
    private TransitionSet<State,Action> transitions;


    /**
     * The reward manager class
     */
    private RewardSet<State,Action> rewards;


    /**
     * The collection of different states
     */
    public final Collection<State> states;


    /**
     * The collection of different actions
     */
    public final Collection<Action> actions;


    /**
     * Initialize an MDP.
     * The transition manager and reward manager should have already been initialized (with initGetters)
     *
     * @param states        The different states
     * @param actions       The different actions
     * @param transitions   The transition manager
     * @param rewards       The reward manager
     */
    MDP(Collection<State> states, Collection<Action> actions,
               TransitionSet<State,Action> transitions, RewardSet<State,Action> rewards) {
        this.states = Collections.unmodifiableCollection(states);
        this.actions = Collections.unmodifiableCollection(actions);
        this.transitions = transitions;
        this.rewards = rewards;
    }


    /**
     * Get the reward given for a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The reward for this transition
     */
    public double getReward(State startState, Action action, State endState) {
        return rewards.getReward(startState, action, endState);
    }


    /**
     * Get the expected reward
     *  @param startState The starting state
     * @param action     The action made on that state
     */
    public double getExpectedReward(State startState, Action action) {
        return rewards.getExpectedReward(startState, action);
    }
   

    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The probability of the transition
     */
    public double getTransitionProbability(State startState, Action action, State endState) {
        return transitions.getProbability(startState, action, endState);
    }


    /**
     * Get the collection of next possible states, given a couple (initialBelief,action)
     *
     * @param startState    The starting state
     * @param action        The last action made
     * @return The collection of next possible states
     */
    public Collection<State> getPossibleNextStates(State startState, Action action) {
        return transitions.getPossibleNextStates(startState, action);
    }


    /**
     * Sample next state in a transition
     *
     * @param startState    The current state
     * @param action        The action made
     * @return A new state sampled according to the transition distribution
     */
    public State sampleNextState(State startState, Action action) {
        return transitions.sampleNextState(startState, action);
    }
}
