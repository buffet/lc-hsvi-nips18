package mdp.mdp;


import java.io.Serializable;

/**
 * A simple structure representing a MDP transition.
 *
 * @param <State>   The state class
 * @param <Action>  The action class
 */
public class Transition<State,Action> implements Serializable {
    /**
     * The version of the class used for serialization
     */
    private static final long serialVersionUID = 1L;

    /**
     * The starting state
     */
    final public  State startState;

    /**
     * The action state
     */
    final public Action action;

    /**
     * The ending state
     */
    final public State endState;


    /**
     * Create a transition. The states and the action cannot be null
     *
     * @param startState The starting state
     * @param action     The action
     * @param endState   The ending state
     */
    public Transition(State startState, Action action, State endState) {
        if(startState == null || action == null || endState == null) {
            throw new NullPointerException("The starting state, the action and the ending state shouldn't be null in a transition");
        }
        this.startState = startState;
        this.action = action;
        this.endState = endState;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transition)) return false;
        Transition<?, ?> that = (Transition<?, ?>) o;
        return startState.equals(that.startState) && action.equals(that.action) && endState.equals(that.endState);
    }



    @Override
    public int hashCode() {
        int result = startState.hashCode();
        result = 31 * result + action.hashCode();
        result = 31 * result + endState.hashCode();
        return result;
    }
}
