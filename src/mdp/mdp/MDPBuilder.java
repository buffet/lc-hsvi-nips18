package mdp.mdp;

import util.random.Distribution;


/**
 * <pre>
 * Build a MDP after giving the transitions probabilities and rewards.
 *
 * The following example show how to build a MDP
 *
 * <code> {@code
 * MDPBuilder<State,Action> mdpBuilder = ...;
 * // Instance the object with a child class
 *
 * mdpBuilder.setTransitionProbability(startState,action,endState,probability);
 * ...
 * mdpBuilder.setTransitionDistribution(startState,action,endStateDistribution);
 * ...
 * mdpBuilder.addReward(startState,action,endState,reward);
 * ...
 * // Add transitions and rewards
 *
 * MDP<State,Action> mdp = mdpBuilder.build();
 * // Build the final mdp
 * }</code></pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
abstract public class MDPBuilder<State,Action> {

    /**
     * The object managing the transitions
     */
    protected TransitionSet<State,Action> transitions;

    /**
     * The object managing the rewards
     */
    protected RewardSet<State,Action> rewards;


    /**
     * Create a MDP builder with a transition manager and a reward manager
     *
     * @param transitions   The transition managing class
     * @param rewards       The reward managing class
     */
    MDPBuilder(TransitionSet<State,Action> transitions, RewardSet<State,Action> rewards) {
        this.transitions = transitions;
        this.rewards = rewards;
    }


    /**
     * Build a MDP
     *
     * @return The MDP which has the parameters entered in the builder
     */
    abstract public MDP<State,Action> build();


    /**
     * Setup the transitions and rewards manager
     */
    protected void setupManagers() {
        transitions.initGetters();
        rewards.initGetters(transitions);
    }


    /**
     * Set the probability for a transition.
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The ending state of the transition
     * @param probability   Its probability
     */
    public void setTransitionProbability(State startState, Action action, State endState, double probability) {
        transitions.setProbability(startState,action,endState,probability);
    }


    /**
     * Set the distribution of the end states
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting states
     * @param action        The action
     * @param endStates     The distribution over the end states
     */
    public void setTransitionDistribution(State startState, Action action, Distribution<State> endStates) {
        transitions.setDistribution(startState,action,endStates);
    }


    /**
     * Set a reward for a transition
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endState      The end state
     * @param reward        The reward for the transition
     */
    public void setReward(State startState, Action action, State endState, double reward) {
        rewards.setReward(startState,action,endState,reward);
    }

}
