package mdp.example;

import mdp.example.positionsearch.PSAction;
import mdp.example.positionsearch.PSObs;
import mdp.example.positionsearch.PSState;
import mdp.example.csurveillance.*;
import mdp.example.tiger.*;
import mdp.pomdp.Belief;
import mdp.rhopomdp.*;
import util.container.Pair;
import util.grid.ColoredGrid;
import util.grid.Direction;
import util.random.Distribution;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class RhoPOMDPFactory {

    /*
    public static RhoPOMDP<TigerState,TigerAction,TigerObservation> generateTigerRhoPOMDP(double listenCorrectlyProbability,
                                                                                          double listenReward, double tigerReward,
                                                                                          double treasureReward) {
        Collection<TigerState> states = Arrays.asList(TigerState.values());
        Collection<TigerAction> actions = Arrays.asList(TigerAction.values());
        Collection<TigerObservation> observations = Arrays.asList(TigerObservation.values());

        RhoPOMDPBuilder<TigerState,TigerAction,TigerObservation> builder;
        builder = new RhoPOMDPBuilderGeneric<>(states, actions, observations, new RewardFunction<TigerState,TigerAction>() {
            public double getReward(TigerState state, Belief<TigerState> belief, TigerAction action) {
                if(action == TigerAction.listen) {
                    return listenReward;
                } if(action == TigerAction.left) {
                    return (tigerReward * belief.getProbability(TigerState.left)) + (treasureReward * belief.getProbability(TigerState.right));
                } else {
                    return (treasureReward * belief.getProbability(TigerState.left)) + (tigerReward * belief.getProbability(TigerState.right));
                }
            }
            public double getMinReward() {return tigerReward;}
            public double getMaxReward() {return treasureReward;}
            public double getLipschitzConstant() {return treasureReward - tigerReward;}
            public double getTangent(Belief<TigerState> belief, TigerAction action, TigerState state) {
                Belief<TigerState> beliefState = new Belief<>();
                beliefState.setWeight(state,1.0);
                return getReward(state,belief,action);
            }
        });


        Distribution<TigerState> uniformState = new Distribution<>();
        uniformState.setWeight(TigerState.left, 0.5);
        uniformState.setWeight(TigerState.right, 0.5);

        for(TigerState state : states) {
            builder.setTransitionDistribution(state,TigerAction.left,uniformState);
            builder.setTransitionDistribution(state,TigerAction.right,uniformState);
            builder.setTransitionDistribution(state,TigerAction.listen,new Distribution<>(state));
        }


        Distribution<TigerObservation> distributionObservationRight = new Distribution<>();
        distributionObservationRight.setWeight(TigerObservation.right,listenCorrectlyProbability);
        distributionObservationRight.setWeight(TigerObservation.left, 1-listenCorrectlyProbability);

        Distribution<TigerObservation> distributionObservationLeft = new Distribution<>();
        distributionObservationLeft.setWeight(TigerObservation.right,1-listenCorrectlyProbability);
        distributionObservationLeft.setWeight(TigerObservation.left, listenCorrectlyProbability);

        Distribution<TigerObservation> uniformObservation = new Distribution<>();
        uniformObservation.setWeight(TigerObservation.left, 0.5);
        uniformObservation.setWeight(TigerObservation.right, 0.5);

        builder.setObservationDistribution(TigerAction.listen,TigerState.right,distributionObservationRight);
        builder.setObservationDistribution(TigerAction.listen,TigerState.left,distributionObservationLeft);

        for(TigerState endState : states) {
            builder.setObservationDistribution(TigerAction.left, endState, uniformObservation);
            builder.setObservationDistribution(TigerAction.right, endState, uniformObservation);
        }

        return builder.build();
    }


    public static Pair<RhoPOMDP<CSState,CSAction,CSObs>,Belief<CSState>> generateChildrenSurveillance(int nbRooms, int nbChildren, RewardFunction<CSState,CSAction> rewards) {
        double childrenStayProbability = 0.9;
        double detectOutsideProb = 0.9;
        double detectInsideProb = 0.3;

        Set<Room> rooms = new HashSet<>();
        if(nbRooms == 1) {
            rooms.add(new Room(0,true));
        } else {
            rooms.add(new Room(0,true));
            for(int i = 1; i<nbRooms-1; i++) {
                rooms.add(new Room(i,false));
            }
            rooms.add(new Room(nbRooms-1,true));
        }

        Collection<Children> possibleObservationChildren = Children.getAllPossibleObservationStates(nbChildren,rooms);
        Set<CSObs> observations = new HashSet<>();
        for(Children children : possibleObservationChildren) {
            observations.add(new CSObs(children));
        }
        observations.add(new CSObs(null));

        Collection<Children> possibleChildren = Children.getAllPossibleStates(nbChildren,rooms);
        Set<CSState> states = new HashSet<>();
        for(Room r : rooms) {
            for(Children c : possibleChildren) {
                states.add(new CSState(r,c));
            }
        }

        Collection<CSAction> actions = Arrays.asList(CSAction.values());

        RhoPOMDPBuilder<CSState,CSAction,CSObs> builder = new RhoPOMDPBuilderGeneric<>(states,actions,observations,rewards);

        for(CSState state : states) {
            for(CSAction action : CSAction.values()){
                builder.setTransitionDistribution(state, action, state.getNextStateDistribution(action,rooms,childrenStayProbability));
            }
        }

        for(CSState state : states) {
            for(CSAction action : CSAction.values()) {
                builder.setObservationDistribution(action,state,state.getObservationDistribution(action,detectInsideProb,detectOutsideProb));
            }
        }

        Belief<CSState> initialBelief = new Belief<>();
        for(CSState state : states) {
            if(state.camera.id == 2) {
                initialBelief.setWeight(state,1);
            }
        }

        return new Pair<>(builder.build(),initialBelief);
    }

    public static Pair<RhoPOMDP<PSState,PSAction,PSObs>,Belief<PSState>> generatePositionSearch(double rightActionProb,
                                                                                                double rightObservationProb,
                                                                                                ColoredGrid grid) {
        Set<PSState> states = new HashSet<>();
        for(int i = 0; i<grid.grid.width; i++) {
            for(int j = 0; j<grid.grid.height; j++) {
                states.add(new PSState(i,j));
            }
        }

        Set<PSAction> actions = new HashSet<>();
        for(Direction direction : Direction.values()) {
            actions.add(new PSAction(direction));
        }

        Set<PSObs> observations = new HashSet<>();
        for(int color = 0; color<grid.nbColors; color++) {
            observations.add(new PSObs(color));
        }

        KLApproximationReward<PSState,PSAction> rewards = new KLApproximationReward<>(states.size(),0.01/states.size());

        RhoPOMDPBuilder<PSState,PSAction,PSObs> builder = new RhoPOMDPBuilderGeneric<>(states,actions,observations,rewards);

        for(PSState state : states) {
            for(PSAction action : actions){
                builder.setTransitionDistribution(state, action, state.getNextStatesDistribution(action,rightActionProb,grid));
            }
        }

        for(PSState state : states) {
            builder.setObservationDistribution(null,state,state.getObsDistribution(rightObservationProb,grid));
        }

        Belief<PSState> initialBelief = new Belief<>();
        for(PSState state : states) {
            initialBelief.setWeight(state,1.0);
        }
        return new Pair<>(builder.build(),initialBelief);
    }

    */
    
}
