package mdp.example;


import mdp.example.tiger.TigerAction;
import mdp.example.tiger.TigerObservation;
import mdp.example.tiger.TigerState;
import mdp.pomdp.POMDP;
import mdp.pomdp.POMDPBuilder;
import mdp.pomdp.POMDPBuilderGeneric;
import util.random.Distribution;

import java.util.Arrays;
import java.util.Collection;

public class POMDPFactory {

    public static POMDP<TigerState,TigerAction,TigerObservation> generateTigerPOMDP(double listenCorrectlyProbability,
                                                                                    double listenReward, double tigerReward,
                                                                                    double treasureReward) {

        Collection<TigerState> states = Arrays.asList(TigerState.values());
        Collection<TigerAction> actions = Arrays.asList(TigerAction.values());
        Collection<TigerObservation> observations = Arrays.asList(TigerObservation.values());

        POMDPBuilder<TigerState, TigerAction, TigerObservation> pomdpBuilder = new POMDPBuilderGeneric<>(states,actions,observations);

        Distribution<TigerState> uniformState = new Distribution<>();
        uniformState.setWeight(TigerState.left, 0.5);
        uniformState.setWeight(TigerState.right, 0.5);

        for(TigerState state : states) {
            pomdpBuilder.setTransitionDistribution(state,TigerAction.left,uniformState);
            pomdpBuilder.setTransitionDistribution(state,TigerAction.right,uniformState);
            pomdpBuilder.setTransitionDistribution(state,TigerAction.listen,new Distribution<>(state));
        }


        Distribution<TigerObservation> distributionObservationRight = new Distribution<>();
        distributionObservationRight.setWeight(TigerObservation.right,listenCorrectlyProbability);
        distributionObservationRight.setWeight(TigerObservation.left, 1-listenCorrectlyProbability);

        Distribution<TigerObservation> distributionObservationLeft = new Distribution<>();
        distributionObservationLeft.setWeight(TigerObservation.right,1-listenCorrectlyProbability);
        distributionObservationLeft.setWeight(TigerObservation.left, listenCorrectlyProbability);

        Distribution<TigerObservation> uniformObservation = new Distribution<>();
        uniformObservation.setWeight(TigerObservation.left, 0.5);
        uniformObservation.setWeight(TigerObservation.right, 0.5);

        pomdpBuilder.setObservationDistribution(TigerAction.listen,TigerState.right,distributionObservationRight);
        pomdpBuilder.setObservationDistribution(TigerAction.listen,TigerState.left,distributionObservationLeft);

        for(TigerState endState : states) {
            pomdpBuilder.setObservationDistribution(TigerAction.left, endState, uniformObservation);
            pomdpBuilder.setObservationDistribution(TigerAction.right, endState, uniformObservation);
        }

        for(TigerState endState : states) {
            pomdpBuilder.setReward(endState,TigerAction.listen,endState,listenReward);
            pomdpBuilder.setReward(TigerState.left,TigerAction.left,endState,tigerReward);
            pomdpBuilder.setReward(TigerState.left,TigerAction.right,endState,treasureReward);
            pomdpBuilder.setReward(TigerState.right,TigerAction.left,endState,treasureReward);
            pomdpBuilder.setReward(TigerState.right,TigerAction.right,endState,tigerReward);
        }

        return pomdpBuilder.build();
    }

}
