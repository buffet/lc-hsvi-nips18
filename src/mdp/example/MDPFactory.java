package mdp.example;

import mdp.mdp.*;
import util.container.Position2D;
import util.grid.Direction;
import util.grid.Grid;
import util.random.Distribution;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class MDPFactory {

    /**
     * Generate a simple GridMDP with a following map :
     * +-*
     * +#+
     * +++
     *
     * Where the # gives a wrongMovePenalty reward,
     *           - gives a smallPenalty reward
     *           * gives a bigReward reward
     *           and every case gives a notFinishedPenalty reward
     *
     * @param wrongMovePenalty    The penalty for doing a wrong move (should be very high)
     * @param smallPenalty        A small penalty
     * @param endReward           A reward for ending the process, by being on the upper right case
     * @param notFinishedPenalty  The reward added to the others the agent will have for every move it makes
     */
    public static MDP<Position2D, Direction> generate3x3GridMDP(double wrongMovePenalty, double smallPenalty, double endReward, double notFinishedPenalty) {
        MDPBuilder<Position2D,Direction> mdpBuilder = generateMDPGrid(3,3, notFinishedPenalty, wrongMovePenalty);

        Position2D smallTrapPosition = new Position2D(1,0);
        Position2D bigTrapPosition = new Position2D(1,1);
        Position2D bigRewardPosition = new Position2D(2,0);
        Position2D finishPosition = new Position2D(-1,-1);

        for(Direction direction : Direction.values()) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    Position2D beginState = new Position2D(i, j);
                    mdpBuilder.setReward(beginState, direction, smallTrapPosition, smallPenalty + notFinishedPenalty);
                    mdpBuilder.setReward(beginState, direction, bigTrapPosition, wrongMovePenalty);
                    mdpBuilder.setReward(beginState, direction, finishPosition, endReward);
                }
            }
        }

        for(Direction direction : Direction.values()) {
            mdpBuilder.setReward(finishPosition,direction,finishPosition, 0.0);
        }

        for(Direction direction : Direction.values()) {
            Distribution<Position2D> distribution = new Distribution<>(finishPosition);
            mdpBuilder.setTransitionDistribution(bigRewardPosition,direction, distribution);
        }

        return mdpBuilder.build();
    }


    /**
     * Generate a MDPBuilder representing a grid (with a finish position on (-1,-1).
     * The agent will act perfectly (no randomization).
     * There is a penalty for not begin in the final position, and a penalty for doing an illegal action
     *
     * @param width                 The width of the grid
     * @param height                The height of the grid
     * @param notFinishedPenalty    The penalty for not being in the finish position
     * @param illegalMovePenalty    The penalty for doing an illegal move
     * @return The MDPBuilder corresponding to the grid
     */
    public static MDPBuilder<Position2D, Direction> generateMDPGrid(int width, int height, double notFinishedPenalty, double illegalMovePenalty) {
        Set<Position2D> states = new HashSet<>();
        Position2D finishPosition = new Position2D(-1,-1);
        for(int i = 0; i<width; i++) {
            for(int j = 0; j<height; j++) {
                states.add(new Position2D(i,j));
            }
        }
        states.add(finishPosition);
        Collection<Direction> actions = Arrays.asList(Direction.values());
        MDPBuilder<Position2D,Direction> mdpBuilder = new MDPBuilderGeneric<>(states,actions);
        Grid grid = new Grid(width,height);

        //Generate transitions and rewards for the grid cases
        for(Position2D beginState : states) {
            for(Direction action : actions) {
                Position2D endState;
                if(beginState == finishPosition) {
                    endState = null;
                } else {
                    endState = grid.getAdjacentPosition(beginState,action);
                }
                Distribution<Position2D> distribution = new Distribution<>();

                //If the move is not valid, the agent will not move and will have a penalty
                if(endState == null && !beginState.equals(finishPosition)) {
                    mdpBuilder.setReward(beginState, action, beginState, illegalMovePenalty);
                    distribution.setWeight(beginState, 1.0);
                } else if(beginState.equals(finishPosition)) {
                    mdpBuilder.setReward(beginState, action, beginState, 0);
                    distribution.setWeight(beginState,1.0);
                } else {
                    mdpBuilder.setReward(beginState, action, endState, notFinishedPenalty);
                    distribution.setWeight(endState,1.0);
                }


                mdpBuilder.setTransitionDistribution(beginState,action, distribution);
            }
        }

        return mdpBuilder;
    }

}
