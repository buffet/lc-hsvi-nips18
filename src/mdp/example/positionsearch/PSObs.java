package mdp.example.positionsearch;

public class PSObs {
    /**
     * The color perceived by the agent
     */
    public final int color;

    /**
     * Create an observation for the Position Search problem
     *
     * @param color The color observed
     */
    public PSObs(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return Integer.toString(color);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PSObs)) return false;

        PSObs psObs = (PSObs) o;

        if (color != psObs.color) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return color;
    }
}
