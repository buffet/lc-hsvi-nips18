package mdp.example.positionsearch;

import util.grid.Direction;

public class PSAction {

    /**
     * A direction in the quadrant
     */
    public final Direction direction;

    /**
     * Create an action for the position search problem
     *
     * @param direction The direction of the robot
     */
    public PSAction(Direction direction) {
        this.direction = direction;
    }


    @Override
    public String toString() {
        return direction.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PSAction)) return false;

        PSAction psAction = (PSAction) o;

        if (direction != psAction.direction) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return direction != null ? direction.hashCode() : 0;
    }
}
