package mdp.example.positionsearch;


import util.container.Position2D;
import util.grid.ColoredGrid;
import util.grid.Direction;
import util.random.Distribution;

public class PSState extends Position2D {

    /**
     * Create a state for the problem position search
     *
     * @param x The x position
     * @param y The y position
     */
    public PSState(int x, int y) {
        super(x,y);
    }

    /**
     * Create a state for the problem position search
     *
     * @param pos The position
     */
    public PSState(Position2D pos) {
        super(pos);
    }


    /**
     * Get the distribution of different observations over the state
     *
     * @param rightObservationProbability   The probability of observing the right color
     * @param grid                          The colored grid
     * @return  The distribution of possible observations
     */
    public Distribution<PSObs> getObsDistribution(double rightObservationProbability, ColoredGrid grid) {
        int currentColor = grid.getColor(this);
        Distribution<PSObs> distribution = new Distribution<>();
        for(int color = 0; color<grid.nbColors; color++) {
            if(color == currentColor) {
                distribution.setWeight(new PSObs(color),rightObservationProbability);
            } else {
                distribution.setWeight(new PSObs(color),(1-rightObservationProbability)/(grid.nbColors-1));
            }
        }
        return distribution;
    }

    /**
     * Get the distribution of possible next states
     *
     * @param action                The action made
     * @param goodActionProbability The probability of doing the
     * @param grid                  The grid
     * @return  The distribution over the possible next states
     */
    public Distribution<PSState> getNextStatesDistribution(PSAction action, double goodActionProbability, ColoredGrid grid) {
        if(action.direction == Direction.none) {
            return new Distribution<>(this);
        }

        Distribution<PSState> distribution = new Distribution<>();

        for(Direction direction : Direction.values()) {
            if(direction == Direction.none) {
                continue;
            }
            double prob;
            if(direction == action.direction) {
                prob = goodActionProbability;
            } else {
                prob = (1-goodActionProbability)/3;
            }
            distribution.setWeight(new PSState(grid.getAdjacentPosition(this,direction)),prob);
        }
        return distribution;
    }
}
