package mdp.example.csurveillance;

public class Room {
    public final int id;
    public final boolean isOutdoor;

    public Room(int id, boolean isOutdoor) {
        this.id = id;
        this.isOutdoor = isOutdoor;
    }

    @Override
    public String toString() {
        return Integer.toString(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return (id == room.id); //You shouldn't have a room inside and outside with same id
    }

    @Override
    public int hashCode() {
        return id; // You shouldn't have a room inside and outside with same id
    }
}
