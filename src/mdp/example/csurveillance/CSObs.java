package mdp.example.csurveillance;


public class CSObs {
    public final Children children;

    public CSObs(Children children) {
        this.children = children;
    }

    @Override
    public String toString() {
        if(children != null) {
            return children.toString();
        } else {
            return "null";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CSObs)) return false;

        CSObs csObs = (CSObs) o;

        if (children != null ? !children.equals(csObs.children) : csObs.children != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return children != null ? children.hashCode() : 0;
    }
}
