package mdp.example.csurveillance;


import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Children {
    final public Room[] positions;

    public Children(Room[] positions) {
        this.positions = positions;
    }


    public double getProbability(Children children, int nbRooms, double stayingProbability) {
        if(children.positions.length != positions.length) {
            throw new IllegalStateException("There must be the same number of children");
        }

        double p = 1.0;
        for(int i = 0; i<children.positions.length; i++) {
            if(positions[i].id == children.positions[i].id) {
                if(positions[i].id == 0 || positions[i].id == nbRooms-1) {
                    p *= stayingProbability + (1-stayingProbability)/2;
                } else {
                    p *= 0.9;
                }
            } else if(positions[i].id == children.positions[i].id -1 || positions[i].id == children.positions[i].id +1) {
                p *= (1-stayingProbability)/2;
            } else {
                return 0.0;
            }
        }
        return p;
    }

    static public Set<Children> getAllPossibleObservationStates(int nbChildren, Collection<Room> rooms) {
        Set<Children> set = new HashSet<>();
        PossibleStatesDFS(rooms,new Room[nbChildren],0,set,true);
        return set;
    }


    static public Set<Children> getAllPossibleStates(int nbChildren, Collection<Room> rooms) {
        Set<Children> set = new HashSet<>();
        PossibleStatesDFS(rooms,new Room[nbChildren],0,set,false);
        return set;
    }

    @SuppressWarnings("ConstantConditions") //canBeNull is always true when called in the if at the end of the method
    static private void PossibleStatesDFS(Collection<Room> rooms, Room[] current, int pos, Set<Children> set, boolean canBeNull) {
        if(pos == current.length) {
            set.add(new Children(Arrays.copyOf(current,current.length)));
            return;
        }
        for(Room r : rooms) {
            current[pos] = r;
            PossibleStatesDFS(rooms,current,pos+1,set,canBeNull);
        }
        if(canBeNull) {
            current[pos] = null;
            PossibleStatesDFS(rooms, current, pos + 1, set, canBeNull);
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(positions);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Children)) return false;
        Children that = (Children) o;
        return Arrays.equals(positions, that.positions);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(positions);
    }
}
