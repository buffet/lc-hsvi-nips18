package mdp.example.csurveillance;

import util.random.Distribution;

import java.util.Arrays;
import java.util.Collection;

public class CSState {
    public final Room camera;
    public final Children children;

    public CSState(Room camera, Children children) {
        this.camera = camera;
        this.children = children;
    }

    public Distribution<CSObs> getObservationDistribution(CSAction action, double detectInsideProb, double detectOutsideProb) {
        if(action == CSAction.left || action == CSAction.right) {
            return new Distribution<>(new CSObs(null));
        }
        Distribution<CSObs> distribution = new Distribution<>();
        ObservationDistributionDFS(detectInsideProb,detectOutsideProb,1.0,new Room[children.positions.length],0,distribution);
        return distribution;
    }

    private void ObservationDistributionDFS(double detectInsideProb, double detectOutsideProb, double currentProb,
                                            Room[] currentObs, int pos, Distribution<CSObs> distribution) {
        if(pos == currentObs.length) {
            distribution.setWeight(new CSObs(new Children(Arrays.copyOf(currentObs,currentObs.length))),currentProb);
            return;
        }

        if(children.positions[pos].id == camera.id) {
            double detectProb = children.positions[pos].isOutdoor ? detectOutsideProb : detectInsideProb;
            currentObs[pos] = children.positions[pos];
            ObservationDistributionDFS(detectInsideProb,detectOutsideProb,currentProb*detectProb,currentObs,pos+1,distribution);
            currentObs[pos] = null;
            ObservationDistributionDFS(detectInsideProb,detectOutsideProb,currentProb*(1-detectProb),currentObs,pos+1,distribution);
        } else {
            currentObs[pos] = null;
            ObservationDistributionDFS(detectInsideProb,detectOutsideProb,currentProb,currentObs,pos+1,distribution);
        }
    }

    public Distribution<CSState> getNextStateDistribution(CSAction action, Collection<Room> rooms, double stayingProbability) {
        int newCameraRoomId;
        if(action == CSAction.left) {
            newCameraRoomId = camera.id > 0 ? camera.id-1 : camera.id;
        } else if(action == CSAction.right) {
            newCameraRoomId = camera.id < rooms.size()-1 ? camera.id+1 : camera.id;
        } else {
            newCameraRoomId = camera.id;
        }

        Room newCameraRoom = camera;
        for(Room room : rooms) {
            if(room.id == newCameraRoomId) {
                newCameraRoom = room;
                break;
            }
        }

        Collection<Children> possibleChildren = Children.getAllPossibleStates(children.positions.length,rooms);

        Distribution<CSState> nextStateDistribution = new Distribution<>();
        for(Children nextChildren : possibleChildren) {
            CSState nextState = new CSState(newCameraRoom,nextChildren);
            nextStateDistribution.setWeight(nextState,children.getProbability(nextChildren,rooms.size(),stayingProbability));
        }
        return nextStateDistribution;
    }

    @Override
    public String toString() {
        return "("+camera+","+children+")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CSState)) return false;

        CSState that = (CSState) o;

        if (camera != null ? !camera.equals(that.camera) : that.camera != null) return false;
        if (children != null ? !children.equals(that.children) : that.children != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = camera != null ? camera.hashCode() : 0;
        result = 31 * result + (children != null ? children.hashCode() : 0);
        return result;
    }
}
