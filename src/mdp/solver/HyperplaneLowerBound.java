package mdp.solver;

import mdp.pomdp.Belief;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;
import util.container.Pair;
import util.function.HyperplaneRepresentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * 
 * Lower-bounding value function approximation based on an (envelope
 * of) hyperplane(s) representation.
 * 
 * </pre>
 */

public class HyperplaneLowerBound<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>
                                                            implements Serializable {

    /**
     * The bound represented with a point set
     */
    public HyperplaneRepresentation representation;

    /**
     * The converted to states, used to interface the pointSetRepresentation.
     */
    private ObjectIntegerConverter<State> converter;


    /**
     * Creates a lower or an upper bound
     *
     * @param problem         The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public HyperplaneLowerBound(SolvableByHSVI<State,Action,Observation> problem, double discount, double errorMargin, boolean pruning) {
        super(false, problem, discount, pruning);

        converter = new ObjectIntegerConverter<>();

        for(State state : problem.getStates()) {
            converter.add(state);
        }
        init(errorMargin, pruning);
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */
    @Override
    public void update(Belief<State> belief, double value) {
        Map<Pair<Action,Observation>,MatrixX> betaAO = getBetaAOVectors(belief);
        Map<Action,MatrixX> betaA = getBetaAVectors(belief, betaAO);

        MatrixX maxiBetaA = null;
        double maxiValue = Double.NEGATIVE_INFINITY;
        MatrixX vectorBelief = belief.toMatrixX(problem.getStates(),converter);
        for(Action action : problem.getActions()) {
            MatrixX vectorBetaA = betaA.get(action);
            double scalarValue = vectorBetaA.times(vectorBelief).get(0,0);
            if(scalarValue > maxiValue) {
                maxiValue = scalarValue;
                maxiBetaA = vectorBetaA;
            }
        }
        representation.addHyperplane(maxiBetaA);
    }


    /**
     * Return the list of \beta_{a,o} vectors  (as defined in the HSVI paper)
     *
     * @param belief  The point where to computes the vectors
     * @return The betaAO vectors
     */
    public Map<Pair<Action,Observation>,MatrixX> getBetaAOVectors(Belief<State> belief) {
        Map<Pair<Action,Observation>,MatrixX> betaAO = new HashMap<>();
        for(Action action : problem.getActions()) {
            for(Observation observation : problem.getObservations()) {
                Belief<State> newBelief = problem.getNewBelief(belief,action,observation);
                MatrixX representative = representation.getRepresentative(newBelief.toMatrixX(problem.getStates(),converter));
                betaAO.put(new Pair<>(action,observation), representative);
            }
        }
        return betaAO;
    }

    /**
     * Return the list of \beta_a vectors  (as defined in the HSVI paper)
     *
     * @param belief    The point where to computes the vectors
     * @param betaAO    The betaAO vectors
     * @return  The betaA vectors
     */
    public Map<Action,MatrixX> getBetaAVectors(Belief<State> belief, Map<Pair<Action,Observation>,MatrixX> betaAO) {
        Map<Action,MatrixX> betaA = new HashMap<>();
        for(Action action : problem.getActions()) {
            MatrixX beta = new MatrixX(1, problem.getStates().size());

            for(State startState : problem.getStates()) {
                double betaBeginStateValue = 0.0;

                for(State endState : problem.getPossibleNextStates(startState,action)) {
                    double temp = 0.0;

                    for(Observation observation : problem.getPossibleObservations(action,endState)) {
                        double temp2 = problem.getObservationProbability(action,endState,observation);
                        temp2 *= betaAO.get(new Pair<>(action,observation)).get(0,converter.toInteger(endState));
                        temp += temp2;
                    }
                    temp *= problem.getTransitionProbability(startState,action,endState);
                    betaBeginStateValue += temp;
                }
                betaBeginStateValue *= discount;
                betaBeginStateValue += problem.getRewardTangent(belief,action,startState);
                beta.set(0,converter.toInteger(startState), betaBeginStateValue);
            }
            betaA.put(action,beta);
        }
        return betaA;
    }


    /**
     * Get the value of the bound on one point
     *
     * @param belief The point where we want the value
     * @return The value of the bound on that point
     */
    @Override
    public double getValue(Belief<State> belief) {
        return representation.getValue(belief.toMatrixX(problem.getStates(),converter));
    }


    /**
     * Initialize the bound.
     */
    private void init(double errorMargin, boolean pruning) {
        Map<State,Double> lowerBound = problem.getValueFunctionLowerBound(discount,errorMargin/1000);

        representation = new HyperplaneRepresentation(true,problem.getStates().size(), pruning);
        MatrixX initialVector = new MatrixX(1, problem.getStates().size());
        for(State state : problem.getStates()) {
            initialVector.set(0,converter.toInteger(state),lowerBound.get(state));
        }
        representation.addHyperplane(initialVector);
    }

    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return representation.getSize();
    }

    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public double getMaxSlope() {
	return representation.getMaxSlope();
    }
            
}
