package mdp.solver;

import mdp.pomdp.Belief;
import sun.misc.Signal;
import util.container.Pair;
import util.signal.ExitSignalHandler;
import util.signal.SingleSignalHandler;


public class HSVI<State,Action,Observation> {



    /**
     * The known upper bound of the value function
     */
    final public ValueFunctionBound<State,Action,Observation> upperBound;


    /**
     * The lower bound of the value function
     */
    final public ValueFunctionBound<State,Action,Observation> lowerBound;


    /**
     * The gamma parameter
     */
    private double gamma;


    /**
     * The error we allow to the initial belief
     */
    private double errorMargin;

    
    /**
     * Whether to check for crossing bounds
     */
    private Boolean exLXU;

    
    /**
     * Whether to check for non-uniformly improvable bounds
     */
    private Boolean exNUI;
    

    /**
     * The pomdp we are trying to solve
     */
    private SolvableByHSVI<State,Action,Observation> pomdp;


    /**
     * The printer used to save files
     */
    private HSVIPythonPrinter<State,Action,Observation> printer;

    /**
     * Create a solver over a certain pomdp
     *
     * @param pomdp         The problem to solve (can be a RhoPOMDP)
     * @param discount      The discount in the problem
     * @param errorMargin   The allowed margin error
     * @param lowerBound    The lower bound
     * @param upperBound    The upper bound
     * @param exLXU         Check for crossing bounds (yes|no)
     * @param exNUI         Check for non-uniform improvability (yes|no)
     */
    HSVI(SolvableByHSVI<State,Action,Observation> pomdp,
            double discount,
            double errorMargin,
            ValueFunctionBound<State,Action,Observation> lowerBound,
	    ValueFunctionBound<State,Action,Observation> upperBound,
	    Boolean exLXU,
	    Boolean exNUI) {
        this.gamma = discount;
        if(gamma >= 1.0 || gamma <= 0) {
            throw new IllegalArgumentException("The discount should be between 0.0 and 1.0");
        }
        this.errorMargin = errorMargin;
        this.pomdp = pomdp;

        this.upperBound = upperBound;
        this.lowerBound = lowerBound;

	this.exLXU = exLXU;
	this.exNUI = exNUI;
    }


    /**
     * Get the approximation of an optimal value for a given belief
     * The approximation will always be a lower bound
     *
     * @param belief  The belief where we want to calculate the value function
     * @param timelimit    when the algorithm should stop (time point in milliseconds)
     * @param iterations   after how many iterations the algorithm should stop
     * @return The approximation of the value function on the belief
     */
    public double getOptimalValue(Belief<State> belief, long time, long timelimit, int iterations, HSVIPythonPrinter printer) {
        solve(belief, time, timelimit, iterations, printer);
        return lowerBound.getValue(belief);
    }


    /**
     * Find the optimal strategy for a given initial belief, and print the steps of the algorithms with a python file
     *
     * @param belief     The belief where we want a good approximation
     * @param timelimit    when the algorithm should stop (time point in milliseconds)
     * @param iterations   after how many iterations the algorithm should stop
     * @param scriptPath The path of the file
     * @param imagePath  The name of the image
     */
    // public void solveAndPrint(Belief<State> belief, long time, long timelimit, int iterations, String scriptPath, String imagePath) {
    //     printer = new HSVIPythonPrinter<>(lowerBound,upperBound);
    //     isPrinting = true;

    //     try {
    //         solve(belief, time, timelimit, iterations);
    //     } catch(IllegalStateException e) {
    //         isPrinting = false;
    //         printer.savePythonProgram(scriptPath, imagePath);
    //         throw(e);
    //     }

    //     isPrinting = false;
    //     printer.savePythonProgram(scriptPath, imagePath);
    // }


    /**
     * Find the optimal strategy for a given initial belief
     *
     * @param belief  The belief where we want a good approximation
     * @param timelimit    when the algorithm should stop (time point in milliseconds)
     * @param iterations   after how many iterations the algorithm should stop
     */
    public void solve(Belief<State> belief, long time, long timelimit, int iterations, HSVIPythonPrinter printer) {
	this.printer = printer;
        // The signal Ctrl-C is handled, to stop the computation whenever you want
        SingleSignalHandler signalHandler = new SingleSignalHandler();
        Signal.handle(new Signal("INT"),signalHandler);

	//System.out.println("<solving>");
	int i=0;
	try{
	    while(getWidth(belief) >= errorMargin
		  && i != iterations
		  && (timelimit <= 0 || System.currentTimeMillis() < timelimit)
		  && !signalHandler.hasHandleSignal()) {
		//System.out.println("<New trial>");
		System.out.format("#- %d %.1f %f %f %d %d\n",
				  i, (System.currentTimeMillis()-time)/1000.,
				  lowerBound.getValue(belief),upperBound.getValue(belief),
				  lowerBound.getSize(), upperBound.getSize());
		explore(belief,0.95*getWidth(belief),0, timelimit);
		i++;
	    }
	    if (timelimit > 0 && System.currentTimeMillis() >= timelimit) 
		    System.out.println("!! Timeout");
		
	    //System.out.println("<end of trials>");
	    System.out.format("## %d %.1f %f %f %d %d %f\n", i, (System.currentTimeMillis()-time)/1000.,
			      lowerBound.getValue(belief),upperBound.getValue(belief),
			      lowerBound.getSize(), upperBound.getSize(), getMaxSlope());
	} catch (IllegalStateException exception) {
	    System.out.format("#! %d %.1f %f %f %d %d %f\n", i, (System.currentTimeMillis()-time)/1000.,
			      lowerBound.getValue(belief),upperBound.getValue(belief),
			      lowerBound.getSize(), upperBound.getSize(), getMaxSlope());
	    throw exception;
	}
	
	if (printer != null)
	    printer.savePythonProgram();

	//System.out.println("</solving>");
        Signal.handle(new Signal("INT"),new ExitSignalHandler());
    }


    /**
     * Return the width of the approximation on a particular belief
     *
     * @param belief The point in which we wants to calculate an approximation
     * @return The width of the approximation on that point
     */
    private double getWidth(Belief<State> belief) {
	//System.out.println("<getWidth>");
        return upperBound.getValue(belief) - lowerBound.getValue(belief);
    }


    /**
     * explore recursively on a single path until finding a satisfying termination condition
     *
     * @param belief                 The current belief on the path
     * @param currentErrorMargin     The current allowed error
     */
    private void explore(Belief<State> belief, double currentErrorMargin, int depth, long timelimit) {
	if ( (timelimit > 0 && System.currentTimeMillis() >= timelimit)
	     || (getWidth(belief) < currentErrorMargin) ) {
            return;
        }

        updateBounds(belief);

        Pair<Action,Observation> path = getForwardExplorationHeuristic(belief,currentErrorMargin);
        Belief<State> newBelief = pomdp.getNewBelief(belief,path.a,path.b);
	//System.out.println("->");
        explore(newBelief,currentErrorMargin/gamma,depth+1,timelimit);
	//System.out.println("<-");

	// One can also interrupt computations when going back up :)
	if (timelimit > 0 && System.currentTimeMillis() >= timelimit) {
            return;
        }

        updateBounds(belief);
    }


    /**
     * Update the bounds on a particular belief
     *
     * @param belief The belief where to update the bounds
     */
    private void updateBounds(Belief<State> belief) {
	//System.out.println("<updateBounds>");
        double maxiQUpperValue = upperBound.getBellman(belief).b;
        double maxiQLowerValue = lowerBound.getBellman(belief).b;
	
	//System.out.println("[lowerBound]");
	double LB = lowerBound.getValue(belief);
	//System.out.println("[upperBound]");
	double UB = upperBound.getValue(belief);
	//System.out.format(" bounds(%s)=(%.3f,%.3f)%n",belief.toString(),LB,UB);
	//System.out.format("Bellman(%s)=(%.3f,%.3f)%n",belief.toString(),maxiQLowerValue,maxiQUpperValue);
	
	if(exNUI)
        if(UB + 0.00001 < maxiQUpperValue
	   || LB - 0.00001 > maxiQLowerValue) {
            System.out.format("!! NUI: %f %f %f %f\n",
			      LB, maxiQLowerValue,
			      UB, maxiQUpperValue);
	    System.out.println("Belief: "+belief.toString());
            throw new IllegalStateException("NUI: The bounds aren't upper/lower bounds");
        }
	
        upperBound.update(belief,maxiQUpperValue);
        lowerBound.update(belief,maxiQLowerValue);

	if(exLXU)
        if(maxiQUpperValue < maxiQLowerValue) {
	    System.out.format("!! LXU: %f %f\n", maxiQLowerValue, maxiQUpperValue );
            throw new IllegalStateException("LXU: The bounds aren't upper/lower bounds");
        }

        //if(isPrinting) {
	if (printer != null) {
            double pointBelief = belief.getProbability(pomdp.getStates().iterator().next());
	    System.out.println("b="+pointBelief);
            printer.newImage(new Pair<>(pointBelief,maxiQLowerValue),new Pair<>(pointBelief,maxiQUpperValue));
        }
    }


    /**
     * Get the action and the observation to explore, as well as the Bellman update value
     *
     * @param belief  The current belief
     * @return The action to explore, the bellman update value, and the observation to explore
     */
    private Pair<Action,Observation> getForwardExplorationHeuristic(Belief<State> belief, double currentErrorMargin) {
	//System.out.println("<getForwardExplorationHeuristic>");
        Action maxiAction = upperBound.getBestAction(belief);
	//System.out.println("<upperBound.getBestAction(belief)> => "+maxiAction);

        double maxiObservationValue = Double.NEGATIVE_INFINITY;
        Observation maxiObservation = null;
        for(Observation observation : pomdp.getObservations()) {
	    //System.out.println("b: "+belief.toString()+" maxA: "+maxiAction+" z: "+observation);
            double observationValue = pomdp.getConditionalProbabilityOfObservation(belief,maxiAction,observation);
            observationValue *= getWidth(pomdp.getNewBelief(belief,maxiAction,observation)) - (currentErrorMargin/gamma);
            if(maxiObservationValue < observationValue) {
                maxiObservation = observation;
                maxiObservationValue = observationValue;
            }
        }

        return new Pair<>(maxiAction,maxiObservation);
    }

    public SolvableByHSVI<State,Action,Observation> getPOMDP() {
	return pomdp;
    }

    
    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public double getMaxSlope() {
	double u = upperBound.getMaxSlope();
	double l = lowerBound.getMaxSlope();

	return (l>u)?l:u;
    }
    
}
