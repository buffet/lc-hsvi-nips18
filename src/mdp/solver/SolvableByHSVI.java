package mdp.solver;

import mdp.pomdp.Belief;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import java.util.Collection;
import java.util.Map;

public interface SolvableByHSVI<State,Action,Observation> {

    /**
     * Get the different states
     *
     * @return A collection of the different states
     */
    Collection<State> getStates();

    /**
     * Get the different actions
     *
     * @return A collection of the different action
     */
    Collection<Action> getActions();

    /**
     * Get the different observations
     *
     * @return A collection of the observations
     */
    Collection<Observation> getObservations();

    /**
     * Get the new belief, after doing an action and getting an observation
     *
     * @param belief      The old belief
     * @param action      The last action made
     * @param observation The observation resulting of that action
     * @return The new belief
     */
    Belief<State> getNewBelief(Belief<State> belief, Action action, Observation observation);


    /**
     * Get the probability of a given observation, knowing the current belief, the last action made.
     *
     * @param belief        The belief before doing the action
     * @param action        The last action made
     * @param observation   The observation we want to probability of
     * @return The probability of getting the observation
     */
    double getConditionalProbabilityOfObservation(Belief<State> belief, Action action, Observation observation);

    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     * @return The probability of the observation knowing (action,endState)
     */
    double getObservationProbability(Action action, State endState, Observation observation);


    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The probability of the transition
     */
    double getTransitionProbability(State startState, Action action, State endState);


    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    double getExpectedReward(Belief<State> belief, Action action);

    
    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    public MatrixX getRewardLipschitzConstant(Belief<State> belief, Action action,  ObjectIntegerConverter converter);

    /**
     * Get the slope of the reward function on a belief, on a certain dimension
     *
     * @param belief    The point where to get the tangent
     * @param action    The action associated with the reward (we want the tangent of r(a))
     * @param state     The dimension for which we want the value
     * @return  The tangent
     */
    double getRewardTangent(Belief<State> belief, Action action, State state);

    /**
     * Get a lower bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    Map<State,Double> getValueFunctionLowerBound(double discount, double errorMargin);


    /**
     * Get a upper bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    Map<State,Double> getValueFunctionUpperBound(double discount, double errorMargin);


    /**
     * Get the collection of next possible states, given a couple (startState,action)
     *
     * @param startState    The starting state
     * @param action        The last action made
     * @return The collection of next possible states
     */
    Collection<State> getPossibleNextStates(State startState, Action action);


    /**
     * Get the possible next observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     * @return The collection of possible next observations
     */
    Collection<Observation> getPossibleObservations(Action action, State endState);
}
