package mdp.solver;

import mdp.mdp.MDP;
import mdp.policy.FiniteHorizonDeterministicPolicy;

public class FiniteHorizonMDPSolver<State,Action> {

    /**
     * The mdp we want to solve
     */
    private MDP<State,Action> mdp;


    /**
     * The number of steps of the horizon
     */
    private int nbSteps;


    /**
     * The current policy
     */
    private FiniteHorizonDeterministicPolicy<State,Action> policy;


    /**
     * Find the optimal deterministic strategy for a main.mdp with finite horizon
     *
     * @param mdp     The Markov Decision Process for which we want the optimal policy
     * @param nbSteps The number of steps
     * @return The optimal policy
     */
    public FiniteHorizonDeterministicPolicy<State,Action> findOptimalStrategy(MDP<State,Action> mdp, int nbSteps) {
        this.mdp = mdp;
        this.nbSteps = nbSteps;
        policy = new FiniteHorizonDeterministicPolicy<>();

        if (nbSteps < 0) {
            throw new IllegalArgumentException("The number of steps left cannot be negative");
        }

        for (int step = 0; step < nbSteps; step++) {
            bellmanUpdate(step);
        }
        return policy;
    }



    /**
     * Compute the value used in the dynamic programming algorithm for main.mdp with finite horizon
     *
     * @param step        The current step
     * @param beginState  The begin state
     * @param action      The action
     * @return The computed value
     */
    private double bellmanSingleUpdate(int step, State beginState, Action action) {
        double value = 0;

        for(State endState : mdp.getPossibleNextStates(beginState,action)) {
            double temp = mdp.getReward(beginState,action,endState);
            value += temp * mdp.getTransitionProbability(beginState,action,endState);
        }
        if(step == 0) {
            return value;
        }

        for(State endState : mdp.getPossibleNextStates(beginState,action)) {
            value += mdp.getTransitionProbability(beginState,action,endState) * policy.getValue(endState,step-1);
        }

        return value;
    }



    /**
     * Do one step of the dynamic programming algorithm for finite horizon
     *
     * @param step The step to compute
     */
    private void bellmanUpdate(int step) {
        for(State beginState : mdp.states) {
            double maxiValue = Double.NEGATIVE_INFINITY;
            Action maxiAction = null;
            for(Action action : mdp.actions) {
                double temp = bellmanSingleUpdate(step,beginState,action);
                if(maxiValue <= temp) {
                    maxiValue = temp;
                    maxiAction = action;
                }
            }
            policy.setAction(beginState,step,maxiAction);
            policy.setValue(beginState,step,maxiValue);
        }
    }
}
