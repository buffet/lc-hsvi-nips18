package mdp.solver;

import mdp.policy.POMDPPolicy;
import mdp.pomdp.Belief;
import util.container.Pair;

import java.io.Serializable;
import java.util.Iterator;

/**
 * <pre>
 * 
 * <i>abstract</i> class for all value-function approximating classes
 * (both upper and lower bounds).
 * 
 * </pre>
 */

public abstract class ValueFunctionBound<State,Action,Observation> implements POMDPPolicy<State,Action>, Serializable {

    /**
     * Allow to verify if this bound is an upper or a lower bound
     */
    protected boolean isUpperBound;


    /**
     * The problem which value function is bounded
     */
    protected SolvableByHSVI<State,Action,Observation> problem;


    /**
     * The discount of the reward function
     */
    protected double discount;

    /**
     * Is pruning on/off ?
     */
    protected boolean pruning;
    
    /**
     * Creates a lower or an upper bound
     *
     * @param isUpperBound  True if the bound is an upper bound
     * @param problem       The problem for which we bound the value function
     * @param discount      The discount of the reward
     */
    public ValueFunctionBound(boolean isUpperBound, SolvableByHSVI<State,Action,Observation> problem, double discount, boolean pruning) {
        this.isUpperBound = isUpperBound;
        this.problem = problem;
        this.discount = discount;
	this.pruning = pruning;
    }

    /**
     * Update the bound
     *
     * @param belief  The belief where we will update the bound
     * @param value   The value on that belief
     */
    public abstract void update (Belief<State> belief, double value);


    /**
     * Get the value of the bound on one point
     *
     * @param belief The point where we want the value
     * @return The value of the bound on that point
     */
    public abstract double getValue(Belief<State> belief);


    /**
     * Get the next action to do
     *
     * @param belief The current belief
     * @return The next action to do
     */
    @Override
    public Action getBestAction(Belief<State> belief) {
        return getBellman(belief).a;
    }

    /**
     * Get the max and the argmax in the bellman update
     *
     * @param belief The current belief
     * @return The action that maximize the value, as well as the value
     */
    public Pair<Action, Double> getBellman(Belief<State> belief) {
	//System.out.println("getBellman("+belief.toString()+")");
        double maxiActionValue = Double.NEGATIVE_INFINITY;
        Action maxiAction = null;
        for(Action action : problem.getActions()){
            double actionValue = getBellmanForAction(belief,action);
	    //System.out.format("a: %s Q(b,a)=%.3f%n",action.toString(),actionValue);
            if(maxiActionValue < actionValue) {
		//System.out.println("new best action");
                maxiActionValue = actionValue;
                maxiAction = action;
            }
        }
        return new Pair<>(maxiAction,maxiActionValue);
    }


    /**
     * Get the value of the bellman update on a particular action
     *
     * @param belief  The current belief
     * @param action  The action
     * @return The Q value
     */
    public double getBellmanForAction(Belief<State> belief, Action action) {
        double qValue = problem.getExpectedReward(belief,action);

        double qValue2 = 0;
        for(Observation observation : problem.getObservations()) {

            double probabilityOfObservation = problem.getConditionalProbabilityOfObservation(belief,action,observation);
            Belief<State> newBelief = problem.getNewBelief(belief,action,observation);
	    double Vbaz = getValue(newBelief);
	    //System.out.format("z: %s getValue(baz)=%.3f Paz=%.3f%n",
	    //		      observation.toString(),Vbaz,probabilityOfObservation);
            qValue2 += probabilityOfObservation * Vbaz;
        }

        return qValue + discount*qValue2;
    }


    /**
     * Return a string representing the value of the function when the bound is in dimension 2
     *
     * @param n The number of samples
     * @return The string representing a table containing the values (like [0.3,1.1,-2.0]);
     */
    public String getValuesString(int n) {
        if(problem.getStates().size() != 2) {
            return "";
        }
        Iterator<State> iterator = problem.getStates().iterator();
        State state1 = iterator.next();
        State state2 = iterator.next();

        StringBuilder str = new StringBuilder();
        str.append("[");

        for(int i = 0; i<=n; i++) {
            Belief<State> belief = new Belief<>();
            belief.setWeight(state1,(double)(i)/n);
            belief.setWeight(state2, 1.0-((double)(i)/n));
            str.append(getValue(belief));
            if(i != n) {
                str.append(",");
            }
        }
        str.append("]");
        return str.toString();
    }

    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public abstract int getSize();

    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public abstract double getMaxSlope();
    
}
