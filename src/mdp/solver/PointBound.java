package mdp.solver;


import mdp.pomdp.Belief;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;
import util.function.PointRepresentation;
import util.function.PointRepresentation.Point;

import java.io.Serializable;
import java.util.Map;


/**
 * <pre>
 * 
 * Upper-bounding value function approximation based on a point
 * representation.
 * 
 * </pre>
 */

public class PointBound<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>
                                                  implements Serializable {

    /**
     * The bound represented with a point set
     */
    private PointRepresentation representation;


    /**
     * The converted to states, used to interface the pointSetRepresentation.
     */
    private ObjectIntegerConverter<State> converter;
    
    /**
     * Create a new bound represented by points.
     * Needs to be initialized with the init() function
     *
     * @param isUpperBound  Defined it as upper or lower bound
     * @param pomdp         The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public PointBound(boolean isUpperBound, SolvableByHSVI<State,Action,Observation> pomdp, double discount, double errorMargin, boolean pruning) {
        super(isUpperBound, pomdp, discount, pruning);
        converter = new ObjectIntegerConverter<>();

        for(State state : pomdp.getStates()) {
            converter.add(state);
        }
        init(errorMargin, pruning);
    }

    /**
     * Update the bound
     *
     * @param belief  The belief where we will update the bound
     * @param value   The value on that belief
     */
    @Override
    public void update(Belief<State> belief, double value) {
        Point point = new Point(belief.toMatrixX(problem.getStates(), converter),value);
        representation.addImprovingPoint(point);
    }


    /**
     * Get the value of the bound on one point
     *
     * @param belief The point where we want the value
     * @return The value of the bound on that point
     */
    @Override
    public double getValue(Belief<State> belief) {
        return representation.getValue(belief.toMatrixX(problem.getStates(), converter));
    }


    /**
     * Initialize the bound.
     */
    private void init(double errorMargin, boolean pruning) {
        if(isUpperBound) {
            initWhenUpperBound(errorMargin, pruning);
        } else {
            initWhenLowerBound(errorMargin, pruning);
        }
    }


    /**
     * Initialize the bound when is upper bound
     */
    private void initWhenUpperBound(double errorMargin, boolean pruning) {
        Map<State,Double> upperBound = problem.getValueFunctionUpperBound(discount,errorMargin/1000);
        MatrixX cornerValues = new MatrixX(1,problem.getStates().size());

        for (State state : problem.getStates()) {
            double valueOnState = upperBound.get(state);
            cornerValues.set(0,converter.toInteger(state), valueOnState);
        }

        representation = new PointRepresentation(false, cornerValues, pruning);
    }


    /**
     * Initialize the bound when is lower bound
     */
    private void initWhenLowerBound(double errorMargin, boolean pruning) {
        Map<State,Double> lowerBound = problem.getValueFunctionLowerBound(discount,errorMargin/1000);
        MatrixX cornerValues = new MatrixX(1,problem.getStates().size());
        for(State state : problem.getStates()) {
            cornerValues.set(0,converter.toInteger(state),lowerBound.get(state));
        }
        representation = new PointRepresentation(true, cornerValues, pruning);
    }

    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return representation.getSize();
    }

    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public double getMaxSlope() {
	return representation.getMaxSlope();
    }
        
}
