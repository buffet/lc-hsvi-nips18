package mdp.solver;


import mdp.pomdp.Belief;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;
import util.function.ConeRepresentation;
import util.function.ConeRepresentation.Cone;

import java.io.Serializable;
import java.util.Map;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * 
 * <u>description:</u> Upper/Lower-bounding value function
 * approximation based on a cone (Lipschitz) representation.
 *
 * Nota: A boolean parameter indicates whether this is an upper or a
 * lower bound.
 * 
 * </pre>
 */

public class ConeBound<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>
                                                 implements Serializable {

    /**
     * The representation for the bound
     */
    private ConeRepresentation representation;

    /**
     * The State/Integer converter
     */
    private ObjectIntegerConverter<State> converter;

    /**
     * The initial corner values
     */
    private MatrixX cornerValues;

    /**
     * The used slope
     */
    private double slope;


    /**
     * Creates a lower or an upper bound without
     * The slope used is the one computed
     *
     * @param isUpperBound True if the bound is an upper bound
     * @param problem         The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public ConeBound(boolean isUpperBound, SolvableByLHSVI<State, Action, Observation> problem,
                     double discount, double errorMargin, boolean pruning) {
        super(isUpperBound, problem, discount, pruning);
        init(errorMargin);
        slope = problem.getValueFunctionLipschitzConstant(discount,errorMargin);
        representation = new ConeRepresentation(!isUpperBound,problem.getStates().size(), pruning);
    }


    /**
     * Creates a lower or an upper bound with a given slope
     *
     * @param isUpperBound  True if the bound is an upper bound
     * @param slope         Pre-computed slope of the cone bound
     * @param problem       The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public ConeBound(boolean isUpperBound, double slope, SolvableByHSVI<State, Action, Observation> problem,
                     double discount, double errorMargin, boolean pruning) {
        super(isUpperBound, problem, discount, pruning);
        init(errorMargin);
        this.slope = slope;
        representation = new ConeRepresentation(!isUpperBound, problem.getStates().size(), pruning);
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */
    @Override
    public void update(Belief<State> belief, double value) {
        Cone cone = new Cone(belief.toMatrixX(problem.getStates(),converter),value,slope);
        representation.addImprovingCone(cone);
    }


    /**
     * Get the value of the bound on one point
     *
     * @param belief The point where we want the value
     * @return The value of the bound on that point
     */
    @Override
    public double getValue(Belief<State> belief) {
        MatrixX vectorBelief = belief.toMatrixX(problem.getStates(),converter);
        double valueRepresentation = representation.getValue(vectorBelief);
        double valueCorner = cornerValues.times(vectorBelief).get(0,0);
        if(isUpperBound) {
            return min(valueRepresentation,valueCorner);
        } else {
            return max(valueRepresentation,valueCorner);
        }
    }


    /**
     * Initialize the corner values and the converter
     */
    private void init(double errorMargin) {
        converter = new ObjectIntegerConverter<>();
        for(State state : problem.getStates()) {
            converter.add(state);
        }
        if(isUpperBound) {
            initWhenUpperBound(errorMargin);
        } else {
            initWhenLowerBound(errorMargin);
        }
    }


    /**
     * Initialize the bound when is upper bound
     * Solve the problem assuming full observability, and then get the cornerValues
     */
    private void initWhenUpperBound(double errorMargin) {
        Map<State,Double> upperBound = problem.getValueFunctionUpperBound(discount,errorMargin/1000);
        cornerValues = new MatrixX(1,problem.getStates().size());

        for (State state : problem.getStates()) {
            double valueOnState = upperBound.get(state);
            cornerValues.set(0,converter.toInteger(state), valueOnState);
        }
    }


    /**
     * Initialize the bound when is lower bound
     */
    private void initWhenLowerBound(double errorMargin) {
        Map<State,Double> upperBound = problem.getValueFunctionLowerBound(discount,errorMargin/1000);
        cornerValues = new MatrixX(1,problem.getStates().size());
        for(State state : problem.getStates()) {
            cornerValues.set(0,converter.toInteger(state),upperBound.get(state));
        }
    }

    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return representation.getSize();
    }

    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public double getMaxSlope() {
	return representation.getMaxSlope();
    }
            
}
