package mdp.solver;

import mdp.pomdp.Belief;
import mdp.pomdp.POMDP;
import mdp.solver.POMDPApproximation;

import util.function.XConeRepresentation;
import util.function.XConeRepresentation.XCone;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *
 * Special interface for POMDP Approximations that should return XCones.
 *
 *  </pre>
 */

public interface XConePOMDPApproximation extends POMDPApproximation {

    // public <S,A,O> POMDPApproximation(POMDP<S,A,O> pomdp, double discount, double errorMargin, boolean isUpper);

    // public Map<S,Double> getCornerValues();

    // public double getValue(Belief belief);

    public XCone getXCone(Belief belief);
}
