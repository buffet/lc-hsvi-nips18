package mdp.solver;


import mdp.pomdp.Belief;
import Jama.Matrix;
import Jama.LUDecomposition;
import util.container.MatrixX;
import util.container.ObjectIntegerConverter;
import util.container.Pair;
import util.function.XConeRepresentation;
import util.function.XConeRepresentation.XCone;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.Queue;
import java.util.LinkedList;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 * 
 * <u>description:</u> Upper/Lower-bounding value function
 * approximation based on an xcone (Lipschitz) representation.
 *
 * Nota: A boolean parameter indicates whether this is an upper or a
 * lower bound.
 * 
 * </pre>
 */

public class XConeBound<State,Action,Observation> extends ValueFunctionBound<State,Action,Observation>
                                                 implements Serializable {

    /**
     * The representation for the bound
     */
    private XConeRepresentation representation;

    /**
     * The State/Integer converter
     */
    private ObjectIntegerConverter<State> converter;

    /**
     * The initial corner values
     */
    private MatrixX cornerValues;

    /**
     * The used fixed slope (if defined by user).
     * 'null' if undefined.
     * 
     * We expect the best approach to compute specific local slopes for each XCone.
     */
    private MatrixX slope;

    /**
     * Stored Paz matrices
     */
    Map<Pair<Action,Observation>,MatrixX> PazMatrices;

    /**
     * Creates a lower or an upper bound without
     * No fixed slope defined.
     *
     * @param isUpperBound  True if the bound is an upper bound
     * @param problem       The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public XConeBound(boolean isUpperBound, SolvableByLHSVI<State, Action, Observation> problem,
		      double discount, double errorMargin, boolean pruning) {
        super(isUpperBound, problem, discount, pruning);
        init(errorMargin);
	slope = null;
        representation = new XConeRepresentation(!isUpperBound,problem.getStates().size(), pruning);

	PazMatrices = new HashMap<>();
    }


    /**
     * Creates a lower or an upper bound with a given slope
     *
     * @param isUpperBound  True if the bound is an upper bound
     * @param scalarSlope   Value of the scalar slope (negative if it should be computed through formula)
     * @param problem       The POMDP to represent
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param pruning       True if pruning is turned on
     */
    public XConeBound(boolean isUpperBound, double scalarSlope, SolvableByLHSVI<State, Action, Observation> problem,
		      double discount, double errorMargin, boolean pruning) {
        super(isUpperBound, problem, discount, pruning);
        init(errorMargin);
	int n = problem.getStates().size();
        slope = new MatrixX(1,n,1);
	if (scalarSlope < 0)
	    scalarSlope = problem.getValueFunctionLipschitzConstant(discount, errorMargin);
	slope.timesEquals(scalarSlope);
        representation = new XConeRepresentation(!isUpperBound, problem.getStates().size(), pruning);
	
	PazMatrices = new HashMap<>();
    }

    /**
     * Transform a MatrixX into a Belief
     *
     * @param vector  The MatrixX
     * @return        The Belief associated
     */
    public Belief<State> toBelief(MatrixX vector) {
        Belief<State> belief = new Belief<>();
	double[][] A = vector.getArray();
        for(State state : problem.getStates()) {
            belief.setWeight(state,A[converter.toInteger(state)][0]);
        }
        return belief;
    }


    /**
     * Get Transition MatrixX for (a,z) pair
     *
     * @param a       The action
     * @param o       The observation
     * @return        The associated MatrixX
     */
    public MatrixX getPazMatrixX(Action a, Observation o) {
        MatrixX Paz = PazMatrices.get(new Pair<>(a,o));
	if (Paz == null) {
	    Paz = new MatrixX(problem.getStates().size(), problem.getStates().size());
	    double[][] A = Paz.getArray();
	    for(State s1 : problem.getStates()) {
		int i = converter.toInteger(s1);
		for(State s2 : problem.getStates()) {
		    int j = converter.toInteger(s2);
		    A[i][j] = problem.getObservationProbability(a,s2,o) * problem.getTransitionProbability(s1, a, s2);
		}
	    }
	    PazMatrices.put(new Pair<>(a,o), Paz);
	}
        return Paz;
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */
    @Override
    public void update(Belief<State> belief, double value) {

	if (isUpperBound)
	    updateV1Upper(belief, value);
	else
	    updateV1Lower(belief, value);
	//updateV2(belief, value); // DO NOT USE ! STUPID !
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */	 
    //@Override
    public void updateV1Upper(Belief<State> belief, double value) {
	// The new value at point "belief", "value", is given. We just
	// need to find the slope vector.
	
	XCone xcone;
	if (this.slope != null) {
	    // If slope vector has been defined manually, just copy it.
	    xcone = new XCone(belief.toMatrixX(problem.getStates(),converter), value, this.slope, !isUpperBound);
	} else {
	    // Otherwise, work.
	    int nStates = problem.getStates().size();
	    MatrixX slope = new MatrixX(1, nStates, Double.NEGATIVE_INFINITY); // row vector !
	    for(Action action : problem.getActions()) { // Compute each action's own slopes.
		MatrixX newSlope = new MatrixX(1, nStates); // create *row* vector !

		double Vmin= Double.POSITIVE_INFINITY;
		double Vmax= Double.NEGATIVE_INFINITY;
		// Compute value-correction term by taking the average
		// between reachable min and max values (depending on
		// the observation).
		Queue<XCone> FIFObetaAZ = new LinkedList<>();
		for(Observation observation : problem.getObservations()) {
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    if (bAZ.getSumOfWeights()==0.0) // In case of impossible observation
			continue;
		    double betaValue = getValueFromCorners(bAZ);
		    //XCone beta = cornerXCone(bAZ);
		    XCone betaAZ = representation.getBeta(bAZ.toMatrixX(problem.getStates(),converter), betaValue);
		    if (betaAZ == null) {
			//System.out.format("+");
			betaAZ = cornerXCone(bAZ); //beta;
		    } else {
			//System.out.format("-");
		    }
		    FIFObetaAZ.add(betaAZ);

		    double V = betaAZ.value; // + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    Vmax = max(V,Vmax);
		    Vmin = min(V,Vmin);
		}
		double k = (Vmin+Vmax)/2;

		// Compute slope vector by averaging slope vectors corresponding to each observation.
		for(Observation observation : problem.getObservations()) {
		    //double probabilityOfObservation = problem.getConditionalProbabilityOfObservation(belief,action,observation);
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    if (bAZ.getSumOfWeights()==0.0) // In case of impossible observation
			continue;
		    // XCone beta = cornerXCone(bAZ);
		    // XCone betaAZ = representation.getBeta(toMatrixX(bAZ), beta.value);
		    // if (betaAZ == null)
		    // 	betaAZ = beta;
		    XCone betaAZ = FIFObetaAZ.remove();

		    //System.out.println("betaAZ.value="+betaAZ.value);
		    //System.out.println("betaAZ.vertex="+betaAZ.vertex.toString());
		    //System.out.println("betaAZ.slope="+betaAZ.slope.toString());
		    double x = Math.abs(betaAZ.value -k ) + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    //MatrixX y = new MatrixX( ( new MatrixX(1, nStates, x)).times( getPazMatrixX(action, observation) ) );
		    //System.out.println("x="+x);
		    /**/
		    MatrixX tmp1 = new MatrixX(1, nStates, x); //System.out.println("tmp1="+tmp1.toString());
		    MatrixX tmp2 = getPazMatrixX(action, observation); //System.out.println("tmp2="+tmp2.toString());
		    MatrixX y = new MatrixX( tmp1.times( tmp2 ) ); //System.out.println("y="+y.toString());
		    /**/

		    newSlope.plusEquals( y );
		}

		// to conclude this "Bellman" update, discount and add the reward's slope vector.
		newSlope.timesEquals(discount);		
		newSlope.plusEquals( problem.getRewardLipschitzConstant( belief, action, converter) );

		// Update "worst" slope vector.
		//System.out.println("xcone.slope="+newSlope);
		slope.arrayMaxEquals(newSlope);
	    }

	    //System.out.println("xcone.slope="+slope);
	    
	    xcone = new XCone(belief.toMatrixX(problem.getStates(),converter), value, slope, !isUpperBound);
	    //System.out.println("xcone="+xcone);
	    
	}
	//representation.addImprovingXCone(xcone);
	representation.addXCone(xcone);
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */	 
    //@Override
    public void updateV1Lower(Belief<State> belief, double value) {
	// For the lower bound, we add all cones (we leave the pruning
	// of dominated cones for the usual pruning process).
	
	XCone xcone;
	if (this.slope != null) {
	    // If slope vector has been defined manually, just copy it.
	    xcone = new XCone(belief.toMatrixX(problem.getStates(),converter), value, this.slope, !isUpperBound);
	} else {
	    // Otherwise, work.
	    int nStates = problem.getStates().size();
	    MatrixX slope = new MatrixX(1, nStates, Double.NEGATIVE_INFINITY); // row vector !
	    for(Action action : problem.getActions()) { // Compute each action's own slopes.
		
		double Vmin= Double.POSITIVE_INFINITY;
		double Vmax= Double.NEGATIVE_INFINITY;
		// Compute value-correction term by taking the average
		// between reachable min and max values (depending on
		// the observation).
		Queue<XCone> FIFObetaAZ = new LinkedList<>();
		for(Observation observation : problem.getObservations()) {
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    if (bAZ.getSumOfWeights()==0.0) // In case of impossible observation
			continue;
		    double betaValue = getValueFromCorners(bAZ);
		    //XCone beta = cornerXCone(bAZ);
		    XCone betaAZ = representation.getBeta(bAZ.toMatrixX(problem.getStates(),converter), betaValue);
		    if (betaAZ == null) {
			//System.out.format("+");
			betaAZ = cornerXCone(bAZ); //beta;
		    } else {
			//System.out.format("-");
		    }
		    FIFObetaAZ.add(betaAZ);

		    double V = betaAZ.value; // + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    Vmax = max(V,Vmax);
		    Vmin = min(V,Vmin);
		}
		double k = (Vmin+Vmax)/2;

		// Compute slope vector and Q-value
		MatrixX newSlope = new MatrixX(1, nStates); // row vector !
		double Q = 0;
		for(Observation observation : problem.getObservations()) {
		    //double probabilityOfObservation = problem.getConditionalProbabilityOfObservation(belief,action,observation);
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    if (bAZ.getSumOfWeights()==0.0) // In case of impossible observation
			continue;
		    // XCone beta = cornerXCone(bAZ);
		    // XCone betaAZ = representation.getBeta(toMatrixX(bAZ), beta.value);
		    // if (betaAZ == null)
		    // 	betaAZ = beta;
		    XCone betaAZ = FIFObetaAZ.remove();

		    Q += problem.getConditionalProbabilityOfObservation(belief,action,observation)*betaAZ.value;
		    
		    //System.out.println("betaAZ.value="+betaAZ.value);
		    //System.out.println("betaAZ.vertex="+betaAZ.vertex.toString());
		    //System.out.println("betaAZ.slope="+betaAZ.slope.toString());
		    double x = Math.abs(betaAZ.value -k ) + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    //MatrixX y = new MatrixX( ( new MatrixX(1, nStates, x)).times( getPazMatrixX(action, observation) ) );
		    //System.out.println("x="+x);
		    /**/
		    MatrixX tmp1 = new MatrixX(1, nStates, x); //System.out.println("tmp1="+tmp1.toString());
		    MatrixX tmp2 = getPazMatrixX(action, observation); //System.out.println("tmp2="+tmp2.toString());
		    MatrixX y = new MatrixX( tmp1.times( tmp2 ) ); //System.out.println("y="+y.toString());
		    /**/

		    newSlope.plusEquals( y );
		}

		Q *= discount;
		Q += problem.getExpectedReward(belief, action);
		//System.out.format("(Q=%6.3e)%n",Q);

		if (Math.abs(Q-value)<0.001) {
		    
		    // to conclude this "Bellman" update, discount and add the reward's slope vector.
		    newSlope.timesEquals(discount);		
		    newSlope.plusEquals( problem.getRewardLipschitzConstant( belief, action, converter) );
		    
		    xcone = new XCone(belief.toMatrixX(problem.getStates(),converter), Q, newSlope, !isUpperBound);
		    //System.out.println("xcone="+xcone);
		    
		    //representation.addImprovingXCone(xcone);
		    representation.addXCone(xcone);
		}
	    }
	    
	}
    }

    /**
     * Update the bound
     *
     * @param belief The belief where we will update the bound
     * @param value  The value on that belief
     */	 
    //@Override
    public void updateV2(Belief<State> belief, double value) {
	// The new value at point "belief", "value", is given. We just
	// need to find the slope vector.
	
	XCone xcone;
	MatrixX mBelief = belief.toMatrixX(problem.getStates(),converter);
	if (this.slope != null) {
	    // If slope vector has been defined manually, just copy it.
	    //System.out.println("slope != null");
	    xcone = new XCone(mBelief, value, this.slope, !isUpperBound);
	} else {
	    //System.out.println("slope == null");
	    int nStates = problem.getStates().size();
	    // Warning: Do not confuse with this class's field (with the exact same name)
	    MatrixX cornerValues = new MatrixX(1, nStates,  // row vector !
					       (isUpperBound) ? Double.NEGATIVE_INFINITY : Double.POSITIVE_INFINITY);
	    for(Action action : problem.getActions()) { // Compute each action's own slopes.

		// 1- compute V correction constant k
		double Vmin= Double.POSITIVE_INFINITY;
		double Vmax= Double.NEGATIVE_INFINITY;
		// Compute value-correction term by taking the average
		// between reachable min and max values (depending on
		// the observation).
		Queue<XCone> FIFObetaAZ = new LinkedList<>();
		for(Observation observation : problem.getObservations()) {
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    double betaValue = getValueFromCorners(bAZ);
		    XCone betaAZ = representation.getBeta(bAZ.toMatrixX(problem.getStates(),converter), betaValue);
		    if (betaAZ == null)
			betaAZ = cornerXCone(bAZ);
		    FIFObetaAZ.add(betaAZ);

		    double V = betaAZ.value; // + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    Vmax = max(V,Vmax);
		    Vmin = min(V,Vmin);
		}
		double k = (Vmin+Vmax)/2;

		// 2- compute slope and Q-value
		MatrixX newSlope = new MatrixX(1, nStates); // row vector !
		double Q = 0;
		for(Observation observation : problem.getObservations()) {
		    //double probabilityOfObservation = problem.getConditionalProbabilityOfObservation(belief,action,observation);
		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
		    // XCone beta = cornerXCone(bAZ);
		    // XCone betaAZ = representation.getBeta(toMatrixX(bAZ), beta.value);
		    // if (betaAZ == null)
		    // 	betaAZ = beta;
		    XCone betaAZ = FIFObetaAZ.remove();

		    Q += problem.getConditionalProbabilityOfObservation(belief,action,observation)*betaAZ.value;
		    
		    //System.out.println("betaAZ.value="+betaAZ.value);
		    //System.out.println("betaAZ.vertex="+betaAZ.vertex.toString());
		    //System.out.println("betaAZ.slope="+betaAZ.slope.toString());
		    double x = Math.abs(betaAZ.value -k ) + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
		    //MatrixX y = new MatrixX( ( new MatrixX(1, nStates, x)).times( getPazMatrixX(action, observation) ) );
		    //System.out.println("x="+x);
		    /**/
		    MatrixX tmp1 = new MatrixX(1, nStates, x); //System.out.println("tmp1="+tmp1.toString());
		    MatrixX tmp2 = getPazMatrixX(action, observation); //System.out.println("tmp2="+tmp2.toString());
		    MatrixX y = new MatrixX( tmp1.times( tmp2 ) ); //System.out.println("y="+y.toString());
		    /**/

		    newSlope.plusEquals( y );
		}

		Q *= discount;
		Q += problem.getExpectedReward(belief, action);
		//System.out.format("(Q=%6.3e)%n",Q);
		
		newSlope.timesEquals(discount);
		newSlope.plusEquals( problem.getRewardLipschitzConstant( belief, action, converter) );

		// 3- compute "best" corner values to compute "best slope"
		// // slope.arrayMaxEquals(newSlope);
		// replaced by:
		// a- computing highest corner values
		double vtmp = Q;
		MatrixX newCornerValues;
		if (isUpperBound) {
		    vtmp += newSlope.times(mBelief).getArray()[0][0];
		    newCornerValues = new MatrixX(1, nStates, vtmp );
		    newCornerValues.plusEquals( (new MatrixX(1, nStates, 1)).minusEquals(mBelief.transpose()).arrayTimes(newSlope) );
		    //System.out.println("(corners="+newCornerValues+")");
		    cornerValues.arrayMaxEquals( newCornerValues );
		} else {
		    vtmp -= newSlope.times(mBelief).getArray()[0][0];
		    newCornerValues = new MatrixX(1, nStates, vtmp );
		    newCornerValues.minusEquals( (new MatrixX(1, nStates, 1)).minusEquals(mBelief.transpose()).arrayTimes(newSlope) );
		    //System.out.println("(corners="+newCornerValues+")");
		    cornerValues.arrayMinEquals( newCornerValues );
		}
		// b- (below, outside for loop) computing slope for these corner values
	    }

	    //System.out.println("updateV2()");
	    //long now = System.currentTimeMillis();
	    MatrixX slope = new MatrixX( computeSlope(mBelief, value, cornerValues).transpose() );
	    //System.out.println("A computeSlope() took " + (System.currentTimeMillis()-now)/1000 + " seconds.");
	    slope.arrayAbsEquals();

	    //System.out.println("vCorners="+cornerValues);
	    //System.out.println("xcone.slope="+slope);
	    xcone = new XCone(belief.toMatrixX(problem.getStates(),converter), value, slope, !isUpperBound);
	    //System.out.println("xcone="+xcone);
	}
	    //representation.addImprovingXCone(xcone);
	    representation.addXCone(xcone);
    }


    // /**
    //  * Update the bound
    //  *
    //  * @param belief The belief where we will update the bound
    //  * @param value  The value on that belief
    //  */	 
    // //@Override
    // public void updateV2(Belief<State> belief, double value) {
    // 	if (this.slope != null) {
    // 	    XCone xcone = new XCone(toMatrixX(belief), value, this.slope, !isUpperBound);
    // 	    //representation.addImprovingXCone(xcone);
    // 	    representation.addXCone(xcone);
    // 	} else {
    // 	    int nStates = problem.getStates().size();
    // 	    //System.out.println("belief: "+belief+" value: "+value);
    // 	    for(Action action : problem.getActions()) {
    // 		double Vmin= Double.POSITIVE_INFINITY;
    // 		double Vmax= Double.NEGATIVE_INFINITY;
    // 		for(Observation observation : problem.getObservations()) {
    // 		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
    // 		    XCone beta = cornerXCone(bAZ);
    // 		    XCone betaAZ = representation.getBeta(toMatrixX(bAZ), beta.value);
    // 		    if (betaAZ == null)
    // 			betaAZ = beta;

    // 		    double V = betaAZ.value;
    // 		    Vmax = max(V,Vmax);
    // 		    Vmin = min(V,Vmin);
    // 		}
    // 		double k = (Vmin+Vmax)/2;

    // 		double Q = 0;
    // 		MatrixX slope = new MatrixX(1, nStates); // row vector !
    // 		for(Observation observation : problem.getObservations()) {
    // 		    Belief<State> bAZ = problem.getNewBelief(belief,action,observation);
    // 		    XCone beta = cornerXCone(bAZ);
    // 		    XCone betaAZ = representation.getBeta(toMatrixX(bAZ), beta.value);
    // 		    if (betaAZ == null)
    // 			betaAZ = beta;

    // 		    Q += problem.getConditionalProbabilityOfObservation(belief,action,observation) * betaAZ.value;

    // 		    //System.out.println("betaAZ.value="+betaAZ.value);
    // 		    //System.out.println("betaAZ.vertex="+betaAZ.vertex.toString());
    // 		    //System.out.println("betaAZ.slope="+betaAZ.slope.toString());
    // 		    double x = Math.abs(betaAZ.value -k ) + ((betaAZ.slope).times(betaAZ.vertex)).getArray()[0][0];
    // 		    //MatrixX y = new MatrixX( ( new MatrixX(1, nStates, x)).times( getPazMatrixX(action, observation) ) );
    // 		    //System.out.println("x="+x);
    // 		    /**/
    // 		    MatrixX tmp1 = new MatrixX(1, nStates, x); //System.out.println("tmp1="+tmp1.toString());
    // 		    MatrixX tmp2 = getPazMatrixX(action, observation); //System.out.println("tmp2="+tmp2.toString());
    // 		    MatrixX y = new MatrixX( tmp1.times( tmp2 ) ); //System.out.println("y="+y.toString());
    // 		    /**/

    // 		    slope.plusEquals( y );
    // 		}

    // 		slope.timesEquals(discount);		
    // 		slope.plusEquals( problem.getRewardLipschitzConstant( belief, action, converter) );
		
    // 		Q *= discount;
    // 		Q += problem.getExpectedReward(belief, action);

    // 		//System.out.println("xcone.slope="+slope.toString());
    // 		XCone xcone = new XCone(toMatrixX(belief), Q, slope, !isUpperBound);
    // 		//System.out.println("xcone="+xcone);
    // 		//representation.addImprovingXCone(xcone);
    // 		representation.addXCone(xcone);
    // 	    }
    // 	}
    // }


    /**
     * Get the value of the bound on one point
     *
     * @param belief The point where we want the value
     * @return The value of the bound on that point
     */
    @Override
    public double getValue(Belief<State> belief) {
	MatrixX vectorBelief = belief.toMatrixX(problem.getStates(),converter);
        double valueRepresentation = representation.getValue(vectorBelief);
        double valueFromCorners = cornerValues.times( vectorBelief ).getArray()[0][0];
	//System.out.format("valueRepresentation=%.3f%n",valueRepresentation);
	//System.out.format("valueFromCorners=%.3f%n",valueFromCorners);
	double value;
        if(isUpperBound) {
	    value = min(valueRepresentation,valueFromCorners);
        } else {
            value = max(valueRepresentation,valueFromCorners);
        }
	return value;
    }


    /**
     * Initialize the corner values and the converter
     */
    private void init(double errorMargin) {
        converter = new ObjectIntegerConverter<>();
        for(State state : problem.getStates()) {
            converter.add(state);
        }
        if(isUpperBound) {
            initWhenUpperBound(errorMargin);
	    System.out.println("Upper bound initialized.");
        } else {
            initWhenLowerBound(errorMargin);
	    System.out.println("Lower bound initialized.");
        }
    }


    /**
     * Initialize the bound when is upper bound
     * Solve the problem assuming full observability, and then get the cornerValues
     */
    private void initWhenUpperBound(double errorMargin) {
        Map<State,Double> upperBound = problem.getValueFunctionUpperBound(discount,errorMargin/1000);
        cornerValues = new MatrixX(1,problem.getStates().size());

        for (State state : problem.getStates()) {
            cornerValues.getArray()[0][converter.toInteger(state)] = upperBound.get(state);
        }
    }


    /**
     * Initialize the bound when is lower bound
     */
    private void initWhenLowerBound(double errorMargin) {
        Map<State,Double> lowerBound = problem.getValueFunctionLowerBound(discount,errorMargin/1000);
        cornerValues = new MatrixX(1,problem.getStates().size());
	
        for(State state : problem.getStates()) {
            cornerValues.getArray()[0][converter.toInteger(state)] = lowerBound.get(state);
        }
    }

    private double getValueFromCorners(Belief<State> belief) {
	int nStates = problem.getStates().size();
	MatrixX bMatrixX = belief.toMatrixX(problem.getStates(),converter);
	return cornerValues.times( bMatrixX ).getArray()[0][0];
    }
    
    /**
     * get xcone wrt the corner values and local belief
     */
    private XCone cornerXCone(Belief<State> belief) {
	//return cornerXConeV1(belief);
	//return cornerXConeV2(belief);
	return cornerXConeV3(belief);
    }
    
    private XCone cornerXConeV1(Belief<State> belief) {
	//System.out.println("cornerXCone v1()");
	int nStates = problem.getStates().size();
	MatrixX bMatrixX = belief.toMatrixX(problem.getStates(),converter);
	double valueFromCorners = cornerValues.times( bMatrixX ).getArray()[0][0];

	MatrixX slopeFromCorners = new MatrixX(1, nStates);
	double[][] A = slopeFromCorners.getArray();
	double[][] B = bMatrixX.getArray();
	double[][] C = cornerValues.getArray();
	for(int i=0; i<nStates; i++) {
	    if (B[i][0] != 1)
		A[0][i] = Math.abs( ( C[0][i] - valueFromCorners ) / ( 1 - B[i][0] ) );
	    else
		A[0][i] = 0;
	}
	
	XCone xcone = new XCone(bMatrixX, valueFromCorners, slopeFromCorners, !isUpperBound);
	return xcone;
    }

    private XCone cornerXConeV2(Belief<State> belief) {
	//System.out.println("cornerXCone v2()");
	int nStates = problem.getStates().size();
	MatrixX bMatrixX = belief.toMatrixX(problem.getStates(),converter);
	double valueFromCorners = cornerValues.times( bMatrixX ).getArray()[0][0];

	MatrixX slopeFromCorners = new MatrixX(1, nStates);
	/*
	double[][] A = slopeFromCorners.getArray();
	double[][] B = bMatrixX.getArray();
	double[][] C = cornerValues.getArray();
	for(int i=0; i<nStates; i++) {
	    if (B[i][0] != 1)
		A[0][i] = Math.abs( ( C[0][i] - valueFromCorners ) / ( 1 - B[i][0] ) );
	    else
		A[0][i] = 0;
	}
	XCone xcone = new XCone(bMatrixX, valueFromCorners, slopeFromCorners, !isUpperBound);
	*/

	//long now = System.currentTimeMillis();
	MatrixX slope = new MatrixX( computeSlope(bMatrixX, valueFromCorners, cornerValues).transpose() );
	//System.out.println("B computeSlope() took " + (System.currentTimeMillis()-now) + " us.");
	slope.arrayAbsEquals();
	
	XCone xcone = new XCone(bMatrixX, valueFromCorners,
				slope,
				!isUpperBound);
	return xcone;
    }

    private XCone cornerXConeV3(Belief<State> belief) {
	//System.out.println("cornerXCone v3()");
	int nStates = problem.getStates().size();
	MatrixX bMatrixX = belief.toMatrixX(problem.getStates(),converter);
	double valueFromCorners = cornerValues.times( bMatrixX ).getArray()[0][0];

	MatrixX slopeFromCorners = new MatrixX(1, nStates);
	double[][] A = slopeFromCorners.getArray();
	double[][] C = cornerValues.getArray();
	if (isUpperBound) {
	    double Vmin = Double.POSITIVE_INFINITY;
	    for(int i=0; i<nStates; i++) {
		if (C[0][i]<Vmin)
		    Vmin=C[0][i];
	    }
	    for(int i=0; i<nStates; i++) {
		A[0][i] = C[0][i] - Vmin;
	    }	    
	} else {
	    double Vmax = Double.NEGATIVE_INFINITY;
	    for(int i=0; i<nStates; i++) {
		if (C[0][i]>Vmax)
		    Vmax = C[0][i];
	    }
	    for(int i=0; i<nStates; i++) {
		A[0][i] = Vmax - C[0][i];
	    }	    
	}
	XCone xcone = new XCone(bMatrixX, valueFromCorners, slopeFromCorners, !isUpperBound);
	
	return xcone;
    }

    /**
     * Take (i) a belief b, (ii) its associated value v, and (iii) vector of corner values vc,
     * and compute the (positive/negative) slopes of the corresponding cone.
     * [Requires inverting a matrix. Uses an LU decomposition to that end.]
     *
     * @param b belief
     * @param v associated value
     * @param vc vector of corner values
     * @return (positive/negative) slopes (vector) of the corresponding cone
     */
    private MatrixX computeSlopeOLD(MatrixX b, double v, MatrixX vc) {
	MatrixX slope;
	
	int nStates = problem.getStates().size();

	//System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);

	double[][] bA = b.getArray();

	Matrix B = ( new Matrix(nStates, 1, -v) ).plusEquals( vc.transpose() );
	//System.out.println("B="+new MatrixX( B ));
	    
	MatrixX A = new MatrixX(nStates, nStates);
	double[][] aA = A.getArray();
	for(int i=0; i<aA.length; i++) { // i = row
	    for(int j=0; j<aA[0].length; j++) { // j = column
		aA[i][j] = (i==j) ? 1-bA[j][0] : bA[j][0];
	    }
	}
	//System.out.println("A="+A);
	
	LUDecomposition lu = new LUDecomposition( A );
	//System.out.format("det(A)= %6.3e%n", lu.det() );
	if (lu.isNonsingular() && lu.det()>0.0000001) {
	    //System.out.println("<LU decomposition> matrix A is non singular.");
	    slope = new MatrixX( lu.solve( B ) );
	} else {
	    System.out.println("<LU decomposition> matrix A is singular.");
	    System.out.format("%ncomputeSlope( b=%s, \n v=%6.3e, \n vc=%s );%n",b,v,vc);
	    System.out.println("A="+A);
	    System.out.println("B="+new MatrixX(B));

	    /*
	     * Does b correspond to a single state?
	     */
	    if (Math.abs(b.normInf()-1)<0.01) {
		//System.out.println("b corresponds to a single state.");
		slope = new MatrixX( B );
		double[][] slopeA = slope.getArray();
		for(int i=0; i<nStates; i++) {
		    if (Math.abs(aA[i][i]) <= 0.01) {
			slopeA[i][0] = 0;
			break;
		    }
		}
	    } else {
		/*
		 * Are only two states probable?
		 */
		int count=0;
		int i1=-1;
		int i2=-1;
		for(int i=0; i<nStates; i++) {
		    if (Math.abs(bA[i][0]) > 0.000001) {
			count++;
			if (i1==-1)
			    i1 = i;
			else
			    i2 = i;
		    }
		}
		if (count!=2) {
		    System.out.format("WHAT'S HAPPENING HERE?! %d non-zero components in b!%n",count);
		    System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);
		    //System.out.format("b=%s%n",b);
		    System.exit(0);
		}
		//System.out.println("Only two states have non-zero probability.");

		MatrixX xA = new MatrixX(nStates+1, nStates);
		double[][] AA = A.getArray();
		double[][] xAA = xA.getArray();
		for(int i=0; i<nStates; i++) { // row
		    for(int j=0; j<nStates; j++) { // column
			xAA[i][j] = AA[i][j];
		    }
		}
		xAA[nStates][i1] = +1;
		xAA[nStates][i2] = -1;

		Matrix xB = new Matrix(nStates+1, 1);
		double[][] xBA = xB.getArray();
		double[][] BA = B.getArray();
		for(int i=0; i<nStates; i++) { // row
		    xBA[i][0] = BA[i][0];
		}
		xBA[nStates][0] = 0;

		//System.out.println("xA="+xA);
		//System.out.println("xB="+new MatrixX(xB));
		lu = new LUDecomposition( xA );
		//System.out.format("det(xA)= %6.3e%n", lu.det() );
		if (! lu.isNonsingular()) {
		    System.out.println("xA is singular !!!");
		    System.exit(0);
		}
		slope = new MatrixX( lu.solve( xB ).getMatrix(0,nStates-1,0,0) );
	    }
	    
	}
	
    	//System.out.println("A^-1 B ="+slope);
	
	return slope;
    }
    
    private MatrixX computeSlope(MatrixX b, double v, MatrixX vc) {
	
	int nStates = problem.getStates().size();
	MatrixX slope = new MatrixX(nStates, 1, 0.0);

	//System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);
	
	double[][] bA = b.getArray();
	int nAS = 0; // number of actual non-zero states in b
	for(int i=0; i<nStates; i++) {
	    if (bA[i][0] != 0.)
		nAS++;
	}

	// if (nAS==2) {
	//     System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);
	//     System.out.format("2!   ");
	// }
	//System.out.println("nAS="+nAS);
	
	if (nAS==1) {
	    slope = new MatrixX(nStates, 1, 0.0);
	    return slope;
	}
	
	Matrix b2 = new Matrix(nAS, 1, 0); double[][] b2A = b2.getArray();
	Matrix B2 = new Matrix(nAS, 1, -v); double[][] B2A = B2.getArray();
	double[][] vcA = vc.getArray();
	int ii=0;
	for(int i=0; i<nStates; i++) {
	    if (bA[i][0] != 0.) {
		b2A[ii][0] = bA[i][0];
		B2A[ii][0] += vcA[0][i];
		ii++;
	    }
	}
	
	//System.out.format("%n b2=%s, %n B2=%s%n",new MatrixX(b2), new MatrixX(B2));
	    
	MatrixX A2 = new MatrixX(nAS, nAS);
	double[][] A2A = A2.getArray();
	for(int i=0; i<A2A.length; i++) { // i = row
	    for(int j=0; j<A2A[0].length; j++) { // j = column
		A2A[i][j] = (i==j) ? 1-b2A[j][0] : b2A[j][0];
	    }
	}
	//System.out.println("A="+A2);

	MatrixX slope2 = null;
	    
	LUDecomposition lu = new LUDecomposition( A2 );
	//if ((nAS==2) && (A2A[0][0]==A2A[1][0])) {
	if ((nAS==2) && (!lu.isNonsingular())) {
	    //System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);
	    //System.out.format("%n b2=%s, %n B2=%s%n",new MatrixX(b2), new MatrixX(B2));
	    //...

	    MatrixX xA2 = new MatrixX(nAS+1, nAS);
	    double[][] xA2A = xA2.getArray();
	    for(int i=0; i<nAS; i++) { // row
		for(int j=0; j<nAS; j++) { // column
		    xA2A[i][j] = A2A[i][j];
		}
	    }
	    xA2A[nAS][0] = +1;
	    xA2A[nAS][1] = -1;
	    
	    Matrix xB2 = new Matrix(nAS+1, 1);
	    double[][] xB2A = xB2.getArray();
	    for(int i=0; i<nAS; i++) { // row
		xB2A[i][0] = B2A[i][0];
	    }
	    xB2A[nAS][0] = 0;
	    
	    //System.out.println("xA="+xA);
	    //System.out.println("xB="+new MatrixX(xB));
	    lu = new LUDecomposition( xA2 );
	    //System.out.format("det(xA)= %6.3e%n", lu.det() );
	    if (! lu.isNonsingular()) {
		System.out.println("xA2 is singular !!!");System.out.format("%ncomputeSlope( b=%s, v=%6.3e, vc=%s );%n",b,v,vc);
		System.out.format("%n b2=%s, %n B2=%s%n",new MatrixX(b2), new MatrixX(B2));
		System.exit(0);
	    }
	    slope2 = new MatrixX( lu.solve( xB2 ).getMatrix(0,nAS-1,0,0) );
	} else {
	    //lu = new LUDecomposition( A2 );
	    //System.out.format("det(A)= %6.3e%n", lu.det() );
	    if (lu.isNonsingular()) {
		//System.out.println("<LU decomposition> matrix A is non singular.");
		slope2 = new MatrixX( lu.solve( B2 ) );
	    } else {
		System.out.println("<LU decomposition> matrix A is singular.");
		System.out.format("%ncomputeSlope( b=%s, \n v=%6.3e, \n vc=%s );%n",b,v,vc);
		System.out.println("A2="+A2);
		System.out.println("B2="+new MatrixX(B2));
		
		System.out.println("xA is singular !!!");
		System.exit(0);
	    }
	}
	    
	ii=0;
	double[][] slopeA = slope.getArray();
	double[][] slope2A = slope2.getArray();
	for(int i=0; i<nStates; i++) {
	    if (bA[i][0] != 0.) {
		slopeA[i][0] = slope2A[ii][0];
		ii++;
	    }
	}
	
    	//System.out.println("A^-1 B ="+slope);
	
	return slope;
    }
    
    /**
     * Get the size of the bound (number of data points)
     *
     * @return The number of data points
     */
    public int getSize() {
	return representation.getSize();
    }

    /**
     * Get the maximum slope
     *
     * @return The maximum slope
     */    
    public double getMaxSlope() {
	return representation.getMaxSlope();
    }
    
}
