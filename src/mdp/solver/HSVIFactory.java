package mdp.solver;

import mdp.pomdp.POMDP;
import mdp.rhopomdp.RhoPOMDP;

public class HSVIFactory {

    public enum BoundType { hyperplane, sawtooth, conic, xconic, point}


    /**
     * Creates a new classic HSVI solver (the original one)
     *
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildClassicHSVI(POMDP<S,A,O> pomdp,
						       double discount, double errorMargin,
						       Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new HyperplaneLowerBound<>(pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new SawtoothBound<>(true,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }


    /**
     * Creates a HSVI solver for a convex rho-pomdp
     *
     * @param rhoPOMDP      The RhoPOMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildRhoHSVI(RhoPOMDP<S,A,O> rhoPOMDP,
						   double discount, double errorMargin,
						   Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new HyperplaneLowerBound<>(rhoPOMDP,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new SawtoothBound<>(true,rhoPOMDP,discount,errorMargin,pruning);
        return new HSVI<>(rhoPOMDP,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }



    /**
     * Creates a new conic HSVI solver
     *
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
      * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildConicHSVI(SolvableByLHSVI<S,A,O> pomdp,
						     double discount, double errorMargin,
						     Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new ConeBound<>(false,pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new ConeBound<>(true,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }


    /**
     * Creates a new conic HSVI solver with a given slope
     *
     * @param slope         The slope of the conic
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildConicHSVIWithSlope(double slope, SolvableByHSVI<S,A,O> pomdp,
                                                              double discount, double errorMargin,
							      Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new ConeBound<>(false,slope,pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new ConeBound<>(true,slope,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }

    /**
     * Creates a new Xconic HSVI solver
     *
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildXConicHSVI(SolvableByLHSVI<S,A,O> pomdp,
						      double discount, double errorMargin,
						      Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new XConeBound<>(false,pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new XConeBound<>(true,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }

    /**
     * Creates a new Xconic HSVI solver with a given slope
     *
     * @param slope         The slope of the conic
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildXConicHSVIWithSlope(double slope, SolvableByLHSVI<S,A,O> pomdp,
                                                              double discount, double errorMargin,
							      Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new XConeBound<>(false,slope,pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new XConeBound<>(true,slope,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }


    /**
     * Creates a new classic HSVI solver (the original one)
     *
     * @param pomdp         The POMDP to represent
     * @param <S>           The state class
     * @param <A>           The action class
     * @param <O>           The observation class
     * @param discount      The discount factor for rewards
     * @param errorMargin   The allowed error
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param pruning       True if pruning is turned on
     * @return  A new instance of a classic HSVI solver
     */
    public static <S,A,O> HSVI<S,A,O> buildPointHSVI(POMDP<S,A,O> pomdp,
						       double discount, double errorMargin,
						       Boolean exLXU, Boolean exNUI, Boolean pruning) {
        ValueFunctionBound<S,A,O> lowerBound = new PointBound<>(false,pomdp,discount,errorMargin,pruning);
        ValueFunctionBound<S,A,O> upperBound = new PointBound<>(true,pomdp,discount,errorMargin,pruning);
        return new HSVI<>(pomdp,discount,errorMargin,lowerBound,upperBound,exLXU,exNUI);
    }


    
}
