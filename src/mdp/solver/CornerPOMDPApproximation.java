package mdp.solver;


import mdp.policy.InfiniteHorizonDeterministicPolicy;
import mdp.pomdp.POMDP;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * <pre>
 *
 * Special class providing methods each computing a single hyperplane
 * upper- or lower-bounding the optimal value function of a POMDP.
 *
 *  </pre>
 */

public class CornerPOMDPApproximation {

    /**
     * Upper Bound Initialization:
     * #1: solve MDP
     */
    public static <S,A,O> Map<S,Double> getUpperWithMDPSolving(POMDP<S,A,O> pomdp, double discount, double errorMargin) {
        InfiniteHorizonMDPSolver<S, A> solver = new InfiniteHorizonMDPSolver<>();
        InfiniteHorizonDeterministicPolicy<S, A> policy = solver.findOptimalPolicy(pomdp.mdp, discount, errorMargin);

        Map<S,Double> cornerValues = new HashMap<>();

        for (S state : pomdp.getStates()) {
            double valueOnState = policy.getValue(state) + errorMargin;
            cornerValues.put(state,valueOnState);
        }

        return cornerValues;
    }

    /**
     * _TODO_
     * Upper Bound Initialization:
     * #2: Fast Informed Bound (FIB)
     */
    /**/
    public static <S,A,O> Map<S,Double> getUpperWithFastInformedBound(POMDP<S,A,O> pomdp, double discount, double errorMargin) {

	/*
	 * Cf Trey Smith's PhD thesis (2007), Algorithm 4.2, page 81.
	 */
	// {Line 3}
	Map<S,Double>  MDPsolution = getUpperWithMDPSolving(pomdp, discount, errorMargin);

	// {Lines 4-5}
	Map<A,Map<S,Double>> alphaA = new HashMap<>();
	for (A action : pomdp.getActions()) {
	    alphaA.put(action, MDPsolution);
	}

	double epsilon;
	do {

	    Map<A,Map<S,Double>> betaA = new HashMap<>();
	    epsilon = Double.NEGATIVE_INFINITY;
	    for (A action : pomdp.getActions()) {
		betaA.put(action,new HashMap<>());
		Map<O,Map<S,Double>> betaAO = new HashMap<>();
		
		for (O observation : pomdp.getObservations()) {
		    betaAO.put(observation, new HashMap<>());
		    
		    for (S state : pomdp.getStates()) {
			double max = Double.NEGATIVE_INFINITY;
			for (A action2 : pomdp.getActions()) {
			    double tmp = 0.0;
			    for (S state2 : pomdp.getStates()) {
				tmp += pomdp.getTransitionProbability(state, action, state2)
				    * pomdp.getObservationProbability(action, state2, observation)
				    * (alphaA.get(action2)).get(state2);
			    }
			    if (tmp > max)
				max = tmp;
			}
			(betaAO.get(observation)).put(state, max);
		    }    
		}
			  
		for (S state : pomdp.getStates()) {
		    double reward = 0.0;
		    for (S state2 : pomdp.getStates()) {
			reward += pomdp.getTransitionProbability(state, action, state2)
			    * pomdp.getReward(state, action, state2);
		    }
		    double sum = 0.0;
		    for (O observation : pomdp.getObservations()) {
			sum += (betaAO.get(observation)).get(state);
		    }
		    double tmp = reward + discount * sum;
		    (betaA.get(action)).put(state, tmp);

		    double epsilon2 = Math.abs( tmp - (alphaA.get(action)).get(state) );
		    if (epsilon2 > epsilon)
			epsilon = epsilon2;
		}
			  
	    }
	    
	    for (A action : pomdp.getActions()) {
		alphaA.put(action, betaA.get(action));
	    }

	} while (epsilon/(1-discount) >= errorMargin);

	//return alphaA;
	
        Map<S,Double> values = new HashMap<>();
	
        for(S state : pomdp.getStates()) {
            values.put(state,Double.NEGATIVE_INFINITY);
        }
        for(A action : pomdp.getActions()) {
            for(S state : pomdp.getStates()) {
                if(alphaA.get(action).get(state) > values.get(state)) {
                    values.put(state, alphaA.get(action).get(state));
                }
            }
        }
        return values;
    }
    /**/


    /**
     * Lower Bound Initialization:
     * #1: WorstRewardBestAction
     * i- Chercher l'action dont la pire récompense sur tous les triplets (s,s',o') soit la meilleure
     * ii- Diviser cette pire récompense par (1-discount).
     */
    public static <S,A,O> Map<S,Double> getLowerWithWorstRewardBestAction(POMDP<S,A,O> pomdp, double discount, double errorMargin) {
        double maxi = Double.NEGATIVE_INFINITY;
        for(A action : pomdp.getActions()) {
            double mini = Double.POSITIVE_INFINITY;

            for(S beginState : pomdp.getStates()) {
                for(S endState : pomdp.getPossibleNextStates(beginState,action)) {
                    for(O observation : pomdp.getPossibleObservations(action,endState)) {
                        mini = min(mini,pomdp.getReward(beginState, action, endState));
                    }
                }
            }
            maxi = max(maxi,mini);
        }
        double value = maxi/(1-discount);
        Map<S,Double> result = new HashMap<>();
        for(S state : pomdp.getStates()) {
            result.put(state,value);
        }
        return result;
    }

    /**
     * Lower Bound Initialization:
     * #2: bestAction
     * i- Compute one hyperplane per action $action$, corresponding this action is always performed.
     * ii- Generate a "worst-case" hyperplane by taking, for each corner/state, the worst value among the possible single-action policies.
     * Comment: A better option is to keep one hyperplane per action!!!
     */
    public static <S,A,O> Map<S,Double> getLowerWithBestAction(POMDP<S,A,O> pomdp, double discount, double errorMargin) {
        Map<S,Double> values = new HashMap<>();
        for(S state : pomdp.getStates()) {
            values.put(state,Double.POSITIVE_INFINITY); // was: NEGATIVE_INFINITY (which seems wrong)
        }
        for(A action : pomdp.getActions()) {
            Map<S,Double> valuesForAction = getLowerWithAction(action,pomdp,discount,errorMargin);
            for(S state : pomdp.getStates()) {
                if(valuesForAction.get(state) < values.get(state)) {
                    values.put(state,valuesForAction.get(state));
                }
            }
        }
        return values;
    }

    /**
     * Given $action$, compute hyperplane (/$\alpha$-vector) corresponding to always performing that action.
     */
    private static <S,A,O> Map<S,Double> getLowerWithAction(A action, POMDP<S,A,O> pomdp, double discount, double errorMargin) {
        Map<S,Double> alpha = getLowerWithWorstRewardBestAction(pomdp,discount,errorMargin);
        Map<S,Double> newAlpha = getLowerWithActionOneStep(action,alpha,pomdp,discount);
        while(!isResidualGoodEnough(alpha,newAlpha,discount,errorMargin)) {
            alpha = newAlpha;
            newAlpha = getLowerWithActionOneStep(action,alpha,pomdp,discount);
        }
        return newAlpha;
    }

    /**
     * Compute new hyperplane given $action$ (performed once) and hyperplane $\alpha$.
     */
    private static <S,A,O> Map<S,Double> getLowerWithActionOneStep(A action, Map<S,Double> alpha, POMDP<S,A,O> pomdp,
                                                                   double discount) {
        Map<S,Double> newAlpha = new HashMap<>();
        for(S beginState : pomdp.getStates()) {
            double temp = 0;
            for(S endState : pomdp.getStates()) {
                temp += alpha.get(endState) * pomdp.getTransitionProbability(beginState, action, endState);
            }
            temp *= discount;
            temp += pomdp.getExpectedReward(beginState,action);
            newAlpha.put(beginState,temp);
        }
        return newAlpha;
    }

    private static <S,A,O> boolean isResidualGoodEnough(Map<S,Double> oldMap, Map<S,Double> newMap, double discount, double errorMargin) {
        double value = Double.NEGATIVE_INFINITY;
        for(S state : newMap.keySet()) {
            value = max(value,abs(oldMap.get(state) - newMap.get(state)));
        }
        return value <= (1-discount) * errorMargin * 0.01;
    }

}
