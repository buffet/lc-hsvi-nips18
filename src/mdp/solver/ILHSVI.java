package mdp.solver;

import mdp.pomdp.Belief;

public class ILHSVI<State,Action,Observation> {


    /**
     * The last slope value used to get results
     */
    private double lastSlope;

    /**
     * The pomdp problem
     */
    private SolvableByHSVI<State, Action, Observation> problem;

    /**
     * The printer used to save files
     */
    private HSVIPythonPrinter<State,Action,Observation> printer;

    /**
     * The discount factor
     */
    double discount;

    /**
     * The allowed error
     */
    double errorMargin;

    
    /**
     * Whether to check for crossing bounds
     */
    private Boolean exLXU;

    
    /**
     * Whether to check for non-uniformly improvable bounds
     */
    private Boolean exNUI;
    
    /**
     * Whether to check for unstable results
     */
    private Boolean exUR;
    
    /**
     * Is true if one wants the pruning process to be applied.
     */
    private boolean pruning;
    
    /**
     * The current HSVI used
     */
    private HSVI<State, Action, Observation> hsvi;

    public HSVI<State, Action, Observation> getHSVI() {
	return hsvi;
    }
    
    /**
     * Initialize the starting slope
     *
     * @param problem       The problem
     * @param discount      Discount factor \gamma
     * @param errorMargin   The error margin
     * @param exLXU         True if algorithm should check whether the lower and upper bounds cross each other
     * @param exNUI         True if algorithm should check that bounds are uniformly improvable
     * @param exUR          True if algorithm should check that results are stable from one iteration to the next
     * @param pruning       True if pruning is turned on
     */
    public ILHSVI(SolvableByHSVI<State, Action, Observation> problem,
		  double discount, double errorMargin,
		  Boolean exLXU, Boolean exNUI, Boolean exUR, Boolean pruning) {
        lastSlope = 1;
        this.problem = problem;
        this.discount = discount;
        this.errorMargin = errorMargin;
	this.exLXU = exLXU;
	this.exNUI = exNUI;
	this.exUR = exUR;
	this.pruning = pruning;
	this.printer = printer;
        hsvi = HSVIFactory.buildConicHSVIWithSlope(lastSlope,problem,discount,errorMargin,exLXU,exNUI, pruning);
    }


    /**
     * Get the optimal value on a belief
     *
     * @param belief       the belief where to get the value
     * @param timelimit    when the algorithm should stop (time point in milliseconds)
     * @param iterations   after how many iterations the algorithm should stop
     * @return The optimal value on the belief
     */
    public double getOptimalValue(Belief<State> belief, long time, long timelimit, int iterations, HSVIPythonPrinter printer) {
	//return getOptimalValueV1(belief, time, timelimit, iterations, printer);
	return getOptimalValueV2(belief, time, timelimit, iterations, printer);
    }

    public double getOptimalValueV1(Belief<State> belief, long time, long timelimit, int iterations, HSVIPythonPrinter printer) {
	double newL = Double.POSITIVE_INFINITY;
        while (true) {
            try {
		newL = hsvi.getOptimalValue(belief, time, timelimit, iterations, printer);

		if (timelimit > 0 && System.currentTimeMillis() >= timelimit) {
		    System.out.println("!! Timeout");
		}

		return newL;
		    
            } catch (IllegalStateException exception) {
                System.out.println("Test another one " + lastSlope * 2);
                lastSlope *= 2;
                hsvi = HSVIFactory.buildConicHSVIWithSlope(lastSlope,problem,discount,errorMargin,exLXU,exNUI,pruning);
            }
        }
    }

    public double getOptimalValueV2(Belief<State> belief, long time, long timelimit, int iterations, HSVIPythonPrinter printer) {
	this.printer = printer;
	double oldL = Double.POSITIVE_INFINITY;
	double newL = Double.POSITIVE_INFINITY;
	double oldU = Double.POSITIVE_INFINITY;
	double newU = Double.POSITIVE_INFINITY;
	
        while (true) {
            try {
		oldL = newL;
		oldU = newU;
		System.out.format("lambda= %f%n",lastSlope);
                newL = hsvi.getOptimalValue(belief, time, timelimit, iterations, printer);
		newU = hsvi.upperBound.getValue(belief);
		
		// System.out.format("oldL=%f newL=%f oldL-newL=%f%n",oldL,newL,oldL-newL);
		
		if (timelimit > 0 && System.currentTimeMillis() >= timelimit) {
		    if (printer != null)
			printer.savePythonProgram();
		    return newL;
		    // //System.out.println("!! Timeout");
		    // if (!exUR) {
		    // 	return newL;
		    // } else {
		    // 	if (Math.abs(oldL-newL) < errorMargin) {
		    // 	    System.out.println("OK. Stable result.");
		    // 	} else {
		    // 	    System.out.println("!! UNSTABLE RESULT.");
		    // 	}
		    // 	return newL;
		    // }
		}

		if (!exUR) {
		    if (printer != null)
			printer.savePythonProgram();
		    return newL;
		} else {
		    if ( (Math.abs(oldL-newL) < errorMargin/10.) && 
			 (Math.abs(oldU-newU) < errorMargin/10.) ) {
			System.out.println("OK. Stable result.");
			
			if (printer != null)
			    printer.savePythonProgram();
			return newL;
		    } else {
			System.out.println("oldL="+oldL+" newL="+newL);
			System.out.println("oldU="+oldU+" newU="+newU);
			System.out.println("!! UNSTABLE RESULT.");
			//System.out.println("Test another one " + lastSlope * 2);
			lastSlope *= 2;
			hsvi = HSVIFactory.buildConicHSVIWithSlope(lastSlope,problem,discount,errorMargin,exLXU,exNUI,pruning);
			if (printer != null)
			    printer.setBounds(hsvi.lowerBound,hsvi.upperBound);
		    }
		}
            } catch (IllegalStateException exception) {
                //System.out.println("Test another one " + lastSlope * 2);
                lastSlope *= 2;
                hsvi = HSVIFactory.buildConicHSVIWithSlope(lastSlope,problem,discount,errorMargin,exLXU,exNUI,pruning);
		if (printer != null)
		    printer.setBounds(hsvi.lowerBound,hsvi.upperBound);
            }
	    
        }

	
    }


    /**
     * Find the optimal strategy for a given initial belief, and print the steps of the algorithms with a python file
     *
     * @param scriptPath    The path of the file
     * @param timelimit    when the algorithm should stop (time point in milliseconds)
     * @param iterations   after how many iterations the algorithm should stop
     * @param imageNameBase The base name of the image
     * @param belief        The belief where we want a good approximation
     */
    // public void solveAndPrint(Belief<State> belief, long time, long timelimit, int iterations, String scriptPath, String imageNameBase) {
    //     for (int i = 0; ; i++) {
    //         try {
    //             hsvi.solveAndPrint(belief, time, timelimit, iterations, scriptPath + "-" + lastSlope + "-.py", imageNameBase + "-" + lastSlope + "-.gif");
    //             return;
    //         } catch (IllegalStateException exception) {
    //             System.out.println("Test another one " + lastSlope * 2);
    //             lastSlope *= 2;
    //             hsvi = HSVIFactory.buildConicHSVIWithSlope(lastSlope,problem,discount,errorMargin,exLXU,exNUI,pruning);
    //         }
    //     }
    // }


    /**
     * Get the lower bound used in the current hsvi
     *
     * @return The lower bound
     */
    public ValueFunctionBound<State,Action,Observation> getLowerBound() {
        return hsvi.lowerBound;
    }

    /**
     * Get the upper bound used in the current hsvi
     *
     * @return The upper bound
     */
    public ValueFunctionBound<State,Action,Observation> getUpperBound() {
        return hsvi.upperBound;
    }

}
