package mdp.solver;

import mdp.mdp.MDP;
import mdp.policy.InfiniteHorizonDeterministicPolicy;

import java.util.*;

import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * <pre>
 * 
 * Solver for infinite horizon MDPs used to initialize the upper
 * bound of a POMDP.  Uses <i>synchronous VI</i>.
 * 
 * </pre>
 */

public class InfiniteHorizonMDPSolver<State,Action> {

    /**
     * The mdp
     */
    private MDP<State,Action> mdp;

    /**
     * The collection of actions
     */
    private Collection<Action> actions;

    /**
     * The collection of states
     */
    private Collection<State> states;

    /**
     * The approximation we allow
     */
    private double epsilon;

    /**
     * The gamma used for the resolution of the mdp
     */
    private double gamma;


    /**
     * Find the optimal deterministic policy for a main.mdp with infinite horizon
     *
     * @param mdp     The Markov Decision Process for which we want the optimal policy
     * @param gamma   The gamma parameter for the main.mdp.main.mdp.MDP
     * @param epsilon The parameter which
     * @return The optimal policy
     */
    public InfiniteHorizonDeterministicPolicy<State,Action> findOptimalPolicy(MDP<State,Action> mdp, double gamma, double epsilon) {
        this.gamma = gamma;
        this.epsilon = epsilon;
        this.mdp = mdp;
        this.states = mdp.states;
        this.actions = mdp.actions;

        Map<State,Double> valueFunction = new HashMap<>();
        for(State state : states) {
            valueFunction.put(state,0.0);
        }
        Map<State,Double> newValueFunction = bellmanUpdate(valueFunction);

        while(!solverHasFinished(valueFunction,newValueFunction)) {
            valueFunction = newValueFunction;
            newValueFunction = bellmanUpdate(valueFunction);
        }

        Map<State,Action> actions = getActionsFromValueFunction(newValueFunction);

        InfiniteHorizonDeterministicPolicy<State,Action> policy = new InfiniteHorizonDeterministicPolicy<>();

        for(State state : states) {
            policy.setAction(state,actions.get(state));
            policy.setValue(state,newValueFunction.get(state));
        }

        return policy;
    }


    /**
     * Get the optimal action given the value function
     *
     * @param valueFunction  The value function
     * @return The optimal action
     */
    private Map<State,Action> getActionsFromValueFunction(Map<State,Double> valueFunction) {
        Map<State,Action> policy = new HashMap<>();
        for(State state : states) {
            double maxiValue = Double.NEGATIVE_INFINITY;
            Action maxiAction = null;
            for(Action action : actions) {
                double temp = bellmanSingleUpdate(valueFunction, valueFunction,state,action);
                if(temp >= maxiValue) {
                    maxiValue = temp;
                    maxiAction = action;
                }
            }
            policy.put(state,maxiAction);
        }
        return policy;
    }


    /**
     * Update the value function on a given couple (state,action) with Gauss-Seidel technique
     *
     * @param valueFunction      The current approximation of the value function
     * @param newValueFunction   The currently updating value function
     * @param beginState         The state
     * @param action             The action
     * @return The value function on this couple
     */
    private double bellmanSingleUpdate(Map<State,Double> valueFunction, Map<State,Double> newValueFunction, State beginState, Action action) {
        double value = 0;

        for(State endState : mdp.getPossibleNextStates(beginState,action)) {
            double temp = mdp.getTransitionProbability(beginState,action,endState);
            Double tempValue = newValueFunction.get(endState);

            if(tempValue == null) {
                value += temp * valueFunction.get(endState);
            } else {
                value += temp * tempValue;
            }
        }


        value *= gamma;

        for(State endState : mdp.getPossibleNextStates(beginState,action)) {
            value += mdp.getReward(beginState,action,endState) * mdp.getTransitionProbability(beginState,action,endState);
        }
        return value;
    }


    /**
     * Iterate one time the algorithm
     *
     * @param valueFunction The current approximation of the value function
     * @return The new approximation of the value function
     */
    private Map<State,Double> bellmanUpdate(Map<State,Double> valueFunction) {
        Map<State,Double> newValueFunction = new HashMap<>();
        for(State beginState : mdp.states) {
            double maxiValue = Double.NEGATIVE_INFINITY;
            for(Action action : mdp.actions) {
                maxiValue = max(maxiValue,bellmanSingleUpdate(valueFunction,newValueFunction,beginState,action));
            }
            newValueFunction.put(beginState,maxiValue);
        }
       return newValueFunction;
    }


    /**
     * Check if the approximation is good enough
     *
     * @param valueFunction     The precedent approximation of the value function
     * @param newValueFunction  The current approximation of the value function
     * @return A boolean indicating if the approximation is good enough
     */
    private boolean solverHasFinished(Map<State,Double> valueFunction, Map<State,Double> newValueFunction) {
        double sum = 0.0;
        for(State state : states) {
            sum += abs(valueFunction.get(state) - newValueFunction.get(state));
        }

        return sum < epsilon;
    }
}
