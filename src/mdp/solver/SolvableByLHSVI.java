package mdp.solver;

public interface SolvableByLHSVI<State,Action,Observation> extends SolvableByHSVI<State,Action,Observation> {

    /**
     * Get the lipschitz constant of the value function of the decision process
     *
     * @param discount      The discount on future reward
     * @param errorMargin   The allowed error
     * @return  The lipschitz constant
     */
    double getValueFunctionLipschitzConstant(double discount, double errorMargin);
}
