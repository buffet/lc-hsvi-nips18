package mdp.solver;

import mdp.pomdp.POMDP;
import mdp.pomdp.Belief;
import mdp.solver.POMDPApproximation;

import util.function.XConeRepresentation;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *
 * Special interface for POMDP Approximation that should return hyperplanes.
 *
 *  </pre>
 */

public interface HyperplanePOMDPApproximation extends POMDPApproximation {

    // public <S,A,O> POMDPApproximation(POMDP<S,A,O> pomdp, double discount, double errorMargin, boolean isUpper);

    // public Map<S,Double> getCornerValues();

    // public double getValue(Belief belief);

    public double getHyperplane(Belief belief);
}
