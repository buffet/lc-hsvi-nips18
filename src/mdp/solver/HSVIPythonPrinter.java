package mdp.solver;


import util.container.Pair;

import java.io.IOException;
import java.io.PrintWriter;

public class HSVIPythonPrinter<State,Action,Observation> {
   /**
     * The upperBound of the HSVI algorithm
     */
    protected ValueFunctionBound<State,Action,Observation> upperBound;

    /**
     * The lowerBound of the HSVI algorithm
     */
    protected ValueFunctionBound<State,Action,Observation> lowerBound;

    /**
     * The pomdp we are trying to solve
     */
    protected SolvableByHSVI<State,Action,Observation> pomdp;

    /**
     * The string containing the python code that will be used
     */
    protected StringBuilder strBuild;

    protected String scriptPath;
    protected String imagePath;
    protected int nSteps;

    /**
     * Create a HSVI printer, with the lowerBound and upperBound used by the HSVI algorithm
     *
     * @param lowerBound  The lowerBound used by HSVI
     * @param upperBound  The upperBound used by HSVI
     */
    public HSVIPythonPrinter(String scriptPath, String imagePath, int nSteps,
			     SolvableByHSVI<State,Action,Observation> pomdp) {
	
	this.scriptPath = scriptPath;
	this.imagePath = imagePath;
	this.nSteps = nSteps;
	this.pomdp = pomdp;
        this.lowerBound = null;
        this.upperBound = null;

        strBuild = new StringBuilder();
        strBuild.append("from __future__ import division\n\n" +
			"import matplotlib.pyplot as plt\n" +
			"import numpy as np\n" +
                        "import subprocess\n" +
                        "import os\n\n\n"
                        );
    }


    /**
     * Create a new image based on the actual values of lowerBound and upperBound
     */
    void newImage(Pair<Double,Double> newPointLower, Pair<Double,Double> newPointUpper) {}

    /**
     * End the python file
     */
    void endFile() {}


    /**
     * Save the program on a file
     *
     * @param scriptPath The path of the file
     * @param imagePath  The path of the image
     */
    void savePythonProgram() {
        endFile();

        try {
            PrintWriter writer = new PrintWriter(scriptPath, "UTF-8");
            writer.print(strBuild.toString());
            writer.close();
        } catch (IOException exception) {
            System.out.println("The HSVIPrinter couldn't write the python script");
        }
    }
    
    /**
     * Set the lower and upper bounds
     *
     * @param lowerBound The lower bound
     * @param upperBound The upper bound
     */
    public void setBounds(ValueFunctionBound<State,Action,Observation> lowerBound, ValueFunctionBound<State,Action,Observation> upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }
}
