package mdp.solver;

import mdp.interpreter.*;
import mdp.pomdp.Belief;
import mdp.pomdp.POMDP;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 *
 * Special class providing methods each computing a single hyperplane
 * upper- or lower-bounding the optimal value function of a POMDP.
 *
 *  </pre>
 */

public interface POMDPApproximation {
    
    //public <S,A,O> POMDPApproximation(POMDP<S,A,O> pomdp, double discount, double errorMargin, boolean isUpper);

    public <S> Map<S,Double> getCornerValues();

    public double getValue(Belief belief);
    
}
