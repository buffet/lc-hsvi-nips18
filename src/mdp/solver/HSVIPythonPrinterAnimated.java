package mdp.solver;


import util.container.Pair;

import java.io.IOException;
import java.io.PrintWriter;

public class HSVIPythonPrinterAnimated<State,Action,Observation> extends HSVIPythonPrinter<State,Action,Observation> {
 
    /**
     * The number of images that the code have already been generated
     */
    private int imageCount;
    
    /**
     * Create a HSVI printer, with the lowerBound and upperBound used by the HSVI algorithm
     *
     * @param lowerBound  The lowerBound used by HSVI
     * @param upperBound  The upperBound used by HSVI
     */
    public HSVIPythonPrinterAnimated(String scriptPath, String imagePath, int nSteps,
			     SolvableByHSVI<State,Action,Observation> pomdp) {
	super(scriptPath, imagePath, nSteps, pomdp);
        imageCount = 0;
    }
    
    /**
     * Create a new image based on the actual values of lowerBound and upperBound
     */
    void newImage(Pair<Double,Double> newPointLower, Pair<Double,Double> newPointUpper) {
        String pngFile = String.format("img-%s.png", imageCount);
        String gifFile = String.format("img-%s.gif", imageCount);

	strBuild.append("x = np.linspace(0, 1, "+(nSteps+1)+")\n");
    
        strBuild.append("lowerBound = " + lowerBound.getValuesString(nSteps) + "\n");
        strBuild.append("upperBound = " + upperBound.getValuesString(nSteps) + "\n");
        strBuild.append("lowerPointX = " + newPointLower.a + "\n");
        strBuild.append("lowerPointY = " + newPointLower.b + "\n");
        strBuild.append("upperPointX = " + newPointUpper.a + "\n");
        strBuild.append("upperPointY = " + newPointUpper.b + "\n");

        strBuild.append("plt.plot(x,lowerBound,c=\"blue\")\n" +
                        "plt.scatter(lowerPointX,lowerPointY,c=\"blue\")\n" +
                        "plt.plot(x,upperBound,c=\"red\")\n" +
                        "plt.scatter(upperPointX,upperPointY,c=\"red\")\n");
        strBuild.append("axes = plt.gca()\n" +
                        "axes.set_xlim([0,1])\n" +
                        "axes.set_ylim([-5,41])\n");
        strBuild.append("plt.savefig(\"" + pngFile + "\")\n");
        strBuild.append("plt.close()\n\n");

        strBuild.append("os.system(\"convert " + pngFile + " " + gifFile + "\")\n");
        strBuild.append("os.system(\"rm " + pngFile + "\")\n\n\n");
        imageCount++;
    }

    /**
     * End the python file
     */
    void endFile() {
        strBuild.append("os.system(\"gifsicle --delay=10 --loop ");
        for(int i = 0; i<imageCount; i++) {
            strBuild.append("img-" + i + ".gif ");
        }
        strBuild.append(" > " + imagePath + "\")\n\n");

        for(int i = 0; i<imageCount; i++) {
            strBuild.append("os.system(\"rm " + i + ".gif\")\n");
        }
    }

}
