package mdp.solver;


import util.container.Pair;
import java.util.Iterator;

import mdp.pomdp.Belief;

import java.io.IOException;
import java.io.PrintWriter;

public class HSVIPythonPrinterFixed<State,Action,Observation> extends HSVIPythonPrinter<State,Action,Observation> {
    
    /**
     * Create a HSVI printer, with the lowerBound and upperBound used by the HSVI algorithm
     *
     * @param lowerBound  The lowerBound used by HSVI
     * @param upperBound  The upperBound used by HSVI
     */
    public HSVIPythonPrinterFixed(String scriptPath, String imagePath, int nSteps,
			     SolvableByHSVI<State,Action,Observation> pomdp) {
	super(scriptPath, imagePath, nSteps, pomdp);
    }

    /**
     * Create a new image based on the actual values of lowerBound and upperBound
     */
    void newImage(Pair<Double,Double> newPointLower, Pair<Double,Double> newPointUpper) {
    }

    /**
     * End the python file
     */
    void endFile() {

	strBuild.append("x = np.linspace(0, 1, "+(nSteps+1)+")\n");

        strBuild.append("lowerBound = " + lowerBound.getValuesString(nSteps) + "\n");
        strBuild.append("upperBound = " + upperBound.getValuesString(nSteps) + "\n");

	//strBuild.append("xl = np.linspace(0, 1, "+(nSteps+1)+")\n");
	strBuild.append("xl = []\n");
	strBuild.append("xu = []\n");
	strBuild.append("for i in range(0,"+(nSteps+1)+"):\n");
	strBuild.append("   xl.append(i/"+nSteps+")\n");
	strBuild.append("   xu.append(i/"+nSteps+")\n");
	for(int i=1; i<10; i++) {
	    double p = (Math.pow(0.85,i)/(Math.pow(0.85,i)+Math.pow(0.15,i)));
	    Belief b = new Belief();
	    Iterator<State> itState = pomdp.getStates().iterator();
	    b.setWeight(itState.next(), 1-p);
	    b.setWeight(itState.next(), p);
	    strBuild.append("xl.append("+p+")\n");
	    strBuild.append("lowerBound.append("+lowerBound.getValue(b)+")\n");
	    strBuild.append("xu.append("+p+")\n");
	    strBuild.append("upperBound.append("+upperBound.getValue(b)+")\n");
	    itState = pomdp.getStates().iterator();
	    b.setWeight(itState.next(), p);
	    b.setWeight(itState.next(), 1-p);
	    strBuild.append("xl.append("+(1-p)+")\n");
	    strBuild.append("lowerBound.append("+lowerBound.getValue(b)+")\n");
	    strBuild.append("xu.append("+(1-p)+")\n");
	    strBuild.append("upperBound.append("+upperBound.getValue(b)+")\n");
	}
	strBuild.append("xl,lowerBound = zip(*sorted(zip(xl, lowerBound)))\n");
	strBuild.append("xu,upperBound = zip(*sorted(zip(xu, upperBound)))\n");
	//strBuild.append("print(xl)\n");
	//strBuild.append("print(lowerBound)\n");
	
        strBuild.append("plt.plot(xl,lowerBound,c=\"blue\")\n");// +
	//	    "plt.plot(xu,upperBound,c=\"red\")\n");
        strBuild.append("axes = plt.gca()\n" +
                        "axes.set_xlim([0,1])\n" +
                        "axes.set_ylim([-5,41])\n");
        strBuild.append("plt.savefig(\"lastImage.pdf\", bbox_inches='tight')\n");
        strBuild.append("plt.close()\n\n");

    }


}
