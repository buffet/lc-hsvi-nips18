/* The following code was generated by JFlex 1.6.1 */

package mdp.interpreter;

import java_cup.runtime.*;
import mdp.interpreter.sym;


/**
 * This class is a scanner generated by 
 * <a href="http://www.jflex.de/">JFlex</a> 1.6.1
 * from the specification file <tt>lexer.flex</tt>
 */
class Lexer implements java_cup.runtime.Scanner {

  /** This character denotes the end of file */
  public static final int YYEOF = -1;

  /** initial size of the lookahead buffer */
  private static final int ZZ_BUFFERSIZE = 16384;

  /** lexical states */
  public static final int YYINITIAL = 0;

  /**
   * ZZ_LEXSTATE[l] is the state in the DFA for the lexical state l
   * ZZ_LEXSTATE[l+1] is the state in the DFA for the lexical state l
   *                  at the beginning of a line
   * l is of the form l = 2*k, k a non negative integer
   */
  private static final int ZZ_LEXSTATE[] = { 
     0, 0
  };

  /** 
   * Translates characters to character classes
   */
  private static final String ZZ_CMAP_PACKED = 
    "\11\0\1\3\1\2\1\0\1\3\1\1\22\0\1\3\2\0\1\14"+
    "\6\0\1\44\1\10\1\0\1\13\1\6\1\0\1\4\11\5\1\43"+
    "\6\0\4\11\1\7\11\11\1\34\2\11\1\35\1\11\1\33\6\11"+
    "\4\0\1\12\1\0\1\26\1\31\1\20\1\15\1\30\1\36\2\11"+
    "\1\16\2\11\1\27\1\37\1\23\1\21\2\11\1\32\1\17\1\24"+
    "\1\22\1\25\1\41\1\42\1\40\1\11\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uffff\0\uff95\0";

  /** 
   * Translates characters to character classes
   */
  private static final char [] ZZ_CMAP = zzUnpackCMap(ZZ_CMAP_PACKED);

  /** 
   * Translates DFA states to action switch labels.
   */
  private static final int [] ZZ_ACTION = zzUnpackAction();

  private static final String ZZ_ACTION_PACKED_0 =
    "\1\0\1\1\2\2\2\3\1\1\1\4\1\5\1\6"+
    "\1\2\12\4\1\7\1\10\1\11\1\12\1\13\2\14"+
    "\1\0\13\4\1\14\1\0\20\4\1\15\12\4\1\16"+
    "\11\4\1\17\2\4\1\20\2\4\1\21\2\4\1\22"+
    "\1\4\1\23\1\24\1\25\1\26\1\27\4\4\1\30";

  private static int [] zzUnpackAction() {
    int [] result = new int[100];
    int offset = 0;
    offset = zzUnpackAction(ZZ_ACTION_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAction(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /** 
   * Translates a state to a row index in the transition table
   */
  private static final int [] ZZ_ROWMAP = zzUnpackRowMap();

  private static final String ZZ_ROWMAP_PACKED_0 =
    "\0\0\0\45\0\112\0\45\0\157\0\224\0\271\0\336"+
    "\0\45\0\45\0\u0103\0\u0128\0\u014d\0\u0172\0\u0197\0\u01bc"+
    "\0\u01e1\0\u0206\0\u022b\0\u0250\0\u0275\0\336\0\336\0\336"+
    "\0\45\0\45\0\157\0\u029a\0\u02bf\0\u02e4\0\u0309\0\u032e"+
    "\0\u0353\0\u0378\0\u039d\0\u03c2\0\u03e7\0\u040c\0\u0431\0\u0456"+
    "\0\u047b\0\u047b\0\u04a0\0\u04c5\0\u04ea\0\u050f\0\u0534\0\u0559"+
    "\0\u057e\0\u05a3\0\u05c8\0\u05ed\0\u0612\0\u0637\0\u065c\0\u0681"+
    "\0\u06a6\0\u06cb\0\336\0\u06f0\0\u0715\0\u073a\0\u075f\0\u0784"+
    "\0\u07a9\0\u07ce\0\u07f3\0\u0818\0\u083d\0\336\0\u0862\0\u0887"+
    "\0\u08ac\0\u08d1\0\u08f6\0\u091b\0\u0940\0\u0965\0\u098a\0\336"+
    "\0\u09af\0\u09d4\0\336\0\u09f9\0\u0a1e\0\336\0\u0a43\0\u0a68"+
    "\0\336\0\u0a8d\0\336\0\336\0\336\0\336\0\336\0\u0ab2"+
    "\0\u0ad7\0\u0afc\0\u0b21\0\336";

  private static int [] zzUnpackRowMap() {
    int [] result = new int[100];
    int offset = 0;
    offset = zzUnpackRowMap(ZZ_ROWMAP_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackRowMap(String packed, int offset, int [] result) {
    int i = 0;  /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int high = packed.charAt(i++) << 16;
      result[j++] = high | packed.charAt(i++);
    }
    return j;
  }

  /** 
   * The transition table of the DFA
   */
  private static final int [] ZZ_TRANS = zzUnpackTrans();

  private static final String ZZ_TRANS_PACKED_0 =
    "\1\2\1\3\2\4\1\5\1\6\1\7\1\10\1\11"+
    "\1\10\1\2\1\12\1\13\1\14\1\15\1\16\1\17"+
    "\1\20\1\21\2\10\1\22\1\23\1\10\1\24\1\10"+
    "\1\25\1\26\1\27\1\30\5\10\1\31\1\32\47\0"+
    "\1\4\46\0\2\33\1\34\1\35\20\0\1\35\20\0"+
    "\2\6\1\34\1\35\20\0\1\35\20\0\2\34\43\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\26\10\2\0"+
    "\1\13\1\3\1\4\42\13\4\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\1\10\1\36\24\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\1\37\5\10\1\40"+
    "\17\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\7\10\1\41\16\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\4\10\1\42\21\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\14\10\1\43\11\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\6\10\1\44"+
    "\17\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\11\10\1\45\14\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\3\10\1\46\22\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\25\10\1\47\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\13\10\1\50\12\10"+
    "\6\0\2\34\1\0\1\35\20\0\1\35\20\0\2\51"+
    "\2\0\1\52\2\0\1\52\35\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\2\10\1\53\23\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\13\10\1\54\12\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\3\10"+
    "\1\55\22\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\11\10\1\56\14\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\2\10\1\57\23\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\2\10\1\60\23\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\1\10"+
    "\1\61\24\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\12\10\1\62\13\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\7\10\1\63\16\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\3\10\1\64\22\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\24\10"+
    "\1\65\1\10\6\0\2\51\43\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\3\10\1\66\22\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\6\10\1\67\17\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\12\10"+
    "\1\70\13\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\7\10\1\71\5\10\1\72\10\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\7\10\1\73\16\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\13\10"+
    "\1\74\12\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\21\10\1\75\4\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\5\10\1\76\20\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\1\10\1\77\24\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\12\10"+
    "\1\100\13\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\11\10\1\101\14\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\4\10\1\102\21\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\7\10\1\103\16\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\5\10"+
    "\1\104\20\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\13\10\1\105\12\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\7\10\1\106\16\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\15\10\1\107\10\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\4\10"+
    "\1\110\21\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\13\10\1\111\12\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\4\10\1\112\21\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\5\10\1\113\20\10"+
    "\6\0\2\10\1\0\1\10\1\0\3\10\1\0\15\10"+
    "\1\114\10\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\5\10\1\115\20\10\6\0\2\10\1\0\1\10"+
    "\1\0\3\10\1\0\1\10\1\116\24\10\6\0\2\10"+
    "\1\0\1\10\1\0\3\10\1\0\1\117\25\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\2\10\1\120"+
    "\23\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\10\10\1\121\15\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\15\10\1\122\10\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\2\10\1\123\23\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\6\10\1\124"+
    "\17\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\1\125\25\10\6\0\2\10\1\0\1\10\1\0\3\10"+
    "\1\0\1\126\25\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\6\10\1\127\17\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\7\10\1\130\16\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\13\10\1\131"+
    "\12\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\11\10\1\132\14\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\22\10\1\133\3\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\2\10\1\134\23\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\13\10\1\135"+
    "\12\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\7\10\1\136\16\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\23\10\1\137\2\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\7\10\1\140\16\10\6\0"+
    "\2\10\1\0\1\10\1\0\3\10\1\0\1\10\1\141"+
    "\24\10\6\0\2\10\1\0\1\10\1\0\3\10\1\0"+
    "\4\10\1\142\21\10\6\0\2\10\1\0\1\10\1\0"+
    "\3\10\1\0\6\10\1\143\17\10\6\0\2\10\1\0"+
    "\1\10\1\0\3\10\1\0\2\10\1\144\23\10\2\0";

  private static int [] zzUnpackTrans() {
    int [] result = new int[2886];
    int offset = 0;
    offset = zzUnpackTrans(ZZ_TRANS_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackTrans(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      value--;
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }


  /* error codes */
  private static final int ZZ_UNKNOWN_ERROR = 0;
  private static final int ZZ_NO_MATCH = 1;
  private static final int ZZ_PUSHBACK_2BIG = 2;

  /* error messages for the codes above */
  private static final String ZZ_ERROR_MSG[] = {
    "Unknown internal scanner error",
    "Error: could not match input",
    "Error: pushback value was too large"
  };

  /**
   * ZZ_ATTRIBUTE[aState] contains the attributes of state <code>aState</code>
   */
  private static final int [] ZZ_ATTRIBUTE = zzUnpackAttribute();

  private static final String ZZ_ATTRIBUTE_PACKED_0 =
    "\1\0\1\11\1\1\1\11\4\1\2\11\16\1\2\11"+
    "\2\1\1\0\14\1\1\0\72\1";

  private static int [] zzUnpackAttribute() {
    int [] result = new int[100];
    int offset = 0;
    offset = zzUnpackAttribute(ZZ_ATTRIBUTE_PACKED_0, offset, result);
    return result;
  }

  private static int zzUnpackAttribute(String packed, int offset, int [] result) {
    int i = 0;       /* index in packed string  */
    int j = offset;  /* index in unpacked array */
    int l = packed.length();
    while (i < l) {
      int count = packed.charAt(i++);
      int value = packed.charAt(i++);
      do result[j++] = value; while (--count > 0);
    }
    return j;
  }

  /** the input device */
  private java.io.Reader zzReader;

  /** the current state of the DFA */
  private int zzState;

  /** the current lexical state */
  private int zzLexicalState = YYINITIAL;

  /** this buffer contains the current text to be matched and is
      the source of the yytext() string */
  private char zzBuffer[] = new char[ZZ_BUFFERSIZE];

  /** the textposition at the last accepting state */
  private int zzMarkedPos;

  /** the current text position in the buffer */
  private int zzCurrentPos;

  /** startRead marks the beginning of the yytext() string in the buffer */
  private int zzStartRead;

  /** endRead marks the last character in the buffer, that has been read
      from input */
  private int zzEndRead;

  /** number of newlines encountered up to the start of the matched text */
  private int yyline;

  /** the number of characters up to the start of the matched text */
  private int yychar;

  /**
   * the number of characters from the last newline up to the start of the 
   * matched text
   */
  private int yycolumn;

  /** 
   * zzAtBOL == true <=> the scanner is currently at the beginning of a line
   */
  private boolean zzAtBOL = true;

  /** zzAtEOF == true <=> the scanner is at the EOF */
  private boolean zzAtEOF;

  /** denotes if the user-EOF-code has already been executed */
  private boolean zzEOFDone;
  
  /** 
   * The number of occupied positions in zzBuffer beyond zzEndRead.
   * When a lead/high surrogate has been read from the input stream
   * into the final zzBuffer position, this will have a value of 1;
   * otherwise, it will have a value of 0.
   */
  private int zzFinalHighSurrogate = 0;

  /* user code: */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }


  /**
   * Creates a new scanner
   *
   * @param   in  the java.io.Reader to read input from.
   */
  Lexer(java.io.Reader in) {
    this.zzReader = in;
  }


  /** 
   * Unpacks the compressed character translation table.
   *
   * @param packed   the packed character translation table
   * @return         the unpacked character translation table
   */
  private static char [] zzUnpackCMap(String packed) {
    char [] map = new char[0x110000];
    int i = 0;  /* index in packed string  */
    int j = 0;  /* index in unpacked array */
    while (i < 146) {
      int  count = packed.charAt(i++);
      char value = packed.charAt(i++);
      do map[j++] = value; while (--count > 0);
    }
    return map;
  }


  /**
   * Refills the input buffer.
   *
   * @return      <code>false</code>, iff there was new input.
   * 
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  private boolean zzRefill() throws java.io.IOException {

    /* first: make room (if you can) */
    if (zzStartRead > 0) {
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
      System.arraycopy(zzBuffer, zzStartRead,
                       zzBuffer, 0,
                       zzEndRead-zzStartRead);

      /* translate stored positions */
      zzEndRead-= zzStartRead;
      zzCurrentPos-= zzStartRead;
      zzMarkedPos-= zzStartRead;
      zzStartRead = 0;
    }

    /* is the buffer big enough? */
    if (zzCurrentPos >= zzBuffer.length - zzFinalHighSurrogate) {
      /* if not: blow it up */
      char newBuffer[] = new char[zzBuffer.length*2];
      System.arraycopy(zzBuffer, 0, newBuffer, 0, zzBuffer.length);
      zzBuffer = newBuffer;
      zzEndRead += zzFinalHighSurrogate;
      zzFinalHighSurrogate = 0;
    }

    /* fill the buffer with new input */
    int requested = zzBuffer.length - zzEndRead;
    int numRead = zzReader.read(zzBuffer, zzEndRead, requested);

    /* not supposed to occur according to specification of java.io.Reader */
    if (numRead == 0) {
      throw new java.io.IOException("Reader returned 0 characters. See JFlex examples for workaround.");
    }
    if (numRead > 0) {
      zzEndRead += numRead;
      /* If numRead == requested, we might have requested to few chars to
         encode a full Unicode character. We assume that a Reader would
         otherwise never return half characters. */
      if (numRead == requested) {
        if (Character.isHighSurrogate(zzBuffer[zzEndRead - 1])) {
          --zzEndRead;
          zzFinalHighSurrogate = 1;
        }
      }
      /* potentially more input available */
      return false;
    }

    /* numRead < 0 ==> end of stream */
    return true;
  }

    
  /**
   * Closes the input stream.
   */
  public final void yyclose() throws java.io.IOException {
    zzAtEOF = true;            /* indicate end of file */
    zzEndRead = zzStartRead;  /* invalidate buffer    */

    if (zzReader != null)
      zzReader.close();
  }


  /**
   * Resets the scanner to read from a new input stream.
   * Does not close the old reader.
   *
   * All internal variables are reset, the old input stream 
   * <b>cannot</b> be reused (internal buffer is discarded and lost).
   * Lexical state is set to <tt>ZZ_INITIAL</tt>.
   *
   * Internal scan buffer is resized down to its initial length, if it has grown.
   *
   * @param reader   the new input stream 
   */
  public final void yyreset(java.io.Reader reader) {
    zzReader = reader;
    zzAtBOL  = true;
    zzAtEOF  = false;
    zzEOFDone = false;
    zzEndRead = zzStartRead = 0;
    zzCurrentPos = zzMarkedPos = 0;
    zzFinalHighSurrogate = 0;
    yyline = yychar = yycolumn = 0;
    zzLexicalState = YYINITIAL;
    if (zzBuffer.length > ZZ_BUFFERSIZE)
      zzBuffer = new char[ZZ_BUFFERSIZE];
  }


  /**
   * Returns the current lexical state.
   */
  public final int yystate() {
    return zzLexicalState;
  }


  /**
   * Enters a new lexical state
   *
   * @param newState the new lexical state
   */
  public final void yybegin(int newState) {
    zzLexicalState = newState;
  }


  /**
   * Returns the text matched by the current regular expression.
   */
  public final String yytext() {
    return new String( zzBuffer, zzStartRead, zzMarkedPos-zzStartRead );
  }


  /**
   * Returns the character at position <tt>pos</tt> from the 
   * matched text. 
   * 
   * It is equivalent to yytext().charAt(pos), but faster
   *
   * @param pos the position of the character to fetch. 
   *            A value from 0 to yylength()-1.
   *
   * @return the character at position pos
   */
  public final char yycharat(int pos) {
    return zzBuffer[zzStartRead+pos];
  }


  /**
   * Returns the length of the matched text region.
   */
  public final int yylength() {
    return zzMarkedPos-zzStartRead;
  }


  /**
   * Reports an error that occured while scanning.
   *
   * In a wellformed scanner (no or only correct usage of 
   * yypushback(int) and a match-all fallback rule) this method 
   * will only be called with things that "Can't Possibly Happen".
   * If this method is called, something is seriously wrong
   * (e.g. a JFlex bug producing a faulty scanner etc.).
   *
   * Usual syntax/scanner level error handling should be done
   * in error fallback rules.
   *
   * @param   errorCode  the code of the errormessage to display
   */
  private void zzScanError(int errorCode) {
    String message;
    try {
      message = ZZ_ERROR_MSG[errorCode];
    }
    catch (ArrayIndexOutOfBoundsException e) {
      message = ZZ_ERROR_MSG[ZZ_UNKNOWN_ERROR];
    }

    throw new Error(message);
  } 


  /**
   * Pushes the specified amount of characters back into the input stream.
   *
   * They will be read again by then next call of the scanning method
   *
   * @param number  the number of characters to be read again.
   *                This number must not be greater than yylength()!
   */
  public void yypushback(int number)  {
    if ( number > yylength() )
      zzScanError(ZZ_PUSHBACK_2BIG);

    zzMarkedPos -= number;
  }


  /**
   * Contains user EOF-code, which will be executed exactly once,
   * when the end of file is reached
   */
  private void zzDoEOF() throws java.io.IOException {
    if (!zzEOFDone) {
      zzEOFDone = true;
      yyclose();
    }
  }


  /**
   * Resumes scanning until the next regular expression is matched,
   * the end of input is encountered or an I/O-Error occurs.
   *
   * @return      the next token
   * @exception   java.io.IOException  if any I/O-Error occurs
   */
  public java_cup.runtime.Symbol next_token() throws java.io.IOException {
    int zzInput;
    int zzAction;

    // cached fields:
    int zzCurrentPosL;
    int zzMarkedPosL;
    int zzEndReadL = zzEndRead;
    char [] zzBufferL = zzBuffer;
    char [] zzCMapL = ZZ_CMAP;

    int [] zzTransL = ZZ_TRANS;
    int [] zzRowMapL = ZZ_ROWMAP;
    int [] zzAttrL = ZZ_ATTRIBUTE;

    while (true) {
      zzMarkedPosL = zzMarkedPos;

      zzAction = -1;

      zzCurrentPosL = zzCurrentPos = zzStartRead = zzMarkedPosL;
  
      zzState = ZZ_LEXSTATE[zzLexicalState];

      // set up zzAction for empty match case:
      int zzAttributes = zzAttrL[zzState];
      if ( (zzAttributes & 1) == 1 ) {
        zzAction = zzState;
      }


      zzForAction: {
        while (true) {
    
          if (zzCurrentPosL < zzEndReadL) {
            zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
            zzCurrentPosL += Character.charCount(zzInput);
          }
          else if (zzAtEOF) {
            zzInput = YYEOF;
            break zzForAction;
          }
          else {
            // store back cached positions
            zzCurrentPos  = zzCurrentPosL;
            zzMarkedPos   = zzMarkedPosL;
            boolean eof = zzRefill();
            // get translated positions and possibly new buffer
            zzCurrentPosL  = zzCurrentPos;
            zzMarkedPosL   = zzMarkedPos;
            zzBufferL      = zzBuffer;
            zzEndReadL     = zzEndRead;
            if (eof) {
              zzInput = YYEOF;
              break zzForAction;
            }
            else {
              zzInput = Character.codePointAt(zzBufferL, zzCurrentPosL, zzEndReadL);
              zzCurrentPosL += Character.charCount(zzInput);
            }
          }
          int zzNext = zzTransL[ zzRowMapL[zzState] + zzCMapL[zzInput] ];
          if (zzNext == -1) break zzForAction;
          zzState = zzNext;

          zzAttributes = zzAttrL[zzState];
          if ( (zzAttributes & 1) == 1 ) {
            zzAction = zzState;
            zzMarkedPosL = zzCurrentPosL;
            if ( (zzAttributes & 8) == 8 ) break zzForAction;
          }

        }
      }

      // store back cached position
      zzMarkedPos = zzMarkedPosL;

      if (zzInput == YYEOF && zzStartRead == zzCurrentPos) {
        zzAtEOF = true;
            zzDoEOF();
          { return new java_cup.runtime.Symbol(sym.EOF); }
      }
      else {
        switch (zzAction < 0 ? zzAction : ZZ_ACTION[zzAction]) {
          case 1: 
            { throw new Error("Illegal character <"+yytext()+">");
            }
          case 25: break;
          case 2: 
            { /* Do nothing */
            }
          case 26: break;
          case 3: 
            { return symbol(sym.INTTOK,new Integer(yytext()));
            }
          case 27: break;
          case 4: 
            { return symbol(sym.STRINGTOK, yytext());
            }
          case 28: break;
          case 5: 
            { return symbol(sym.PLUSTOK);
            }
          case 29: break;
          case 6: 
            { return symbol(sym.MINUSTOK);
            }
          case 30: break;
          case 7: 
            { return symbol(sym.TTOK);
            }
          case 31: break;
          case 8: 
            { return symbol(sym.OTOK);
            }
          case 32: break;
          case 9: 
            { return symbol(sym.RTOK);
            }
          case 33: break;
          case 10: 
            { return symbol(sym.COLONTOK);
            }
          case 34: break;
          case 11: 
            { return symbol(sym.ASTERICKTOK);
            }
          case 35: break;
          case 12: 
            { return symbol(sym.FLOATTOK,new Double(yytext()));
            }
          case 36: break;
          case 13: 
            { return symbol(sym.COSTTOK);
            }
          case 37: break;
          case 14: 
            { return symbol(sym.STARTTOK);
            }
          case 38: break;
          case 15: 
            { return symbol(sym.STATESTOK);
            }
          case 39: break;
          case 16: 
            { return symbol(sym.VALUESTOK);
            }
          case 40: break;
          case 17: 
            { return symbol(sym.REWARDTOK);
            }
          case 41: break;
          case 18: 
            { return symbol(sym.INCLUDETOK);
            }
          case 42: break;
          case 19: 
            { return symbol(sym.UNIFORMTOK);
            }
          case 43: break;
          case 20: 
            { return symbol(sym.ACTIONSTOK);
            }
          case 44: break;
          case 21: 
            { return symbol(sym.EXCLUDETOK);
            }
          case 45: break;
          case 22: 
            { return symbol(sym.DISCOUNTTOK);
            }
          case 46: break;
          case 23: 
            { return symbol(sym.IDENTITYTOK);
            }
          case 47: break;
          case 24: 
            { return symbol(sym.OBSERVATIONSTOK);
            }
          case 48: break;
          default:
            zzScanError(ZZ_NO_MATCH);
        }
      }
    }
  }


}
