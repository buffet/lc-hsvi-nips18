package mdp.interpreter;


import java.io.FileReader;

public class POMDPReader {
    @SuppressWarnings( "deprecation" )
    public static POMDPFromFile getPOMDPFromFile(String fileName) throws Exception {
        parser p = new parser(new Lexer(new FileReader(fileName)));
        AST ast = (AST)(p.parse().value);
        return ASTToPOMDP.translate(ast);
    }
}
