package mdp.interpreter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


class AST {
    AST(Preamble preamble, InitialBelief initialBelief, List<ParamSpec> paramSpecList) {
        this.preamble = preamble; this.initialBelief = initialBelief; this.paramSpecList = paramSpecList;
    }
    Preamble preamble;
    InitialBelief initialBelief;
    List<ParamSpec> paramSpecList;
}

class Preamble {
    Preamble() {params = new ArrayList<>();}
    List<PreambleParam> params;
}

class PreambleParam {
}

class DiscountParam extends PreambleParam {
    DiscountParam(Double discount) { this.discount = discount; }
    Double discount;
}

class ValueParam extends PreambleParam {
    ValueParam(ValueType valueType) { this.valueType = valueType; }
    enum ValueType {reward,cost}
    ValueType valueType;
}

class StateObsActionParam extends PreambleParam {
    enum Type {state, action, obs}
    Type type;
}

class StateObsActionParamInt extends StateObsActionParam {
    StateObsActionParamInt(int numberOfElements) { this.numberOfElements = numberOfElements; }
    int numberOfElements;
}

class StateObsActionParamStrings extends StateObsActionParam {
    StateObsActionParamStrings(List<String> names) { this.names = names; }
    List<String> names;
}


class State {
}

class StateInteger extends State {
    int index;
}

class StateString extends State {
    String name;
}

class StateAll extends State {
}


class Action {
}

class ActionInteger extends Action {
    int index;
}

class ActionString extends Action {
    String name;
}

class ActionAll extends Action {
}


class Observation {
}

class ObservationInteger extends Observation {
    int index;
}

class ObservationString extends Observation {
    String name;
}

class ObservationAll extends Observation {
}


class InitialBelief {
}


class InitialBeliefMatrix extends InitialBelief {
    InitialBeliefMatrix(UMatrix matrix) { this.matrix = matrix; }
    UMatrix matrix;
}


class InitialBeliefString extends InitialBelief {
    InitialBeliefString(String initialBeliefName) { this.initialBeliefName = initialBeliefName; }
    String initialBeliefName;
}


class InitialBeliefSet extends InitialBelief {
    InitialBeliefSet(boolean isInclude, Set<State> states) { this.isInclude = isInclude; this.states = states; }
    boolean isInclude;
    Set<State> states;
}

class InitialBeliefEmpty extends InitialBelief {
}


class ParamSpec {
}


class ParamSpecTrans extends ParamSpec {
}


class ParamSpecTransUnique extends ParamSpecTrans {
    ParamSpecTransUnique(Action action, State startState, State endState, Double prob) {
        this.action = action; this.startState = startState; this.endState = endState; this.prob = prob;
    }
    Action action;
    State startState;
    State endState;
    Double prob;
}


class ParamSpecTransUMatrix extends ParamSpecTrans {
    ParamSpecTransUMatrix(Action action, State startState, UMatrix matrix) {
        this.action = action; this.startState = startState; this.matrix = matrix;
    }
    Action action;
    State startState;
    UMatrix matrix;
}


class ParamSpecTransUIMatrix extends ParamSpecTrans {
    ParamSpecTransUIMatrix(Action action, UIMatrix matrix) {
        this.action = action; this.matrix = matrix;
    }
    Action action;
    UIMatrix matrix;
}


class ParamSpecObs extends ParamSpec {
}

class ParamSpecObsUnique extends ParamSpecObs {
    ParamSpecObsUnique(Action action, State endState, Observation obs, Double prob) {
        this.action = action; this.endState = endState; this.obs = obs; this.prob = prob;
    }
    Action action;
    State endState;
    Observation obs;
    Double prob;
}

class ParamSpecObsMatrix extends ParamSpecObs {
    ParamSpecObsMatrix(Action action, State endState, UMatrix matrix) {
        this.action = action; this.endState = endState; this.matrix = matrix;
    }
    Action action;
    State endState;
    UMatrix matrix;
}

class ParamSpecObsMatrixOnlyAction extends ParamSpecObs {
    ParamSpecObsMatrixOnlyAction(Action action, UMatrix matrix) {
        this.action = action; this.matrix = matrix;
    }
    Action action;
    UMatrix matrix;
}

class ParamSpecReward extends ParamSpec {
}

class ParamSpecRewardUnique extends ParamSpecReward {
    ParamSpecRewardUnique(Action action, State startState, State endState, Observation obs, Double value) {
        this.action = action; this.startState = startState; this.endState = endState; this.obs = obs; this.value = value;
    }
    Action action;
    State startState;
    State endState;
    Observation obs;
    Double value;
}


class ParamSpecRewardMatrix extends ParamSpecReward {
    ParamSpecRewardMatrix(Action action, State startState, State endState, List<Double> matrix) {
        this.action = action; this.startState = startState; this.endState = endState; this.matrix = matrix;
    }
    Action action;
    State startState;
    State endState;
    List<Double> matrix;
}


class ParamSpecRewardMatrixOnlyActionState extends ParamSpecReward {
    ParamSpecRewardMatrixOnlyActionState(Action action, State startState, List<Double> matrix) {
        this.action = action; this.startState = startState; this.matrix = matrix;
    }
    Action action;
    State startState;
    List<Double> matrix;
}



class UIMatrix {
    enum UIMatrixType {uniform, identity, prob_matrix}
    UIMatrixType type;
    List<Double> matrix;
}


class UMatrix {
    enum UMatrixType {uniform, reset, prob_matrix}
    UMatrixType type;
    List<Double> matrix;
}
