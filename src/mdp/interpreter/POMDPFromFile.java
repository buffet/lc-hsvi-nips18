package mdp.interpreter;

import mdp.pomdp.POMDP;
import mdp.pomdp.Belief;
import util.container.ObjectIntegerConverter;

public class POMDPFromFile {
    public POMDP<Integer,Integer,Integer> pomdp;
    public Belief<Integer> initialBelief;
    public Double discount;
    public ObjectIntegerConverter<String> stateNames;
    public ObjectIntegerConverter<String> obsNames;
    public ObjectIntegerConverter<String> actionNames;
}
