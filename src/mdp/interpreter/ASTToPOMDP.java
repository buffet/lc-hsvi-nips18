package mdp.interpreter;


import mdp.pomdp.POMDPBuilder;
import mdp.pomdp.POMDPBuilderInteger;
import mdp.mdp.Transition;
import mdp.pomdp.Belief;
import util.container.ObjectIntegerConverter;
import util.random.Distribution;

import java.util.HashSet;
import java.util.Set;

public class ASTToPOMDP {
    /**
     * Translate an AST to a POMDPFromFile object
     *
     * @param ast The ast returned by the parsing of a file
     * @return The POMDPFromFile translation
     */
    public static POMDPFromFile translate(AST ast) throws Exception {
        initPOMDP();
        parsePreambleAndSetupPOMDP(ast.preamble);
        pomdp.initialBelief = parseInitialBelief(ast.initialBelief);
        for(ParamSpec param : ast.paramSpecList) {
            parseParamSpecAndFillPOMDP(param);
        }
        pomdp.pomdp = pomdpBuilder.build();

        return pomdp;
    }

    /**
     * Initialize the POMDP that will be returned
     */
    private static void initPOMDP() {
        pomdp = new POMDPFromFile();

        valueType = null;
        pomdp.discount = null;
        pomdp.stateNames = null;
        pomdp.obsNames = null;
        pomdp.actionNames = null;
        pomdp.initialBelief = null;
    }

    /**
     * Get the parameters from the preamble
     *
     * @param preamble The preamble
     */
    private static void parsePreambleAndSetupPOMDP(Preamble preamble) throws Exception{
        for(PreambleParam param : preamble.params) {
            if(param instanceof ValueParam) {
                valueType = ((ValueParam) param).valueType;
            } else if(param instanceof DiscountParam) {
                pomdp.discount = ((DiscountParam) param).discount;
            } else if(param instanceof StateObsActionParam) {
                switch (((StateObsActionParam) param).type) {
                    case state:
                        pomdp.stateNames = toObjectIntegerConverter((StateObsActionParam) param);
                        break;
                    case action:
                        pomdp.actionNames = toObjectIntegerConverter((StateObsActionParam) param);
                        break;
                    case obs:
                        pomdp.obsNames = toObjectIntegerConverter((StateObsActionParam) param);
                        break;
                    default:
                        throwError("Unexpected error.");
                }
            } else {
                throwError("Unexpected error");
            }
        }

        if(valueType == null || pomdp.discount == null || pomdp.stateNames == null || pomdp.actionNames == null || pomdp.obsNames == null) {
            throwError("The preamble is not complete.");
        }

        pomdpBuilder = new POMDPBuilderInteger(pomdp.stateNames.getSize(), pomdp.actionNames.getSize(), pomdp.obsNames.getSize());
    }


    /**
     * Get the state/observation/action converter from the objects from the AST
     *
     * @param param The AST parameter
     * @return The converter to a string
     */
    private static ObjectIntegerConverter<String> toObjectIntegerConverter(StateObsActionParam param) throws Exception {
        if(param instanceof StateObsActionParamInt) {
            ObjectIntegerConverter<String> converter = new ObjectIntegerConverter<>();
            int nbElements = ((StateObsActionParamInt) param).numberOfElements;
            for(int i = 0; i< nbElements; i++) {
                converter.add(Integer.toString(i));
            }
            return converter;
        } else if (param instanceof StateObsActionParamStrings) {
            return new ObjectIntegerConverter<>(((StateObsActionParamStrings) param).names);
        } else {
            throwError("Unexpected error");
            return null; //the code here will not be executed, but IDEA don't think that way
        }
    }


    private static Belief<Integer> parseInitialBelief(InitialBelief initialBelief) throws Exception {
        if(initialBelief instanceof InitialBeliefEmpty) {
            return getUniformBelief();
        } else if(initialBelief instanceof InitialBeliefString) {
            return parseInitialBeliefString((InitialBeliefString) initialBelief);
        } else if(initialBelief instanceof InitialBeliefMatrix) {
            return parseInitialBeliefMatrix((InitialBeliefMatrix) initialBelief);
        } else if(initialBelief instanceof InitialBeliefSet) {
            return parseInitialBeliefSet((InitialBeliefSet) initialBelief);
        } else {
            throwError("Unexpected error");
            return null;//This will never be executed
        }
    }

    private static Belief<Integer> parseInitialBeliefString(InitialBeliefString initialBelief) {
        Belief<Integer> belief = new Belief<>();
        Integer state = pomdp.stateNames.toInteger(initialBelief.initialBeliefName);
        belief.setWeight(state,1.0);
        return belief;
    }

    private static Belief<Integer> parseInitialBeliefMatrix(InitialBeliefMatrix initialBelief) throws Exception {
        if(initialBelief.matrix.type == UMatrix.UMatrixType.reset) {
            throwError("Reset for matrix is not implemented");
            return null; //This will never be executed
        } else if(initialBelief.matrix.type == UMatrix.UMatrixType.uniform) {
            return getUniformBelief();
        } else if(initialBelief.matrix.type == UMatrix.UMatrixType.prob_matrix) {
            if(initialBelief.matrix.matrix.size() != pomdp.stateNames.getSize()) {
                throwError("The probability matrix should have the right dimension");
            }
            Belief<Integer> belief = new Belief<>();
            for(int i = 0; i<initialBelief.matrix.matrix.size(); i++) {
                belief.setWeight(i,initialBelief.matrix.matrix.get(i));
            }
            return belief;
        } else {
            throwError("Unexpected Error");
            return null; //This will never be executed
        }
    }

    private static Belief<Integer> getUniformBelief() {
        Belief<Integer> belief = new Belief<>();
        double probability = 1.0/pomdp.stateNames.getSize();
        for(int i = 0; i<pomdp.stateNames.getSize(); i++) {
            belief.setWeight(i,probability);
        }
        return belief;
    }

    private static Belief<Integer> parseInitialBeliefSet(InitialBeliefSet initialBelief) throws Exception {
        Set<Integer> initialBeliefSet = new HashSet<>();
        if(initialBelief.isInclude) {
            for(State state : initialBelief.states) {
                initialBeliefSet.add(toInteger(state));
            }
        } else {
            for(int i = 0; i<pomdp.stateNames.getSize(); i++) {
                initialBeliefSet.add(i);
            }
            for(State state : initialBelief.states) {
                initialBeliefSet.remove(toInteger(state));
            }
        }
        double prob = 1.0/initialBeliefSet.size();
        Belief<Integer> belief = new Belief<>();
        for(Integer state : initialBeliefSet) {
            belief.setWeight(state,prob);
        }
        return belief;
    }


    private static void parseParamSpecAndFillPOMDP(ParamSpec param) throws Exception {
        if(param instanceof ParamSpecTrans) {
            parseParamSpecTransAndFillPOMDP((ParamSpecTrans) param);
        } else if(param instanceof ParamSpecObs) {
            parseParamSpecObsAndFillPOMDP((ParamSpecObs) param);
        } else if(param instanceof ParamSpecReward) {
            parseParamSpecRewardAndFillPOMDP((ParamSpecReward) param);
        }
    }

    private static void parseParamSpecTransAndFillPOMDP(ParamSpecTrans param) throws Exception {
        if(param instanceof ParamSpecTransUnique) {
            parseParamSpecTransUniqueAndFillPOMDP((ParamSpecTransUnique) param);
        } else if (param instanceof ParamSpecTransUMatrix) {
            parseParamSpecTransUMatrixAndFillPOMDP((ParamSpecTransUMatrix) param);
        } else if (param instanceof ParamSpecTransUIMatrix) {
            parseParamSpecTransUIMatrixAndFillPOMDP((ParamSpecTransUIMatrix) param);
        } else {
            throwError("Unexpected Error");
        }
    }

    private static void parseParamSpecTransUniqueAndFillPOMDP(ParamSpecTransUnique param) throws Exception {
        Integer startState = toInteger(param.startState);
        Integer action = toInteger(param.action);
        Integer endState = toInteger(param.endState);
        pomdpBuilder.setTransitionProbability(startState,action,endState,param.prob);
    }

    private static void parseParamSpecTransUMatrixAndFillPOMDP(ParamSpecTransUMatrix param) throws Exception {
        Integer startState = toInteger(param.startState);
        Integer action = toInteger(param.action);
        if(param.matrix.type == UMatrix.UMatrixType.prob_matrix) {
            if(param.matrix.matrix.size() != pomdp.stateNames.getSize()) {
                throwError("The matrix should have the correct number of elements");
            }
            for (int endState = 0; endState < param.matrix.matrix.size(); endState++) {
                pomdpBuilder.setTransitionProbability(startState,action,endState, param.matrix.matrix.get(endState));
            }
        } else if(param.matrix.type == UMatrix.UMatrixType.uniform) {
            double prob = 1.0/pomdp.stateNames.getSize();
            pomdpBuilder.setTransitionProbability(startState,action,null,prob);
        } else if(param.matrix.type == UMatrix.UMatrixType.reset) {
            throwError("Reset value for matrix is not implemented");
        } else {
            throwError("Unexpected error");
        }
    }

    private static void parseParamSpecTransUIMatrixAndFillPOMDP(ParamSpecTransUIMatrix param) throws Exception {
        int n = pomdp.stateNames.getSize();
        Integer action = toInteger(param.action);
        if(param.matrix.type == UIMatrix.UIMatrixType.prob_matrix) {
            if(param.matrix.matrix.size() != n*n) {
                throwError("The matrix should have the correct number of elements");
            }
            for (int startState = 0; startState < n; startState++) {
                for(int endState = 0; endState < n; endState++) {
                    Transition<Integer,Integer> transition = new Transition<>(startState,action,endState);
                    pomdpBuilder.setTransitionProbability(startState,action,endState, param.matrix.matrix.get(n*startState+endState));
                }
            }
        } else if(param.matrix.type == UIMatrix.UIMatrixType.identity) {
            for(int state = 0; state < n; state++) {
                pomdpBuilder.setTransitionDistribution(state,action,new Distribution<>(state));
            }
        } else if(param.matrix.type == UIMatrix.UIMatrixType.uniform) {
            pomdpBuilder.setTransitionProbability(null,action,null,1.0/n);
        } else {
            throwError("Unexpected Error");
        }
    }

    private static void parseParamSpecObsAndFillPOMDP(ParamSpecObs param) throws Exception {
        if(param instanceof ParamSpecObsUnique) {
            parseParamSpecObsUniqueAndFillPOMDP((ParamSpecObsUnique) param);
        } else if(param instanceof ParamSpecObsMatrix) {
            parseParamSpecObsMatrixAndFillPOMDP((ParamSpecObsMatrix) param);
        } else if(param instanceof ParamSpecObsMatrixOnlyAction) {
            parseParamSpecObsMatrixOnlyActionAndFillPOMDP((ParamSpecObsMatrixOnlyAction) param);
        } else {
            throwError("Unexpected Error");
        }
    }

    private static void parseParamSpecObsUniqueAndFillPOMDP(ParamSpecObsUnique param) throws Exception {
        Integer endState = toInteger(param.endState);
        Integer action = toInteger(param.action);
        Integer obs = toInteger(param.obs);
        pomdpBuilder.setObservationProbability(action,endState,obs,param.prob);
    }

    private static void parseParamSpecObsMatrixAndFillPOMDP(ParamSpecObsMatrix param) throws Exception {
        Integer action = toInteger(param.action);
        Integer endState = toInteger(param.endState);
        int n = pomdp.obsNames.getSize();
        if(param.matrix.type == UMatrix.UMatrixType.uniform) {
            pomdpBuilder.setObservationProbability(action,endState,null,1.0/n);
        } else if(param.matrix.type == UMatrix.UMatrixType.prob_matrix) {
            if(param.matrix.matrix.size() != n) {
                throwError("The matrix should have the correct size");
            }
            for(int observation = 0; observation < n; observation++) {
                pomdpBuilder.setObservationProbability(action,endState,observation,param.matrix.matrix.get(observation));
            }
        } else if(param.matrix.type == UMatrix.UMatrixType.reset) {
            throwError("Reset value for matrix is not implemented");
        }
    }


    private static void parseParamSpecObsMatrixOnlyActionAndFillPOMDP(ParamSpecObsMatrixOnlyAction param) throws Exception {
        Integer action = toInteger(param.action);
        int nObs = pomdp.obsNames.getSize();
        int nState = pomdp.stateNames.getSize();
        if(param.matrix.type == UMatrix.UMatrixType.uniform) {
            pomdpBuilder.setObservationProbability(action,null,null,1.0/nObs);
        } else if(param.matrix.type == UMatrix.UMatrixType.prob_matrix) {
            if(param.matrix.matrix.size() != nObs*nState) {
                throwError("The matrix should have the correct size");
            }
            for(int state = 0; state < nState; state++) {
                for(int observation = 0; observation < nObs; observation++) {
                    pomdpBuilder.setObservationProbability(action,state,observation,param.matrix.matrix.get(state*nObs + observation));
                }
            }
        } else if(param.matrix.type == UMatrix.UMatrixType.reset) {
            throwError("Reset value for matrix is not implemented");
        } else {
            throwError("Unexpected Error");
        }
    }


    private static void parseParamSpecRewardAndFillPOMDP(ParamSpecReward param) throws Exception {
        if(param instanceof ParamSpecRewardUnique) {
            parseParamSpecRewardUniqueAndFillPOMDP((ParamSpecRewardUnique) param);
        } else if(param instanceof ParamSpecRewardMatrix) {
            parseParamSpecRewardMatrixAndFillPOMDP((ParamSpecRewardMatrix) param);
        } else if(param instanceof ParamSpecRewardMatrixOnlyActionState) {
            parseParamSpecRewardMatrixOnlyActionStateAndFillPOMDP((ParamSpecRewardMatrixOnlyActionState) param);
        } else {
            throwError("Unexpected Error");
        }
    }

    private static void parseParamSpecRewardUniqueAndFillPOMDP(ParamSpecRewardUnique param) throws Exception {
        Integer startState = toInteger(param.startState);
        Integer action = toInteger(param.action);
        Integer endState = toInteger(param.endState);
        if(!(param.obs instanceof ObservationAll)) {
            throwError("The reward can't depend on the observation");
        }
        if(valueType == ValueParam.ValueType.cost) {
            pomdpBuilder.setReward(startState,action,endState, -param.value);
        } else {
            pomdpBuilder.setReward(startState,action,endState, param.value);
        }
    }

    private static void parseParamSpecRewardMatrixAndFillPOMDP(ParamSpecRewardMatrix param) throws Exception {
        throwError("The reward cant't depend on the observation");
    }

    private static void parseParamSpecRewardMatrixOnlyActionStateAndFillPOMDP(ParamSpecRewardMatrixOnlyActionState param) throws Exception {
        throwError("The reward cant't depend on the observation");
    }

        private static Integer toInteger(State state) throws Exception {
        if(state instanceof StateInteger) {
            return ((StateInteger) state).index;
        } else if(state instanceof StateString) {
            return pomdp.stateNames.toInteger(((StateString) state).name);
        } else if(state instanceof StateAll) {
            return null;
        } else {
            throwError("Unexpected Error");
            return null; //This will never be executed
        }
    }

    private static Integer toInteger(Action action) throws Exception {
        if(action instanceof ActionInteger) {
            return ((ActionInteger) action).index;
        } else if(action instanceof ActionString) {
            return pomdp.actionNames.toInteger(((ActionString) action).name);
        } else if(action instanceof ActionAll) {
            return null;
        } else {
            throwError("Unexpected Error");
            return null; //This will never be executed
        }
    }

    private static Integer toInteger(Observation obs) throws Exception {
        if(obs instanceof ObservationInteger) {
            return ((ObservationInteger) obs).index;
        } else if(obs instanceof ObservationString) {
            return pomdp.obsNames.toInteger(((ObservationString) obs).name);
        } else if(obs instanceof ObservationAll) {
            return null;
        } else {
            throwError("Unexpected Error");
            return null; //This will never be executed
        }
    }

    /**
     * Throw a basic exception due to an invalid AST
     */
    private static void throwError(String message) throws Exception {
        throw new Exception("Error while parsing the AST: " + message);
    }


    private static POMDPFromFile pomdp;
    private static POMDPBuilder<Integer,Integer,Integer> pomdpBuilder;
    private static ValueParam.ValueType valueType;
}
