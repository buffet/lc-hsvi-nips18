package mdp.simulation;

import mdp.pomdp.Belief;

public interface SimulablePOMDP<State,Action,Observation> {
    /**
     * Sample the next state
     *
     * @param startState    The current state
     * @param action        The action made
     * @return  The new state
     */
    State sampleNextState(State startState, Action action);


    /**
     * Sample the next observation
     *
     * @param action    The action made
     * @param endState  The end state
     * @return  The observation on the end state
     */
    Observation sampleObservation(Action action, State endState);


    /**
     * Get the reward on the last transition
     *
     * @param startState    The starting state of the transition
     * @param action        The last action made
     * @param endState      The end state of the transition (the current state)
     * @param belief        The current belief
     * @return  The reward given for that transition
     */
    double getReward(State startState, Action action, State endState, Belief<State> belief);


    /**
     * Get the maximum reward possible.
     * This is used to know when to stop the simulation
     *
     * @return The maximum possible reward
     */
    double getMaxAbsReward();


    /**
     * Get the new belief, after doing an action and getting an observation
     *
     * @param oldBelief   The old belief
     * @param action      The last action made
     * @param observation The observation resulting of that action
     * @return The new belief
     */
    Belief<State> getNewBelief(Belief<State> oldBelief, Action action, Observation observation);
}
