package mdp.simulation;

import mdp.policy.POMDPPolicy;
import mdp.pomdp.Belief;
import sun.misc.Signal;
import util.signal.ExitSignalHandler;
import util.signal.SingleSignalHandler;

import java.io.IOException;
import java.io.Serializable;

public class POMDPSimulation<State,Action,Observation> implements Serializable {

    /**
     * The current belief
     */
    final public Belief<State> initialBelief;


    /**
     * The pomdp to simulate (can be rhoPOMDP for instance)
     */
    final public SimulablePOMDP<State,Action,Observation> pomdp;


    /**
     * The policy to evaluate
     */
    final public POMDPPolicy<State,Action> policy;


    /**
     * The time discount
     */
    final public double discount;


    /**
     * Create a simulator for a POMDP and a policy associated (given an initial belief)
     *
     * @param pomdp             The pomdp to simulate
     * @param policy            The policy to use
     * @param initialBelief     The initial belief
     */
    public POMDPSimulation(SimulablePOMDP<State,Action,Observation> pomdp, POMDPPolicy<State,Action> policy,
                           Belief<State> initialBelief, double discount) {
        this.pomdp = pomdp;
        this.policy = policy;
        this.initialBelief = initialBelief;
        this.discount = discount;
    }


    public double simulate(double errorMargin, int nbSimulation) {
        double cumulativeSum = 0.0;
        int nbSimulationDone = 0;

        SingleSignalHandler signalHandler = new SingleSignalHandler();
        Signal.handle(new Signal("INT"),signalHandler);

	System.out.println("Iteration : value - current_mean");
        while(nbSimulationDone < nbSimulation && !signalHandler.hasHandleSignal()) {
            double currentValue = simulateOnce(errorMargin,false,false);
            nbSimulationDone++;
            cumulativeSum += currentValue;
            System.out.format("%3d : %3.5f - %3.5f\n",nbSimulationDone,currentValue,cumulativeSum/nbSimulationDone);
        }

        Signal.handle(new Signal("INT"), new ExitSignalHandler());

        return cumulativeSum / nbSimulationDone;
    }


    public double simulateOnce(double errorMargin, boolean display, boolean pause) {
        SingleSignalHandler signalHandler = new SingleSignalHandler();
        Signal.handle(new Signal("INT"),signalHandler);

        double maxDepth = Math.log((errorMargin/pomdp.getMaxAbsReward())*(1-discount)) / Math.log(discount);
	Belief<State> currentBelief = initialBelief;
        State currentState = initialBelief.sample();
        double currentDiscount = 1.0;
        double cumulativeReward = 0.0;

        for(int i = 0; i<maxDepth; i++) {
            // One simulation step
            State oldState = currentState;
            Belief<State> oldBelief = currentBelief;
            Action currentAction = policy.getBestAction(currentBelief);
            currentState = pomdp.sampleNextState(currentState,currentAction);
            Observation currentObservation = pomdp.sampleObservation(currentAction,currentState);
            currentBelief = pomdp.getNewBelief(currentBelief,currentAction,currentObservation);
            double transitionReward = pomdp.getReward(oldState,currentAction,currentState,currentBelief);
            cumulativeReward += transitionReward * currentDiscount;
            currentDiscount *= discount;

            // Print if necessary
            if(display)//  {
		// [this format may be more convenient when states/actions/obs not limited to integers]
            //     System.out.format("Last State : %s\n",oldState);
            //     System.out.format("Last Belief : %s\n",oldBelief);
            //     System.out.format("Action made : %s\n",currentAction);
            //     System.out.format("Current State : %s\n",currentState);
            //     System.out.format("Current Observation : %s\n",currentObservation);
            //     System.out.format("Current Belief : %s\n",currentBelief);
            //     System.out.format("Reward earned : %f\n",transitionReward);
            //     System.out.format("Cumulative reward : %f\n",cumulativeReward);
            //     System.out.format("\n\n\n");
            // }
	    {
                System.out.format("Last Belief :\t\t%s\n",oldBelief);
                System.out.format("(s,a) => s',z :\t\t(%s, %s) => %s, %s\n",
				  oldState, currentAction,
				  currentState, currentObservation);
                System.out.format("Current Belief :\t\t%s\n",currentBelief);
                System.out.format("Reward instant/cumulative :\t(%f / %f) \n",transitionReward, cumulativeReward);
                System.out.format("\n");
            }

            // If we want to pause the process for every step
            if(pause && !signalHandler.hasHandleSignal()) {
                try {
                    int ignored = System.in.read();
                } catch(IOException ignored) {
                }
            }
        }

        Signal.handle(new Signal("INT"),new ExitSignalHandler());

        return cumulativeReward;
    }
}
