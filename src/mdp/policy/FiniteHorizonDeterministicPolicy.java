package mdp.policy;

import util.container.Pair;

import java.util.HashMap;
import java.util.Map;


/**
 * <pre>
 * Represent a deterministic policy for a finite horizon problem.
 * Contains also the value function.
 *
 * The following example shows how to use the class:
 * <code> {@code
 * FiniteHorizonDeterministicPolicy<State,Action> policy = new FiniteHorizonDeterministicPolicy<>();
 * policy.setAction(state1,action1);
 * ...
 * policy.setValue(state1,value1);
 * ...
 * // Set the actions for the policy, and the value function
 *
 * Action action = policy.getAction(state);
 * double value = policy.getValue(state);
 * // Get the policy action for a state, and the expected reward starting from the state
 * }
 * </code>
 * </pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public class FiniteHorizonDeterministicPolicy<State,Action> {
    /**
     * The action chose for every (state,nbOfStepsLeft)
     */
    private Map<Pair<State,Integer>,Action> policy;


    /**
     * The value function
     */
    private Map<Pair<State,Integer>,Double> valueFunction;


    /**
     * Initialize the policy
     */
    public FiniteHorizonDeterministicPolicy() {
        policy = new HashMap<>();
        valueFunction = new HashMap<>();
    }


    /**
     * Get the action the agent will do given a current state, knowing the number of steps left
     *
     * @param state         The state
     * @param nbOfStepsLeft The number of steps left
     *
     * @return The action the agent will do
     */
    public Action getAction(State state, int nbOfStepsLeft) {
        return policy.get(new Pair<>(state,nbOfStepsLeft));
    }


    /**
     * Set an action to a couple (state,nbOfStepsLeft)
     *
     * @param state         The state
     * @param nbOfStepsLeft The number of steps left
     * @param action        The new action
     */
    public void setAction(State state, int nbOfStepsLeft, Action action) {
        policy.put(new Pair<>(state,nbOfStepsLeft),action);
    }


    /**
     * Get the expected reward the agent will have given a state, knowing the number of steps left
     *
     * @param state         The state
     * @param nbOfStepsLeft The number of steps left
     *
     * @return The expected reward
     */
    public double getValue(State state, int nbOfStepsLeft) {
        return valueFunction.get(new Pair<>(state,nbOfStepsLeft));
    }


    /**
     * Set a value to the valueFunction
     *
     * @param state         The state
     * @param nbOfStepsLeft The number of steps left
     *
     * @param value         The new value
     */
    public void setValue(State state, int nbOfStepsLeft, double value) {
        valueFunction.put(new Pair<>(state,nbOfStepsLeft),value);
    }
}