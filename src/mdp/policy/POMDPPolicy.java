package mdp.policy;


import mdp.pomdp.Belief;


/**
 * Represent a policy for a POMDP/RhoPOMDP
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public interface POMDPPolicy<State,Action> {

    /**
     * Get the next action to do
     *
     * @param belief    The current belief
     * @return  The next action to do
     */
    Action getBestAction(Belief<State> belief);
}
