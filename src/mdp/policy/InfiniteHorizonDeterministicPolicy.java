package mdp.policy;

import java.util.HashMap;
import java.util.Map;



/**
 * <pre>
 * Represent a deterministic policy for a finite horizon problem.
 * Contains also the value function.
 *
 * The following example shows how to use the class:
 * <code> {@code
 * InfiniteHorizonDeterministicPolicy<State,Action> policy = new InfiniteHorizonDeterministicPolicy<>();
 * policy.setAction(state,nbOfStepsLeft,action);
 * ...
 * policy.setValue(state,nbOfStepsLeft,value);
 * ...
 * // Set the actions for the policy, and the value function
 *
 * Action action = policy.getAction(state,n);
 * double value = policy.getValue(state,n);
 * // Get the policy action for a state, and the expected reward starting from the state, knowing there is n number of steps left
 * }
 * </code>
 * </pre>
 *
 * @param <State>   The class representing the states
 * @param <Action>  The class representing the actions
 */
public class InfiniteHorizonDeterministicPolicy<State,Action> {

    /**
     * The action chose for every state
     */
    private Map<State,Action> policy;

    /**
     * The value function
     */
    private Map<State,Double> valueFunction;

    /**
     * Initialize the policy
     */
    public InfiniteHorizonDeterministicPolicy() {
        policy = new HashMap<>();
        valueFunction = new HashMap<>();
    }


    /**
     * Get the action the agent will do given a current state
     *
     * @param state The state
     *
     * @return The action the agent will do
     */
    public Action getAction(State state) {
        return policy.get(state);
    }


    /**
     * Set an action to a state
     *
     * @param state     The state
     * @param action    The new action
     */
    public void setAction(State state, Action action) {
        policy.put(state,action);
    }


    /**
     * Get the expected reward the agent will have given a state
     *
     * @param state     The state
     *
     * @return The expected reward
     */
    public double getValue(State state) {
        return valueFunction.get(state);
    }


    /**
     * Set a value to the value function
     *
     * @param state     The state
     * @param value     The new value
     */
    public void setValue(State state, double value) {
        valueFunction.put(state,value);
    }
}