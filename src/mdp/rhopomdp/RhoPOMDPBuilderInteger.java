package mdp.rhopomdp;


import mdp.mdp.TransitionSetInteger;
import mdp.pomdp.ObservationSetInteger;

import java.util.HashSet;
import java.util.Set;


/**
 * <pre>
 * Build a Rho-POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a Rho-POMDP:
 *
 * <code> {@code
 * RhoPOMDPBuilder<Integer,Integer,Integer> builder = new RhoPOMDPBuilderGeneric<>(nbStates,nbActions,nbObservations,rewardFunction);
 * // Instance the object knowing the different number of states,actions, and observations, and the reward function
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * builder.setObservationProbability(action,endState,observation,probability);
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions and observations
 *
 * builder.setTransitionProbability(null,action,endState,probability);
 * // Setters can have null parameters, to represent all the values.
 * // Here, this is equivalent to the code :
 * // for(int startState = 0; startState < nbStates; startState++) {
 * //   builder.setTransitionProbability(startState,action,endState,probability);
 * // }
 *
 * RhoPOMDP<Integer,Integer,Integer> rhoPomdp = builder.build();
 * // Build the final rho-pomdp
 * }</code></pre>
 */
public class RhoPOMDPBuilderInteger extends RhoPOMDPBuilder<Integer,Integer,Integer> {

    /**
     * The number of different states
     */
    final public int nbStates;


    /**
     * The number o different actions
     */
    final public int nbActions;


    /**
     * The number of different observations
     */
    final public int nbObservations;


    /**
     * Create a new POMDPBuilder, by giving new managers
     *
     * @param nbActions         The number of actions
     * @param nbStates          The number of states
     * @param nbObservations    The number of observations
     * @param rewards           The reward manager
     */
    public RhoPOMDPBuilderInteger(int nbStates, int nbActions, int nbObservations, RewardFunction<Integer, Integer> rewards) {
        super(new TransitionSetInteger(nbStates,nbActions), rewards, new ObservationSetInteger(nbStates,nbActions,nbObservations));
        this.nbStates = nbStates;
        this.nbActions = nbActions;
        this.nbObservations = nbObservations;
    }


    /**
     * Create a RhoPOMDP with the builders managers
     *
     * @return A new RhoPOMDP
     */
    @Override
    public RhoPOMDP<Integer, Integer, Integer> build() {
        setupManagers();
        Set<Integer> states = new HashSet<>();
        for(int i = 0; i<nbStates; i++) {
            states.add(i);
        }
        Set<Integer> actions = new HashSet<>();
        for(int i = 0; i<nbActions; i++) {
            actions.add(i);
        }
        Set<Integer> observations = new HashSet<>();
        for(int i = 0; i<nbObservations; i++) {
            observations.add(i);
        }
        return new RhoPOMDP<>(states,actions,observations,transitions,rewards,super.observationFunction);
    }
}
