package mdp.rhopomdp;

import mdp.mdp.TransitionSet;
import mdp.pomdp.ObservationSet;
import util.random.Distribution;


/**
 * <pre>
 * Build a Rho-POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a Rho-POMDP:
 *
 * <code> {@code
 * RhoPOMDPBuilder<State,Action,Observation> builder = ...;
 * // Instance the object with a child class
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * builder.setObservationProbability(action,endState,observation,probability);
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions and observations
 *
 * builder.setTransitionProbability(null,action,endState,probability);
 * // Setters can have null parameters, to represent all the values.
 * // Here, this is equivalent to the code :
 * // for(State startState : states) {
 * //   builder.setTransitionProbability(startState,action,endState,probability);
 * // }
 *
 * RhoPOMDP<State,Action,Observation> rhoPomdp = builder.build();
 * // Build the final rho-pomdp
 * }</code></pre>
 *
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
abstract public class RhoPOMDPBuilder<State,Action,Observation> {

    /**
     * The transition manager
     */
    protected TransitionSet<State,Action> transitions;

    /**
     * The reward manager
     */
    protected RewardFunction<State,Action> rewards;

    /**
     * The observation manager
     */
    protected ObservationSet<State,Action,Observation> observationFunction;


    /**
     * Create a new POMDPBuilder, by giving new managers
     *
     * @param transitions   The transition manager
     * @param rewards       The reward manager
     * @param observations  The observation manager
     */
    public RhoPOMDPBuilder(TransitionSet<State,Action> transitions, RewardFunction<State,Action> rewards,
                           ObservationSet<State,Action,Observation> observations) {
        this.transitions = transitions;
        this.rewards = rewards;
        this.observationFunction = observations;
    }


    /**
     * Create a RhoPOMDP with the builders managers
     *
     * @return A new RhoPOMDP
     */
    public abstract RhoPOMDP<State,Action,Observation> build();


    /**
     * Initialize the managers, to be able to use them for getting values
     */
    protected void setupManagers() {
        transitions.initGetters();
        observationFunction.initGetters();
    }


    /**
     * Set the distribution of probabilities of end states for a given couple (startState,action)
     * If startState is null, the distribution is set for all starting states.
     * If action is null, the distribution is set for all actions.
     *
     * @param startState    The starting state
     * @param action        The action
     * @param endStates     The distribution of probabilities over the end states
     */
    public void setTransitionDistribution(State startState, Action action, Distribution<State> endStates) {
        transitions.setDistribution(startState, action, endStates);
    }


    /**
     * Set the probability for a single transition (the elements are non-null)
     * If startState is null, the probability is set for all starting states.
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     *
     * @param startState    The start state
     * @param action        The action
     * @param endState      The end state
     * @param probability   The probability of this transition
     */
    public void setTransitionProbability(State startState, Action action, State endState, double probability) {
        transitions.setProbability(startState, action, endState, probability);
    }


    /**
     * Set a probability for an observation
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * If observation is null, the probability is set for all observations.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation on the new state
     * @param probability   The new probability of this observation
     */
    public void setObservationProbability(Action action, State endState, Observation observation, double probability) {
        observationFunction.setProbability(action, endState, observation, probability);
    }


    /**
     * Set a distribution of probability for observations
     * If action is null, the probability is set for all actions;
     * If endState is null, the probability is set for all ending states.
     * distribution cannot be null.
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param distribution  The distribution of probabilities for observations
     */
    public void setObservationDistribution(Action action, State endState, Distribution<Observation> distribution) {
        observationFunction.setDistribution(action, endState, distribution);
    }
}
