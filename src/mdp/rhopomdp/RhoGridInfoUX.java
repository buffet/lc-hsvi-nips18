package mdp.rhopomdp;

import mdp.rhopomdp.RewardFunction;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.MDP;
import mdp.pomdp.Belief;

import java.io.Serializable;

/**
 * <pre>
 * Represent the (rho) reward function for rho-POMDP "GridInfo".
 * </pre>
 *
 * @param <State>   The class representing the state
 * @param <Action>  The class representing the action
 */
public class RhoGridInfoUX<State,Action> extends RhoGridInfo<State,Action> {

    /**
     * Initialize the reward function.
     *
     * @param mdp       the underlying MDP
     */
    public RhoGridInfoUX(MDP<State,Action> mdp) {
	super(mdp);

	// Pre-compute (global) Lipschitz constant
	mLipschitzConstant = new MatrixX(1, mdp.states.size(), 1.0);

    }
    
    /**
     * Get the reward
     *
     * @param state     The current state
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public double getReward(State state, Belief<State> belief, Action action) {
	throw new UnsupportedOperationException("getReward(State state, Belief<State> belief, Action action) undefined.");
    }

    /**
     * Get the expected reward
     *
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public double getExpectedReward(Belief<State> belief, Action action) {

	// for (int x=0; x<=2; x++) {
	//     double tmp = 0.0;
	//     for (int y=0; y<=2; y++) {
	// 	tmp += 0.0; //belief.getProbability((State)(x+3*y));
	//     }
	//     r  += Math.abs(tmp-1./3.);
	// }

	// System.out.println("States:");
        // for(State startState : mdp.states) {
	//     System.out.println(startState);
	// }
	// System.exit(1);
	
	double ra = 0.0;
	double rb = 0.0;
	double rc = 0.0;
	for(State s : mdp.states) {
	    int i = hashToInt[s.hashCode()];
	    switch (i%3) {
	    case 0:
		ra += belief.getProbability(s);
		break;
	    case 1:
		rb += belief.getProbability(s);
		break;
	    case 2:
		rc += belief.getProbability(s);
		break;
	    default:
		System.out.println("I shouldn't be there.");
		System.exit(1);
	    }
	}

	double r = Math.abs(ra-1./3.) + Math.abs(rb-1./3.) + Math.abs(rc-1./3.) ;

	return -r;
    }


    /**
     * Get the lowest reward possible
     *
     * @return The value of the lowest reward possible
     */
    public double getMinReward() {
	return -4./3.;
    }


    /**
     * Get the highest reward possible
     *
     * @return The value of the highest reward possible
     */
    public double getMaxReward() {
	return 0.;
    }

    /**
     * Get the lipschitz constant (null if nonexistent)
     * This method is used when using LHSVI on a RhoPOMDP
     *
     * @return The constant
     */
    public double getLipschitzConstant() {
	// System.out.println("RhoGridInfo.getLipschitzConstant()");
	return 1.0;
	//throw new UnsupportedOperationException("Cannot give the Lipschitz constant of this function");
    }
}
