package mdp.rhopomdp;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.TransitionSet;
import mdp.pomdp.Belief;
import mdp.pomdp.ObservationSet;
import mdp.simulation.SimulablePOMDP;
import mdp.solver.SolvableByLHSVI;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;


/**
 * <pre>
 * Contains the RhoPOMDP dynamics (transitions, rewards, and observations)
 * To see how to build the RhoPOMDP, see RhoPOMDPBuilder class.
 *
 * The RhoPOMDP can be simulated (see POMDPSimulation), and solved with HSVI and LHSVI (see HSVI)
 * if all the functions are redefined in RewardFunction.
 *
 * It contains the same functions as POMDP, except for the reward function,
 * that is different.
 *
 * The main functions are getters for states,actions and observations.
 * It is also possible to generate states and observations, following the dynamics.
 * </pre>
 *
 * @param <State>       The class used to represent the states
 * @param <Action>      The class used to represent the actions
 * @param <Observation> The class used to represent the observations
 */
public class RhoPOMDP<State,Action,Observation> implements SolvableByLHSVI<State,Action,Observation>,
							   SimulablePOMDP<State,Action,Observation> {

    /**
     * The different states
     */
    private Collection<State> states;

    /**
     * The different actions
     */
    private Collection<Action> actions;

    /**
     * The different observations
     */
    private Collection<Observation> observations;

    /**
     * The transitions manager
     */
    private TransitionSet<State,Action> transitions;

    /**
     * The reward manager
     */
    private RewardFunction<State,Action> rewards;

    /**
     * The observation function
     */
    private ObservationSet<State,Action,Observation> observationFunction;

    /**
     * The converted to states, used to interface the pointSetRepresentation.
     */
    private ObjectIntegerConverter<State> converter;


    /**
     * Create a rho-pomdp with given states,actions,and observations, and
     * different managers for transitions,rewards and observations
     *
     * @param states                The different states
     * @param actions               The different actions
     * @param observations          The different observations
     * @param transitions           The transition manager
     * @param rewards               The reward manager
     * @param observationFunction   The observation function
     */
    RhoPOMDP(Collection<State> states, Collection<Action> actions, Collection<Observation> observations,
             TransitionSet<State,Action> transitions, RewardFunction<State,Action> rewards,
             ObservationSet<State,Action,Observation> observationFunction) {
        this.states = Collections.unmodifiableCollection(states);
        this.actions = Collections.unmodifiableCollection(actions);
        this.observations = Collections.unmodifiableCollection(observations);
        this.transitions = transitions;
        this.rewards = rewards;
        this.observationFunction = observationFunction;
	
        this.converter = new ObjectIntegerConverter<>();
    }


    /**
     * Get the different states
     *
     * @return The collection of the different states
     */
    @Override
    public Collection<State> getStates() {
        return states;
    }


    /**
     * Get the different states
     *
     * @return The collection of the different states
     */
    @Override
    public Collection<Action> getActions() {
        return actions;
    }


    /**
     * Get the different observations
     *
     * @return The collection of the different observations
     */
    @Override
    public Collection<Observation> getObservations() {
        return observations;
    }


    /**
     * Get the new belief, after doing an action and getting an observation
     *
     * @param belief      The old belief
     * @param action      The last action made
     * @param observation The observation resulting of that action
     * @return The new belief
     */
    @Override
    public Belief<State> getNewBelief(Belief<State> belief, Action action, Observation observation) {
        Belief<State> newBelief = new Belief<>();
        for(State endState : getStates()) {
            newBelief.setWeight(endState, getNewBeliefNumerator(belief,action,endState,observation));
        }
        //We do not need to normalize since the belief do it internally
        return newBelief;
    }


    /**
     * Return the new belief on a state before the normalization
     *
     * @param oldBelief     The old belief
     * @param action        The last action made
     * @param endState      The state resulting of the action
     * @param observation   The observation resulting of the last transition
     * @return   The belief on a state before the normalization
     */
    private double getNewBeliefNumerator(Belief<State> oldBelief, Action action, State endState, Observation observation) {
        if(getObservationProbability(action,endState,observation) == 0) {
            return 0;
        }
        double numerator = 0;
        for(State startState : oldBelief.getNonZero()) {
            numerator += getTransitionProbability(startState,action,endState) * oldBelief.getProbability(startState);
        }
        numerator *= getObservationProbability(action,endState,observation);
        return numerator;
    }


    /**
     * Get a lower bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionLowerBound(double discount, double errorMargin) {
        Map<State,Double> lowerBound = new HashMap<>();
        double value = rewards.getMinReward()/(1-discount);
        for(State state : getStates()) {
            lowerBound.put(state,value);
        }
        return lowerBound;
    }


    /**
     * Get a upper bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionUpperBound(double discount, double errorMargin) {
        Map<State,Double> lowerBound = new HashMap<>();
        double value = rewards.getMaxReward()/(1-discount);
        for(State state : getStates()) {
            lowerBound.put(state,value);
        }
        return lowerBound;
    }



    /**
     * Get the probability of a given observation, knowing the current belief, the last action made.
     *
     * @param belief      The belief before doing the action
     * @param action      The last action made
     * @param observation The observation we want to probability of
     * @return The probability of getting the observation
     */
    @Override
    public double getConditionalProbabilityOfObservation(Belief<State> belief, Action action, Observation observation) {
        double probability = 0;
        for(State startState : belief.getNonZero()) {
            if(belief.getProbability(startState) == 0.0) {
                continue;
            }
            double temp = 0;
            for(State endState : getPossibleNextStates(startState,action)) {
                temp += getTransitionProbability(startState,action,endState) * getObservationProbability(action,endState,observation);
            }
            probability += temp*belief.getProbability(startState);
        }

        return probability;
    }


    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief The current belief
     * @param action The last action made
     * @return The expected reward
     */
    @Override
    public double getExpectedReward(Belief<State> belief, Action action) {
        double value = 0;
        for(State state : belief.getNonZero()) {
            value += rewards.getReward(state,belief,action) * belief.getProbability(state);
        }
        return value;
    }

    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    @Override
    public MatrixX getRewardLipschitzConstant(Belief<State> belief, Action action, ObjectIntegerConverter converter) {
	return rewards.getLipschitzConstant(belief, action);
    }

    /**
     * Get the reward
     *
     * @param state     The current state
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public double getReward(State state, Belief<State> belief, Action action) {
        return rewards.getReward(state, belief, action);
    }


    /**
     * Get the reward on the last transition
     *
     * @param startState The starting state of the transition
     * @param action     The last action made
     * @param endState   The end state of the transition (the current state)
     * @param belief     The current belief
     * @return The reward given for that transition
     */
    @Override
    public double getReward(State startState, Action action, State endState, Belief<State> belief) {
        return getReward(endState,belief,action);
    }

    /**
     * Get the maximum reward possible.
     * This is used to know when to stop the simulation
     *
     * @return The maximum possible reward
     */
    @Override
    public double getMaxAbsReward() {
        return max(rewards.getMaxReward(),-rewards.getMinReward());
    }


    /**
     * Get the slope of the reward function on a belief, on a certain dimension
     *
     * @param belief The point where to get the tangent
     * @param action The action associated with the reward (we want the tangent of r(a))
     * @param state  The dimension for which we want the value
     * @return The tangent
     */
    @Override
    public double getRewardTangent(Belief<State> belief, Action action, State state) {
        return rewards.getTangent(belief,action,state);
    }

    /**
     * Sample the next state
     *
     * @param startState    The current state
     * @param action        The action made
     * @return  The next state
     */
    @Override
    public State sampleNextState(State startState, Action action) {
        return transitions.sampleNextState(startState, action);
    }

    /**
     * Return the probability of a transition
     *
     * @param startState    The starting state of the transition
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @return The probability of the transition
     */
    @Override
    public double getTransitionProbability(State startState, Action action, State endState) {
        return transitions.getProbability(startState, action, endState);
    }


    /**
     * Get the collection of next possible states, given a couple (initialBelief,action)
     *
     * @param startState    The starting state
     * @param action        The last action made
     * @return The collection of next possible states
     */
    @Override
    public Collection<State> getPossibleNextStates(State startState, Action action) {
        return transitions.getPossibleNextStates(startState, action);
    }


    /**
     * Get the probability of an observation knowing the couple (action,endState)
     *
     * @param action        The action of the transition
     * @param endState      The end state of the transition
     * @param observation   The observation
     * @return The probability of the observation knowing (action,endState)
     */
    @Override
    public double getObservationProbability(Action action, State endState, Observation observation) {
        return observationFunction.getProbability(action, endState, observation);
    }

    /**
     * Get the possible next observations
     *
     * @param action    The action of the transition
     * @param endState  The end state of the transition
     * @return The collection of possible next observations
     */
    @Override
    public Collection<Observation> getPossibleObservations(Action action, State endState) {
        return observationFunction.getPossibleObservations(action, endState);
    }

    /**
     * Sample an observation
     *
     * @param action    The action of the transition
     * @param endState  The ending state of the transition
     * @return An observation following the observation distribution, knowing (action, endState)
     */
    @Override
    public Observation sampleObservation(Action action, State endState) {
        return observationFunction.sampleObservation(action, endState);
    }

    /**
     * Return the \lambda for 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of the lambda
     */
    private double get1NormLambda() {
        return 1.0;
    }


    /**
     * Return the \mu{a,o} for the 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @param action      The action
     * @param observation The observation
     * @return The value of the MuAO
     */
    private double get1NormMuAO(Action action, Observation observation) {
        double maxi = Double.NEGATIVE_INFINITY;
        for(State beginState : getStates()) {
            double value = 0;
            for(State endState : getPossibleNextStates(beginState,action)) {
                double temp = getTransitionProbability(beginState,action,endState);
                temp *= getObservationProbability(action,endState,observation);
                value += temp;
            }
            maxi = max(maxi,value);
        }
        return maxi;
    }


    /**
     * Return the \mu for the 1 norm as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of the mu
     */
    private double get1NormMu() {
        double maxi = Double.NEGATIVE_INFINITY;
        for(Action action : getActions()) {
            for(Observation observation : getObservations()) {
                maxi = max(maxi,get1NormMuAO(action,observation));
            }
        }
        return maxi;
    }


    /**
     * Return the \rho as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of the rho
     */
    /**
     * private double getRho() {
     *    return rewards.getLipschitzConstant();
     *}
     */
     
    /**
     * Return the Lipschitz constant of the reward function
     * (Lipschitz constant of the reward function)
     *
     * @return The value of the Lipschitz constant of the reward function
     */
    private double getLipschitzR() {
        return rewards.getLipschitzConstant();
    }


    /**
     * Return the value of VLim as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of VLim
     */
    private double getVLim(double discount, double errorMargin) {
        Map<State,Double> lowerCorner = getValueFunctionLowerBound(discount, errorMargin);
        Map<State,Double> upperCorner = getValueFunctionUpperBound(discount, errorMargin);

        double mini = Double.POSITIVE_INFINITY;
        double maxi = Double.NEGATIVE_INFINITY;
        for(Map.Entry<State,Double> entry : lowerCorner.entrySet()) {
            mini = min(mini,entry.getValue());
        }
        for(Map.Entry<State,Double> entry : upperCorner.entrySet()) {
            maxi = max(maxi,entry.getValue());
        }
        return max(abs(maxi),abs(mini)) / (1 - discount);
    }

    /**
     * Return the slope of the cones
     *
     * @return The slope of the cones
     */
    private double getConeSlope(double discount, double errorMargin) {
	if (discount >= 0.5)
	    return Double.POSITIVE_INFINITY;

        double vLim = getVLim(discount, errorMargin);
	
	return (getLipschitzR() + discount * vLim ) / (1 - 2*discount);

	/**
	 * vieille version invalide
	 */
        //double lambda = get1NormLambda();
        //double mu = get1NormMu();
        // double rho = getRho();

        // if(discount*lambda >= 1.0) {
        //     return Double.POSITIVE_INFINITY;
        // }
        // return (rho + discount * vLim * mu * getObservations().size()) / (1 - discount*lambda);
    }

    /**
     * Get the lipschitz constant of the value function of the decision process
     *
     * @param discount    The discount on future reward
     * @param errorMargin The allowed error
     * @return The lipschitz constant
     */
    @Override
    public double getValueFunctionLipschitzConstant(double discount, double errorMargin) {
        return getConeSlope(discount,errorMargin);
    }
}
