package mdp.rhopomdp;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.MDP;
import mdp.pomdp.Belief;

import java.io.Serializable;

/**
 * <pre>
 * Represent a reward function for rho-POMDP.
 * This class is principally used in the RhoPOMDP class.
 * </pre>
 *
 * @param <State>   The class representing the state
 * @param <Action>  The class representing the action
 */
public abstract class RewardFunction<State,Action> implements Serializable {

    /**
     * The underlying mdp
     */
    public final MDP<State,Action> mdp;

    /**
     * Initialize the reward function.
     *
     * @param mdp       the underlying MDP
     */
    public RewardFunction(MDP<State,Action> mdp) {
	this.mdp = mdp;
    }
    
    /**
     * Get the reward
     *
     * @param state     The current state
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public abstract double getReward(State state, Belief<State> belief, Action action);

    /**
     * Get the expected reward
     *
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public abstract double getExpectedReward(Belief<State> belief, Action action);


    /**
     * Get the lowest reward possible
     *
     * @return The value of the lowest reward possible
     */
    public abstract double getMinReward();


    /**
     * Get the highest reward possible
     *
     * @return The value of the highest reward possible
     */
    public abstract double getMaxReward();

    /**
     * Get the lipschitz constant (null if nonexistent)
     * This method is used when using LHSVI on a RhoPOMDP
     *
     * @return The constant
     */
    public double getLipschitzConstant() {
        throw new UnsupportedOperationException("Cannot give the Lipschitz constant of this function");
    }

    /**
     * Get the lipschitz constant (null if nonexistent)
     * This method is used when using LHSVI on a RhoPOMDP
     *
     * @return The constant
     */
    public MatrixX getLipschitzConstant(Belief<State> belief, Action action) {
        throw new UnsupportedOperationException("Cannot give the Lipschitz constant of this function");
    }


    /**
     * Get the value of the tangent hyperplane in the belief point state.
     * This method is only used when using HSVI on a RhoPOMDP
     *
     * @param belief    The point where to compute the derivative
     * @param action    The value is computed on the reward knowing this action
     * @param state     The value is on the belief point {state = 1}
     */
    public double getTangent(Belief<State> belief, Action action, State state) {
        throw new UnsupportedOperationException("The tangent value is not implemented for this reward function");
    }
}
