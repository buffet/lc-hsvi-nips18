package mdp.rhopomdp;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.MDP;
import mdp.pomdp.*;
import mdp.simulation.SimulablePOMDP;
import mdp.solver.CornerPOMDPApproximation;
import mdp.solver.SolvableByLHSVI;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;



/**
 * <pre>
 * Contains the POMDP dynamics (transitions, rewards, and observations)
 * To see how to build the POMDP, see POMDPBuilder class.
 *
 * The POMDP can be simulated (see POMDPSimulation), and solved with HSVI and LHSVI (see HSVI)
 *
 * The following example shows the important functions of a POMDP
 *
 * <code> {@code
 * POMDP<State,Action,Observation> pomdp = pomdpBuilder.build();
 * // See POMDPBuilder for more information
 *
 * double reward = pomdp.getReward(startState,action,endState);
 * double expectedReward = pomdp.getReward(startState,action);
 * ...
 * // The POMDP implements the MDP function, so see MDP for more information
 *
 * double probability = pomdp.getObservationProbability(action,endState,observation);
 * Observation observation = pomdp.generateObservation(action,endState);
 * Collection<Observation> observations = pomdp.getPossibleObservations(action,endState);
 * // Get the probability of an observation, generate an observation, and get the possible next observations
 *
 * Belief<State> newBelief = pomdp.getNewBelief(oldBelief,action,observation);
 * // Get the new belief, knowing the old one, the last action made, and the current observation made
 *
 * }</code>
 * </pre>
 * @param <State>   The class used to represent the states
 * @param <Action>  The class used to represent the actions
 */
public class RhoPOMDP2<State,Action,Observation> extends POMDP<State,Action,Observation> implements Serializable {

    /**
     * Lipschitz Constant
     */
    //private Map<Action,MatrixX> lipschitzConstants;

    /**
     * The reward function
     */
    private RewardFunction<State,Action> rho;

    /**
     * Initialize the POMDP with an underlying MDP, and an observation manager.
     * Ths observation manager must have been initialize, as well as the mdp
     *
     * @param mdp                   The underlying mdp
     * @param observations          The different observations
     * @param observationManager    The observation distribution function
     * @param rho                   The reward function
     */
    public RhoPOMDP2(MDP<State,Action> mdp, Collection<Observation> observations,
          ObservationSet<State,Action,Observation> observationManager, RewardFunction<State,Action> rho) {
	super(mdp, observations, observationManager);
        this.rho = rho;
    }

    /**
     * Get the expected reward
     *
     * @param startState The starting state
     * @param action     The action made on that state
     */
    public double getExpectedReward(State startState, Action action) {
	throw new UnsupportedOperationException("RhoPOMDP2.getExpectedReward(State startState, Action action)");
    }

    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    @Override
    public double getExpectedReward(Belief<State> belief, Action action) {
	//System.out.println("RhoPOMDP2.getExpectedReward(Belief<State> belief, Action action)");
	return rho.getExpectedReward(belief, action);
    }

    /**
     * Get the reward on the last transition.
     *
     * @param startState The starting state of the transition
     * @param action     The last action made
     * @param endState   The end state of the transition (the current state)
     * @param belief     The current belief
     *
     * @return The reward given for that transition
     */
    @Override
    public double getReward(State startState, Action action, State endState, Belief<State> belief) {
	//System.out.println("RhoPOMDP2.getReward");
        return rho.getExpectedReward(belief,action);
    }

    /**
     * Get the expected reward, knowing the current belief, and the last action made
     *
     * @param belief    The current belief
     * @param action    The last action made
     * @return The expected reward
     */
    @Override
    public MatrixX getRewardLipschitzConstant(Belief<State> belief, Action action, ObjectIntegerConverter converter) {
	return rho.getLipschitzConstant(belief, action);
    }


    /**
     * Get the maximum "absolute" reward possible.
     *
     * @return The maximum "absolute" possible reward
     */
    @Override
    public double getMaxAbsReward() {
	return Math.max(Math.abs(rho.getMinReward()), Math.abs(rho.getMaxReward()));
    }

    /**
     * Get the maximum reward possible.
     *
     * @return The maximum possible reward
     */
    //@Override
    public double getMaxReward() {
        return rho.getMaxReward();
    }

    /**
     * Get the minimum expected reward possible.
     *
     * @return The minimum possible expected reward
     */
    //@Override
    public double getMinReward() {
        return rho.getMinReward();
    }


    /**
     * Get the slope of the reward function on a belief, on a certain dimension
     *
     * @param belief    The point where to get the tangent
     * @param action    The action associated with the reward (we want the tangent of r(a))
     * @param state     The dimension for which we want the value
     * @return  The tangent
     */
    @Override
    public double getRewardTangent(Belief<State> belief, Action action, State state) {
        //In a POMDP, the reward do not depend on the belief
        return rho.getTangent(belief, action, state);
    }


    /**
     * Get a lower bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionLowerBound(double discount, double errorMargin) {
        //return CornerPOMDPApproximation.getLowerWithWorstRewardBestAction(this,discount,errorMargin);
	double rMin = rho.getMinReward()/(1. - discount);
	Map<State, Double> map = new HashMap<>();
	
	for(State s : mdp.states) {
	    map.put(s, rMin);
	}

	return map;	
    }

    /**
     * Get an upper bound of the value function in the corners
     *
     * @return A map associating the states to the corner value of the state in the upper bound
     */
    @Override
    public Map<State, Double> getValueFunctionUpperBound(double discount, double errorMargin) {
        //return CornerPOMDPApproximation.getUpperWithMDPSolving(this,discount,errorMargin);
	double rMax = rho.getMaxReward()/(1. - discount);
	Map<State, Double> map = new HashMap<>();
	
	for(State s : mdp.states) {
	    map.put(s, rMax);
	}

	return map;	
    }


    /**
     * Return the Lipschitz constant of the reward function
     * (Lipschitz constant of the reward function)
     *
     * @return The value of the Lipschitz constant of the reward function
     */
    // private double getLipschitzR() {
    //     double lmaxi = Double.NEGATIVE_INFINITY;
    //     for(Action action : getActions()) {
    // 	    double rmini = Double.POSITIVE_INFINITY;
    // 	    double rmaxi = Double.NEGATIVE_INFINITY;
    //         for (State state : getStates()) {
    // 		double r = mdp.getExpectedReward(state,action);
    //             rmaxi = max(rmaxi,r);
    //             rmini = min(rmini,r);
    //         }
    // 	    lmaxi = max( lmaxi, (rmaxi-rmini)/2. );
    //     }
    //     return lmaxi;
    // }


    /**
     * Return the value of VLim as defined in the "MDP s-lipschitziens et ρ-POMDP non-convexes" paper
     *
     * @return The value of VLim
     */
    private double getVLim(double discount, double errorMargin) {
        Map<State,Double> lowerCorner = getValueFunctionLowerBound(discount, errorMargin);
        Map<State,Double> upperCorner = getValueFunctionUpperBound(discount, errorMargin);

        double mini = Double.POSITIVE_INFINITY;
        double maxi = Double.NEGATIVE_INFINITY;
        for(Map.Entry<State,Double> entry : lowerCorner.entrySet()) {
            mini = min(mini,entry.getValue());
        }
        for(Map.Entry<State,Double> entry : upperCorner.entrySet()) {
            maxi = max(maxi,entry.getValue());
        }
        return max(abs(maxi),abs(mini)) / (1 - discount);
    }

}
