package mdp.rhopomdp;

import mdp.mdp.MDP;
import mdp.pomdp.Belief;

import java.io.Serializable;


/**
 * <pre>
 * Represent a reward function based on the Kullback-Leibler Divergence:
 * https://en.wikipedia.org/wiki/Kullback–Leibler_divergence
 *
 * The prior/reference distribution is the uniform distribution.
 * </pre>
 *
 * @param <State>   The class representing the state
 * @param <Action>  The class representing the action
 */
public class KLReward<State,Action>  extends RewardFunction<State,Action> implements Serializable {

    /**
     * The number of states
     */
    int nbStates;

    /**
     * The log of the number of states
     * Used to optimize computations
     */
    double nbStatesLog;


    /**
     * Initialize the reward function.
     *
     * @param mdp       the underlying MDP
     */
    public KLReward(MDP<State,Action> mdp) {
	super(mdp);
	throw new UnsupportedOperationException("KLReward.java: KLApproximationReward() not implemented.");
    }
    
    /**
     * Create a new Reward based on the KL Divergence,
     * with the uniform distribution as reference
     *
     * @param nbStates  The number of states
     */
    /*
    public KLReward(int nbStates) {
        this.nbStates = nbStates;
        this.nbStatesLog = Math.log(nbStates);
    }
    */

    /**
     * Get the reward
     *
     * @param currentState  The current state
     * @param belief        The current belief
     * @param action        The action done
     *
     * @return The reward for these parameters
     */
    @Override
    public double getReward(State currentState, Belief<State> belief, Action action) {
        double value = 0;
        for(State state : belief.getNonZero()) {
            double stateValue = belief.getProbability(state);
            value += stateValue*Math.log(stateValue);
        }
        return value + nbStatesLog;
    }

    /**
     * Get the expected reward
     *
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    @Override
    public double getExpectedReward(Belief<State> belief, Action action) {
        throw new UnsupportedOperationException("KLApproximationReward.java: getExpectedReward() not implemented.");
    }


    /**
     * Get the lowest reward possible
     *
     * @return The value of the lowest reward possible
     */
    @Override
    public double getMinReward() {
        return 0;
    }

    /**
     * Get the highest reward possible
     *
     * @return The value of the highest reward possible
     */
    @Override
    public double getMaxReward() {
        return nbStatesLog;
    }


    /**
     * Get the value of the tangent hyperplane in the belief point state.
     * This method is only used when using HSVI on a RhoPOMDP
     *
     * @param belief    The point where to compute the derivative
     * @param action    The value is computed on the reward knowing this action
     * @param state     The value is on the belief point {state = 1}
     */
    @Override
    public double getTangent(Belief<State> belief, Action action, State state) {
        return Math.log(belief.getProbability(state)) + nbStatesLog;
    }
}
