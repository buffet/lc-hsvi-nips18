package mdp.rhopomdp;

import mdp.rhopomdp.RewardFunction;

import util.container.MatrixX;
import util.container.ObjectIntegerConverter;

import mdp.mdp.MDP;
import mdp.pomdp.Belief;

import java.io.Serializable;

/**
 * <pre>
 * Represent the (rho) reward function for rho-POMDP "GridInfo".
 * </pre>
 *
 * @param <State>   The class representing the state
 * @param <Action>  The class representing the action
 */
public class RhoGridInfo<State,Action> extends RewardFunction<State,Action> implements Serializable {

    MatrixX mLipschitzConstant;

    int[] hashToInt;
    
    /**
     * Initialize the reward function.
     *
     * @param mdp       the underlying MDP
     */
    public RhoGridInfo(MDP<State,Action> mdp) {
	super(mdp);

	// Pre-compute (global) Lipschitz constant
	mLipschitzConstant = null;

	// Pre-compute mapping between States' hash codes and state numbers.
	// Assumes (and checks) that this is a one-to-one mapping.
	if (mdp.states.size() != 9) {
	    System.out.println("I should have 9 states in that problem...");
	    System.exit(1);
	}
	hashToInt = new int[9];
	for(int i=0; i<9; i++) hashToInt[i]=-1;
	int i=0;
	for(State s : mdp.states) {
	    int h = s.hashCode();
		//Integer.parseInt(s.toString());
	    if (hashToInt[h] != -1) { // Check the one-to-one mapping assumption.
		System.out.println("hashToInt[h] != -1 !!!");
		System.exit(0);
	    }
	    hashToInt[h] = i;
	    //System.out.println("state "+s+" h="+h+" i="+i);
	    i++;
	}
    }
    
    /**
     * Get the reward
     *
     * @param state     The current state
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public double getReward(State state, Belief<State> belief, Action action) {
	//throw new UnsupportedOperationException("getReward(State state, Belief<State> belief, Action action) undefined.");
	return getExpectedReward(belief, action);
    }

    /**
     * Get the expected reward
     *
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    public double getExpectedReward(Belief<State> belief, Action action) {
	throw new UnsupportedOperationException("getExpectedReward(Belief<State> belief, Action action) undefined.");
    }


    /**
     * Get the lowest reward possible
     *
     * @return The value of the lowest reward possible
     */
    public double getMinReward() {
	throw new UnsupportedOperationException("getMinReward() undefined.");
    }


    /**
     * Get the highest reward possible
     *
     * @return The value of the highest reward possible
     */
    public double getMaxReward() {
	throw new UnsupportedOperationException("getMaxReward() undefined.");
    }

    /**
     * Get the lipschitz constant (null if nonexistent)
     * This method is used when using LHSVI on a RhoPOMDP
     *
     * @return The constant
     */
    public double getLipschitzConstant() {
	throw new UnsupportedOperationException("getLipschitzConstant() undefined.");
    }

    /**
     * Get the lipschitz constant (null if nonexistent)
     * This method is used when using LHSVI on a RhoPOMDP
     *
     * @return The constant
     */
    public MatrixX getLipschitzConstant(Belief<State> belief, Action action) {
	// System.out.println("RhoGridInfo.getLipschitzConstant(Belief<State> belief, Action action)");
	// System.exit(1);
	return mLipschitzConstant;
	// throw new UnsupportedOperationException("Cannot give the Lipschitz constant of this function");
    }


    /**
     * Get the value of the tangent hyperplane in the belief point state.
     * This method is only used when using HSVI on a RhoPOMDP
     *
     * @param belief    The point where to compute the derivative
     * @param action    The value is computed on the reward knowing this action
     * @param state     The value is on the belief point {state = 1}
     */
    public double getTangent(Belief<State> belief, Action action, State state) {
        throw new UnsupportedOperationException("The tangent value is not implemented for this reward function");
    }
}
