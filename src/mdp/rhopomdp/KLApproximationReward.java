package mdp.rhopomdp;


import mdp.mdp.MDP;
import mdp.pomdp.Belief;
import util.container.MatrixX;

import java.io.Serializable;

import static java.lang.Math.log;
import static java.lang.Math.min;


/**
 * <pre>
 * Represent an approximation of the reward function based on the Kullback-Leibler Divergence:
 * https://en.wikipedia.org/wiki/Kullback–Leibler_divergence
 * The prior/reference distribution is the uniform distribution.
 *
 * The approximation is on the edge of the simplex.
 * If there is a dimension in the belief that has a value lesser than eta, then the belief is shifted
 * in the direction (belief - \vec{1}/n), until all the dimensions are greater than eta.
 *
 * This approximation ensure that the function is Lipschitz.
 * </pre>
 *
 * @param <State>   The class representing the state
 * @param <Action>  The class representing the action
 */
public class KLApproximationReward<State,Action> extends RewardFunction<State,Action> implements Serializable {

    /**
     * The number of states
     */
    int nbStates;

    /**
     * The log of the number fo states. Used to be more efficient
     */
    double nbStatesLog;

    /**
     * The minimum value of a dimension for the computation of entropy
     */
    double eta;


    /**
     * Initialize the reward function.
     *
     * @param mdp       the underlying MDP
     */
    public KLApproximationReward(MDP<State,Action> mdp) {
	super(mdp);
	throw new UnsupportedOperationException("KLApproximationReward.java: KLApproximationReward() not implemented.");
    }
    
    /**
     * Create a new Reward based on an approximation of the KL Divergence.
     * eta should be less than 1/nbStates.
     *
     * @param nbStates  The number of states
     * @param eta       The minimum value of a dimension for the computation of entropy
     */
    /* ???
    public KLApproximationReward(int nbStates, double eta) {
        if(eta > 1.0/nbStates) {
            throw new IllegalArgumentException("Eta should be lower than 1/nbStates");
        }
        this.nbStates = nbStates;
        this.nbStatesLog = log(nbStates);
        this.eta = eta;
    }
    */

    /**
     * Get the reward
     *
     * @param currentState  The current state
     * @param belief        The current belief
     * @param action        The action done
     *
     * @return The reward for these parameters
     */
    @Override
    public double getReward(State currentState, Belief<State> belief, Action action) {
        // We check if the belief should be centered (if one dimension is less than eta)
        boolean shouldCenter = false;
        if(belief.getNonZero().size() == nbStates) {
            for(State state : belief.getNonZero()) {
                if(belief.getProbability(state) < eta) {
                    shouldCenter = true;
                    break;
                }
            }
        } else {
            shouldCenter = true;
        }

        if(!shouldCenter) {
            return computeKL(belief);
        }

        // We get the center belief, and then we compute the KL-Divergence
        MatrixX vector = getCenteredBelief(belief);
        return computeKL(vector);
    }

    /**
     * Get the expected reward
     *
     * @param belief    The current belief
     * @param action    The action done
     * @return The reward for these parameters
     */
    @Override
    public double getExpectedReward(Belief<State> belief, Action action) {
        throw new UnsupportedOperationException("KLApproximationReward.java: getExpectedReward() not implemented.");
    }


    /**
     * Get the centered belief
     *
     * @param belief    The current belief
     * @return  The nearest belief (for infinity norm) that has every dimension greater than eta
     */
    private MatrixX getCenteredBelief(Belief<State> belief) {
        //We need to use the vector because we don't know which other state have a 0 probability
        MatrixX vector = new MatrixX(nbStates, 1);
        double distance = getCenterDistance(belief);
        int i = 0;

        for(State state : belief.getNonZero()) {
            double valueState = belief.getProbability(state);
            vector.set(i, 0, distance * (1.0/nbStates - valueState) + valueState);
            i++;
        }

        for(;i<nbStates;i++) {
            vector.set(i, 0, eta);
        }

        return vector;
    }


    /**
     * Get the distance to the centered belief
     *
     * @param belief    The current belief
     * @return  The smallest infinity norm distance to a belief that has every dimension greater than eta
     */
    private double getCenterDistance(Belief<State> belief) {
        if(belief.getNonZero().size() < nbStates) {
            return eta * nbStates;
        }

        double mini = Double.POSITIVE_INFINITY;

        for(State state : belief.getNonZero()) {
            double valueState = belief.getProbability(state);
            if(eta > valueState) {
                mini = min(mini,(eta - valueState)/(1.0/nbStates - valueState));
            }
        }
        return mini;
    }


    /**
     * The value of the entropy of a belief vector
     *
     * @param belief The belief
     * @return The entropy of the belief in base e
     */
    private double computeKL(Belief<State> belief) {
        double value = 0;
        for(State state : belief.getNonZero()) {
            double stateValue = belief.getProbability(state);
            value += stateValue*Math.log(stateValue);
        }
        return value + nbStatesLog;
    }


    /**
     * The value of the entropy of a vector
     *
     * @param vector The vector
     * @return The entropy of the vector in base e
     */
    private double computeKL(MatrixX vector) {
        double value = 0;
        for(int i = 0; i<vector.getRowDimension(); i++) {
            double stateValue = vector.get(i,0);
            value += stateValue*Math.log(stateValue);
        }
        return value + nbStatesLog;
    }


    /**
     * Get the lowest reward possible
     *
     * @return The value of the lowest reward possible
     */
    @Override
    public double getMinReward() {
        return 0;
    }

    /**
     * Get the highest reward possible
     *
     * @return The value of the highest reward possible
     */
    @Override
    public double getMaxReward() {
        return nbStatesLog;
    }
}
