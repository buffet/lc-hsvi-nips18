package mdp.rhopomdp;


import mdp.mdp.TransitionSetGeneric;
import mdp.pomdp.ObservationSetGeneric;

import java.util.Collection;
import java.util.Collections;


/**
 * <pre>
 * Build a Rho-POMDP after giving the transitions/observations probabilities and rewards.
 *
 * The following example show how to build a Rho-POMDP:
 *
 * <code> {@code
 * RhoPOMDPBuilder<State,Action,Observation> builder = new RhoPOMDPBuilderGeneric<>(states,actions,observations,rewardFunction);
 * // Instance the object knowing the different states,actions, and observations, and the reward function
 *
 * builder.setTransitionProbability(startState,action,endState,probability);
 * builder.setTransitionDistribution(startState,action,endStateDistribution);
 * builder.setObservationProbability(action,endState,observation,probability);
 * builder.setObservationDistribution(action,endState,observationDistribution);
 * ...
 * // Add transitions and observations
 *
 * builder.setTransitionProbability(null,action,endState,probability);
 * // Setters can have null parameters, to represent all the values.
 * // Here, this is equivalent to the code :
 * // for(State startState : states) {
 * //   builder.setTransitionProbability(startState,action,endState,probability);
 * // }
 *
 * RhoPOMDP<State,Action,Observation> rhoPomdp = builder.build();
 * // Build the final rho-pomdp
 * }</code></pre>
 *
 *
 * @param <State>       The class representing the states
 * @param <Action>      The class representing the actions
 * @param <Observation> The class representing the observations
 */
public class RhoPOMDPBuilderGeneric<State,Action,Observation> extends RhoPOMDPBuilder<State,Action,Observation> {

    /**
     * The different states
     */
    Collection<State> states;


    /**
     * The different actions
     */
    Collection<Action> actions;


    /**
     * The different observations
     */
    Collection<Observation> observations;


    /**
     * Create a new POMDPBuilder, by giving new managers
     *
     * @param rewards      The reward manager
     */
    public RhoPOMDPBuilderGeneric(Collection<State> states, Collection<Action> actions,
                                  Collection<Observation> observations, RewardFunction<State,Action> rewards) {
        super(new TransitionSetGeneric<>(states,actions), rewards, new ObservationSetGeneric<>(states,actions,observations));
        this.states = Collections.unmodifiableCollection(states);
        this.actions = Collections.unmodifiableCollection(actions);
        this.observations = Collections.unmodifiableCollection(observations);
    }


    /**
     * Create a RhoPOMDP with the builders managers
     *
     * @return A new RhoPOMDP
     */
    @Override
    public RhoPOMDP<State, Action, Observation> build() {
        setupManagers();
        return new RhoPOMDP<>(states,actions,observations,transitions,rewards,observationFunction);
    }
}
