#! /bin/bash

# Usage (in a terminal):
#
# 1- Connect on Grid 5000, e.g., in Nancy:
#
#    ssh LOGIN@access.nancy.grid5000.fr
#
# 2- Go to this directory:
#
#    cd PATH/L3-2017-Mathieu_Fehr/Stage-POMDP
#
# 3- Get last version of your code (assuming your Java source code and
#    shell scripts are on the GIT repository) and compile it:
#
#    git pull
#    ant clean
#    ant compile
#
# 4- Check that the directory ./XP-g5k-dir/ is empty (or empty it).
#
# 3- Run experiments:
#
#    screen        # create terminal that will survive when disconnecting from the machine
#    ./XP-g5k.sh   # run experiments
#    Ctrl-a d      # "detach" from this terminal (see /screen/ manual)
#    [...]         # live your live
#    screen -r     # re-attach to this terminal


export LC_NUMERIC=C # avoid pbs with floats (commas vs points)



if [ "$1" == "g5k" ] || [ "$1" == "grid5000" ] ; then
    if [[ "$HOSTNAME" != *"grid5000.fr" ]] ; then
	echo "[*ERROR*] It doesn't look like we are on Grid 5000."
	exit 0
    else
	cat <<EOF
==============================
Listing usable nodes and cores
==============================
EOF
	rm -f *.available *.running
	N_CORES=`cat $OAR_NODEFILE | wc -l`
	N_NODES=`uniq $OAR_NODEFILE | wc -l`
	echo ${N_CORES} cores on ${N_NODES} nodes
	CperN=$(expr ${N_CORES} / ${N_NODES})
	echo ${CperN} cores per node
	
	NODES=`uniq $OAR_NODEFILE`
	CORES=""
	for n in ${NODES} ; do
	    for i in `seq $CperN` ; do
		CORES=${CORES}" "$n.$i
		touch $n.$i.available
	    done
	done
	echo $CORES
    fi
else
    if [[ "$HOSTNAME" = *"grid5000.fr" ]] ; then
	echo "[*WARNING*] Not using Grid 5000."
	echo "[*WARNING*] Too bad..."
	exit 0
    fi
fi

cat<<EOF
==============
Initialization
==============
EOF

PBDIR="./resources/pomdp/"
OUTDIR="./XP-g5k-dir/"

TIMEOUT=3600
EPSILON=0.1

# SOLVERS="pwlc#pwlc \
# point#pw#--exNUI \
# xconic#lc#--exNUI \
# itconic#inc-lc \
# itconic#noLXU#--exLXU \
# itconic#noNUI#--exNUI \
# itconic#noUR#--exUR"
SOLVERS="pwlc#pwlc \
point#pw#--exNUI \
xconic#lc#--exNUI \
itconic#inc-lc \
itconic#noLXU#--exLXU \
itconic#noNUI#--exNUI \
itconic#noUR#--exUR"
#==== Mini sélection d'algos pour tester ====
# SOLVERS="xconic#lc#--exNUI \
# pwlc#pwlc"
NB_SOLVERS=`echo ${SOLVERS} | wc -w`
echo [solvers] ${NB_SOLVERS} - ${SOLVERS}

#PBS=`ls ${PBDIR}*.pomdp`1d
PBS="4x3.95 \
4x4.95 \
cheese.95 \
cit \
fourth \
hallway \
hallway2 \
milos-aaai97 \
mini-hall2 \
mit \
network \
paint.95 \
parr95.95 \
pentagon \
shuttle.95 \
sunysb \
tiger85 \
tiger-grid \
grid-info#GridInfoKX#grid-info~k\$x\$ \
grid-info#GridInfoKY#grid-info~k\$y\$ \
grid-info#GridInfoUX#grid-info~u\$x\$ \
grid-info#GridInfoUY#grid-info~u\$y\$"
#======================
# PBS="4x3.95 \
# 4x4.95 \
# cheese.95 \
# cit \
# hallway2 \
# hallway \
# milos-aaai97 \
# mini-hall2 \
# mit \
# network \
# paint.95 \
# pentagon \
# shuttle.95 \
# sunysb \
# tiger70 \
# tiger-grid \
# grid-info#GridInfoUX#grid-info~u\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
#======== PETITS PBS pour tester==============
# PBS="4x3.95 \
# cheese.95 \
# tiger70 \
# grid-info#GridInfoUX#grid-info~u\$x\$ \
# grid-info#GridInfoKX#grid-info~k\$x\$"
NB_PBS=`echo ${PBS} | wc -w`
echo [PBs] ${NB_PBS} - ${PBS//${PBDIR}/} | sed -e 's/.pomdp//g'

RUNTIME=$(expr $(expr 2 + $TIMEOUT) \* $NB_SOLVERS \* $NB_PBS)
HOURS=$(expr $RUNTIME / 3600)
MINUTES=$(expr $(expr $RUNTIME % 3600) / 60)
SECONDS=$(expr $RUNTIME % 60)
echo "RER: (Roughly Estimated Runtime)"
echo " (2+TIMEOUT)*NB_SOLVERS*NB_PBS = (2+$TIMEOUT)*$NB_SOLVERS*$NB_PBS"
echo " = $RUNTIME\" = ${HOURS}h$MINUTES'$SECONDS\""
echo ""

mkdir $OUTDIR

ant compile
if [ $? -ne 0 ] ; then
    echo "=================="
    echo "COMPILATION FAILED"
    echo "=================="
    exit 0
fi

cat <<EOF
===================
Running experiments
===================
EOF

function runong5k {
	HOST=${1/.[0-9]*.available/}
	#rm -f ${1}.available
	touch ${1/available/running}
	oarsh ${HOST} "$2"
	touch ${1}
	rm -f ${1/available/running}
}

for PB in $PBS ; do
    IFS='#' read -r -a PBarr <<< $PB
    echo ${PBarr[0]} ${PBarr[1]} ${PBarr[2]}
    if [ -z "${PBarr[2]}" ]; then
	PBarr[2]=${PBarr[0]}
    fi
    FILE=${PBDIR}/${PBarr[0]}.pomdp
    for SOLVER in $SOLVERS ; do
	IFS='#' read -r -a SOLVERarr <<< $SOLVER
	echo $SOLVER $PB
	LOGFILE=${OUTDIR}/${SOLVERarr[0]}-${PBarr[0]}-${PBarr[1]}-t${TIMEOUT}-e${EPSILON}${SOLVERarr[2]}.log
	rm -f ${LOGFILE} # SAFETY OPERATION
	
	if [ ! -f ${LOGFILE}.gz ] || [ `zgrep -c "THE END" ${LOGFILE}.gz` -eq "0" ] ; then
	    ARGS="solve ${SOLVERarr[0]} ${FILE} --rhoClass=${PBarr[1]} --timeout=${TIMEOUT} --epsilon=${EPSILON} ${SOLVERarr[2]}"
	    #ant justrun -Dargs="$ARGS"
	    if [ "$1" == "g5k" ] || [ "$1" == "grid5000" ] ; then
		if [ "`ls | grep "available$" | wc -w`" == "0" ] ; then echo -n "Waiting for free core" ; fi
		while [ "`ls | grep "available$" | wc -w`" == "0" ] ; do sleep 1 ; echo -n "." ; done
		echo ""
		
		HOST=`ls | grep available$ | head -n 1`
		echo ${HOST/.available/} is available. # "=>" ${HOST/.[0-9]*.available/}
		rm -f ${HOST}
		runong5k ${HOST} "cd ${PWD} ; java -cp lib/*:bin Main ${ARGS} > ${LOGFILE} ; gzip -f ${LOGFILE}" &
	    else
		java -cp lib/*:bin Main ${ARGS} | tee ${LOGFILE} ; gzip -f ${LOGFILE}
	    fi
	else
	    echo "*** ALREADY DONE ***"
	fi
    done
done

if [ "$1" == "g5k" ] || [ "$1" == "grid5000" ] ; then
    cat<<EOF
=============================
Waiting for last running jobs
=============================
EOF
    while [ "`ls | grep "running$" | wc -w`" != "0" ] ; do
	echo -n "."
	sleep 1
    done

    cat<<EOF
===========
Cleaning up
===========
EOF
    rm -f *.available *.running
fi

cat<<EOF
==========
Job's done
==========
EOF
