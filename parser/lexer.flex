package mdp.interpreter;

import java_cup.runtime.*;
import mdp.interpreter.sym;

%%

/*
  The name of the class JFlex will create will be lexer
 */
%class Lexer

/*
  Setup cup compatibility
 */
%cup

/*
  Functions that will be copied in the Parser class
 */
%{
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}


/*
  Macros
 */
lineTerminator  = \r|\n|\r\n
whiteSpace      = {lineTerminator} | [ \t\f]
integerLitteral = 0 | [1-9][0-9]*
doubleLitteral  = ([0-9]+ \. [0-9]* | \. [0-9]+ | [0-9]+ ) ([eE] [+-]? [0-9]+)?
stringLitteral  = [a-zA-Z] ( [a-zA-Z0-9] | [\_\-] )*
inputCharacter  = [^\r\n]
comment         = "#" {inputCharacter}* {lineTerminator}?


%%

/*
  Tokens
 */
<YYINITIAL> {
    "discount"                      { return symbol(sym.DISCOUNTTOK); }
    "values"                        { return symbol(sym.VALUESTOK); }
    "states"                        { return symbol(sym.STATESTOK); }
    "actions"                       { return symbol(sym.ACTIONSTOK); }
    "actions"                       { return symbol(sym.ACTIONSTOK); }
    "observations"                  { return symbol(sym.OBSERVATIONSTOK); }
    "T"                             { return symbol(sym.TTOK); }
    "O"                             { return symbol(sym.OTOK); }
    "R"                             { return symbol(sym.RTOK); }
    "uniform"                       { return symbol(sym.UNIFORMTOK); }
    "identity"                      { return symbol(sym.IDENTITYTOK); }
    "reward"                        { return symbol(sym.REWARDTOK); }
    "cost"                          { return symbol(sym.COSTTOK); }
    "start"                         { return symbol(sym.STARTTOK); }
    "include"                       { return symbol(sym.INCLUDETOK); }
    "exclude"                       { return symbol(sym.EXCLUDETOK); }
    ":"                             { return symbol(sym.COLONTOK); }
    "*"                             { return symbol(sym.ASTERICKTOK); }
    "+"                             { return symbol(sym.PLUSTOK); }
    "-"                             { return symbol(sym.MINUSTOK); }
    {integerLitteral}               { return symbol(sym.INTTOK,new Integer(yytext())); }
    {doubleLitteral}                { return symbol(sym.FLOATTOK,new Double(yytext())); }
    {stringLitteral}                { return symbol(sym.STRINGTOK, yytext()); }
    {whiteSpace}                    { /* Do nothing */ }
    {comment}                       { /* Do nothing */ }
}


[^]                                 { throw new Error("Illegal character <"+yytext()+">"); }