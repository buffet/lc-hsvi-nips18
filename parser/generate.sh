#! /bin/sh

java -jar jflex-1.6.1.jar -d ../src/mdp/interpreter lexer.flex
java -jar java-cup-11b.jar -package mdp.interpreter -destdir ../src/mdp/interpreter < parser.cup
